import http from 'http';
import pty from 'node-pty';

class Connector {
    #core_host;
    #core_port;
    #core_directory;
    #io;
    #pty;

    constructor(chost, cport, cdir, io) {
        this.#core_host = chost;
        this.#core_port = cport;
        this.#core_directory = cdir;
        this.#io = io;
        this.#pty = null;
        this.#startPty();

        this.#io.on('connection', (socket) => {
            socket.on('input', (data) => {
                this.#onWsData(data);
            });
        });
    }

    #startPty() {
        const args = [
            '--host', this.#core_host,
            '--port', this.#core_port,
        ]
        if (this.#core_directory) {
            args.push('-d', this.#core_directory)
        }
        this.#pty = pty.spawn('cockpit-core', args, {
            name: 'xterm-color',
            cols: 80,
            rows: 24,
            cwd: process.env.PWD,
            env: process.env }
        );

        this.#pty.onData((data) => this.#onPtyData(data));

        // auto-restart
        this.#pty.onExit(() => {
            this.#pty = null;
            this.#startPty();
        })
    }

    #onPtyData(data) {
        this.#io.emit('output', data);
    }

    #onWsData(data) {
        if (data.charCodeAt(0) == 3) {
            // CTRL-C
            // we cannot send ctrl-c directly to the PTY, as a SIGINT
            // would create havoc in the emulator and its child
            // processes, for example by killing an auto eval or the
            // thread listening to the controller or the controller
            // itself (SIGINT is forwarded to child processes in linux).
            // So instead, we send a message to a special api endpoint
            // on the emulator.
            // The endpoint raises a keyboardinterrupt exception in the
            // emulator's main thread (where the cli is running).
            //
            // However, the emulator may at this point still be waiting
            // on a line of input from its stdin, and our method for
            // raising the exception unfortunately does not wake up the
            // main thread from this wait.
            // So in order for the emulator to actually process the
            // interrupt in its main thread, we also need to send a \n.
            const options = {
                hostname: this.#core_host,
                port: this.#core_port,
                path: '/api/cli/sigint',
                method: 'POST',
            };
            const req = http.request(options, () => {
                this.#pty.write('\n');
            });
            req.end();
            return;
        }
        if (this.#pty) {
            this.#pty.write(data);
        }
    }
}
export default Connector;
