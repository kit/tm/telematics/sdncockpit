#!/usr/bin/env node

import express from 'express';
import http from 'http';
import yargs from 'yargs';
import {hideBin} from 'yargs/helpers';
import {Server} from 'socket.io';
import cors from 'cors';

import Connector from './connector.mjs';

const HOST = '0.0.0.0';
const PORT = 8085;
const CORE_HOST = '0.0.0.0';
const CORE_PORT = 8090;

var argv = yargs(hideBin(process.argv))
    .option('host', {
        alias: 'h',
        type: 'string',
        description: 'The ip on which this server will listen',
        default: HOST
    })
    .option('port', {
        alias: 'p',
        type: 'string',
        description: 'The port on which this server will listen',
        default: PORT
    })
    .option('core-host', {
        alias: 'ch',
        type: 'string',
        description: 'The ip on which the core server will listen',
        default: CORE_HOST
    })
    .option('core-port', {
        alias: 'cp',
        type: 'string',
        description: 'The port on which the core server will listen',
        default: CORE_PORT
    })
    .option('core-directory', {
        alias: 'cd',
        type: 'string',
        description: "The directory to which the core's virtual file system " +
            "API will be mapped.",
    })
    .help('help')
    .argv

function main() {
    const host = argv['host']
    const port = argv['port']
    const chost = argv['core-host']
    const cport = argv['core-port']
    const cdir = argv['core-directory']

    const app = express();
    app.use(cors());
    const httpServer = http.createServer(app);
    const io = new Server(httpServer, {
        cors: {
            origin: "*",
        }
    });

    app.get('/core_port.json', (req, res) => res.json({
        core_port: cport,
    }));

    new Connector(chost, cport, cdir, io);
    httpServer.listen(port, host);
}

main();
