# -*- mode: ruby -*-
# vi: set ft=ruby :

# This Vagrantfile can be used to bring up a simple development
# environment for SDN-Cockpit.

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/jammy64"
  config.vm.box_version = "20231027.0.0"
  config.vm.box_check_update = false

  config.vm.boot_timeout = 600

  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 8085, host: 8085
  config.vm.network "forwarded_port", guest: 8090, host: 8090

  # Sync SDNcockpit files.
  # We actually just need a one-time copy, but this is far faster,
  # convenient and customizable than the file provisioner.
  config.vm.synced_folder ".", "/vagrant", type: "rsync",
    rsync__exclude: [".vagrant/", ".git/", "data/", "doc/"],
    rsync__args: ["--verbose", "--archive", "-z", "--copy-links"]

  # shared folder for the data directory
  config.vm.synced_folder "./data", "/data"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the virtual hardware:
    vb.memory = "4096"
    vb.cpus = 4
  end

  # install SDNcockpit
  config.vm.provision :shell, inline: "cd /vagrant/utils && bash install.sh -p"
end
