#!/usr/bin/env node

import * as url from 'url';
import express from 'express';
import http from 'http';
import path from 'path';
import yargs from 'yargs';
import {hideBin} from 'yargs/helpers';

const HOST = '0.0.0.0';
const PORT = 8080;
const PROXY_PORT = 8085;

// I'm not happy about this trickery, but we'll go with it
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

console.log(__dirname);

var argv = yargs(hideBin(process.argv))
    .option('host', {
        alias: 'h',
        type: 'string',
        description: 'The ip on which this server will listen',
        default: HOST
    })
    .option('port', {
        alias: 'p',
        type: 'string',
        description: 'The port on which this server will listen',
        default: PORT
    })
    .option('proxy-port', {
        alias: 'cp',
        type: 'string',
        description: 'The port on which the core server will listen',
        default: PROXY_PORT
    })
    .help('help')
    .argv

function main() {
    const host = argv['host']
    const port = argv['port']
    const pport = argv['proxy-port']

    const app = express();
    const httpServer = http.createServer(app);

    // TODO: Serving from /src and /node_modules is obviously bad.
    // Proper bundling would fix that, but before setting that up,
    // I want to bring us back to a proper one-page app, instead of
    // having flow tables as a separate page.
    app.use('/', express.static(path.join(__dirname, './src')));
    app.use('/dist', express.static(path.join(__dirname, './dist')));
    app.use(
        '/node_modules',
        express.static(path.join(__dirname, './node_modules'))
    );

    // arg passing
    app.get('/proxy_port.json', (req, res) => res.json({
        proxy_port: pport,
    }));

    httpServer.listen(port, host);
}

main();
