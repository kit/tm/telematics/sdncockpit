
/**
 * @namespace Util
 * */

/**
 * Observers are simple callbacks without parameters.
 *
 * @callback ObservableMixin~Observer
 * */

export default (superclass) => {
    /**
     * Standard observable base class as a mixin.
     *
     * @mixin
     * @memberof Util
     * */
    class ObservableMixin extends superclass {
        /**
         * Constructor. Forwards args to superclass constructor.
         * */
        constructor(...args) {
            super(...args);
            this._observers = [];
        }

        /**
         * Register an observer on this observable.
         *
         * @param {ObservableMixin~Observer} observer - The callback to
         * be notified of changes to the observable.
         * */
        register(observer) {
            this.remove(observer);
            if (!this._observers.some((obs) => obs === observer)) {
                this._observers.push(observer);
            }
        }

        /**
         * Remove a previously registered observer from this observable.
         * If the same observer had been registered multiple times, all
         * registrations will be removed.
         *
         * @param {ObservableMixin~Observer} observer - The callback to
         * be removed.
         * */
        remove(observer) {
            this._observers = this._observers.filter((obs) => obs !== observer);
        }

        /**
         * Notify all observers registered on this observable.
         * */
        _notifyAll() {
            /* Alert all observers. */
            this._observers.forEach((obs) => { obs(this); });
        }
    }
    return ObservableMixin;
}; // mixin func
