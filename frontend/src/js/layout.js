import Split from 'split.js';

/* eslint new-cap: "off" */

/** @namesapce Main */

/**
 * This function applies the basic split-screen layout to the page.
 *
 * @memberof Main */
function makeSplits() {
    Split(['#file-picker-main-box', '#file-picker-pseudo-box'], {
        sizes: [35, 65],
        minSize: [200, 200],
    });

    Split(['#left-box', '#right-box'], {
        sizes: [50, 50],
        minSize: [200, 200],
    });

    Split(['#code-box', '#controller-box'], {
        sizes: [75, 25],
        minSize: [100, 100],
        direction: 'vertical',
    });

    Split(['#scenario-box', '#terminal-box', '#evaluation-box'], {
        sizes: [25, 25, 50],
        minSize: [100, 100, 100],
        direction: 'vertical',
    });
}
export default makeSplits;
