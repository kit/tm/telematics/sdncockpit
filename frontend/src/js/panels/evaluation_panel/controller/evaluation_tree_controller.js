import ReportTreeifier from './report_treeifier.js';

/**
 * A controller for an evaluation tree.
 * Automatically reads progress reports from an ionet and appends them
 * to the given evaluation tree.
 *
 * @memberof Panels.EvaluationPanel
 * */
class EvaluationTreeController {
    /**
     * Builds a new EvaluationTreeController.
     *
     * @param {Proxy.IONet} ionet - The network to read progress reports
     * from.
     * @param {Panels.EvaluationPanel.EvaluationTree} evalTree - The
     * evaluation tree to update.
     * */
    constructor(ionet, evalTree) {
        this._ionet = ionet;
        this._evalTree = evalTree;
        this._treeifier = new ReportTreeifier();
        this._clearOnEval = true;

        const treeifyAndAdd = (report) => {
            const type = report.type;

            switch (type) {
                case 'start_emulator':
                    this._evalTree.clear();
                    this._clearOnEval = false;
                    break;
                case 'start_evaluation':
                    if (this._clearOnEval) {
                        this._evalTree.clear();
                    }
                    this._clearOnEval = true;
                    break;
                default:
                    break;
            }

            const node = this._treeifier.treeify(report);
            if (node) {
                this._evalTree.addFromNode(node);
            }
        };

        this._ionet.registerProgressReportHandler(treeifyAndAdd)
    }
}
export default EvaluationTreeController;
