import StartNetworkActionDialogue
    from '../../../dialogues/start_network_action_dialogue.js';

/**
 * A controller for a network action button.
 * Keeps the button view updated and starts/stops a network action when
 * the button is pressed. If no network is running and the button is
 * pressed, the button will attempt to start the network.
 *
 * @memberof Panels.EvaluationPanel
 * */
class NetworkActionButtonController {
    /**
     * Builds a new NetworkActionButtonController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection. Specifies what scenario/controller to use
     * when starting the network via this button.
     * @param {Proxy.IONet} ionet - The network to start/stop.
     * @param {Panels.EvaluationPanel.NetworkActionButton} button - The
     * view controlled by this controller.
     * */
    constructor(ems, ionet, button) {
        this.actioncode = button.actioncode;
        this.ems = ems;
        this.ionet = ionet;
        this.button = button;

        button.bindStartClick(() => this._onStart());
        button.bindStopClick(() => this._onStop());

        this.ionet.register(() => this.button.update(this.ionet));
        this.button.update(this.ionet);
    }

    /**
     * Handles a start click on the controlled button.
     * */
    _onStart() {
        const dialogue = new StartNetworkActionDialogue();
        dialogue.start(this.actioncode, this.ionet, this.ems);
    }

    /**
     * Handles a stop click on the controlled button.
     * */
    _onStop() {
        this.ionet.stopAction(this.actioncode);
    }
}
export default NetworkActionButtonController;
