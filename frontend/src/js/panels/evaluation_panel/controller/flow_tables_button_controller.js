/**
 * A controller for a button that opens the flow tables popup.
 *
 * @memberof Panels.EvaluationPanel
 * */
class FlowTablesButtonController {
    /**
     * Builds a new FlowTablesButtonController.
     *
     * @param {Panels.EvaluationPanel.IconButton} button - The
     * view controlled by this controller.
     * */
    constructor(button) {
        button.onclick = () => this._onClick();
    }

    /**
     * Handles a click on the controlled button.
     * */
    _onClick() {
        window.open(
            './flow_tables.html',
            'flow tables',
            'width=600,height=900',
        );
    }
}
export default FlowTablesButtonController;
