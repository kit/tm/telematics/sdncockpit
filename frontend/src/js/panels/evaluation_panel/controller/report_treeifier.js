import TreeNode from '../view/tree_node.js';

/**
 * Instances of this class offer a variety of methods which take
 * full or partial progress reports from an IONet as parameters and
 * parse them into tree nodes for display.
 *
 * @memberof Panels.EvaluationPanel
 * */
class ReportTreeifier {
    /**
     * Builds ReportTreeifier.
     * */
    constructor() {

    }

    /**
     * Takes a report and turns it into a tree node.
     *
     * @param {API.ProgressReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    treeify(report) {
        const type = report.type;

        switch (type) {
            case 'start_emulator':
                return this.startEmulatorTree();
            case 'stop_emulator':
                return this.stopEmulatorTree();
            case 'start_network':
                return this.startNetworkTree();
            case 'stop_network':
                return this.stopNetworkTree();
            case 'restart_network':
                return this.restartNetworkTree();
            case 'start_network_action':
                return this.startActionTree(report);
            case 'abort_network_action':
                return this.abortActionTree(report);
            case 'complete_network_action':
                return this.completeActionTree(report);
            case 'start_test':
                return this.startTestTree(report);
            case 'start_event':
                return this.startEventTree(report);
            case 'test_feedback':
                return this.completeTestTree(report);
            case 'ping':
                return this.pingResultTree(report);
            default:
                return null;
        }
    }

    /**
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    startEmulatorTree() {
        return new TreeNode('Starting the network emulator');
    }

    /**
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    stopEmulatorTree() {
        return new TreeNode('Stopped the network emulator');
    }

    /**
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    startNetworkTree() {
        return new TreeNode('Starting the network');
    }

    /**
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    stopNetworkTree() {
        return new TreeNode('Stopped the network');
    }

    /**
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    restartNetworkTree() {
        return new TreeNode('Restarting the network');
    }

    /**
     * @param {API.StartNetworkActionReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     */
    startActionTree(report) {
        const actionType = report.action_type;

        return new TreeNode(`Starting ${actionType}`);
    }

    /**
     * @param {API.AbortNetworkActionReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     */
    abortActionTree(report) {
        const actionType = report.action_type;

        return new TreeNode(`${actionType} aborted`, [], false);
    }

    /**
     * @param {API.StopNetworkActionReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     */
    completeActionTree(report) {
        const actionType = report.action_type;

        return new TreeNode(`${actionType} complete`);
    }

    /**
     * @param {API.StartTestReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     */
    startTestTree(report) {
        const node = this.testTree(report.test);
        node.lable = `Running test: ${node.lable}`;
        return node;
    }

    /**
     * @param {API.SerializedTest} test
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    testTree(test) {
        const name = test.name;
        const description = test.description;
        const restart = test.restart;
        const events = test.events;
        const obsPoints = test.obs_points;

        const children = [];
        children.push(new TreeNode(`Name: ${name}`));
        children.push(new TreeNode(`Description: ${description}`));

        if (restart) {
            children.push(new TreeNode('Restarts the network'));
        }

        let eventText = `Runs ${events.length} traffic event`;
        if (events.length == 0 || events.length > 1) {
            eventText += 's'; // grammar
        }
        children.push(new TreeNode(eventText));

        children.push(this.observationPointListTree(obsPoints));

        return new TreeNode(name, children);
    }

    /**
     * @param {API.SerializedObservationPoint[]} obsps
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    observationPointListTree(obsps) {
        const lable = 'Observation points:';
        const children = obsps.map((obsp) => this.observationPointTree(obsp));

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedObservationPoint} obsp
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    observationPointTree(obsp) {
        const count = obsp.count;
        const rules = obsp.header_rules;

        const lable = this.observationPointLable(obsp);

        const children = [];
        if (count) {
            const countTree = this.rangeTree(count);
            countTree.lable = `Number of packets must be ${countTree.lable}`;
            children.push(countTree);
        }

        children.concat(rules.map((rule) => this.headerRuleTree(rule)));

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedObservationPoint} obsp
     *
     * @return {string}
     * */
    observationPointLable(obsp) {
        const direction = obsp.direction;
        const node1 = obsp.link[0];
        const node2 = obsp.link[1];

        let arrow = '<->';
        if (direction) {
            if (direction == node1) {
                arrow = '<--';
            } else {
                arrow = '-->';
            }
        }

        return `${node1} ${arrow} ${node2}`;
    }

    /**
     * @param {API.SerializedRangeList} range
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    rangeTree(range) {
        // lable: any
        // lable: between min and max / in network net
        // lable: in any of the following ranges: ...
        if (Array.isArray(range)) {
            const lable = 'in any of the following ranges:';
            const children = range.map((range) => this.rangeTree(range));
            return new TreeNode(lable, children);
        } else if ('net' in range) {
            return new TreeNode(`in network ${range.net}`);
        } else if ('minimum' in range && 'maximum' in range) {
            const min = range.minimum;
            const max = range.maximum;

            if (min == max) {
                return new TreeNode(`${min}`);
            } else {
                return new TreeNode(`between ${min} and ${max}`);
            }
        }
    }

    /**
     * @param {API.SerializedHeaderRule} rule
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    headerRuleTree(rule) {
        const proto = rule.proto;
        const field = rule.field;
        const range = rule.range;

        const node = this.rangeTree(range);
        node.lable = `${proto}.${field} must be ${node.lable}`;
        return node;
    }

    /**
     * @param {API.StartEventReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    startEventTree(report) {
        const event = report.event;
        const host = event.host;
        const pktCount = event.pkt_count;
        const headers = event.headers;

        const plural = pktCount == 0 || pktCount > 1 ? 's' : '';
        const lable = `Sending ${pktCount} packet${plural} from ${host}`;

        const children = [];
        children.push(new TreeNode(`Sender: ${host}`));
        children.push(new TreeNode(`Number of packets: ${pktCount}`));
        children.push(this.headersTree(headers));

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedHeaders} headers
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    headersTree(headers) {
        const lable = 'Headers:';

        const children = Object.keys(headers).map((key) => {
            const text = `${key.replace(/_/, '.')} = ${headers[key]}`;
            return new TreeNode(text);
        });

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.TestFeedbackReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    completeTestTree(report) {
        const feedback = report.feedback;
        const status = feedback.status;
        const test = feedback.test;
        const obspFbs = feedback.observation_point_feedbacks;

        const statusText = this.statusToText(status);
        const lable = `Test completed (${statusText})`;

        const children = [];

        children.push(this.testTree(test));
        children.push(this.observationPointFeedbackListTree(obspFbs));

        return new TreeNode(lable, children, status);
    }

    /**
     * @param {API.SerializedObservationPointFeedback[]} obspfbs
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    observationPointFeedbackListTree(obspfbs) {
        const lable = 'Feedback from observation points:';
        const children = obspfbs.map((obspfb) => {
            return this.observationPointFeedbackTree(obspfb);
        });

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedObservationPointFeedback} obspfb
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    observationPointFeedbackTree(obspfb) {
        const obs = obspfb.observation_point;
        const status = obspfb.status;
        const packetFeedbacks = obspfb.packet_feedbacks;

        const obsName = this.observationPointLable(obs);
        const statusText = this.statusToText(status);
        const lable = `${obsName} (${statusText})`;

        const children = [];

        const obsNode = this.observationPointTree(obs);
        obsNode.lable = `Observation point (${obsNode.lable})`;
        children.push(obsNode);

        const pktNode = this.packetFeedbackListTree(packetFeedbacks);
        const pktCount = packetFeedbacks.length;
        pktNode.lable = `received ${pktCount} packets:`;
        children.push(pktNode);

        return new TreeNode(lable, children, status);
    }

    /**
     * @param {boolean} status
     *
     * @return {string}
     * */
    statusToText(status) {
        if (status) {
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }

    /**
     * @param {API.SerializedPacketFeedback[]} packetfbs
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    packetFeedbackListTree(packetfbs) {
        const lable = `${packetfbs.length} packets:`;
        const children = packetfbs.map((fb) => this.packetFeedbackTree(fb));

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedPacketFeedback} packetfb
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    packetFeedbackTree(packetfb) {
        const status = packetfb.status;
        const packet = packetfb.packet;
        const headerFeedbacks = packetfb.header_feedbacks;

        const statusText = this.statusToText(status);
        const lable = `Packet (${statusText})`;

        const children = [];
        children.push(new TreeNode('Content: ', [new TreeNode(packet)]));
        children.push(this.headerFeedbackListTree(headerFeedbacks));

        return new TreeNode(lable, children, status);
    }

    /**
     * @param {API.SerializedHeaderFeedback[]} headerfbs
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    headerFeedbackListTree(headerfbs) {
        const lable = 'Header Feedback:';
        const children = headerfbs.map((fb) => this.headerFeedbackTree(fb));

        return new TreeNode(lable, children);
    }

    /**
     * @param {API.SerializedHeaderFeedback} headerfb
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     * */
    headerFeedbackTree(headerfb) {
        const status = headerfb.status;
        const headerRule = headerfb.header_rule;

        const statusText = this.statusToText(status);
        const headerRuleTree = this.headerRuleTree(headerRule);
        headerRuleTree.lable = `(${statusText}) ${headerRuleTree.lable}`;
        headerRuleTree.status = status;

        return headerRuleTree;
    }

    /**
     * @param {API.PingReport} report
     *
     * @return {Panels.EvaluationPanel.TreeNode}
     */
    pingResultTree(report) {
        const result = report.result;
        const status = result.status;
        const source = result.source;
        const destination = result.destination;

        const statusText = this.statusToText(status);

        const lable = `(${statusText}) Ping: ${source} -> ${destination}`;

        return new TreeNode(lable, [], status);
    }
}
export default ReportTreeifier;
