import StartNetworkDialogue from '../../../dialogues/start_network_dialogue.js';

/**
 * A controller for the network button.
 * Keeps the button view updated and starts/stops the network when the
 * button is pressed.
 *
 * @memberof Panels.ScenarioPanel
 * */
class NetworkButtonController {
    /**
     * Builds a new NetworkButtonController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection. Specifies what scenario/controller to use
     * when starting the network via this button.
     * @param {Proxy.IONet} ionet - The network to start/stop.
     * @param {Panels.ScenarioPanel.NetworkButton} button - The view
     * controlled by this controller.
     * */
    constructor(ems, ionet, button) {
        this.ems = ems;
        this.ionet = ionet;
        this.button = button;

        button.bindStartClick(() => this._onStart());
        button.bindStopClick(() => this._onStop());

        this.ionet.register(() => this.button.update(this.ionet));
        this.button.update(this.ionet);
    }

    /**
     * Handles start clicks.
     * */
    _onStart() {
        const dialogue = new StartNetworkDialogue();
        dialogue.start(this.ionet, this.ems).catch(() => {});
    }

    /**
     * Handles stop clicks.
     * */
    _onStop() {
        this.ionet.stop();
    }
}
export default NetworkButtonController;
