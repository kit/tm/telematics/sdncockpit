import View from '../../view.js';
import StartStopButton, {SSBState}
    from '../../components/buttons/start_stop_button.js';

/**
 * This view encapsulates a button which shows the current status of
 * an IONet and allows the user to start or stop it.
 *
 * @extends Panels.View
 * @memberof Panels.EvaluationPanel
 * */
class NetworkButton extends View {
    /**
     * Builds a new NetworkButton.
     * */
    constructor() {
        super();
        this._ssb = new StartStopButton(
            'waiting for the network to respond',
            'start the network emulatior',
            'stop the network emulator',
            ['fa-play'],
            'waiting',
            'start emulator',
            'stop emulator',
        );
    }

    /** @inheritdoc */
    get element() { return this._ssb.element; }

    /**
     * Binds the given handler to the button.
     *
     * @param {function} handler - Will be called when the button is
     * displaying as a start button and is clicked.
     * */
    bindStartClick(handler) { this._ssb.bindOnclick(SSBState.start, handler); }

    /**
     * Binds the given handler to the button.
     *
     * @param {function} handler - Will be called when the button is
     * displaying as a stop button and is clicked.
     * */
    bindStopClick(handler) { this._ssb.bindOnclick(SSBState.stop, handler); }

    /**
     * Change the appearance of the button to match the state of the
     * network. If the network is started, the button turns itself into
     * a stop button. If the network is stopped, the button turns itself
     * into a start button.
     *
     * @param {Proxy.IONet} ionet - The network whos state the button
     * will reflect.
     * */
    async update(ionet) {
        if (!ionet.connected || ionet.isChangePending()) {
            this._ssb.active = SSBState.idle;
        } else {
            if (await ionet.isRunning()) {
                this._ssb.active = SSBState.stop;
            } else {
                this._ssb.active = SSBState.start;
            }
        }
    }
}
export default NetworkButton;
