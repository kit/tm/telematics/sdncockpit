import '../../components/tree_view/tree_view_branch_list.js';
import '../../components/tree_view/tree_view_item.js';
import '../../components/tree_view/tree_view_lable.js';

const TVBL = 'tree-view-branch-list';
const TVI = 'tree-view-item';
const TVL = 'tree-view-lable';

/**
 * A class which serves as an intermediary step in the rendering of
 * network progress reports into viewable HTML elements.
 * It implements a simple tree structure.
 *
 * @memberof Panels.EvaluationPanel
 * */
class TreeNode {
    /**
     * Builds a new TreeNode.
     *
     * @param {string} lable - A text to show in this node's rendering.
     * @param {Panels.EvaluationPanel.TreeNode[]} children - This node's
     * children.
     * @param {boolean?} status - Whether this node indicates success
     * (true), failure (false), or is neutral (null).
     * */
    constructor(lable, children = [], status = null) {
        this.lable = lable;
        this.children = children;
        this.status = status;
    }

    /**
     * Render a lable for this node.
     *
     * @return {Panels.Components.TreeViewLable} The rendered lable.
     * */
    _renderLable() {
        const tvl = document.createElement(TVL);

        const expander = document.createElement('span');
        expander.classList.add('expand-symbol');
        if (this.children.length == 0) {
            expander.classList.add('spacer');
        }

        const title = document.createElement('span');
        title.innerText = this.lable;

        tvl.appendChild(expander);
        tvl.appendChild(title);

        return tvl;
    }

    /**
     * Recursively render the list of children of this node.
     *
     * @return {Panels.Components.TreeViewBranchList} The rendered
     * list.
     * */
    _renderChildren() {
        const tvbl = document.createElement(TVBL);

        this.children.forEach((child) => {
            const tvi = child.render();
            tvbl.appendChild(tvi);
        });

        return tvbl;
    }

    /**
     * Recursively render this node.
     *
     * @return {Panels.Components.TreeViewItem} The rendered item.
     * */
    render() {
        const tvi = document.createElement(TVI);
        tvi.appendChild(this._renderLable());
        tvi.appendChild(this._renderChildren());

        if (this.status != null) {
            tvi.setAttribute('status', this.status);
        }

        return tvi;
    }
}
export default TreeNode;
