import View from '../../view.js';
import '../../components/tree_view/tree_view.js';
import '../../components/tree_view/tree_view_branch_list.js';
import AutoScroller from '../../components/auto_scroller.js';

const TV = 'tree-view';
const TVI = 'tree-view-item';
const TVBL = 'tree-view-branch-list';

/**
 * This view encapsulates a series of network progress reports turned
 * into tree nodes for easy viewing.
 *
 * @extends Panels.View
 * @memberof Panels.EvaluationPanel
 * */
class EvaluationTree extends View {
    /**
     * Builds a new EvaluationTree.
     * */
    constructor() {
        super();

        this._element = document.createElement('div');
        this._element.id = 'activity-view-overflow';

        this._tv = document.createElement(TV);
        this._element.appendChild(this._tv);

        this._tvbl = document.createElement(TVBL);
        this._tv.appendChild(this._tvbl);

        this._element.addEventListener('click', (e) => {
            const item = e.target.closest(TVI);
            if (item) {
                item.toggle();
            }
        });

        this._autoScroll = new AutoScroller(this._element);
    }

    /** @inheritdoc */
    get element() { return this._element; }

    /**
     * Add the elements resulting from rendering the given tree node
     * to the evaluation tree as an immediate child of the tree's root
     * node.
     *
     * @param {Panels.EvaluationPanel.TreeNode} node - The node to have
     * its rendered HTMLElement added.
     * */
    addFromNode(node) {
        this._tvbl.appendChild(node.render());
        this._autoScroll.autoscroll();
    }

    /**
     * Clear out the eval tree.
     */
    clear() {
        this._tvbl.innerHTML = '';
    }
}
export default EvaluationTree;
