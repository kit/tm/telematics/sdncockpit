import View from '../../view.js';
import StartStopButton, {SSBState}
    from '../../components/buttons/start_stop_button.js';

/**
 * This view encapsulates a button which shows the current status of
 * a network action in an IONet and allows the user to start or stop it.
 *
 * @extends Panels.View
 * @memberof Panels.EvaluationPanel
 * */
class NetworkActionButton extends View {
    /**
     * Builds a new NetworkActionButton.
     *
     * @param {string} actioncode - The code of the action to start.
     * @param {string} actionname - The name of the action to display
     * to the user.
     * @param {string} short - A short form of the action's name.
     * @param {string[]} startIconTypes - A list of FontAwesome icon
     * classes, e.g. ["fa-folder"].
     * Will be applied to the button's icon when in start state.
     * */
    constructor(actioncode, actionname, short, startIconTypes = []) {
        super();
        this.actioncode = actioncode;
        this._ssb = new StartStopButton(
            'waiting for the network to respond',
            `start the ${actionname}`,
            `stop the ${actionname}`,
            startIconTypes,
            `waiting`,
            `start ${short}`,
            `stop ${short}`,
        );
    }

    /** @inheritdoc */
    get element() { return this._ssb.element; }

    /**
     * Binds the given handler to the button.
     *
     * @param {function} handler - Will be called when the button is
     * displaying as a start button and is clicked.
     * */
    bindStartClick(handler) { this._ssb.bindOnclick(SSBState.start, handler); }

    /**
     * Binds the given handler to the button.
     *
     * @param {function} handler - Will be called when the button is
     * displaying as a stop button and is clicked.
     * */
    bindStopClick(handler) { this._ssb.bindOnclick(SSBState.stop, handler); }

    /**
     * Change the appearance of the button to match the state of the
     * network. If an evaluation is running on the network, the button
     * turns itself into a stop button. If the network is stopped, the
     * button turns itself into a start button.
     *
     * @param {Proxy.IONet} ionet - The network whos state the button
     * will reflect.
     * */
    async update(ionet) {
        if (
            !ionet.connected ||
            ionet.isChangePending() ||
            ionet.isActionChangePending(this.actioncode)
        ) {
            this._ssb.active = SSBState.idle;
        } else {
            if (await ionet.isActionRunning(this.actioncode)) {
                this._ssb.active = SSBState.stop;
            } else {
                this._ssb.active = SSBState.start;
            }
        }
    }
}
export default NetworkActionButton;
