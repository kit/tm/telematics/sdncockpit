import View from '../../view.js';

/**
 * This view is for the panel body of the evaluation panel.
 * It mainly contains the given evaluation tree but contstructs some DOM
 * elements around it for visual reasons.
 *
 * @extends Panels.View
 * @memberof Panels.EvaluationPanel
 * */
class EvaluationPanelBody extends View {
    /**
     * Builds a new EvaluationPanelBody.
     *
     * @param {Panels.EvaluationPanel.EvaluationTree} evaluationTree -
     * The evaluation tree to display inside this view.
     * */
    constructor(evaluationTree) {
        super();
        this._element = document.createElement('div');
        this._element.classList.add('panel-body');

        this._element.appendChild(evaluationTree.element);
    }

    /** @inheritdoc */
    get element() { return this._element; }
}
export default EvaluationPanelBody;
