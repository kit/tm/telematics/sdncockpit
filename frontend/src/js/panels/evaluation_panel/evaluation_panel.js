import ButtonBar from '../components/button_bar.js';

import NetworkButton from './view/network_button.js';
import NetworkActionButton from './view/network_action_button.js';
import PanelHead from '../components/panel_head.js';
import EvaluationTree from './view/evaluation_tree.js';
import EvaluationPanelBody from './view/evaluation_panel_body.js';

import NetBtnC from './controller/network_button_controller.js';
import NABtnC from './controller/network_action_button_controller.js';
import FTBtnC from './controller/flow_tables_button_controller.js';
import TreeC from './controller/evaluation_tree_controller.js';

import {Actioncodes} from '../../api/generated/actioncodes.js';
import IconButton from '../components/buttons/icon_button.js';

/**
 * @namespace Panels.EvaluationPanel
 * */

/**
 * This is the main class for the evaluation panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.EvaluationPanel
 * */
class EvaluationPanel {
    /**
     * Builds a new EvaluationPanel.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection. Used here to start networks on demand for
     * the evaluation.
     * @param {Proxy.IONet} ionet - The network where evaluations take
     * place. Used here to start/stop networks and evaluations and to
     * read evaluation results and other network events.
     * */
    constructor(container, ems, ionet) {
        this.container = container;
        this.ems = ems;
        this.ionet = ionet;

        this.network_btn = new NetworkButton();
        this.eval_btn = new NetworkActionButton(
            Actioncodes.EVALUATION,
            'automatic evaluation',
            'auto eval',
            ['fa-list-check'],
        );
        this.pingall_btn = new NetworkActionButton(
            Actioncodes.PINGALL,
            'pingall',
            'pingall',
            ['fa-circle-nodes'],
        );
        this.flow_tables_btn = new IconButton(['fa-table'], "flow tables");
        this.head = new PanelHead('Automated Evaluation');
        this.buttonBar = new ButtonBar();
        this.tree = new EvaluationTree();
        this.body = new EvaluationPanelBody(this.tree);

        this._net_btn_c = new NetBtnC(this.ems, this.ionet, this.network_btn);
        this._eval_btn_c = new NABtnC(this.ems, this.ionet, this.eval_btn);
        this._pingall_btn_c =
            new NABtnC(this.ems, this.ionet, this.pingall_btn);
        this._flow_tables_btn_c = new FTBtnC(this.flow_tables_btn);
        this._eval_tree_c = new TreeC(this.ionet, this.tree);

        this.buttonBar.appendAsButton(this.network_btn);
        this.buttonBar.appendAsButton(this.eval_btn);
        this.buttonBar.appendAsButton(this.pingall_btn);
        this.buttonBar.appendAsButton(this.flow_tables_btn);
        this.container.appendChild(this.head.element);
        this.container.appendChild(this.buttonBar.element);
        this.container.appendChild(this.body.element);
    }
}
export default EvaluationPanel;
