import {Actioncodes} from '../../../api/generated/actioncodes.js';

const START_READ_MSG =
'### SDN-Cockpit: Starting to read controller output ###';

const STOP_READ_MSG =
'### SDN-Cockpit: Stopped reading controller output ###';

/**
 * A controller for the controller's terminal.
 * Automatically reads output reports from an ionet and prints them on
 * the given terminal.
 *
 * @memberof Panels.ControllerPanel
 * */
class ControllerTerminalController {
    /**
     * Builds a new ControllerTerminalController.
     *
     * @param {Proxy.IONet} ionet - The network to read output reports
     * from.
     * @param {Panels.Components.TerminalOutput} terminalOutput - The
     * terminal to print to.
     * */
    constructor(ionet, terminalOutput) {
        this._ionet = ionet;
        this._terminalOutput = terminalOutput;
        this._clearOnEval = true;

        const handleReport = (report) => {
            const type = report.type;

            switch (type) {
                case 'start_emulator': {
                    this._terminalOutput.clear();
                    // Don't clear right after the emulator is started,
                    // in case the controller printed important info
                    // on initialization.
                    this._clearOnEval = false;
                    break;
                }
                case 'start_network_action': {
                    const action = report.action_type;
                    if (this._clearOnEval && action == Actioncodes.EVALUATION) {
                        this._terminalOutput.clear();
                    }
                    this._clearOnEval = true;
                    break;
                }
                case 'process_read_start': {
                    this._terminalOutput.printLine(START_READ_MSG);
                    break;
                }
                case 'process_read_end': {
                    this._terminalOutput.printLine(STOP_READ_MSG);
                    break;
                }
                case 'output': {
                    const line = report.line.trim();
                    if (line == '$SDNC_CLEAR_SCREEN$') {
                        this._terminalOutput.clear();
                    } else {
                        this._terminalOutput.printLine(line);
                    }
                    break;
                }
                default:
                    break;
            }
        };

        this._ionet.registerProgressReportHandler(handleReport);
    }
}
export default ControllerTerminalController;
