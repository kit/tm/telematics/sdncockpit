import SelectControllerDialogue from
    '../../../dialogues/select_controller_dialogue';

/**
 * A controller for the controller file button.
 *
 * @memberof Panels.ControllerPanel
 * */
class ControllerFileButtonController {
    /**
     * Builds a new ControllerFileButtonController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection. Will be changed when a user selects a file
     * after clicking the controller file button.
     * @param {Panels.FilePicker.FilePicker} filePicker - The file
     * picker to use to choose a controller file when this button is
     * clicked.
     * @param {Panels.Components.OpenFileButton} button - The button
     * this controller controls.
     * */
    constructor(ems, filePicker, button) {
        this.ems = ems;
        this.filePicker = filePicker;
        this.button = button;

        this.button.onclick = () => {
            this.filePicker.open('Select a Controller', (path) => {
                this.filePicker.close();
                const dialogue = new SelectControllerDialogue();
                dialogue.start(this.ems, path);
            });
        };
    }
}
export default ControllerFileButtonController;
