/**
 * A controller to keep the controller panel head in sync with the
 * current emulation selection.
 *
 * @memberof Panels.ControllerPanel
 * */
class ControllerPanelHeadController {
    /**
     * Builds a new ControllerPanelHeadController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection.
     * @param {Panels.ControllerPanel.ControllerPanelHead} head - The
     * panel head to be kept up to date.
     * */
    constructor(ems, head) {
        this.ems = ems;
        this.head = head;

        this.ems.register(() => this.head.update(ems));
        this.head.update(ems);
    }
}
export default ControllerPanelHeadController;
