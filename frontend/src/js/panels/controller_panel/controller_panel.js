import OpenFileButton from '../components/buttons/open_file_button.js';
import ControllerPanelHead from './view/controller_panel_head.js';
import ControllerPanelBody from './view/controller_panel_body.js';

import '../components/terminal/terminal_output.js';

import FileBtnC from './controller/controller_file_button_controller.js';
import HeadC from './controller/controller_panel_head_controller.js';
import TerminalC from './controller/controller_terminal_controller.js';

/**
 * @namespace Panels.ControllerPanel
 * */

/**
 * This is the main class for the controller panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.ControllerPanel
 * */
class ControllerPanel {
    /**
     * Builds a new ControllerPanel.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {Main.EmulationSelection} ems - The application's current
     * emulation selection. Will be modified by the panel.
     * @param {Proxy.IONet} ionet - The application's network, which
     * this panel will access to read the network's controller's output.
     * @param {Panels.FilePicker.FilePicker} filePicker - A reference to
     * this application's file picker panel. The controller panel uses
     * this to allow the user to pick a controller file.
     * */
    constructor(container, ems, ionet, filePicker) {
        this.container = container;
        this.ems = ems;
        this.ionet = ionet;
        this.filePicker = filePicker;

        this.file_btn = new OpenFileButton();
        this.file_btn.title = 'select an app to run';
        this.head = new ControllerPanelHead();
        this.terminalOutput = document.createElement('terminal-output');
        this.body = new ControllerPanelBody(this.terminalOutput);

        this._file_btn_c = new FileBtnC(
            this.ems,
            this.filePicker,
            this.file_btn,
        );
        this._head_c = new HeadC(this.ems, this.head);
        this._terminal_c = new TerminalC(this.ionet, this.terminalOutput);

        this.head.appendAsButton(this.file_btn);
        this.container.appendChild(this.head.element);
        this.container.appendChild(this.body.element);
    }
}
export default ControllerPanel;
