import PanelHead from '../../components/panel_head.js';
const pathlib = require('path');

const DEFAULT_TITLE = 'Please Select an App to Run';

/**
 * This view is for the panel head of the controller panel.
 * It can update its own title from the ems when prompted.
 *
 * @extends Panel.Components.PanelHead
 * @memberof Panels.ControllerPanel
 * */
class ControllerPanelHead extends PanelHead {
    /**
     * Builds a new ControllerPanelHead.
     *
     * @param {string} title - An initial title for the panel.
     * */
    constructor(title = DEFAULT_TITLE) {
        super(title);
    }

    /**
     * Update the panel's title based on the currently selected
     * controller file.
     *
     * @param {Main.EmulationSelection} ems - The emulation selection
     * from which the current controller file is read. If no controller
     * file is currently selected, a default title will be displayed.
     * */
    update(ems) {
        if (ems.controllerPath) {
            const name = pathlib.basename(ems.controllerPath);
            this.title = `App selected: ${name}`;
        } else {
            this.title = DEFAULT_TITLE;
        }
    }
}
export default ControllerPanelHead;
