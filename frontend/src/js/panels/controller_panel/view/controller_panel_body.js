import View from '../../view.js';

/**
 * This view is for the panel body of the controller panel.
 * It mainly contains the given terminal output but contstructs some DOM
 * elements around it for visual reasons.
 *
 * @extends Panels.View
 * @memberof Panels.ControllerPanel
 * */
class ControllerPanelBody extends View {
    /**
     * Builds a new ControllerPanelBody.
     *
     * @param {Panels.Components.TerminalOutput} terminalOutput -
     * The terminal output to display inside this view.
     * */
    constructor(terminalOutput) {
        super();
        this._element = document.createElement('div');
        this._element.classList.add('panel-body');
        this._element.appendChild(terminalOutput);
    }

    /** @inheritdoc */
    get element() { return this._element; }
}
export default ControllerPanelBody;
