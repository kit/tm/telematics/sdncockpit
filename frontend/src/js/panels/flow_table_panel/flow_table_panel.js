/**
 * @namespace Panels.FlowTablePanel
 * */

import FlowTableListView from './view/flow_table_list_view.js';

import FTLC from './controller/flow_table_list_controller.js';

/**
 * This is the main class for the flow table panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.FlowTablePanel
 */
class FlowTablePanel {
    /**
     * Builds a new FlowTablePanel.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {API.API} api - The api from which to pull flow table
     * info.
     */
    constructor(container, api) {
        this.container = container;
        this.api = api;

        this.view = new FlowTableListView();
        this.controller = new FTLC(this.api, this.view);

        this.container.appendChild(this.view.element);
    }
}
export default FlowTablePanel;
