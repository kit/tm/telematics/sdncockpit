import View from '../../view';
import FlowTableView from './flow_table_view';

/**
 * This view encapsulates an updatable list of flow tables.
 *
 * @extends Panels.View
 * @memberof Panels.FlowTablePanel
 */
class FlowTableListView extends View {
    /**
     * Builds a new FlowTableListView.
     */
    constructor() {
        super();

        this._element = document.createElement('div');
        this._element.id = 'flow-table-list';

        this._list = document.createElement('div');
        this._element.appendChild(this._list);

        this._flow_table_views = [];
    }

    /**
     * Updates the view to represent the given table of flow tables.
     *
     * @param {API.FlowTables} flowTables - The flow tables to
     * represent in this view.
     */
    update(flowTables = {}) {
        // We take the easy route and just replace the entire list.
        const flowTableViews = [];
        const list = document.createElement('div');

        for (const [name, flowTable] of Object.entries(flowTables)) {
            const view = new FlowTableView(name, flowTable);
            flowTableViews.push(view);
            list.appendChild(view._element);
        }

        this._list.replaceWith(list);
        this._flow_table_views = flowTableViews;
    }
}
export default FlowTableListView;
