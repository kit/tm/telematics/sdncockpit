import View from '../../view';
import PanelHead from '../../components/panel_head.js';
import TextContainer from '../../components/text_container';

/**
 * This view encapsulates an updatable flow table.
 *
 * @extends Panels.View
 * @memberof Panels.FlowTablePanel
 */
class FlowTableView extends View {
    /**
     * Builds a new FlowTableView.
     *
     * @param {string} name - The name of the switch who's flow table is
     * displayed in this view.
     * @param {string} flowTable - The initial contents of the
     * flow table.
     */
    constructor(name, flowTable) {
        super();

        this._element = document.createElement('div');
        this._element.classList.add('flow-table');
        this._element.classList.add('panel');

        this._head = new PanelHead(name);
        this._element.appendChild(this._head._element);

        this._body = document.createElement('div');
        this._body.classList.add('panel-body');

        this._text_container = new TextContainer();
        this._body.appendChild(this._text_container._element);

        this._element.appendChild(this._body);

        this.update(flowTable);
    }

    /**
     * Updates the view to represent the given flow table.
     *
     * @param {string} flowTable - The flow tables to represent
     * in this view.
     */
    update(flowTable) {
        // the first line in the flow table string is debug output and
        // should be removed before displaying.
        const lines = flowTable.split('\n');
        lines.shift();
        flowTable = lines.join('\n');
        this._text_container.setText(flowTable);
    }
}
export default FlowTableView;
