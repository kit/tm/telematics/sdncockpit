
/**
 * A controller for a flow table list.
 * Keeps the list updated.
 *
 * @memberof Panels.FlowTablePanel
 */
class FlowTableListController {
    /**
     * Builds a new FlowTableListController.
     *
     * @param {API.API} api - The api via which this controller will
     * pull updates for the list.
     * @param {Panels.FlowTablePanel.FlowTableListView} view - The view
     * to update.
     */
    constructor(api, view) {
        this.api = api;
        this.view = view;

        this._update();
    }

    /**
     * Update the view.
     *
     * @private
     * */
    async _update() {
        try {
            const flowTables = await this.api.flowTables();
            this.view.update(flowTables);
        } catch (error) {
            console.log(error);
            this.view.update();
        }
    }
}
export default FlowTableListController;
