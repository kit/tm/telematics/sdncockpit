import FilePickerView from './view/file_picker_view.js';
import FileTree from './view/file_tree.js';

import FileTreeController from './controller/file_tree_controller.js';

/**
 * @namespace Panels.FilePicker
 * */

/**
 * A callback for when a file has been selected from the panel.
 *
 * @callback Panels.FilePicker.FilePicker~SelectCallback
 * @param {string} path - The path to the selected file in the file
 * system the panel operates on.
 *
 * @memberof Panels.FilePicker
 * */

/**
 * This is the main class for the file picker panel.
 * It connects the panel's views and controllers to each other and to
 * the model and makes both accessible through a single interface.
 *
 * @memberof Panels.FilePicker
 * */
class FilePicker {
    /**
     * Builds a new FilePicker.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {HTMLElement} visibilityElement - The greater container
     * element which controls the visibility of the panel.
     * @param {Proxy.FileSystem} fs - The file system the panel will
     * display and interact with.
     * */
    constructor(container, visibilityElement, fs) {
        const tree = new FileTree();
        this._tree_controller = new FileTreeController(fs, tree);

        this._view = new FilePickerView(container, visibilityElement, tree);
        this._view.bindClose(() => this.close());
    }

    /**
     * Open the file picker panel.
     *
     * @param {string} title - The title for the panel.
     * @param {Panels.FilePicker.FilePicker~SelectCallback} onselect -
     * A callback for when a file has been selected from the panel.
     * */
    open(title, onselect = () => {}) {
        if (title) {
            this._view.title = title;
        } else {
            this._view.title = 'File Explorer';
        }

        this._tree_controller.on_file_open = onselect;

        this._view.show();
    }

    /**
     * Close the file picker panel.
     * */
    close() {
        this._view.hide();
    }

    /**
     * Signals whether the panel is open or closed.
     * @type {boolean}
     * */
    get is_open() {
        return this._view.is_visible;
    }
}
export default FilePicker;
