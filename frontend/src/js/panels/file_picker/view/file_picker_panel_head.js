import View from '../../view.js';
import PanelHead from '../../components/panel_head.js';
import CloseButton from '../../components/buttons/close_button.js';

/**
 * This view is for the panel head of the file picker panel.
 * It contains a title element and a close button.
 *
 * @extends Panels.View
 * @memberof Panels.FilePicker
 * */
class FilePickerPanelHead extends View {
    /**
     * Builds a new FilePickerPanelHead.
     *
     * @param {string} title - An initial title for the panel.
     * */
    constructor(title) {
        super();
        this._head = new PanelHead(title);

        this._closeButton = new CloseButton();
        this._closeButton.title = 'close the file explorer';
        this._head.appendAsButton(this._closeButton);
    }

    /** @inheritdoc */
    get element() {
        return this._head.element;
    }

    /**
     * The panel's title.
     * @type {string}
     * */
    get title() {
        return this._head.title;
    }

    /* eslint-disable require-jsdoc */
    set title(val) {
        this._head.title = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * Binds the given handler to the close button.
     *
     * @param {function} handler - Will be called when the panel head's
     * close button is clicked.
     * */
    bindClose(handler) {
        this._closeButton.onclick = handler;
    }
}
export default FilePickerPanelHead;
