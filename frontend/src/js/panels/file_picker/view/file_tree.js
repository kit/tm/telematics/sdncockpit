import View from '../../view.js';

import '../../components/tree_view/tree_view.js';
import '../../components/tree_view/tree_view_branch_list.js';
import '../../components/tree_view/tree_view_item.js';
import '../../components/tree_view/tree_view_lable.js';

import RootContextMenu from './root_context_menu.js';
import FileContextMenu from './file_context_menu.js';
import DirContextMenu from './dir_context_menu.js';

import {ConnectionError, ServerError} from '../../../api/api.js';

const TV = 'tree-view';
const TVBL = 'tree-view-branch-list';
const TVI = 'tree-view-item';
const TVL = 'tree-view-lable';
const TVI_F = `${TVI}[data-type="file"]`;
const TVI_D = `${TVI}[data-type="dir"]`;
// const TVI_DE = `${TVI_D}[expanded]`;
// const TVI_DNE = `${TVI_D}:not([expanded])`;

const pathlib = require('path');

/**
 * @callback Panels.FilePicker.FileTree~FileActionCallback
 * @param {API.DirectoryEntry} file - The file or directory an action
 * was requested for.
 * */

/**
 * The file tree is the central view of the file picker panel.
 * It shows the files and directories of a file system as a tree and
 * provides many ways to interact with them. Handlers for such
 * interactions can be bound via the appropriate methods of this class.
 *
 * @extends Panels.View
 * @memberof Panels.FilePicker
 * */
class FileTree extends View {
    /**
     * Builds a new FileTree.
     * */
    constructor() {
        super();
        this._element = document.createElement(TV);

        /* eslint-disable no-unused-vars */
        this._bound = {
            open_file: (entry) => {},
            cut: (entry) => {},
            copy: (entry) => {},
            paste: (entry) => {},
            delete: (entry) => {},
            rename: (entry) => {},
            new_file: (entry) => {},
            new_dir: (entry) => {},
        };
        /* eslint-enable no-unused-vars */

        const ltof = (lable) => {
            const item = lable.parentElement;
            const type = item.dataset.type;
            const path = item.dataset.path;

            return {path: path, type: type};
        };

        const rootSelector = `${TVI_D}[data-path='/'] > ${TVL}`;
        const rootMenu = new RootContextMenu(rootSelector);
        rootMenu.bindPaste((lable) => this._bound.paste(ltof(lable)));
        rootMenu.bindNewFile((lable) => this._bound.new_file(ltof(lable)));
        rootMenu.bindNewDir((lable) => this._bound.new_dir(ltof(lable)));

        const fileSelector = `${TVI_F} > ${TVL}`;
        const fileMenu = new FileContextMenu(fileSelector);
        fileMenu.bindOpenFile((lable) => this._bound.open_file(ltof(lable)));
        fileMenu.bindCut((lable) => this._bound.cut(ltof(lable)));
        fileMenu.bindCopy((lable) => this._bound.copy(ltof(lable)));
        fileMenu.bindDelete((lable) => this._bound.delete(ltof(lable)));
        fileMenu.bindRename((lable) => this._bound.rename(ltof(lable)));

        const dirSelector = `${TVI_D} > ${TVL}:not(${rootSelector})`;
        const dirMenu = new DirContextMenu(dirSelector);
        dirMenu.bindCut((lable) => this._bound.cut(ltof(lable)));
        dirMenu.bindCopy((lable) => this._bound.copy(ltof(lable)));
        dirMenu.bindPaste((lable) => this._bound.paste(ltof(lable)));
        dirMenu.bindDelete((lable) => this._bound.delete(ltof(lable)));
        dirMenu.bindRename((lable) => this._bound.rename(ltof(lable)));
        dirMenu.bindNewFile((lable) => this._bound.new_file(ltof(lable)));
        dirMenu.bindNewDir((lable) => this._bound.new_dir(ltof(lable)));

        this._element.addEventListener('click', (e) => {
            const item = e.target.closest(TVI);
            if (item) {
                const type = item.dataset.type;
                const path = item.dataset.path;

                if (type == 'dir') {
                    item.toggle();
                } else {
                    this._bound.open_file({path: path, type: type});
                }
            }
        });
    }

    /** @inheritdoc */
    get element() { return this._element; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a file or directory to be opened.
     * */
    bindOpenFile(handler) { this._bound.open_file = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a file or directory to be cut (perhaps for later
     * pasting).
     * */
    bindCut(handler) { this._bound.cut = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a file or directory to be selected for copying.
     * */
    bindCopy(handler) { this._bound.copy = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want previously cut/copied files or directories to be pasted
     * somewhere.
     * */
    bindPaste(handler) { this._bound.paste = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a file or directory to be deleted.
     * */
    bindDelete(handler) { this._bound.delete = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a file or directory to be renamed.
     * */
    bindRename(handler) { this._bound.rename = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a new file to be added to a selected directory.
     * */
    bindNewFile(handler) { this._bound.new_file = handler; }

    /**
     * Binds the given handler to the 'open file' action.
     *
     * @param {Panels.FilePicker.FileTree~FileActionCallback} handler -
     * Will be called when a user interacts with the view to indicate
     * they want a new directory to be added to a selected directory.
     * */
    bindNewDir(handler) { this._bound.new_dir = handler; }

    /**
     * Update a tree view item recursively.
     *
     * @param {Proxy.FileSystem} fs - The file system from which to
     * update.
     * @param {Panels.Components.TreeViewItem} tvi - The item to update.
     * */
    async _updateItem(fs, tvi) {
        const itemPath = tvi.dataset.path;
        const itemType = tvi.dataset.type;

        const query = `:scope > ${TVBL}`;
        const tvbl = tvi.querySelector(query);

        if (itemType == 'file') {
            if (tvbl != null) {
                tvbl.remove();
            }
        } else if (itemType == 'dir') {
            try {
                const content = await fs.readDir(itemPath);
                this._updateBranchList(fs, tvbl, content);
            } catch (error) {
                console.error(error);
                if (
                    !(error instanceof ConnectionError) &&
                    !(error instanceof ServerError)
                ) {
                    tvi.remove();
                    return;
                }
            }
        }
    }

    /**
     * Update a tree view branch list.
     *
     * @param {Proxy.FileSystem} fs - The file system from which to
     * update.
     * @param {Panels.Components.TreeViewBranchList} tvbl - The list to
     * update.
     * @param {API.DirectoryEntry[]} entries - The entries of the branch
     * list as read from the fs.
     * */
    async _updateBranchList(fs, tvbl, entries) {
        const TVI_QUERY = `:scope > ${TVI}`;

        // let's start off with some helper functions
        /* eslint-disable require-jsdoc */
        function tviToFSEntry(tvi) {
            return {path: tvi.dataset.path, type: tvi.dataset.type};
        }

        /* Introduces an order for file system entries */
        function compare(a, b) {
            const pathComp = a.path < b.path ? -1 : (a.path > b.path ? 1 : 0);
            if (a.type == 'dir' && b.type == 'dir') {
                return pathComp;
            } else if (a.type == 'dir') {
                return -1;
            } else if (b.type == 'dir') {
                return 1;
            } else {
                return pathComp;
            }
        }

        /*
         * Builds a <tree-view-lable> for the given file system entry.
         */
        function buildLable(entry) {
            const lable = document.createElement(TVL);

            const icon = document.createElement('i');
            icon.classList.add('fa', 'fa-fw', 'fa-solid', 'path-lable-icon');
            lable.appendChild(icon);

            const name = document.createElement('span');
            if (entry.path == '/') {
                name.innerText = '/';
            } else {
                name.innerText = pathlib.basename(entry.path);
            }
            lable.appendChild(name);

            const link = document.createElement('a');
            const url = new URL(`/fs${entry.path}`, window.location.href);
            url.port = '8090';
            link.setAttribute('href', url.href);
            link.setAttribute('target', '_blank');
            lable.appendChild(link);

            return lable;
        }

        function buildItem(entry) {
            /*
             * Builds a <tree-view-item> containing an appropriate
             * <tree-view-lable> and an empty <tree-view-branch-list>.
             */
            const tvi = document.createElement(TVI);
            tvi.dataset.path = entry.path;
            tvi.dataset.type = entry.type;
            tvi.appendChild(buildLable(entry));
            tvi.appendChild(document.createElement(TVBL));

            if (entry.path == '/') {
                tvi.expand();
            }

            return tvi;
        }
        /* eslint-enable require-jsdoc */

        // remove items corresponding to deleted entries
        let tvis = tvbl.querySelectorAll(TVI_QUERY);
        for (const tvi of tvis) {
            const tviEntry = tviToFSEntry(tvi);

            if (!entries.some((entry) => compare(entry, tviEntry) == 0)) {
                tvi.remove();
            }
        }

        // insert items corresponding to new entries
        // ensure correct ordering via duplicate-avioding insertion sort
        entries.sort(compare);

        for (const entry of entries) {
            let inserted = false;
            tvis = [...tvbl.querySelectorAll(TVI_QUERY)];

            for (const tvi of tvis) {
                const comparison = compare(tviToFSEntry(tvi), entry);
                if (comparison < 0) { // tvi < entry
                    // keep searching
                    continue;
                } else if (comparison == 0) { // tvi == entry
                    // there is already an item for entry
                    inserted = true;
                    break;
                } else if (comparison > 0) { // tvi > entry
                    // first item where the above is true
                    // => insert entry
                    tvbl.insertBefore(buildItem(entry), tvi);
                    inserted = true;
                    // there is now an item for entry
                    break;
                }
            }

            if (!inserted) {
                tvbl.appendChild(buildItem(entry));
            }
        }

        // recurse
        tvis = [...tvbl.querySelectorAll(TVI_QUERY)];
        await Promise.all(tvis.map(async (tvi) => {
            await this._updateItem(fs, tvi);
        }));
    }

    /**
     * Update the file tree to reflect the current state of a given
     * file system.
     *
     * @param {Proxy.FileSystem} fs - The file system to show.
     * */
    async update(fs) {
        const query = `:scope > ${TVBL}`;
        let tvbl = this._element.querySelector(query);

        if (tvbl == null) {
            tvbl = document.createElement(TVBL);
            this._element.appendChild(tvbl);
        }

        await this._updateBranchList(fs, tvbl, [{path: '/', type: 'dir'}]);
    }

    // TODO: Add an expandpath(path) method so that the controller can
    // expand the path of directories that just had something copied
    // or moved or created into them.
}
export default FileTree;
