import ContextMenuBuilder
    from '../../components/context_menu/context_menu_builder.js';

/**
 * A class wrapping a context menu for the root directory of the file
 * tree. All menu items have a corresponding binding that a handler
 * can be assigned to.
 *
 * @memberof Panels.FilePicker
 * */
class RootContextMenu {
    /**
     * Builds RootContextMenu.
     *
     * @param {string} selector - A css selector to select the root
     * directory's element.
     * */
    constructor(selector) {
        /* eslint-disable no-unused-vars */
        this._bound = {
            paste: (target) => {},
            new_file: (target) => {},
            new_dir: (target) => {},
        };
        /* eslint-enable no-unused-vars */

        const _builder = new ContextMenuBuilder(selector);
        _builder.addItem('paste', (target) => this._bound.paste(target));
        _builder.addItem('new file', (target) => this._bound.new_file(target));
        _builder.addItem(
            'new directory',
            (target) => this._bound.new_dir(target),
        );

        document.body.appendChild(_builder.get());
    }

    /**
     * Binds the given handler to the 'paste' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'paste' is
     * clicked.
     * */
    bindPaste(handler) { this._bound.paste = handler; }

    /**
     * Binds the given handler to the 'new file' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'new file' is
     * clicked.
     * */
    bindNewFile(handler) { this._bound.new_file = handler; }

    /**
     * Binds the given handler to the 'new directory' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'new directory'
     * is clicked.
     * */
    bindNewDir(handler) { this._bound.new_dir = handler; }
}
export default RootContextMenu;
