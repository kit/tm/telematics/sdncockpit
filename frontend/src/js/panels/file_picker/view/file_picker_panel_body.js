import View from '../../view.js';

/**
 * This view is for the panel body of the file picker panel.
 * It mainly contains the given file tree but contstructs some DOM
 * elements around it for visual reasons.
 *

 *
 * @extends Panels.View
 * @memberof Panels.FilePicker
 * */
class FilePickerPanelBody extends View {
    /**
     * Builds a new FilePickerPanelBody
     *
     * @param {Panels.FilePicker.FileTree} fileTree - The file tree to
     * display inside this view.
     * */
    constructor(fileTree) {
        super();
        this._element = document.createElement('div');
        this._element.classList.add('panel-body');

        const overflow = document.createElement('div');
        overflow.id = 'file-picker-overflow';
        overflow.appendChild(fileTree.element);

        this._element.appendChild(overflow);
    }

    /** @inheritdoc */
    get element() { return this._element; }
}
export default FilePickerPanelBody;
