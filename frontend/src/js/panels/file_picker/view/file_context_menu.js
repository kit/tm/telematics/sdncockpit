import ContextMenuBuilder
    from '../../components/context_menu/context_menu_builder.js';

/**
 * A class wrapping a context menu for files displayed via the
 * file tree. All menu items have a corresponding binding that a handler
 * can be assigned to.
 *
 * @memberof Panels.FilePicker
 * */
class FileContextMenu {
    /**
     * Builds a new FileContextMenu.
     *
     * @param {string} selector - A css selector to select all elements
     * this menu should open for.
     * */
    constructor(selector) {
        /* eslint-disable no-unused-vars */
        this._bound = {
            open_file: (target) => {},
            cut: (target) => {},
            copy: (target) => {},
            delete: (target) => {},
            rename: (target) => {},
        };
        /* eslint-enable no-unused-vars */

        const builder = new ContextMenuBuilder(selector);
        builder.addItem('open', (target) => this._bound.open_file(target));
        builder.addItem('cut', (target) => this._bound.cut(target));
        builder.addItem('copy', (target) => this._bound.copy(target));
        builder.addItem('delete', (target) => this._bound.delete(target));
        builder.addItem('rename', (target) => this._bound.rename(target));
        document.body.appendChild(builder.get());
    }

    /**
     * Binds the given handler to the 'open' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'open' is
     * clicked.
     * */
    bindOpenFile(handler) { this._bound.open_file = handler; }

    /**
     * Binds the given handler to the 'cut' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'cut' is
     * clicked.
     * */
    bindCut(handler) { this._bound.cut = handler; }

    /**
     * Binds the given handler to the 'copy' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'copy' is
     * clicked.
     * */
    bindCopy(handler) { this._bound.copy = handler; }

    /**
     * Binds the given handler to the 'delete' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'delete' is
     * clicked.
     * */
    bindDelete(handler) { this._bound.delete = handler; }

    /**
     * Binds the given handler to the 'rename' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'rename' is
     * clicked.
     * */
    bindRename(handler) { this._bound.rename = handler; }
}
export default FileContextMenu;
