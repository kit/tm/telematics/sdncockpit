import ContextMenuBuilder
    from '../../components/context_menu/context_menu_builder.js';

/**
 * A class wrapping a context menu for directories displayed via the
 * file tree. All menu items have a corresponding binding that a handler
 * can be assigned to.
 *
 * @memberof Panels.FilePicker
 * */
class DirContextMenu {
    /**
     * Builds a new DirContextMenu.
     *
     * @param {string} selector - A css selector to select all elements
     * this menu should open for.
     * */
    constructor(selector) {
        /* eslint-disable no-unused-vars */
        this._bound = {
            expand: (target) => {},
            cut: (target) => {},
            copy: (target) => {},
            paste: (target) => {},
            delete: (target) => {},
            rename: (target) => {},
            new_file: (target) => {},
            new_dir: (target) => {},
        };
        /* eslint-enable no-unused-vars */

        const builder = new ContextMenuBuilder(selector);
        builder.addItem('open', (target) => this._bound.expand(target));
        builder.addItem('cut', (target) => this._bound.cut(target));
        builder.addItem('copy', (target) => this._bound.copy(target));
        builder.addItem('paste', (target) => this._bound.paste(target));
        builder.addItem('delete', (target) => this._bound.delete(target));
        builder.addItem('rename', (target) => this._bound.rename(target));
        builder.addItem('new file', (target) => this._bound.new_file(target));
        builder.addItem(
            'new directory',
            (target) => this._bound.new_dir(target),
        );
        document.body.appendChild(builder.get());
    }

    /**
     * Binds the given handler to the 'expand' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'expand' is
     * clicked.
     * */
    bindExpand(handler) { this._bound.expand = handler; }

    /**
     * Binds the given handler to the 'cut' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'cut' is
     * clicked.
     * */
    bindCut(handler) { this._bound.cut = handler; }

    /**
     * Binds the given handler to the 'copy' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'copy' is
     * clicked.
     * */
    bindCopy(handler) { this._bound.copy = handler; }

    /**
     * Binds the given handler to the 'delete' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'delete' is
     * clicked.
     * */
    bindDelete(handler) { this._bound.delete = handler; }

    /**
     * Binds the given handler to the 'paste' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'paste' is
     * clicked.
     * */
    bindPaste(handler) { this._bound.paste = handler; }

    /**
     * Binds the given handler to the 'rename' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'rename' is
     * clicked.
     * */
    bindRename(handler) { this._bound.rename = handler; }

    /**
     * Binds the given handler to the 'new file' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'new file' is
     * clicked.
     * */
    bindNewFile(handler) { this._bound.new_file = handler; }

    /**
     * Binds the given handler to the 'new directory' menu item.
     *
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * handler - Will be called when the button labled 'new directory'
     * is clicked.
     * */
    bindNewDir(handler) { this._bound.new_dir = handler; }
}
export default DirContextMenu;
