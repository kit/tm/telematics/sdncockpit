import View from '../../view.js';
import FilePickerPanelHead from './file_picker_panel_head.js';
import FilePickerPanelBody from './file_picker_panel_body.js';

/**
 * A view for the entire file picker. Combines the partial views for
 * head and body of the panel with the container and visibility
 * elements.
 *
 * @extends Panels.View
 * @memberof Panels.FilePicker
 * */
class FilePickerView extends View {
    /**
     * Builds a new FilePickerView.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {HTMLElement} visibilityElement - The greater container
     * element which controls the visibility of the panel.
     * @param {Panels.FilePicker.FileTree} fileTree - The file tree to
     * display inside this view.
     * */
    constructor(container, visibilityElement, fileTree) {
        super();
        this._container = container;
        this._visibilityElement = visibilityElement;
        this._fileTree = fileTree;
        this.isVisible = false;

        this._bound = {
            close: () => {},
        };

        this._head = new FilePickerPanelHead('File Explorer');
        this._container.appendChild(this._head.element);

        this._body = new FilePickerPanelBody(fileTree);
        this._container.appendChild(this._body.element);
    }

    /** @inheritdoc */
    get element() { return this._visibility_element; }

    /**
     * Binds the given handler to the panel's close button.
     *
     * @param {function} handler - Will be called when the panel head's
     * close button is clicked.
     * */
    bindClose(handler) { this._head.bindClose(handler); }

    /**
     * The panel's title.
     * @type {string}
     * */
    get title() {
        return this._head.title;
    }

    /* eslint-disable require-jsdoc */
    set title(val) {
        this._head.title = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * Makes the panel visible.
     * */
    show() {
        this.isVisible = true;
    }

    /**
     * Makes the panel invisible.
     * */
    hide() {
        this.isVisible = false;
    }

    /**
     * Signals whether the panel is currently visible.
     *
     * @type {boolean}
     * */
    get isVisible() {
        return this._visibilityElement.hasAttribute('is-visible');
    }

    /* eslint-disable require-jsdoc */
    set isVisible(val) {
        if (val) {
            this._visibilityElement.setAttribute('is-visible', '');
        } else {
            this._visibilityElement.removeAttribute('is-visible');
        }
    }
    /* eslint-enable require-jsdoc */
}
export default FilePickerView;
