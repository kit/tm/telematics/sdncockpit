import CopyDirectoryDialogue
    from '../../../dialogues/copy_directory_dialogue.js';
import CopyFileDialogue
    from '../../../dialogues/copy_file_dialogue.js';
import DeleteDirectoryDialogue
    from '../../../dialogues/delete_directory_dialogue.js';
import DeleteFileDialogue
    from '../../../dialogues/delete_file_dialogue.js';
import MoveDirectoryDialogue
    from '../../../dialogues/move_directory_dialogue.js';
import MoveFileDialogue
    from '../../../dialogues/move_file_dialogue.js';
import NewDirectoryDialogue
    from '../../../dialogues/new_directory_dialogue.js';
import NewFileDialogue
    from '../../../dialogues/new_file_dialogue.js';

const pathlib = require('path');

const ClipboardModes = {
    empty: 0,
    cut: 1,
    copy: 2,
};

/**
 * A callback for when a file has been opened from the panel.
 *
 * @callback Panels.FilePicker.FileTreeController~OpenFileCallback
 * @param {string} path - The path to the selected file in the file
 * system the controller operates on.
 *
 * @memberof Panels.FilePicker
 * */

/**
 * This class is the controller for the file tree view.
 * As all controllers, it handles user interactions initiated through
 * the view and updates the view to reflect changes made through those
 * interactions or otherwise.
 *
 * @memberof Panels.FilePicker
 * */
class FileTreeController {
    /**
     * Builds a new FileTreeController.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {Panels.FilePicker.FileTree} fileTree - The view to
     * attach to.
     * @property {Panels.FilePicker.FileTreeController~OpenFileCallback}
     * on_file_open - Can be assigned to to register a callback for when
     * a file from the file tree is opened.
     * */
    constructor(fs, fileTree) {
        this.fs = fs;
        this.fileTree = fileTree;
        /* eslint-disable no-unused-vars */
        this.on_file_open = (path) => {};
        /* eslint-enable no-unused-vars */

        this._clipboard = [];
        this._clipboard_mode = ClipboardModes.empty;

        this.fileTree.bindOpenFile((...args) => this._onOpenFile(...args));
        this.fileTree.bindCut((...args) => this._onCut(...args));
        this.fileTree.bindCopy((...args) => this._onCopy(...args));
        this.fileTree.bindPaste((...args) => this._onPaste(...args));
        this.fileTree.bindDelete((...args) => this._onDelete(...args));
        this.fileTree.bindRename((...args) => this._onRename(...args));
        this.fileTree.bindNewFile((...args) => this._onNewFile(...args));
        this.fileTree.bindNewDir((...args) => this._onNewDir(...args));

        this.fs.register(() => this.fileTree.update(this.fs));
        this.fileTree.update(this.fs);
    }

    /**
     * Clears the internal clipboars.
     * */
    _clearClipboard() {
        this._clipboard = [];
        this._clipboard_mode = ClipboardModes.empty;
    }

    /**
     * Handles a view event to open a file.
     *
     * @param {API.DirectoryEntry} file - The file that needs to be
     * opened.
     * */
    _onOpenFile(file) {
        const path = file.path;

        this.on_file_open(path);
    }

    /**
     * Handles a view event to cut a file or directory.
     *
     * @param {API.DirectoryEntry} file - The file or directory that
     * needs to be cut.
     * */
    _onCut(file) {
        this._clipboard = [file];
        this._clipboard_mode = ClipboardModes.cut;
    }

    /**
     * Handles a view event to copy a file or directory.
     *
     * @param {API.DirectoryEntry} file - The file or directory that
     * needs to be copied.
     * */
    _onCopy(file) {
        this._clipboard = [file];
        this._clipboard_mode = ClipboardModes.copy;
    }

    /**
     * Handles a view event to paste into a directory.
     *
     * @param {API.DirectoryEntry} file - The directory that needs to be
     * pasted into.
     * */
    _onPaste(file) {
        const path = file.path;

        if (this._clipboard_mode == ClipboardModes.empty) {
            return;
        }

        const args = this._clipboard.map((entry) => {
            const type = entry.type;
            const src = entry.path;
            const name = pathlib.basename(entry.path);
            const dst = pathlib.join(path, name);

            return {type: type, src: src, dst: dst};
        });

        if (this._clipboard_mode == ClipboardModes.cut) {
            for (const entry of args) {
                if (entry.type == 'file') {
                    const dialogue = new MoveFileDialogue();
                    dialogue.start(this.fs, entry.src, entry.dst)
                        .catch(() => {});
                } else {
                    const dialogue = new MoveDirectoryDialogue();
                    dialogue.start(this.fs, entry.src, entry.dst)
                        .catch(() => {});
                }
            }
        } else if (this._clipboard_mode == ClipboardModes.copy) {
            for (const entry of args) {
                if (entry.type == 'file') {
                    const dialogue = new CopyFileDialogue();
                    dialogue.start(this.fs, entry.src, entry.dst)
                        .catch((e) => { console.error(e); });
                } else {
                    const dialogue = new CopyDirectoryDialogue();
                    dialogue.start(this.fs, entry.src, entry.dst)
                        .catch((e) => { console.error(e); });
                }
            }
        }

        this._clearClipboard();
    }

    /**
     * Handles a view event to delete a file or directory.
     *
     * @param {API.DirectoryEntry} file - The file or directory that
     * needs to be deleted.
     * */
    _onDelete(file) {
        const type = file.type;
        const path = file.path;

        if (type == 'file') {
            const dialogue = new DeleteFileDialogue();
            dialogue.start(this.fs, path).catch((e) => { console.error(e); });
        } else {
            const dialogue = new DeleteDirectoryDialogue();
            dialogue.start(this.fs, path).catch((e) => { console.error(e); });
        }

        this._clearClipboard();
    }

    /**
     * Handles a view event to rename a file or directory.
     *
     * @param {API.DirectoryEntry} file - The file or directory that
     * needs to be renamed.
     * */
    _onRename(file) {
        const type = file.type;
        const path = file.path;

        // TODO: Make this a dialogue
        const name = prompt('Enter a new name', pathlib.basename(path));
        if (name != null) {
            const newPath = pathlib.join(pathlib.dirname(path), name);
            if (type == 'file') {
                const dialogue = new MoveFileDialogue();
                dialogue.start(this.fs, path, newPath)
                    .catch((e) => { console.error(e); });
            } else if (type == 'dir') {
                const dialogue = new MoveDirectoryDialogue();
                dialogue.start(this.fs, path, newPath)
                    .catch((e) => { console.error(e); });
            }
        }

        this._clearClipboard();
    }

    /**
     * Handles a view event to create a new file.
     *
     * @param {API.DirectoryEntry} file - The directory in which the new
     * file needs to be created.
     * */
    _onNewFile(file) {
        const path = file.path;

        const dialogue = new NewFileDialogue();
        dialogue.start(this.fs, pathlib.join(path, 'new_file.py'))
            .catch((e) => { console.error(e); });
    }

    /**
     * Handles a view event to create a new directory.
     *
     * @param {API.DirectoryEntry} file - The directory in which the new
     * directory needs to be created.
     * */
    _onNewDir(file) {
        const path = file.path;

        const dialogue = new NewDirectoryDialogue();
        dialogue.start(this.fs, pathlib.join(path, 'new_dir'))
            .catch((e) => { console.error(e); });
    }
}
export default FileTreeController;
