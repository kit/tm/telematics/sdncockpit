import OpenFileButton from '../components/buttons/open_file_button.js';
import ScenarioPanelHead from './view/scenario_panel_head.js';
import ScenarioPanelBody from './view/scenario_panel_body.js';

import FileBtnC from './controller/scenario_file_button_controller.js';
import HeadC from './controller/scenario_panel_head_controller.js';
import BodyC from './controller/scenario_panel_body_controller.js';

/**
 * @namespace Panels.ScenarioPanel
 * */

/**
 * This is the main class for the scenario panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioPanel {
    /**
     * Builds a new ScenarioPanel.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {Main.EmulationSelection} ems - The application's current
     * emulation selection. Will be modified by the panel.
     * @param {API.API} api - The api, used to parse scenario files.
     * @param {Proxy.IONet} ionet - The application's network, which
     * this panel will start, stop, and read from extensively.
     * @param {Panels.FilePicker.FilePicker} filePicker - A reference
     * to this application's file picker panel. The scenario panel uses
     * this to allow the user to pick a scenario file.
     * */
    constructor(container, ems, api, ionet, filePicker) {
        this.container = container;
        this.ems = ems;
        this.api = api;
        this.ionet = ionet;
        this.filePicker = filePicker;

        this.file_btn = new OpenFileButton();
        this.file_btn.title = 'select a scenario file';
        this.head = new ScenarioPanelHead();
        this.body = new ScenarioPanelBody();

        this._file_btn_c = new FileBtnC(
            this.ems, this.filePicker, this.file_btn);
        this._head_c = new HeadC(this.ems, this.head);
        this._body_c = new BodyC(this.ems, this.api, this.body);

        this.head.appendAsButton(this.file_btn);
        this.container.appendChild(this.head.element);
        this.container.appendChild(this.body.element);
    }
}
export default ScenarioPanel;
