import {
    FileNotFoundError,
    ScenarioFileSemanticError,
    ScenarioFileSyntaxError,
} from '../../../api/generated/errorcodes';

/**
 * A controller to keep the scenario panel body in sync with the current
 * emulation selection.
 *
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioPanelBodyController {
    /**
     * Builds a new ScenarioPanelBodyController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection.
     * @param {API.API} api - The API, used to parse scenario files.
     * @param {Panels.ScenarioPanel.ScenarioPanelBody} body - The panel
     * body to be kept up to date.
     * */
    constructor(ems, api, body) {
        this.ems = ems;
        this.api = api;
        this.body = body;

        const update = async () => {
            const path = this.ems.scenarioPath;
            if (!path) {
                this.body.setText('');
                return;
            }

            try {
                const output = await this.api.parse_scenario(path);
                const scenario = output.scenario;
                this.body.setText(scenario.description);
            } catch (error) {
                if (error instanceof FileNotFoundError) {
                    this.body.setText('');
                } else if (error instanceof ScenarioFileSyntaxError) {
                    this.body.setText(
                        "The selected scenario file has a syntactic error:\n"
                        + error.data
                    )
                } else if (error instanceof ScenarioFileSemanticError) {
                    this.body.setText(
                        "The selected scenario file has a semantic error:\n"
                        + `${error.data.message}\n`
                        + `in: ${error.data.stack}`
                    )
                }
            }
        };

        this.ems.register(() => update());
        update();
    }
}
export default ScenarioPanelBodyController;
