/**
 * A controller to keep the scenario panel head in sync with the current
 * emulation selection.
 *
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioPanelHeadController {
    /**
     * Builds a new ScenarioPanelHeadController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection.
     * @param {Panels.ScenarioPanel.ScenarioPanelHead} head - The panel
     * head to be kept up to date.
     * */
    constructor(ems, head) {
        this.ems = ems;
        this.head = head;

        this.ems.register(() => this.head.update(ems));
        this.head.update(ems);
    }
}
export default ScenarioPanelHeadController;
