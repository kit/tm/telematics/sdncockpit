import SelectScenarioDialogue from
    '../../../dialogues/select_scenario_dialogue';

/**
 * A controller for the scenario file button.
 *
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioFileButtonController {
    /**
     * Builds a new ScenarioFileButtonController.
     *
     * @param {Main.EmulationSelection} ems - The application's
     * emulation selection. Will be changed when a user selects a file
     * after clicking the scenario file button.
     * @param {Panels.FilePicker.FilePicker} filePicker - The file
     * picker to use to choose a scenario file when this button is
     * clicked.
     * @param {Panels.Components.OpenFileButton} button - The button
     * this controller controls.
     * */
    constructor(ems, filePicker, button) {
        this.ems = ems;
        this.filePicker = filePicker;
        this.button = button;

        this.button.onclick = () => {
            this.filePicker.open('Select a Scenario', (path) => {
                this.filePicker.close();
                const dialogue = new SelectScenarioDialogue();
                dialogue.start(this.ems, path);
            });
        };
    }
}

export default ScenarioFileButtonController;
