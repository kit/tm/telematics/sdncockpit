import PanelHead from '../../components/panel_head.js';
const pathlib = require('path');

const DEFAULT_TITLE = 'Please Select a Scenario';

/**
 * This view is for the panel head of the scenario panel.
 * It can update its own title from the ems when prompted.
 *
 * @extends Panels.Components.PanelHead
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioPanelHead extends PanelHead {
    /**
     * Builds a new ScenarioPanelHead.
     *
     * @param {string} title - An initial title for the panel.
     * */
    constructor(title = DEFAULT_TITLE) {
        super(title);
    }

    /**
     * Update the panel's title based on the currently selected scenario
     * file.
     *
     * @param {Main.EmulationSelection} ems - The emulation selection
     * from which the current scenario file is read. If no scenario file
     * is currently selected, a default title will be displayed.
     * */
    update(ems) {
        if (ems.scenarioPath) {
            const name = pathlib.basename(ems.scenarioPath);
            this.title = `Scenario: ${name}`;
        } else {
            this.title = DEFAULT_TITLE;
        }
    }
}
export default ScenarioPanelHead;
