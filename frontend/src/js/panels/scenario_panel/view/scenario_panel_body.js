import View from '../../view.js';

import TextContainer from '../../components/text_container.js';

/**
 * This view is for the panel body of the scenario panel.
 * It is a simple text output.
 *
 * @extends Panels.View
 * @memberof Panels.ScenarioPanel
 * */
class ScenarioPanelBody extends View {
    /**
     * Builds a new ScenarioPanelBody
     * */
    constructor() {
        super();

        this._element = document.createElement('div');
        this._element.classList.add('panel-body');

        this._text_container = new TextContainer();
        this._element.appendChild(this._text_container._element);
    }

    /**
     * Makes the given text appear in this panel. Replaces any existing
     * text. Interprets newlines and tabs.
     *
     * @param {string} text - The text to display.
     */
    setText(text) {
        this._text_container.setText(text);
    }
}
export default ScenarioPanelBody;
