/**
 * An MVC View.
 *
 * @memberof Panels
 * */
class View {
    /**
     * Builds a new View.
     * */
    constructor() {
        this._element = document.createElement('div');
    }

    /**
     * All views in this app have a root DOM element which the rest of
     * the view lives inside. This root element can be accessed to
     * position the view in the DOM.
     * @readonly
     * @type {HTMLElement}
     * */
    get element() {
        return this._element;
    }
}
export default View;
