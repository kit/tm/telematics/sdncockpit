import TerminalPanelBody from './view/terminal_panel_body.js';
import PanelHead from '../components/panel_head.js';

import BodyC from './controller/terminal_panel_body_controller.js';

/**
 * @namespace Panels.TerminalPanel
 * */

/**
 * This is the main class for the scenario panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.TerminalPanel
 * */
class TerminalPanel {
    #container;
    #connection;
    #head;
    #body;
    #body_c;

    /**
     * Builds a new TerminalPanel.
     *
     * @param {HTMLElement} container - The container element the panel
     * lives inside.
     * @param {API.Connection} connection - The connection, for cli
     * interactions.
     * */
    constructor(container, connection) {
        this.#container = container;
        this.#connection = connection;

        this.#head = new PanelHead('Console');
        this.#body = new TerminalPanelBody();

        this.#body_c = new BodyC(this.#connection, this.#body);

        this.#container.appendChild(this.#head.element);
        this.#container.appendChild(this.#body.element);
    }
}
export default TerminalPanel;
