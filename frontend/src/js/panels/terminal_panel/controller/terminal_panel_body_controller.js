/**
 * A controller to forward i/o between the terminal and the server
 * backend.
 *
 * @memberof Panels.TerminalPanel
 * */
class TerminalPanelBodyController {
    #connection;
    #body;

    /**
     * Builds a new TerminalPanelBodyController.
     *
     * @param {API.Connection} connection - The connection, for cli
     * interactions.
     * @param {Panels.TerminalPanel.TerminalPanelBody} body - The panel
     * body to be kept up to date.
     * */
    constructor(connection, body) {
        this.#connection = connection;
        this.#body = body;

        this.#connection.cliReader((data) => this.#body.write(data));
        this.#body.bindOnData((data) => this.#connection.cliWrite(data));
        this.#body.bindOnBinary((data) => {
            const buffer = new Uint8Array(data.length);
            for (let i = 0; i < data.length; ++i) {
                buffer[i] = data.charCodeAt(i) & 255;
            }
            this.#connection.cliWrite(buffer);
        });
    }
}
export default TerminalPanelBodyController;
