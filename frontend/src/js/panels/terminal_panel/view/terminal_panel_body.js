import View from '../../view.js';
import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';

/**
 * A function that accepts a data from the terminal.
 *
 * @callback stringCallback
 * @param {string} data
 */

/**
 * This view is for the panel body of the terminal panel.
 *
 * @extends Panels.View
 * @memberof Panels.TerminalPanel
 * */
class TerminalPanelBody extends View {
    #bound;
    #term;
    #resizeObs;

    /**
     * Builds a new TerminalPanelBody
     * */
    constructor() {
        super();

        /* eslint-disable no-unused-vars */
        this.#bound = {
            on_data: (data) => {},
            on_binary: (data) => {},
        };
        /* eslint-enable no-unused-vars */

        this._element = document.createElement('div');
        this._element.classList.add('panel-body');

        this.#term = new Terminal();
        const fitAddon = new FitAddon();

        this.#term.loadAddon(fitAddon);
        this.#term.open(this._element);

        this.#resizeObs = new ResizeObserver(() => {
            fitAddon.fit();
        });
        this.#resizeObs.observe(this._element);
        fitAddon.fit();

        this.#term.onData(data => this.#bound.on_data(data));
        this.#term.onBinary(data => this.#bound.on_binary(data));
    }

    /**
     * Bings the given handler to be called when the user writes data
     * into the terminal.
     *
     * @param {stringCallback} handler
     */
    bindOnData(handler) { this.#bound.on_data = handler; }

    /**
     * Bings the given handler to be called when the user writes data
     * into the terminal.
     *
     * @param {stringCallback} handler
     */
    bindOnBinary(handler) { this.#bound.on_binary = handler; }

    /**
     * Write to the terminal.
     *
     * @param {string | ArrayBuffer} data - The data to be written.
     */
    write(data) {
        this.#term.write(
            typeof data === 'string' ? data : new Uint8Array(data)
        );
    }
}
export default TerminalPanelBody;
