import View from '../../view.js';
import './file_tab_lable.js';

const pathlib = require('path');

/**
 * This view is a file tab lable that is synchronized to a file tab.
 *
 * @extends Panels.View
 * @memberof Panels.EditorPanel
 * */
class FileTabView extends View {
    /**
     * Builds a new FileTabView.
     *
     * @param {Panels.EditorPanel.FileTab} fileTab - The file tab who's
     * state this view will reflect.
     * */
    constructor(fileTab) {
        super();
        this._fileTab_observer = () => this.update();
        this._fileTab = fileTab;
        this._fileTab.register(this._fileTab_observer);
        this._element = document.createElement('file-tab-lable');

        this._bound = {
            close: () => {},
            select: () => {},
        };

        this._element.addEventListener('closeclick', () => {
            this._bound.close();
        });

        this._element.addEventListener('selectclick', () => {
            this._bound.select();
        });

        this.update();
    }

    /** @inheritdoc */
    get element() { return this._element; }

    /**
     * Binds the given handler to this tab's close button.
     *
     * @param {function} handler - The handler. Will be called whenever
     * this tab view's close button is clicked.
     * */
    bindClose(handler) { this._bound.close = handler; }

    /**
     * Binds the given handler to this tab's select button.
     *
     * @param {function} handler - The handler. Will be called whenever
     * this tab view's select button is clicked.
     * */
    bindSelect(handler) { this._bound.select = handler; }

    /**
     * Whether this tab is displaying that it is the currently selected
     * tab or not.
     *
     * @type {boolean}
     * */
    get selected() { return this._element.selected; }

    /* eslint-disable require-jsdoc */
    set selected(val) { this._element.selected = val; }
    /* eslint-enable require-jsdoc */

    /**
     * The file tab who's state this view will reflect.
     *
     * @type {Panels.EditorPanel.FileTab}
     * */
    get fileTab() { return this._fileTab; }

    /* eslint-disable require-jsdoc */
    set fileTab(val) {
        if (val != this._fileTab) {
            this._fileTab.remove(this._fileTab_observer);
            this._fileTab = val;
            this._fileTab.register(this._fileTab_observer);
        }
    }
    /* eslint-enable require-jsdoc */

    /**
     * Makes sure this view reflects the current state of the file tab
     * it is attached to.
     * */
    update() {
        this._fileTab.isSaved().then((saved) => this._element.saved = saved);
        this._element.innerHTML = pathlib.basename(this._fileTab.path);
    }
}
export default FileTabView;
