import IconButton from '../../components/buttons/icon_button.js';

/**
 * A {@link Panels.Components.IconButton} with a default appearance
 * suitable for a 'save' button.
 *
 * @extends Panels.Components.IconButton
 * @memberof Panels.Components
 * */
class SaveButton extends IconButton {
    /**
     * Builds a new SaveButton.
     *
     * @param {string[]} iconTypes - A list of FontAwesome icon classes,
     * e.g. ["fa-folder"]. Will be applied to the button's icon.
     * */
    constructor(iconTypes = ['fa-floppy-disk']) {
        super(iconTypes, 'save file');
        this.title = 'save the current file (ctrl + s)';
    }
}
export default SaveButton;
