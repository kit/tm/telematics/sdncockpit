import View from '../../view.js';
import FileTabView from './file_tab_view.js';

/**
 * @callback Panels.EditorPanel.FileTabListView~TabCallback
 *
 * @param {Panels.EditorPanel.FileTab} tab - The tab that was interacted
 * with.
 * */

/**
 * This view displays a tab for every entry in a given tab list.
 *
 * @extends Panels.View
 * @memberof Panels.EditorPanel
 * */
class FileTabListView extends View {
    /**
     * Builds a new FileTabListView.
     *
     * @param {Panels.EditorPanel.FileTabList} tabList - The tab list to
     * display.
     * */
    constructor(tabList) {
        super();
        this._element = document.createElement('div');
        this._tabviews = [];

        this._tabList = tabList;
        this._tabList.register(() => this.update(this));

        /* eslint-disable no-unused-vars */
        this._bound = {
            tabClose: (fileTab) => {},
            tabSelect: (fileTab) => {},
        };
        /* eslint-enable no-unused-vars */
    }

    /** @inheritdoc */
    get element() { return this._element; }

    /**
     * Binds a handler to this tab list.
     *
     * @param {Panels.EditorPanel.FileTabListView~TabCallback} handler -
     * The handler. Is called whenever a tab's close button is clicked.
     * */
    bindTabClose(handler) { this._bound.tabClose = handler; }

    /**
     * Binds a handler to this tab list.
     *
     * @param {Panels.EditorPanel.FileTabListView~TabCallback} handler -
     * The handler. Is called whenever a tab's select button is clicked.
     * */
    bindTabSelect(handler) { this._bound.tabSelect = handler; }

    /**
     * Makes sure this view reflects the current state of the tab list
     * it is attached to.
     * */
    update() {
        // delete lables that no longer have corresponding entries in
        // the tabList
        for (let i = this._tabviews.length - 1; i >= 0; i--) {
            if (!this._tabList.hasTab(this._tabviews[i].fileTab)) {
                this._tabviews[i].element.remove();
                this._tabviews.splice(i, 1);
            }
        }

        // add lables for entries which do not yet have corresponding
        // lables
        for (const entry of this._tabList.asArray()) {
            const hasTab = (tabview) => tabview.fileTab === entry;
            if (!this._tabviews.some(hasTab)) {
                const tabview = new FileTabView(entry);
                tabview.bindClose(
                    () => this._bound.tabClose(tabview.fileTab),
                );
                tabview.bindSelect(
                    () => this._bound.tabSelect(tabview.fileTab),
                );
                this._tabviews.push(tabview);
                this._element.appendChild(tabview.element);
            }
        }

        // update selected status
        for (const tabview of this._tabviews) {
            if (tabview.fileTab == this._tabList.selected) {
                tabview.selected = true;
            } else {
                tabview.selected = false;
            }
        }
    }
}
export default FileTabListView;
