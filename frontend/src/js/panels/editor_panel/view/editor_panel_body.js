import View from '../../view.js';

/**
 * The editor panel's body. Mostly exists to put some visual wrappers
 * around the tab list, as well as the monaco editor instance which will
 * eventually be put into the editor_container member of this body.
 *
 * @extends Panels.View
 * @memberof Panels.EditorPanel
 * */
class EditorPanelBody extends View {
    /**
     * Builds a new EditorPanelBody.
     *
     * @param {Panels.EditorPanel.FileTabListView} tabListView - The tab
     * list to be displayed at the top of the body.
     * */
    constructor(tabListView) {
        super();
        this._tabListView = tabListView;

        this._element = document.createElement('div');
        this._element.classList.add('panel-body');

        const box = document.createElement('div');
        box.classList.add('editor-box');
        this._element.appendChild(box);

        const editorHead = document.createElement('div');
        editorHead.id = 'editor-head';
        editorHead.appendChild(tabListView.element);
        box.appendChild(editorHead);

        this._editorContainer = document.createElement('div');
        this._editorContainer.id = 'editor-container';
        box.appendChild(this._editorContainer);
    }

    /** @inheritdoc */
    get element() { return this._element; }

    /**
     * A container to house a monaco editor instance in the right place
     * of the body.
     *
     * @type {HTMLElement}
     * */
    get editorContainer() {
        return this._editorContainer;
    }
}
export default EditorPanelBody;
