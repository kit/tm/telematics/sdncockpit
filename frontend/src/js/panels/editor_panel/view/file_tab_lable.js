const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: inline-block;
    height: 1.5eM;
    color: #d9d9d9;
    background-color: #292a2d;
    border-right: 1px solid #494c50;
    padding: 0px 15px;
}

:host([selected]) {
    background-color: #000000;
    color: #eaeaea;
}

span {
    vertical-align: top;
}

button {
    background: none;
    color: inherit;
    border: none;
    padding: 0;
    font: inherit;
    cursor: pointer;
    outline: inherit;

    height: 100%;
    margin-left: 1.5eX;
}

button::before {
    content: '\\25CF';
}

:host([saved]) button::before {
    content: '\\2716';
}
</style>
<span><slot></slot></span>
<button id='btn'></button>`;

/**
 * Signals that a user has clicked on the close button of a
 * {@Link Panels.EditorPanel.FileTabLable}.
 *
 * @event FileTabLable#closeclick
 * @memberof Panels.EditorPanel
 * */

/**
 * Signals that a user has clicked on the select button of a
 * {@Link Panels.EditorPanel.FileTabLable}.
 *
 * @event FileTabLable#selectclick
 * @memberof Panels.EditorPanel
 * */

/**
 * This custom element is a lable for a file tab.
 * It can be used to signal to the user whether the file tab it
 * represents has unsaved changes and whether the tab is in focus
 * (currently being displayed in the editor).
 * It can be interacted with in two different ways to either
 * select the tab or close it. Either triggers an event.
 *
 * @fires Panels.EditorPanel.FileTabLable#closeclick
 * @fires Panels.EditorPanel.FileTabLable#selectclick
 *
 * @extends HTMLElement
 * @memberof Panels.EditorPanel
 * */
class FileTabLable extends HTMLElement {
    /**
     * Builds a new FileTabLable.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    /** This is really more of an internal method. */
    connectedCallback() {
        const button = this.shadowRoot.getElementById('btn');

        this.shadowRoot.addEventListener('click', (event) => {
            const evOpt = {bubbles: true, detail: null};
            if (event.target === button) {
                this.dispatchEvent(new CustomEvent('closeclick', evOpt));
            } else {
                this.dispatchEvent(new CustomEvent('selectclick', evOpt));
            }
        });
    }

    /**
     * Whether this tab is displaying that it has unsaved changes or
     * not.
     *
     * @type {boolean}
     * */
    get saved() {
        return this.hasAttribute('saved');
    }

    /* eslint-disable require-jsdoc */
    set saved(val) {
        if (val) {
            this.setAttribute('saved', '');
        } else {
            this.removeAttribute('saved');
        }
    }
    /* eslint-enable require-jsdoc */

    /**
     * Whether this tab is displaying that it is the currently selected
     * tab or not.
     *
     * @type {boolean}
     * */
    get selected() {
        return this.hasAttribute('selected');
    }

    /* eslint-disable require-jsdoc */
    set selected(val) {
        if (val) {
            this.setAttribute('selected', '');
        } else {
            this.removeAttribute('selected');
        }
    }
    /* eslint-enable require-jsdoc */
}
export default FileTabLable;
customElements.define('file-tab-lable', FileTabLable);
