import FileTabList from './model/file_tab_list.js';

import OpenFileButton from '../components/buttons/open_file_button.js';
import SaveButton from './view/save_button.js';
import PanelHead from '../components/panel_head.js';
import FileTabListView from './view/file_tab_list_view.js';
import EditorPanelBody from './view/editor_panel_body.js';

import FileBtnC from './controller/editor_file_button_controller.js';
import SaveBtnC from './controller/save_button_controller.js';
import TabListC from './controller/file_tab_list_controller.js';
import EditorC from './controller/editor_controller.js';
import FileTab from './model/file_tab.js';

/**
 * @namespace Panels.EditorPanel
 * */

/**
 * This is the main class for the editor panel.
 * It connects the panel's views and controllers to each other and to
 * the model.
 *
 * @memberof Panels.EditorPanel
 * */
class EditorPanel {
    /**
     * Builds a new EditorPanel.
     *
     * @param {HTMLElement} container - The controler element the panel
     * lives inside.
     * @param {Proxy.FileSystem} fs - The file system whos files will be
     * edited through this panel.
     * @param {Panels.FilePicker.FilePicker} filePicker - A reference to
     * this application's file picker panel. The editor panel uses this
     * to allow the user to open files to edit.
     * @param {string} welcomePath - A path to a file to display in the
     * editor on startup.
     * */
    constructor(container, fs, filePicker, welcomePath) {
        this.container = container;
        this.fs = fs;
        this.filePicker = filePicker;

        this.tabList = new FileTabList(this.fs);

        this.fileButton = new OpenFileButton();
        this.fileButton.title = 'open a file to edit';
        this.saveButton = new SaveButton();
        this.head = new PanelHead('Editor');
        this.tabListView = new FileTabListView(this.tabList);
        this.body = new EditorPanelBody(this.tabListView);

        const editorContainer = this.body.editorContainer;

        this.fileBtnC = new FileBtnC(
            this.fs,
            this.filePicker,
            this.tabList,
            this.fileButton,
        );
        this.tabListC = new TabListC(this.tabList, this.tabListView);
        this.editorC = new EditorC(this.fs, this.tabList, editorContainer);
        this.saveBtnC = new SaveBtnC(this.editorC, this.saveButton);

        this.head.appendAsButton(this.saveButton);
        this.head.appendAsButton(this.fileButton);
        this.container.appendChild(this.head.element);
        this.container.appendChild(this.body.element);

        const tab = new FileTab(this.fs, welcomePath);
        tab.reload();
        this.tabList.addTab(tab);
    }
}
export default EditorPanel;
