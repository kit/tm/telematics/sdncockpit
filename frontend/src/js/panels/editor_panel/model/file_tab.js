import ObservableMixin from '../../../util/observable.js';

import * as monaco from 'monaco-editor/esm/vs/editor/editor.api';

const pathlib = require('path');

// when introducing a new language here, also add it to LANG_TO_OPT!
const EXT_TO_LANG = {
    'py': 'python',
    'yml': 'yaml',
    'yaml': 'yaml',
};

const LANG_TO_OPT = {
    'python': {tabSize: 4},
    'yaml': {tabSize: 2},
};

/**
 * This class caches reads of a file's content from the server.
 *
 * @memberof Panels.EditorPanel
 * */
class RemoteFileReadCache {
    /**
     * Builds a new RemoteFileReadCache.
     *
     * @param {Proxy.FileSystem} fs - The file system on which the file
     * is stored.
     * @param {string} path - The cached file's path.
     * */
    constructor(fs, path) {
        this._value = null;
        this._valid = false;

        this.fs = fs;
        this._path = path;
    }

    /**
     * The opened file's path.
     *
     * @type {string}
     * */
    get path() {
        return this._path;
    }

    /* eslint-disable require-jsdoc */
    set path(val) {
        this._path = val;
        this.invalidate();
    }
    /* eslint-enable require-jsdoc */

    /**
     * Retrieves the content of the file from the cache, or the server
     * if the cache is invalid.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The file's content.
     * */
    async get_content() {
        if (!this._valid) {
            this._value = await this.fs.readFile(this.path);
            this._valid = true;
        }
        return this._value;
    }

    /**
     * Retrieves the content of the file from the cache.
     *
     * @return {string | null} The most recent cached content, or null
     * if no content has been cached yet.
     */
    get_cached() {
        return this._value;
    }

    /**
     * Whether the cache is valid or not.
     *
     * @readonly
     * @type {boolean}
     * */
    get valid() {
        return this._valid;
    }

    /**
     * Invalidate the cache.
     * */
    invalidate() {
        this._valid = false;
    }
}

/**
 * Represents a file opened in a tab of the editor. It links the
 * editor's model of the file's content with the file's content stored
 * on the server.
 *
 * You can register listeners on this file tab to be informed every
 * time the file's content in the editor's model or the file's content
 * on the server changes.
 *
 * @mixes Util.Observable
 * @memberof Panels.EditorPanel
 * */
class FileTab extends ObservableMixin(Object) {
    /**
     * Builds a new FileTab. Note, that the tab's monaco model will be
     * empty initially. To load the file's content into the tab from
     * the server, call reload().
     *
     * @param {Proxy.FileSystem} fs - The file system on which the file
     * is stored.
     * @param {string} path - The opened file's path.
     * */
    constructor(fs, path) {
        super();
        this.fs = fs;
        this._remote = new RemoteFileReadCache(fs, path);

        this._observer = () => this._invalidate();
        this.fs.register(this._observer);

        let language = 'plaintext';
        let opt = {};
        for (const ext in EXT_TO_LANG) {
            if (pathlib.extname(this.path) == ('.' + ext)) {
                language = EXT_TO_LANG[ext];
                opt = LANG_TO_OPT[language];
            }
        }

        this._monacoModel = monaco.editor.createModel('', language);
        this._monacoModel.updateOptions(opt);
        this._monacoModel.setEOL(monaco.editor.EndOfLineSequence.LF);
        this._monacoModel.onDidChangeContent(() => this._notifyAll());
    }

    /**
     * The model used by the editor to represent the current content of
     * the file in the editor.
     *
     * @readonly
     * @type {monaco.editor.ITextModel}
     * */
    get monacoModel() {
        return this._monacoModel;
    }

    /**
     * Replaces the internal monaco model's content with the content of
     * the file on the server.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     */
    async reload() {
        const content = await this._remote.get_content();
        this.monacoModel.setValue(content);
    }

    /**
     * The opened file's path.
     *
     * @type {string}
     * */
    get path() {
        return this._remote.path;
    }

    /* eslint-disable require-jsdoc */
    set path(val) {
        this._remote.path = val;
        this._notifyAll();
    }
    /* eslint-enable require-jsdoc */

    /**
     * The content of the file in the editor. Equivalent to the
     * underlying monaco model's getValue().
     *
     * @readonly
     * @type {string}
     * */
    get content() {
        // override EOL manually because preference setting with
        // ITextModel.setEOL() is broken. See:
        // https://github.com/microsoft/monaco-editor/issues/3440
        return this._monacoModel.getValue().replaceAll('\r\n', '\n');
    }

    /**
     * Checks whether the content in the editor and the file on the
     * server are in sync.
     *
     * @return {boolean}
     * */
    async isSaved() {
        const local = this.content;
        try {
            const remote = await this._remote.get_content();
            return local == remote;
        } catch (error) {
            return false;
        }
    }

    /**
     * Checks whether the content in the editor and the last known state
     * of the file on the server are in sync. This method should rarely
     * be used, prefer isSaved where possible.
     *
     * @return {boolean}
     */
    isSavedCached() {
        const local = this.content;
        try {
            const remote = this._remote.get_cached();
            return local == remote;
        } catch (error) {
            return false;
        }
    }

    /**
     * Invalidates the internal cache.
     * */
    _invalidate() {
        this._remote.invalidate();
        this._notifyAll();
    }

    /**
     * Call this before discarding the last reference to this object
     * to make sure it stops calling the server for updates about the
     * file.
     * */
    free() {
        this.fs.remove(this._observer);
    }
}
export default FileTab;
