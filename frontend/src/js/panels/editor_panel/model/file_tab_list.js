import ObservableMixin from '../../../util/observable.js';

/**
 * Thrown when a tab for a path is added to a tablist which already
 * has a tab for that path.
 *
 * @memberof Panels.EditorPanel
 */
class TabExistsError extends Error {
    /**
     * Builds a new TabExistsError.
     *
     * @param {string} message
     * @param  {...any} args
     */
    constructor(message='', ...args) {
        super(message, args);
        this.name = 'TabExistsError';
    }
}
export {TabExistsError};

/**
 * This class contains the definitive list of all files currently opened
 * as tabs of the editor.
 *
 * You can register listeners on this tab list to be informed of every
 * time a tab is added to or removed from the list. You will also be
 * informed every time the selected tab (the one intended to be
 * currently visible in the editor) is changed.
 *
 * @mixes Util.Observable
 * @memberof Panels.EditorPanel
 * */
class FileTabList extends ObservableMixin(Array) {
    /**
     * Builds a new FileTabList.
     *
     * @param {Proxy.FileSystem} fs - The file system who's files are
     * being opened in the editor.
     * */
    constructor(fs) {
        super();
        this.fs = fs;
        this._tabs = [];
        this._selected = null;
    }

    /**
     * The file tab that is intended to be visible in the editor
     * right now.
     *
     * @type {Panels.EditorPanel.FileTab | null}
     * */
    get selected() {
        return this._selected;
    }

    /**
     * Adds a new tab to the tab list and selects it.
     *
     * @param {Panels.EditorPanel.FileTab} tab - The tab to be added.
     *
     * @throws {Panels.EditorPanel.TabExistsError} If this tab list
     * already has a tab for the given tab's path.
     */
    addTab(tab) {
        if (this.getTab(tab.path) != null) {
            throw new TabExistsError();
        }

        this._tabs.push(tab);
        this._selected = tab;
        this._notifyAll();
    }

    /**
     * Remove a tab from the tab list.
     *
     * @param {Panels.EditorPanel.FileTab} tab - The tab to be removed.
     * The removed tab's free() method will be called by this method.
     * */
    removeTab(tab) {
        this._tabs = this._tabs.filter((_tab) => _tab !== tab);
        if (this._selected === tab) {
            this._selected = null;
        }
        this._notifyAll();
        tab.free();
    }

    /**
     * Makes the given tab the selected tab, if that tab is in this tab
     * list.
     *
     * @param {Panels.EditorPanel.FileTab | null} tab - The tab to be
     * selected.
     * */
    selectTab(tab) {
        if (this._tabs.includes(tab) || tab == null) {
            this._selected = tab;
            this._notifyAll();
        }
    }

    /**
     * Finds a tab in this tab list by its file's path.
     *
     * @param {string} path - The path of the file whos tab we want to
     * find.
     *
     * @return {Panels.EditorPanel.FileTab | null}
     * */
    getTab(path) {
        return this._tabs.find((tab) => tab.path == path);
    }

    /**
     * Checks if the given tab is in this tab list.
     *
     * @param {Panels.EditorPanel.FileTab} tab - The tab to be searched
     * for.
     *
     * @return {boolean}
     * */
    hasTab(tab) {
        return this.getTab(tab.path) === tab;
    }

    /**
     * Get the list of tabs in this tab list as an array for easy
     * iteration. Deleting or adding items in the array will not change
     * this tab list.
     *
     * @return {Panels.EditorPanel.FileTab[]} The list.
     * */
    asArray() {
        return [...this._tabs];
    }
}
export default FileTabList;
