import SaveFileDialogue from '../../../dialogues/save_file_dialogue.js';

import * as monaco from 'monaco-editor/esm/vs/editor/editor.api';

/**
 * The controller for the monaco editor instance that is part of the
 * editor panel. Creates a monaco instance inside the provided
 * container.
 *
 * The controller will read which tab to display in the editor instance
 * from the provided file tab list and swap tabs in and out of the
 * editor accordingly.
 *
 * Allows users to save the file shown in the editor via ctrl+s.
 *
 * @memberof Panels.EditorPanel
 * */
class EditorController {
    /**
     * Builds a new EditorController.
     *
     * @param {Proxy.FileSystem} fs - The file system to save changed
     * files to.
     * @param {Panels.EditorPanel.FileTabList} fileTabList - The tab
     * list whos selected tab the editor instance controlled by this
     * class will reflect.
     * @param {HTMLElement} container - An element to create the monaco
     * instance in.
     * */
    constructor(fs, fileTabList, container) {
        this.fs = fs;
        this.fileTabList = fileTabList;
        this.container = container;

        this.instance = monaco.editor.create(this.container, {
            theme: 'vs-dark',
            language: 'plaintext',
            automaticLayout: true,
        });
        this.instance.setModel(null);

        this.instance.addCommand(
            monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyS, () => {
                this.saveActive();
            },
        );

        this.fileTabList.register(() => this._onListChange());

        this.tab_to_view_state = new Map();

        window.onbeforeunload = () => {
            const notSaved = (tab) => !tab.isSavedCached();
            if (this.fileTabList.asArray().some(notSaved)) {
                return 'There are unsaved changes in the editor!';
            }
        };

        this._active = null;
    }

    /**
     * Handles changes to the underlying list.
     * */
    async _onListChange() {
        // remove obsolete view mappings
        for (const key of this.tab_to_view_state.keys()) {
            if (key !== this.fileTabList.getTab(key.path)) {
                this.tab_to_view_state.delete(key);
            }
        }

        // swap to new selected tab
        const selected = this.fileTabList.selected;
        if (this._active !== selected) {
            // deactivate active
            if (this._active) {
                const vs = this.instance.saveViewState();
                this.tab_to_view_state.set(this._active, vs);
            }

            // make selected active
            if (selected) {
                // set model
                this.instance.setModel(selected.monacoModel);

                // recover view state
                const vs = this.tab_to_view_state.get(selected);
                if (vs) {
                    this.instance.restoreViewState(vs);
                }

                // finish up
                this._active = selected;
                this.instance.focus();
            } else {
                this.instance.setModel(null);
                this._active = null;
            }
        }
    }

    /**
     * Saves the active tab.
     * */
    saveActive() {
        if (this._active) {
            const dialogue = new SaveFileDialogue();
            dialogue.start(this.fs, this._active.path, this._active.content);
        }
    }
}
export default EditorController;
