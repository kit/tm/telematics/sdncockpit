import CloseFileDialogue from '../../../dialogues/close_file_dialogue.js';

/**
 * Controller for a file tab list view.
 *
 * @memberof Panels.EditorPanel
 * */
class FileTabListController {
    /**
     * Builds a FileTabListController.
     *
     * @param {Panels.EditorPanel.FileTabList} fileTabList - The tab
     * list this controller will interact with for closing and selecting
     * of tabs. Must be the same tab list that the given view is
     * connected to.
     * @param {Panels.EditorPanel.FileTabListView} view - The tab list
     * view this controller will control and handle user interactions
     * for.
     * */
    constructor(fileTabList, view) {
        this.fileTabList = fileTabList;
        this.view = view;

        this.view.bindTabClose((fileTab) => this._onTabClose(fileTab));
        this.view.bindTabSelect((fileTab) => this._onTabSelect(fileTab));
    }

    /**
     * Handles a click in the tab list to close a tab.
     *
     * @param {Panels.EditorPanel.FileTab} fileTab - The clicked tab.
     * */
    _onTabClose(fileTab) {
        const dialogue = new CloseFileDialogue();
        fileTab.isSaved().then((isSaved) => {
            if (isSaved || dialogue.start()) {
                this.fileTabList.removeTab(fileTab);
            }
        });
    }

    /**
     * Handles a click in the tab list to select a tab.
     *
     * @param {Panels.EditorPanel.FileTab} fileTab - The clicked tab.
     * */
    _onTabSelect(fileTab) {
        this.fileTabList.selectTab(fileTab);
    }
}
export default FileTabListController;
