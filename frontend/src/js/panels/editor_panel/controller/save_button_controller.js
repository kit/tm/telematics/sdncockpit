
/**
 * A controller for the save button.
 *
 * @memberof Panels.EditorPanel
 */
class SaveButtonController {
    /**
     * Builds a new SaveButtonController.
     *
     * @param {Panels.EditorPanel.EditorController} ctrl - The main
     * controller of the editor panel.
     * @param {Panels.EditorPanel.SaveButton} button - The controlled
     * view.
     */
    constructor(ctrl, button) {
        this.ctrl = ctrl;
        this.button = button;

        this.button.onclick = () => this.ctrl.saveActive();
    }
}
export default SaveButtonController;
