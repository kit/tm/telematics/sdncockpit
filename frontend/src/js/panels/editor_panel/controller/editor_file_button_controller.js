import OpenFileDialogue from '../../../dialogues/open_file_dialogue.js';

/**
 * Controller for the editor panel's file select button.
 *
 * @memberof Panels.EditorPanel
 * */
class EditorFileButtonController {
    /**
     * Builds a new EditorFileButtonController.
     *
     * @param {Proxy.FileSystem} fs - The file system on which files
     * to edit are stored. Should be the same file system as the one
     * the file picker operates on.
     * @param {Panels.FilePicker.FilePicker} filePicker - The file
     * picker which will be opened to allow the user to select a file
     * whenever the button is clicked.
     * @param {Panels.EditorPanel.FileTabList} fileTabList - The list of
     * files opened in the editor. This button will add new files
     * selected by the user to that list.
     * @param {Panels.Components.OpenFileButton} button - The button to
     * control.
     * */
    constructor(fs, filePicker, fileTabList, button) {
        this.fs = fs;
        this.filePicker = filePicker;
        this.fileTabList = fileTabList;
        this.button = button;

        this.button.onclick = () => {
            this.filePicker.open('Open a File', (path) => {
                this.filePicker.close();
                const dialogue = new OpenFileDialogue();
                dialogue.start(this.fs, path)
                    .then((tab) => {
                        this.fileTabList.addTab(tab);
                    })
                    .catch(() => {});
            });
        };
    }
}
export default EditorFileButtonController;
