const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: inline-block;
    width: 100%;
    white-space: nowrap;
}
</style>
<slot></slot>`;

/**
 * A custom HTML element to lable a
 * {@Link Panels.Components.TreeViewItem}.
 *
 * Name of element: 'tree-view-lable'.
 *
 * Should only be used as a direct child of a
 * {@Link Panels.Components.TreeViewItem}.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class TreeViewLable extends HTMLElement {
    /**
     * Builds a new TreeViewLable.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }
}
export default TreeViewLable;
customElements.define('tree-view-lable', TreeViewLable);
