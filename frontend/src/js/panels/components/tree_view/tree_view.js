const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: block;
    width: 100%;
}
</style>
<slot></slot>`;

/**
 * A custom HTML element to form the base of a tree view.
 *
 * Name of element: 'tree-view'.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class TreeView extends HTMLElement {
    /**
     * Builds a new TreeView.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    /**
     * All currently selected tree view items inside of this element.
     * @type {NodeList}
     * */
    get selected() {
        return this.querySelectorAll('tree-view-item[selected]');
    }
}
export default TreeView;
customElements.define('tree-view', TreeView);
