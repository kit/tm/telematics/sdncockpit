const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: block;
    padding-left: 1em;
}
</style>
<slot></slot>`;

/**
 * A custom HTML element to encapsulate a list of
 * {@Link Panels.Components.TreeViewItem} as part of a
 * {@Link Panels.Components.TreeView}.
 *
 * Name of element: 'tree-view-branch-list'.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class TreeViewBranchList extends HTMLElement {
    /**
     * Builds a new TreeViewBranchList.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }
}
export default TreeViewBranchList;
customElements.define('tree-view-branch-list', TreeViewBranchList);
