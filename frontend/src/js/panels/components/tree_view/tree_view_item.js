const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: block;
}
</style>
<slot></slot>`;

/**
 * A custom HTML element. An item of a
 * {@Link Panels.Components.TreeView}. Should be used inside a
 * {@Link Panels.Components.TreeViewBranchList}.
 *
 * Name of element: 'tree-view-item'.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class TreeViewItem extends HTMLElement {
    /**
     * Builds a new TreeViewItem.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    /**
     * Selects this item.
     * */
    select() {
        this.selected = true;
    }

    /**
     * Unselects this item.
     * */
    unselect() {
        this.selected = false;
    }

    /**
     * Signals whether this item is selected.
     * @type {boolean}
     * */
    get selected() {
        return this.hasAttribute('selected');
    }

    /* eslint-disable require-jsdoc */
    set selected(val) {
        if (val) {
            this.setAttribute('selected', '');
        } else {
            this.removeAttribute('selected');
        }
    }
    /* eslint-enable require-jsdoc */

    /**
     * Expands the list of child items.
     * */
    expand() {
        this.expanded = true;
    }

    /**
     * Collapses the list of child items.
     * */
    collapse() {
        this.expanded = false;
    }

    /**
     * Toggles between expanded and not expanded.
     * */
    toggle() {
        this.expanded = !this.expanded;
    }

    /**
     * Signals whether the list of child items of this item is expanded.
     * @type {boolean}
     * */
    get expanded() {
        return this.hasAttribute('expanded');
    }

    /* eslint-disable require-jsdoc */
    set expanded(val) {
        if (val) {
            this.setAttribute('expanded', '');
        } else {
            this.removeAttribute('expanded');
        }
    }
    /* eslint-enable require-jsdoc */
}
export default TreeViewItem;
customElements.define('tree-view-item', TreeViewItem);
