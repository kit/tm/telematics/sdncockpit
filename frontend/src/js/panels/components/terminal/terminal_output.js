import AutoScroller from '../auto_scroller';

const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    width: 100%;
    height: 100%;
}

div {
    width: calc(100% - 1ch);
    height: 100%;
    overflow-y: scroll;

    padding-left: 1ch;

    font-family: Consolas, "Courier New", monospace;
    font-feature-settings: "liga" 0, "calt" 0;
    font-size: 0.9em;
    font-weight: 400;
    letter-spacing: normal;
    line-height: 1.1em;
    text-size-adjust: 100%;
}

p {
    margin: 0;
    padding: 0;
    white-space: pre-wrap;
}
</style>`;

/**
 * A custom HTML element with the appearance and behaviour of a terminal
 * output.
 *
 * Name of element: 'terminal-output'.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class TerminalOutput extends HTMLElement {
    /**
     * Builds a new TerminalOutput.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this.wrapper = document.createElement('div');
        this.shadowRoot.appendChild(this.wrapper);

        this.cursor = '>';
        this.indent = 4;

        this.newLine = true;

        this.autoScroller = new AutoScroller(this.wrapper);
    }

    /**
     * Create a new paragraph in the wrapper.
     *
     * @return {HTMLElement} the paragraph.
     * */
    _beginLine() {
        const p = document.createElement('p');
        this.wrapper.appendChild(p);
        this._appendToLine(this.cursor + ' ');
        return p;
    }

    /**
     * Append to the current last paragraph in the wrapper.
     *
     * @param {string} str - The text to append.
     * */
    _appendToLine(str) {
        const p = this.wrapper.lastElementChild;
        p.textContent += str;
    }

    /**
     * Print the given string, on a new line if this.newline demands.
     *
     * @param {string} str - The text to print.
     * */
    _printNoInterpret(str) {
        if (this.newLine) {
            this._beginLine();
            this.newLine = false;
        }
        this._appendToLine(str);
        this.autoScroller.autoscroll();
    }

    /**
     * Prints the given string to the terminal.
     * \n, \r and \r\n are all correctly understood.
     * \t is replaced with this.indent number of spaces (default of 4).
     *
     * @param {string} str - Thre string to print.
     * */
    print(str) {
        const tRe = /(?:\t)/g;
        const rnRe = /(?:\r\n|\r|\n)/g;
        const nRe = /(?:\n)/g;

        str.replaceAll(tRe, ' '.repeat(this.indent));
        str.replaceAll(rnRe, '\n');

        let idx = str.search(nRe);
        while (idx != -1) {
            const pre = str.slice(0, idx);
            this._printNoInterpret(pre);
            this.newLine = true;

            str = str.slice(idx + 1);
            idx = str.search(nRe);
        }

        if (str.length > 0) {
            this._printNoInterpret(str);
        }
    }

    /**
     * Prints the given string on its own line.
     *
     * @param {string} line - The string to print.
     * */
    printLine(line) {
        this.newLine = true;
        this._printNoInterpret(line);
        this.newLine = true;
    }

    /**
     * Removes all previously printed output.
     * */
    clear() {
        this.wrapper.innerHTML = '';
    }
}
export default {TerminalOutput};
customElements.define('terminal-output', TerminalOutput);
