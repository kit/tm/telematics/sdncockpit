import IconButton from './icon_button.js';

/**
 * A {@link Panels.Components.IconButton} with a default appearance
 * suitable for a 'close' button.
 *
 * @extends Panels.Components.IconButton
 * @memberof Panels.Components
 * */
class CloseButton extends IconButton {
    /**
     * Builds a new CloseButton.
     *
     * @param {string[]} iconTypes - A list of FontAwesome icon classes,
     * e.g. ["fa-folder"]. Will be applied to the button's icon.
     * */
    constructor(iconTypes = ['fa-xmark'], lable = "close") {
        super(iconTypes, lable);
    }
}
export default CloseButton;
