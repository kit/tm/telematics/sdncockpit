import StateIconButton, {ButtonState} from './state_icon_button.js';

/**
 * This enum defines the possible states of a StartStopButton.
 *
 * @readonly
 * @enum {string}
 * @memberof Panels.Components
 * */
const SSBState = {
    idle: 'idle',
    start: 'start',
    stop: 'stop',
};
export {SSBState};

/**
 * A StartStopButton is a StateIconButton with three predefined states:
 * idle, start and stop. Their keys are defined in
 * {@link Panels.Components.SSBState}.
 * These buttons can be used to start some stopped process or to stop
 * a running one. They can also indicate that the application is waiting
 * for the process to change its state.
 *
 * @extends Panels.Components.StateIconButton
 * @memberof Panels.Components
 * */
class StartStopButton extends StateIconButton {
    /**
     * Produce a new StartStopButton.
     *
     * @param {string} idleTitle - The button's title while it is in
     * the idle state.
     * @param {string} startTitle - The button's title while it is in
     * the start state.
     * @param {string} stopTitle - The button's title while it is in
     * the stop state.
     * @param {string[]} startIconTypes - A list of FontAwesome icon
     * classes, e.g. ["fa-folder"].
     * Will be applied to the button's icon when in start state.
     * @param {string} idleLable - The button's lable while it is in
     * the idle state.
     * @param {string} startLable - The button's lable while it is in
     * the start state.
     * @param {string} stopLable - The button's lable while it is in
     * the stop state.
     */
    constructor(
        idleTitle,
        startTitle,
        stopTitle,
        startIconTypes = [],
        idleLable,
        startLable,
        stopLable
    ) {
        const states = {};
        states[SSBState.idle] = new ButtonState(
            ['fa-spinner', 'fa-pulse'],
            () => {},
            idleTitle,
            idleLable,
        );
        states[SSBState.start] = new ButtonState(
            startIconTypes.length == 0 ? ['fa-play'] : startIconTypes,
            () => {},
            startTitle,
            startLable,
        );
        states[SSBState.stop] = new ButtonState(
            ['fa-pause'],
            () => {},
            stopTitle,
            stopLable,
        );
        super(states, SSBState.start);
    }
}
export default StartStopButton;
