import View from '../../view.js';

const DEFAULT_CLASSES = ['fa', 'fa-fw', 'fa-solid'];

/**
 * An IconButton is a view that contains only a button with a
 * FontAwesome icon.
 *
 * @extends Panels.View
 * @memberof Panels.Components
 * */
class IconButton extends View {
    #lable

    /**
     * Builds a new IconButton.
     *
     * @param {string[]} iconTypes - A list of FontAwesome icon classes,
     * e.g. ["fa-folder"]. Will be applied to the button's icon.
     * */
    constructor(iconTypes = [], lable = "") {
        super();
        this._button = document.createElement('button');
        this._icon = document.createElement('i');
        this._icon.classList.add(...DEFAULT_CLASSES);
        this._button.appendChild(this._icon);
        this.#lable = document.createElement('span');
        this.#lable.innerText = lable;
        this._button.appendChild(this.#lable);

        this.iconTypes = iconTypes;
    }

    /** @inheritdoc */
    get element() {
        return this._button;
    }

    /**
     * A list of FontAwesome icon classes, e.g. ["fa-folder"].
     * Will be applied to the button's icon.
     * */
    get iconTypes() {
        const classes = [...this._icon.classList];
        return classes.filter( (elem) => {
            return !DEFAULT_CLASSES.includes(elem);
        });
    }

    /* eslint-disable require-jsdoc */
    set iconTypes(val) {
        this._icon.classList.remove(...this.iconTypes);
        this._icon.classList.add(...val);
    }
    /* eslint-enable require-jsdoc */

    /**
     * Same as any button's onclick.
     * @type {function}
     * */
    get onclick() {
        return this._button.onclick;
    }

    /* eslint-disable require-jsdoc */
    set onclick(val) {
        this._button.onclick = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * The button's lable.
     * @type {string}
     */
    get lable() {
        return this.#lable.innerText;
    }

    /* eslint-disable require-jsdoc */
    set lable(val) {
        this.#lable.innerText = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * Same as any button's title.
     * @type {string}
     * */
    get title() {
        return this._button.title;
    }

    /* eslint-disable require-jsdoc */
    set title(val) {
        if (!val) {
            this._button.removeAttribute(this.title);
        }
        this._button.title = val;
    }
    /* eslint-enable require-jsdoc */
}
export default IconButton;
