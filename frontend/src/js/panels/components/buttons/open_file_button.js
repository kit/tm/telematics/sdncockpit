import IconButton from './icon_button.js';

/**
 * A {@link Panels.IconButton} with a default appearance suitable for an
 * 'open file' button.
 *
 * @extends Panels.Components.IconButton
 * @memberof Panels.Components
 * */
class OpenFileButton extends IconButton {
    /**
     * Builds a new OpenFileButton.
     *
     * @param {string[]} iconTypes - A list of FontAwesome icon classes,
     * e.g. ["fa-folder"]. Will be applied to the button's icon.
     * */
    constructor(iconTypes = ['fa-folder-open'], lable = "open file") {
        super(iconTypes, lable);
    }
}
export default OpenFileButton;
