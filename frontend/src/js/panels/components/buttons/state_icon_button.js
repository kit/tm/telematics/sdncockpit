import View from '../../view.js';

import IconButton from './icon_button.js';

/**
 * The state of an IconButton.
 * Each property of this object corresponds to the same property on an
 * {@link Panels.Components.IconButton}. When this state is the active
 * state of a StateIconButton, this state's properties are copied to the
 * StateIconButton's underlying IconButton.
 *
 * @memberof Panels.Components
 * */
class ButtonState {
    /**
     * Builds a new ButtonState.
     *
     * @param {string[]} iconTypes - The button's iconTypes while in
     * this state.
     * @param {function} onclick - The button's onclick while in this
     * state.
     * @param {string} title - The button's title while in this state.
     * @param {string} lable - The button's lable while in this state.
     * */
    constructor(iconTypes, onclick = () => {}, title = '', lable = '') {
        this.iconTypes = iconTypes;
        this.onclick = onclick;
        this.title = title;
        this.lable = lable;
    }
}
export {ButtonState};

/**
 * A StateIconButton is a button that can switch appearance and behavior
 * based on what state it is in.
 * Underneath, it is an IconButton which is reconfigured on each state
 * change. Each state must therefore define some iconTypes, an onclick
 * function and a title to configure the IconButton with.
 * States must be defined during construction, but to be in line with
 * most other views, the onclick functions of states can be bound to
 * later on.
 *
 * See also: {@link Panels.Components.IconButton}.
 *
 * @extends Panels.View
 * @memberof Panels.Components
 * */
class StateIconButton extends View {
    /**
     * Builds a new StateIconButton.
     *
     * @param {Object.<string, Panels.Components.ButtonState>} states -
     * The possible states of this button as a key, value object.
     * The keys are used to refer to the states later on.
     * @param {string} active - The key of the first state to be
     * activated.
     * */
    constructor(states, active) {
        super();
        this._button = new IconButton();

        this.states = states;
        this.active = active;
    }

    /** @inheritdoc */
    get element() { return this._button.element; }

    /**
     * The key of the active state.
     * */
    get active() {
        return this._active;
    }

    /* eslint-disable require-jsdoc */
    set active(key) {
        if (key in this.states) {
            this._active = key;
            this._button.iconTypes = this.states[key].iconTypes;
            this._button.onclick = () => this.states[key].onclick();
            this._button.title = this.states[key].title;
            this._button.lable = this.states[key].lable;
        }
    }
    /* eslint-enable require-jsdoc */

    /**
     * Bind a new onclick function to a state.
     *
     * @param {string} key - The state's key.
     * @param {function} onclick - The onclick handler.
     */
    bindOnclick(key, onclick) {
        this.states[key].onclick = onclick;
    }
}
export default StateIconButton;
