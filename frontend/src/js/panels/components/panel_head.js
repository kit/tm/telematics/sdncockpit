import View from '../view.js';

/**
 * A view for a generic panel head with a title and a set of buttons.
 *
 * @extends Panels.View
 * @memberof Panels.Components
 * */
class PanelHead extends View {
    /**
     * Builds a new PanelHead.
     *
     * @param {string} title - The default title.
     * */
    constructor(title) {
        super();
        this._element = document.createElement('div');
        this._element.classList.add('panel-head');

        this._titleDiv = document.createElement('div');
        this._titleDiv.classList.add('panel-title');

        const buttonDiv = document.createElement('div');
        buttonDiv.classList.add('panel-buttons');

        this._buttonList = document.createElement('ul');
        buttonDiv.appendChild(this._buttonList);

        this._element.appendChild(this._titleDiv);
        this._element.appendChild(buttonDiv);

        this.title = title;
    }

    /** @inheritdoc */
    get element() {
        return this._element;
    }

    /**
     * The Panel's title.
     * */
    get title() {
        return this._titleDiv.innerText;
    }

    /* eslint-disable require-jsdoc */
    set title(val) {
        this._titleDiv.innerText = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * Append a {@Link Panels.View} into the buttons section of this
     * panel head.
     *
     * @param {Panels.View} view - The view to append.
     * */
    appendAsButton(view) {
        const li = document.createElement('li');
        li.appendChild(view.element);

        this._buttonList.appendChild(li);
    }
}
export default PanelHead;
