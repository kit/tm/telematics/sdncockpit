import './context_menu.js';

/**
 * A callback for when a menu item is clicked.
 *
 * @callback Panels.Components.ContextMenuBuilder~ClickCallback
 * @param {HTMLElement} target - The element which the context menu was
 * opened on.
 * */

/**
 * A helper to ease the construction of a fully filled
 * {@Link Panels.Components.ContextMenu}.
 *
 * @memberof Panels.Components
 * */
class ContextMenuBuilder {
    /**
     * Builds a new ContextMenuBuilder.
     *
     * @param {string} selector - The selector for the new
     * {@Link Panels.Components.ContextMenu}.
     * */
    constructor(selector) {
        this._element = document.createElement('context-menu');
        this._element.selector = selector;
    }

    /**
     * Add an item to the menu.
     *
     * @param {string} name - The displayed name of the menu item.
     * @param {Panels.Components.ContextMenuBuilder~ClickCallback}
     * callback - The function to be called when the menu item is
     * clicked.
     * */
    addItem(name, callback) {
        const button = document.createElement('button');
        button.innerText = name;

        button.onclick = () => callback(this._element.target);
        this._element.appendChild(button);
    }

    /**
     * Retrieve the newly constructed menu.
     *
     * @return {Panels.Components.ContextMenu}.
     * */
    get() {
        return this._element;
    }
}
export default ContextMenuBuilder;
