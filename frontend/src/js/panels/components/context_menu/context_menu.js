const template = document.createElement('template');
template.innerHTML = `
<style>
:host {
    display: none;
    background: none;
    color: inherit;
    border: none;
    font: inherit;
    cursor: pointer;
    outline: inherit;
    position: absolute;
}

:host([is-open]) {
    display: block;
}
</style>
<slot></slot>`;

/**
 * A custom HTML element with the internal structure of a context menu.
 *
 * Name of element: 'context-menu'.
 *
 * See also: {@Link Panels.Components.ContextMenuBuilder}.
 *
 * @property {HTMLELement} target - While the menu is open, this
 * property will hold the element that was clicked on to open the menu.
 *
 * @extends HTMLElement
 * @memberof Panels.Components
 * */
class ContextMenu extends HTMLElement {
    /**
     * Builds a new ContextMenu.
     * */
    constructor() {
        super();

        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this.target = null;
        this._selector = null;
    }

    /** Internal Method */
    connectedCallback() {
        document.addEventListener('contextmenu', (e) => {
            this.show(e);
        }, false);
        document.addEventListener('click', () => this.hide());
    }

    /**
     * A CSS selector describing which elements this context menu should
     * open up for when they are clicked.
     * @type {string}
     * */
    get selector() {
        return this._selector;
    }

    /* eslint-disable require-jsdoc */
    set selector(val) {
        this._selector = val;
    }
    /* eslint-enable require-jsdoc */

    /**
     * Show this context menu in response to a contextmenu event.
     *
     * @param {MouseEvent} event - The triggering event.
     * */
    show(event) {
        if (this._selector) {
            this.target = event.target.closest(this._selector);
        } else {
            this.target = null;
            this.hide();
        }

        if (this.target) {
            event.preventDefault();
            event.stopPropagation();

            this.style.zIndex = 100;
            this.style.left = `${event.pageX}px`;
            this.style.top = `${event.pageY}px`;

            this.is_open = true;
        } else {
            this.target = null;
            this.hide();
        }
    }

    /**
     * Hides this context menu.
     * */
    hide() {
        this.target = null;
        this.is_open = false;
    }

    /**
     * Signals whether this context menu is currently open.
     * */
    get is_open() {
        return this.hasAttribute('is-open');
    }

    /* eslint-disable require-jsdoc */
    set is_open(val) {
        if (val) {
            this.setAttribute('is-open', '');
        } else {
            this.removeAttribute('is-open');
        }
    }
    /* eslint-enable require-jsdoc */
}
export default ContextMenu;
customElements.define('context-menu', ContextMenu);
