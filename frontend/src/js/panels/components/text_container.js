import View from '../view.js';

/**
 * This view nicely displays text.
 *
 * @extends Panels.View
 * @memberof Panels.Components
 * */
class TextContainer extends View {
    /**
     * Builds a new TextContainer.
     * */
    constructor() {
        super();

        this._element = document.createElement('div');
        this._element.classList.add('text-container');
    }

    /**
     * Makes the given text appear in this panel. Replaces any existing
     * text. Interprets newlines and tabs.
     *
     * @param {string} text - The text to display.
     */
    setText(text) {
        text = text.replace(/(?:\r\n|\r|\n)/g, '<br>');
        text = text.replace(/(?:\t)/g, '    ');
        this._element.innerHTML = text;
    }
}
export default TextContainer;
