import View from '../view.js';

/**
 * A view for a generic bar of buttons to be placed as a row in a panel.
 *
 * @extends Panels.View
 * @memberof Panels.Components
 */
class ButtonBar extends View {
    /**
     * Builds a new, empty ButtonBar.
     */
    constructor() {
        super();
        this._element = document.createElement('div');
        this._element.classList.add('button-bar');

        this._buttonList = document.createElement('ul');
        this._element.appendChild(this._buttonList);
    }

    /** @inheritdoc */
    get element() {
        return this._element;
    }

    /**
     * Insert a {@Link Panels.View} into the button bar.
     *
     * @param {Panels.View} view - The view to append.
     */
    appendAsButton(view) {
        const li = document.createElement('li');
        li.appendChild(view.element);

        this._buttonList.appendChild(li);
    }
}
export default ButtonBar;
