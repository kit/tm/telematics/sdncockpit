/**
 * A helper class to scroll a container automatically.
 */
class AutoScroller {
    /**
     * Builds a new AutoScroller.
     *
     * @param {HTMLElement} container
     */
    constructor(container) {
        this._container = container;
        this._scroll = this._isAtBottom();
        this._scrolling = false;

        this._container.addEventListener('scroll', () => {
            if (! this._scrolling) {
                // the user scrolled, not us
                this._scroll = this._isAtBottom();
            }
        });
    }

    /**
     * Checks if the container is currently scrolled all the way to the
     * bottom.
     *
     * @return {boolean}
     */
    _isAtBottom() {
        const scroll = this._container.scrollTop + this._container.clientHeight;
        const maxScroll = this._container.scrollHeight;

        return maxScroll - scroll == 0;
    }

    /**
     * Autoscroll the container, unless the user scrolled away from the
     * bottom of the container. This should be called after every
     * modification to the container's content.
     */
    autoscroll() {
        this._scrolling = true;
        if (this._scroll) {
            this._container.scrollTo({
                top: this._container.scrollHeight,
                left: 0,
                behavior: 'smooth',
            });
        }
        this._scrolling = false;
    }
}
export default AutoScroller;
