import ObservableMixin from '../util/observable.js';

import {NetworkStoppedError, UnknownActionError}
    from '../api/generated/errorcodes.js';
import {Actioncodes} from '../api/generated/actioncodes.js';

/**
 * Keeps in sync with the server's IONet while accounting for
 * missmatches in the server's and the client's state.
 *
 * @memberof Proxy
 * @mixes Util.Observable
 * */
class IONet extends ObservableMixin(Object) {
    /**
     * Creates an IONet.
     *
     * @param {API.Connection} connection - The connection through which
     * this FileSystem may communicate with the server.
     * */
    constructor(connection) {
        super();
        this._connection = connection;
        this._connection.register(() => this._notifyAll());
        this._connection.registerProgressReportHandler((report) => {
            this.#handleReport(report);
        });

        this._isRunning = false;
        this._isActionRunning = {};
        this._pending = 0;
        this._actionPending = {};
        this._isUpdating = false;

        for (const key in Actioncodes) {
            if (Object.hasOwnProperty.call(Actioncodes, key)) {
                this._isActionRunning[Actioncodes[key]] = false;
                this._actionPending[Actioncodes[key]] = 0;
            }
        }
    }

    /**
     * Whether we are connected to the server.
     *
     * @type {boolean}
     *
     * @readonly
     * */
    get connected() {
        return this._connection.connected;
    }

    /**
     * Handle reports coming in from the server.
     */
    #handleReport(report) {
        const type = report.type;

        switch(type) {
            case 'network_status': {
                this._isRunning = report.network_running;
                for (const action_type in report.actions_running) {
                    this._isActionRunning[action_type] =
                        report.actions_running[action_type];
                }
                this._notifyAll();
                break;
            }
            case 'start_emulator': {
                this._isRunning = true;
                this._notifyAll();
                break;
            }
            case 'stop_emulator': {
                this._isRunning = false;
                this._notifyAll();
                break;
            }
            case 'start_network_action': {
                this._isActionRunning[report.action_type] = true;
                this._notifyAll();
                break;
            }
            case 'stop_network_action':
            case 'abort_network_action':
            case 'complete_network_action': {
                this._isActionRunning[report.action_type] = false;
                this._notifyAll();
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Register a handler for network progress reports.
     *
     * @param {*} handler
     */
    registerProgressReportHandler(handler) {
        this._connection.registerProgressReportHandler(handler);
    }

    /**
     * Starts a network on the server.
     *
     * @param {string} scenarioPath - The virtual file system path of
     * the scenario file to be used to create the network.
     * @param {controller} controllerPath - The virtual file system
     * path of the controller's source file to be used to create the
     * network's controller.
     *
     * @throws {NetworkRunningError}
     * @throws {FileNotFoundError}
     * @throws {ScenarioFileSyntaxError}
     * @throws {ScenarioFileSemanticError}
     * @throws {ControllerConnectTimeoutError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async start(scenarioPath, controllerPath) {
        this._pending += 1;
        this._notifyAll();

        try {
            await this._connection.start(scenarioPath, controllerPath);
            this._isRunning = true;
        } finally {
            this._pending -= 1;
            this._notifyAll();
        }
    }

    /**
     * Stops the network on the server.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async stop() {
        this._pending += 1;
        this._notifyAll();

        try {
            await this._connection.stop();
            this._isRunning = false;
            this._isEvalRunning = false;
        } finally {
            this._pending -=1;
            this._notifyAll();
        }
    }

    /**
     * Checks if the network on the server is running.
     * May return a stale value.
     *
     * @return {boolean}
     * */
    async isRunning() {
        return this._isRunning;
    }

    /**
     * Checks if any changes to the server's network state have been
     * requested and currently remain unanswered.
     *
     * @return {boolean}
     */
    isChangePending() {
        return this._pending > 0;
    }

    /**
     * Starts the specified network action in the server's network.
     *
     * @param {string} name - The name of the action.
     *
     * @throws {UnknownActionError}
     * @throws {NetworkStoppedError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async startAction(name) {
        if (!Object.values(Actioncodes).includes(name)) {
            throw new UnknownActionError();
        }

        this._evalPending += 1;
        this._notifyAll();

        try {
            await this._connection.startAction(name);
            this._isActionRunning[name] = true;
        } catch (error) {
            if (error instanceof NetworkStoppedError) {
                this._isRunning = false;
                this._isActionRunning[name] = false;
            }
            throw error;
        } finally {
            this._actionPending[name] -= 1;
            this._notifyAll();
        }
    }

    /**
     * Stops the specified network action running in the server's
     * network.
     *
     * @param {string} name - The name of the action.
     *
     * @throws {UnknownActionError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async stopAction(name) {
        if (!Object.values(Actioncodes).includes(name)) {
            throw new UnknownActionError();
        }

        this._actionPending[name] += 1;
        this._notifyAll();

        try {
            await this._connection.stopAction(name);
            this._isActionRunning[name] = false;
        } finally {
            this._actionPending[name] -= 1;
            this._notifyAll();
        }
    }

    /**
     * Checks if the specified network action is running in the server's
     * network.
     * May return a stale value.
     *
     * @param {string} name - The name of the action.
     *
     * @return {boolean} True if the action is running, false
     * otherwise.
     *
     * @throws {UnknownActionError}
     * */
    async isActionRunning(name) {
        if (!Object.values(Actioncodes).includes(name)) {
            throw new UnknownActionError();
        }
        return this._isRunning && this._isActionRunning[name];
    }

    /**
     * Checks if the server has been sent a request to start/stop a
     * network action that currently remains unanswered.
     *
     * @param {string} name - The name of the action.
     *
     * @return {boolean}
     *
     * @throws {UnknownActionError}
     * */
    isActionChangePending(name) {
        if (!Object.values(Actioncodes).includes(name)) {
            throw new UnknownActionError();
        }

        return this._actionPending[name] > 0;
    }

    /**
     * Retrieve the table of flow tables from the server.
     *
     * @throws {NetworkStoppedError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {FlowTables}
     */
    async flowTables() {
        return this._connection.flowTables();
    }
}
export default IONet;
