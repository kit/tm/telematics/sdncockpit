import ObservableMixin from '../util/observable.js';
import * as errc from '../api/generated/errorcodes.js';

/**
 * A mirror of the server's file system.
 *
 * @memberof Proxy
 * @mixes Util.Observable
 * */
class FileSystem extends ObservableMixin(Object) {
    /**
     * Creates a FileSystem.
     *
     * @param {API.Connection} connection - The connection through which
     * this FileSystem may communicate with the server.
     * */
    constructor(connection) {
        super();
        this._connection = connection;
        this._connection.register(() => this._notifyAll());
        this._connection.registerFileSystemUpdateHandler((path) => {
            console.log(path);
            this._notifyAll();
        });
    }

    /**
     * Whether we are connected to the server.
     *
     * @type {boolean}
     *
     * @readonly
     * */
    get connected() {
        return this._connection.connected;
    }

    /**
     * Creates a new directory at the given path.
     *
     * @param {string} path - The new directory's path.
     *
     * @throws {FileExistsError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mkdir(path) {
        await this._connection.mkdir(path);
        this._notifyAll();
    }

    /**
     * Creates a new file at the given path.
     *
     * @param {string} path - The new file's path.
     *
     * @throws {FileExistsError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mkfile(path) {
        await this._connection.mkfile(path);
        this._notifyAll();
    }

    /**
     * Reads a the contents of the directory at the given path.
     *
     * @param {string} path - The directory's path.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {API.DirectoryEntry[]} The contents of the directory.
     * */
    async readDir(path) {
        return this._connection.readDir(path);
    }

    /**
     * Reads the file at the given path.
     *
     * @param {string} path - The file's path.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The content of the file.
     * */
    async readFile(path) {
        return this._connection.readFile(path);
    }

    /**
     * Writes the given content to the file at the given path.
     *
     * @param {string} path - The file's path.
     * @param {string} content - The file's content.
     *
     * @throws {NotADirectoryError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async writeFile(path, content) {
        await this._connection.writeFile(path, content);
        this._notifyAll();
    }

    /**
     * Deletes the directory at the given path.
     *
     * @param {string} path - The directory's path.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async rmdir(path) {
        await this._connection.rmdir(path);
        this._notifyAll();
    }

    /**
     * Deletes the file at the given path.
     *
     * @param {string} path - The file's path.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async rmfile(path) {
        await this._connection.rmfile(path);
        this._notifyAll();
    }

    /**
     * Recursively copies a directory to another location.
     *
     * @param {string} src - The path of the directory which will be
     * copied.
     * @param {string} dst - The path the directory at src will be
     * copied to.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async cpdir(src, dst) {
        await this._connection.cpdir(src, dst);
        this._notifyAll();
    }

    /**
     * Copies a file to another location.
     *
     * @param {string} src - The path of the file which will be copied.
     * @param {string} dst - The path the file at src will be copied to.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileExistsError}
     * @throws {SameFileError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async cpfile(src, dst) {
        await this._connection.cpfile(src, dst);
        this._notifyAll();
    }

    /**
     * Recursively moves a directory to another location.
     *
     * @param {string} src - The path of the directory which will be
     * moved.
     * @param {string} dst - The path the directory at src will be
     * moved to.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mvdir(src, dst) {
        await this._connection.cpdir(src, dst);
        await this._connection.rmdir(src);
        this._notifyAll();
    }

    /**
     * Moves a file to another location.
     *
     * @param {string} src - The path of the file which will be moved.
     * @param {string} dst - The path the file at src will be moved to.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mvfile(src, dst) {
        try {
            await this._connection.cpfile(src, dst);
            await this._connection.rmfile(src);
        } catch (error) {
            if (error instanceof errc.SameFileError) {
                // ignore
            } else {
                throw error;
            }
        }

        this._notifyAll();
    }
}
export default FileSystem;
