/**
 * @namespace FlowTables
 */

import {APIErrorConverter} from './api/generated/errorcodes.js';
import API from './api/api.js';

import FlowTablePanel from './panels/flow_table_panel/flow_table_panel.js';

/**
 * Main class for flow tables page.
 */
class FlowTables {
    /**
     * Constructor.
     */
    constructor() {
        const configure_proxy = () => {
            fetch('./proxy_port.json').then(response => {
                response.json().then(proxy_port => {
                    this.proxy_port = proxy_port.proxy_port;
                    configure_core();
                });
            });
        }
        const configure_core = () => {
            const url = new URL('./core_port.json', window.location.href);
            url.port = this.proxy_port;
            fetch(url.href).then(response => {
                response.json().then(core_port => {
                    this.core_port = core_port.core_port;
                    this.build();
                });
            })
        }
        configure_proxy();
    }

    build() {
        this.apiErrorConverter = new APIErrorConverter();
        this.api = new API(
            this.apiErrorConverter, this.proxy_port, this.core_port
        );

        const container = document.getElementById('content');
        this.flowTablePanel = new FlowTablePanel(container, this.api);
    }
}
document.addEventListener(
    'DOMContentLoaded',
    () => window.ft = new FlowTables(),
);
