import DisconnectedDialogue from './dialogues/disconnected_dialogue';

/**
 * Monitors a connection for disconnects and informs the user when they
 * occur.
 *
 * @memberof Main
 */
class ConnectionMonitor {
    #connected;
    #connection;

    /**
     * Builds a new ConnectionMonitor.
     *
     * @param {API.Connection} connection - The connection to monitor.
     */
    constructor(connection) {
        this.#connected = false;
        this.#connection = connection;
        this.#connection.register(() => {
            if (!this.#connection.connected && this.#connected) {
                this.#show_disconnect();
            }
            this.#connected = this.#connection.connected;
        });
    }

    /**
     * Show a message to the user to inform them that we are currently
     * disconnected from the server.
     */
    #show_disconnect() {
        new DisconnectedDialogue().start();
    }
}
export default ConnectionMonitor;
