import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import {InvalidFileTypeError} from '../emulation_selection.js';

/**
 * This dialogue places a given file as the scenario currently selected
 * in some emulation_selection while informing the user of any errors
 * which may occur in the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 */
class SelectScenarioDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Main.EmulationSelection} ems - The emulation selection
     * in which to place the file.
     * @param {string} path - The path of the file to select.
     */
    start(ems, path) {
        try {
            ems.scenarioPath = path;
        } catch (error) {
            if (error instanceof InvalidFileTypeError) {
                this._alert(DiaT.ERROR, error.message);
            } else {
                throw error;
            }
        }
    }
}
export default SelectScenarioDialogue;
