import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue allows the user to save a file and address some common
 * problems which may occur as part of the saving process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class SaveFileDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue. Over the course of the dialogue, the path
     * of the file that is saved to may change.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} path - The path of the file that should be saved.
     * @param {string} content - The content that is to be saved to the
     * file.
     *
     * @throws {NotADirectoryError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The path that was actually written to.
     * */
    async start(fs, path, content) {
        try {
            await fs.writeFile(path, content);
            return path;
        } catch (error) {
            const msg = `Could not write to file ${path}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.NotADirectoryError) {
                const detail =
                    'Cannot create a file inside of a file. ' +
                    'Please enter a new path.';
                const errMsg_ = errMsg(msg, detail);
                const newPath = this._prompt(DiaT.ERROR, errMsg_, path);
                if (newPath != null) {
                    return this.start(newPath, content);
                }
            } else if (error instanceof errc.IsADirectoryError) {
                const detail =
                    'The given path points to a directory. ' +
                    'Please enter a new path.';
                const errMsg_ = errMsg(msg, detail);
                const newPath = this._prompt(DiaT.ERROR, errMsg_, path);
                if (newPath != null) {
                    return this.start(newPath, content);
                }
            }
            throw error;
        }
    }
}
export default SaveFileDialogue;
