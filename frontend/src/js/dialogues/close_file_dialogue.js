import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';

/**
 * This dialogue asks the user whether or not they would like to close
 * a file despite it having unsaved changes.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class CloseFileDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @return {boolean} True if the user agrees to close the file.
     * */
    start() {
        const msg = 'This file has unsaved changes which will be lost.';

        return this._confirm(DiaT.WARN, msg);
    }
}
export default CloseFileDialogue;
