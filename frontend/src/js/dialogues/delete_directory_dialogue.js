import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue can be used to delete a directory and inform the user
 * of any errors which may occur in the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class DeleteDirectoryDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} path - The path of the directory that should be
     * deleted.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async start(fs, path) {
        try {
            await fs.rmdir(path);
        } catch (error) {
            const msg = `Could not delete the directory at ${path}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.FileNotFoundError) {
                const detail =
                    'There is no file or directory at the given path.';
                this._alert(DiaT.Error, errMsg(msg, detail));
            } else if (error instanceof errc.NotADirectoryError) {
                const detail =
                    'The given path points to a file';
                this._alert(DiaT.Error, errMsg(msg, detail));
            }
            throw error;
        }
    }
}
export default DeleteDirectoryDialogue;
