
/**
 * @namespace Dialogues
 * */

/**
 * Base class for all dialogues.
 * Dialogues are interactions with the user through a series of alerts
 * and prompts.
 *
 * @memberof Dialogues
 * */
class Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {

    }

    /**
     * Alerts the user of a message.
     *
     * @param {Dialogues.DialogueType} type - The type of dialogue box
     * to be used.
     * @param {string} [message] - A message to the user.
     * */
    _alert(type, message) {
        alert(message);
    }

    /**
     * Asks the user to confirm a message.
     *
     * @param {Dialogues.DialogueType} type - The type of dialogue box
     * to be used.
     * @param {string} [message] - A message to the user.
     *
     * @return {boolean} - A boolean indicating whether the user
     * confirmed the message or not.
     * */
    _confirm(type, message) {
        return confirm(message);
    }

    /**
     * Displays a dialog box that prompts the user for input.
     *
     * @param {Dialogues.DialogueType} type - The type of dialogue box
     * to be used.
     * @param {string} [message] - A string of text to display to the
     * user.
     * @param {string} [defaultValue] - A string containing the default
     * value displayed in the text input field.
     *
     * @return {?string} The text entered by the user or null if the
     * user aborted.
     * */
    _prompt(type, message, defaultValue) {
        return prompt(message, defaultValue);
    }
}
export default Dialogue;
