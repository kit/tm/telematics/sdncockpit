import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue can be used to move a file from one location in the
 * file system to another and inform the user of any problems that may
 * occur during the operation.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class MoveFileDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} src - The path of the file that should be moved.
     * @param {string} dst - The path which the file should be moved to.
     *
     * @throws {IsADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The path the file was actually moved to.
     * */
    async start(fs, src, dst) {
        try {
            return await fs.mvfile(src, dst);
        } catch (error) {
            const msg = `Could not move file ${src} to ${dst}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.FileNotFoundError) {
                const detail =
                    'There is no file or directory at the given source path.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            } else if (error instanceof errc.IsADirectoryError) {
                const detail =
                    'Is a directory.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            } else if (error instanceof errc.FileExistsError) {
                const detail =
                    'Destination exists already. Delete it first.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            }
            throw error;
        }
    }
}
export default MoveFileDialogue;
