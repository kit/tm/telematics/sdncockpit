import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';

/**
 * This dialogue allows the user to start the server's network.
 * It informs the user of any errors which may occur during the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class StartNetworkDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.IONet} ionet - The network to interact with.
     * @param {Main.EmulationSelection} ems - The current emulation
     * selection.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {boolean} True if the network was started, false
     * otherwise.
     * */
    async start(ionet, ems) {
        if (ems.scenarioPath == null) {
            const msg = 'Error: Cannot start network. No scenario selected.';
            this._alert(DiaT.ERROR, msg);
            return false;
        }

        if (ems.controllerPath == null) {
            const msg = 'Error: Cannot start network. No controller selected.';
            this._alert(DiaT.ERROR, msg);
            return false;
        }

        try {
            await ionet.start(ems.scenarioPath, ems.controllerPath);
            return true;
        } catch (error) {
            if (error instanceof errc.NetworkRunningError) {
                const msg = 'Error: Cannot start network. A network is ' +
                'running on the server already.';
                this._alert(DiaT.ERROR, msg);
                return false;
            } else if (error instanceof errc.FileNotFoundError) {
                const msg = 'Error: Cannot start network. The specified ' +
                'scenario file or controller file was not found on the ' +
                'server. Perhaps it was moved?';
                this._alert(DiaT.ERROR, msg);
                return false;
            } else if (error instanceof errc.ScenarioFileSyntaxError) {
                const msg = 'Error: Cannot start network. The specified ' +
                'scenario file has syntax errors.';
                this._alert(DiaT.ERROR, msg);
                return false;
            } else if (error instanceof errc.ScenarioFileSemanticError) {
                const msg = 'Error: Cannot start network. The specified ' +
                'scenario file has semantic errors.';
                this._alert(DiaT.ERROR, msg);
                return false;
            } else if (error instanceof errc.ControllerConnectTimeoutError) {
                const msg = 'Error: Cannot start network. Timed out while ' +
                'waiting for the controller to connect to the switches.';
                this._alert(DiaT.ERROR, msg);
                return false;
            } else {
                throw error;
            }
        }
    }
}
export default StartNetworkDialogue;
