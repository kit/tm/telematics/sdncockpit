import NewObjectDialogue from './new_object_dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue can be used to copy a directory from one location in
 * the file system to another and inform the user of any problems that
 * may occur during the operation.
 *
 * @extends Dialogues.NewObjectDialogue
 * @memberof Dialogues
 * */
class CopyDirectoryDialogue extends NewObjectDialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} src - The path of the directory that should be
     * copied.
     * @param {string} dst - The path which the directory should be
     * copied to.
     *
     * @throws {IsADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The path the directory was actually copied to.
     * */
    async start(fs, src, dst) {
        let renaming = false;

        const create = async (path) => {
            try {
                await fs.cpdir(src, path);
                return true;
            } catch (error) {
                if (error instanceof errc.SameFileError) {
                    renaming = true;
                    return false;
                } else if (renaming && error instanceof errc.FileExistsError) {
                    return false;
                } else {
                    throw error;
                }
            }
        };

        try {
            return await this._create(dst, create);
        } catch (error) {
            const msg = `Could not copy directory ${src} to ${dst}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.Error, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.FileNotFoundError) {
                const detail =
                    'There is no file or directory at the given source path.';
                this._alert(DiaT.Error, errMsg(msg, detail));
            } else if (error instanceof errc.NotADirectoryError) {
                const detail =
                    'Source or destination is not a directory.';
                this._alert(DiaT.Error, errMsg(msg, detail));
            } else if (error instanceof errc.FileExistsError) {
                const detail =
                    'Destination exists already. Delete it first.';
                this._alert(DiaT.Error, errMsg(msg, detail));
            }
            throw error;
        }
    }
}
export default CopyDirectoryDialogue;
