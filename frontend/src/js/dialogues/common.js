
export const FSE_DETAIL =
    'It appears that the server\'s file system is experiencing some ' +
    'difficulty and was unable to process your request.';

/**
 * This function takes an error message and a description of its details
 * and formats them into a single string.
 *
 * This function has proven valuable for reducing repetition in string
 * literals across dialogues.
 *
 * @param {string} msg - The message.
 * @param {string} detail - The details of the message.
 * @return {string} The formatted message.
 *
 * @memberof Dialogues
 * */
function errMsg(msg, detail) {
    return `Error: ${msg}, ${detail}`;
}
export {errMsg};
