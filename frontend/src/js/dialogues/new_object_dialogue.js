import Dialogue from './dialogue.js';

const pathlib = require('path');

/**
 * This class is the base for the new directory and new file dialogues.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class NewObjectDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Create a new object, automatically renaming it if necessary.
     *
     * @param {string} path - The path of the new object.
     * @param {NewObjectDialogue~CreateFunc} create - A function to
     * actually create the new object.
     *
     * @return {string} The path where the new object was actually
     * created.
     * */
    async _create(path, create) {
        const regex = /\(\d+\)$/;

        // eslint-disable-next-line no-constant-condition
        while (true) {
            if (await create(path)) {
                return path;
            } else {
                let basename = pathlib.basename(path);
                const split = basename.split('.');

                let name = split[0];
                const found = name.match(regex);
                if (found) {
                    let num = parseInt(found[0].slice(1, -1));
                    num++;
                    name = name.replace(regex, `(${num})`);
                } else {
                    name = `${name} (2)`;
                }
                split[0] = name;

                basename = split.join('.');
                path = pathlib.join(pathlib.dirname(path), basename);
            }
        }
    }
}
export default NewObjectDialogue;

/**
 * A function to create a file system object.
 *
 * @typedef NewObjectDialogue~CreateFunc
 * @type {function}
 * @param {path} path - The path of the object to be created.
 * @return {boolean} True if the object was created successfully, false
 * if its path was already occupied.
 * */
