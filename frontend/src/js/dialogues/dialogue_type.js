/**
 * These generally modify the appearance and behavior of dialogue boxes.
 *
 * @readonly
 * @enum {number}
 *
 * @memberof Dialogues
 * */
const DialogueType = {
    INFO: 0,
    WARN: 1,
    ERROR: 2,
};
export default DialogueType;
