import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue can be used to read from a file and inform the user
 * of any errors which may occur in the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class ReadFileDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} path - The path of the file that should be read.
     *
     * @throws {NotADirectoryError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The read content of the file.
     * */
    async start(fs, path) {
        try {
            return await fs.readFile(path);
        } catch (error) {
            const msg = `Failed to read contents of file ${path}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.FileNotFoundError) {
                const detail = 'The file does not exist.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            } else if (error instanceof errc.IsADirectoryError) {
                const detail = 'The given path points to a directory.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            }
            throw error;
        }
    }
}
export default ReadFileDialogue;
