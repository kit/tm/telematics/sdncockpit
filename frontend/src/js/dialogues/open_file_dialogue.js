import Dialogue from './dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';
import FileTab from '../panels/editor_panel/model/file_tab.js';

/**
 * This dialogue opens a file for editing and informs the user of any
 * errors which may occur in the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class OpenFileDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {Proxy.FileSystem} fs - The file system on which the file
     * is stored.
     * @param {string} path - The path of the file to open.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {Panels.EditorPanel.FileTab} A reloaded file tab.
     * */
    async start(fs, path) {
        const tab = new FileTab(fs, path);
        try {
            await tab.reload();
        } catch (error) {
            const msg = `Failed to open file ${path}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.FileNotFoundError) {
                const detail = 'The file does not exist.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            } else if (error instanceof errc.IsADirectoryError) {
                const detail = 'The given path points to a directory.';
                this._alert(DiaT.ERROR, errMsg(msg, detail));
            } else {
                this._alert(DiaT.ERROR, errMsg(msg, error.message));
            }
            throw error;
        }
        return tab;
    }
}
export default OpenFileDialogue;
