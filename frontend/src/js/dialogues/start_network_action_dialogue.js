import Dialogue from './dialogue.js';
import StartNetworkDialogue from './start_network_dialogue.js';
import * as errc from '../api/generated/errorcodes.js';

/**
 * This dialogue allows the user to start a network action.
 * It informs the user of any errors which may occur during the process.
 *
 * @extends Dialogues.Dialogue
 * @memberof Dialogues
 * */
class StartNetworkActionDialogue extends Dialogue {
    /**
     * Create an instance of this dialogue.
     */
    constructor() {
        super();
    }

    /**
     * Start the dialogue.
     *
     * @param {string} actioncode - The code of the action to start.
     * @param {Proxy.IONet} ionet - The network to interact with.
     * @param {Main.EmulationSelection} ems - The current emulation
     * selection. Will be used to start a network if none is currently
     * running.
     *
     * @return {boolean} True if the action was started, false
     * otherwise.
     * */
    async start(actioncode, ionet, ems) {
        try {
            await ionet.startAction(actioncode);
            return true;
        } catch (error) {
            if (error instanceof errc.NetworkStoppedError) {
                const netStarter = new StartNetworkDialogue();
                if (await netStarter.start(ionet, ems)) {
                    await this.start(actioncode, ionet, ems);
                } else {
                    return false;
                }
            } else {
                throw error;
            }
        }
    }
}
export default StartNetworkActionDialogue;
