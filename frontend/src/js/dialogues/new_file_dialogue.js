import NewObjectDialogue from './new_object_dialogue.js';
import DiaT from './dialogue_type.js';
import * as errc from '../api/generated/errorcodes.js';
import {FSE_DETAIL, errMsg} from './common.js';

/**
 * This dialogue allows the user to create a new file.
 *
 * @extends Dialogues.NewObjectDialogue
 * @memberof Dialogues
 * */
class NewFileDialogue extends NewObjectDialogue {
    /**
     * Create an instance of this dialogue.
     * */
    constructor() {
        super();
    }

    /**
     * Start the dialogue. Over the course of the dialogue, the path
     * of the created file may change.
     *
     * @param {Proxy.FileSystem} fs - The file system to interact with.
     * @param {string} path - The path of the new file.
     *
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The path the file was actually copied to.
     * */
    async start(fs, path) {
        // Right now, this dialogue is tailored to the 'new file' button
        // in the file picker, i.e. we already have a path for the new
        // file.

        const create = async (path) => {
            try {
                await fs.mkfile(path);
                return true;
            } catch (error) {
                if (error instanceof errc.FileExistsError) {
                    return false;
                } else {
                    throw error;
                }
            }
        };

        try {
            return await this._create(path, create);
        } catch (error) {
            const msg = `Could not write to file ${path}!`;

            if (error instanceof errc.FileSystemError) {
                this._alert(DiaT.ERROR, errMsg(msg, FSE_DETAIL));
            } else if (error instanceof errc.NotADirectoryError) {
                const detail =
                    'Cannot create a file inside of a file. ' +
                    'Please enter a new path.';
                const errMsg = errMsg(msg, detail);
                const newPath = this._prompt(DiaT.ERROR, errMsg, path);
                if (newPath != null) {
                    return this.start(fs, path);
                }
            }
            throw error;
        }
    }
}
export default NewFileDialogue;
