import ObservableMixin from './util/observable.js';

const CONTROLLER_FILE_TYPES = ['.py'];
const SCENARIO_FILE_TYPES = ['.yaml', '.yml'];

/**
 * Checks if a given path has one of a set of suffixes.
 *
 * @param {string[]} extensions - The acceptable extensions
 * @param {string} path - The path
 *
 * @return {boolean} True if the path matches at least one extension
 */
function checkExt(extensions, path) {
    return extensions.some((ext) => path.endsWith(ext));
}

/**
 * Signals that a path had the wrong file type for the property it was
 * being assigned to.
 *
 * @memberof Main
 * */
class InvalidFileTypeError extends Error {
    /**
     * Builds a new InvalidFileTypeError
     *
     * @param {string} message - The error message.
     * @param {string[]} accepted - Accepted extensions.
     * */
    constructor(message, accepted) {
        super(message);
        this.name = 'InvalidFileTypeError';
        this.accepted = accepted;
    }
}
export {InvalidFileTypeError};

/**
 * Objects of this class hold the currently selected controller and
 * scenario file paths for the application. Typically, only one object
 * of this class exists throughout the application so all parts of the
 * application agree on the current selection.
 *
 * @mixes Observable
 * @memberof Main
 * */
class EmulationSelection extends ObservableMixin(Object) {
    /**
     * Create an EmulationSelection.
     *
     * @param {string} [controllerPath] - The path of the currently
     * selected controller's source file. Must be a '.py' file.
     * @param {string} [scenarioPath] - The path of the currently
     * selected scenario file. Must be a '.yaml' file.
     * */
    constructor(controllerPath = null, scenarioPath = null) {
        super();
        this.controllerPath = controllerPath;
        this.scenarioPath = scenarioPath;
    }

    /**
     * The path of the currently selected controller's source file.
     * Must be a '.py' file.
     * */
    get controllerPath() {
        return this._controllerPath;
    }

    /* eslint-disable require-jsdoc */
    set controllerPath(val) {
        if (val !== null && !checkExt(CONTROLLER_FILE_TYPES, val)) {
            throw new InvalidFileTypeError(
                'Application must be a \'.py\' file!',
                CONTROLLER_FILE_TYPES,
            );
        }

        if (this._controllerPath != val) {
            this._controllerPath = val;
            this._notifyAll();
        }
    }
    /* eslint-enable require-jsdoc */

    /**
     * The path of the currently selected scenario file. Must be a
     * '.yaml' file.
     * */
    get scenarioPath() {
        return this._scenarioPath;
    }

    /* eslint-disable require-jsdoc */
    set scenarioPath(val) {
        if (val !== null && !checkExt(SCENARIO_FILE_TYPES, val)) {
            throw new InvalidFileTypeError(
                'Scenario must be a \'.yaml\' file!',
                SCENARIO_FILE_TYPES,
            );
        }

        if (this._scenarioPath != val) {
            this._scenarioPath = val;
            this._notifyAll();
        }
    }
    /* eslint-enable require-jsdoc */
}
export default EmulationSelection;
