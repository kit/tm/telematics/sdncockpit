import { io } from "socket.io-client"

/**
 * @namespace API
 * */

/**
 * @typedef {Object} LogEntry
 *
 * @property {number} index
 * @property {API.ProgressReport} data
 *
 * @memberof API
 * */

/**
 * @typedef {Object} OutputLogEntry
 *
 * @property {number} index
 * @property {API.OutputReport} data
 *
 * @memberof API
 * */

/**
 * @typedef {(
 *      API.StartEmulatorReport|
 *      API.StopEmulatorReport|
 *      API.StartNetworkReport|
 *      API.StopNetworkReport|
 *      API.RestartNetworkReport|
 *      API.StartNetworkActionReport|
 *      API.AbortNetworkActionReport|
 *      API.CompleteNetworkActionReport|
 *      API.StartTestReport|
 *      API.StartEventReport|
 *      API.TestFeedbackReport|
 *      API.PingReport|
 *      API.OutputReport|
 *      API.ProcessReadStartReport|
 *      API.ProcessReadEndReport
 * )} ProgressReport
 *
 * @memberof API
 * */

/**
 * @typedef {Object} ParserOutput
 *
 * @property {API.SerializedScenario} scenario
 * @property {API.SerializedParseAlert[]} info
 * @property {API.SerializedParseAlert[]} warn
 *
 * @memberof API
 */

/**
 * @typedef {Object} SerializedScenario
 *
 * @property {string} name
 * @property {string} description
 * @property {API:SerializedTest[]} tests
 *
 * @memberof API
 */

/**
 * @typedef {Object} SerializedParseAlert
 *
 * @property {string} message
 * @property {string[]} stack
 *
 * @memberof API
 */

/**
 * @typedef {Object} StartEmulatorReport
 *
 * @property {string} type - Value: 'start_emulator'
 *
 * @memberof API
 */

/**
 * @typedef {Object} StopEmulatorReport
 *
 * @property {string} type - Value: 'stop_emulator'
 *
 * @memberof API
 */

/**
 * @typedef {Object} StartNetworkReport
 *
 * @property {string} type - Value: 'start_network'
 *
 * @memberof API
 * */

/**
 * @typedef {Object} StopNetworkReport
 *
 * @property {string} type - Value: 'stop_network'
 *
 * @memberof API
 * */

/**
 * @typedef {Object} RestartNetworkReport
 *
 * @property {string} type - Value: 'restart_network'
 *
 * @memberof API
 * */

/**
 * @typedef {Object} StartNetworkActionReport
 *
 * @property {string} type - Value: 'start_network_action'
 * @property {string} action_type
 *
 * @memberof API
 */

/**
 * @typedef {Object} AbortNetworkActionReport
 *
 * @property {string} type - Value: 'abort_network_action'
 * @property {string} action_type
 *
 * @memberof API
 */

/**
 * @typedef {Object} CompleteNetworkActionReport
 *
 * @property {string} type - Value: 'complete_network_action'
 * @property {string} action_type
 *
 * @memberof API
 */

/**
 * @typedef {Object} StartTestReport
 *
 * @property {string} type - Value: 'start test'
 * @property {API.SerializedTest} test
 *
 * @memberof API
 * */

/**
 * @typedef {Object} StartEventReport
 *
 * @property {string} type - Value: 'start event'
 * @property {API.SerializedEvent} event
 *
 * @memberof API
 * */

/**
 * @typedef {Object} TestFeedbackReport
 *
 * @property {string} type - Value: 'test feedback'
 * @property {API.SerializedTestFeedback} feedback
 *
 * @memberof API
 * */

/**
 * @typedef {Object} StartPingallReport
 *
 * @property {string} type - Value: 'start_pingall'
 *
 * @memberof API
 */

/**
 * @typedef {Object} AbortPingallReport
 *
 * @property {string} type - Value: 'abort_pingall'
 *
 * @memberof API
 */

/**
 * @typedef {Object} PingReport
 *
 * @property {string} type - Value: 'ping'
 * @property {PingResult} result
 *
 * @memberof API
 */

/**
 * @typedef {Object} OutputReport
 *
 * @property {string} type - Value: 'output'
 * @property {string} line
 *
 * @memberof API
 * */

/**
 * @typedef {Object} ProcessReadStartReport
 *
 * @property {string} type - Value: 'process_read_start'
 *
 * @memberof API
 */

/**
* @typedef {Object} ProcessReadEndReport
*
* @property {string} type - Value: 'process_read_end'
*
* @memberof API
*/

/**
 * @typedef SerializedTestFeedback
 *
 * @property {boolean} status
 * @property {API.SerializedTest} test
 * @property {
 *      API.SerializedObservationPointFeedback[]
 * } observation_point_feedbacks
 *
 * @memberof API
 * */

/**
 * @typedef SerializedObservationPointFeedback
 *
 * @property {boolean} status
 * @property {API.SerializedObservationPoint} observation_point
 * @property {API.SerializedPacketFeedback[]} packet_feedbacks
 *
 * @memberof API
 * */

/**
 * @typedef SerializedPacketFeedback
 *
 * @property {boolean} status
 * @property {string} packet
 * @property {API.SerializedHeaderFeedback[]} header_feedbacks
 *
 * @memberof API
 * */

/**
 * @typedef SerializedHeaderFeedback
 *
 * @property {boolean} status
 * @property {API.SerializedHeaderRule} header_rule
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedTest
 *
 * @property {string} name
 * @property {string} name
 * @property {boolean} restart
 * @property {API.SerializedRandomEvent[]}
 * @property {API.SerializedEvent[]} events
 * @property {API.SerializedObservationPoint[]} obs_points
 * @property {number} test_id
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedRandomEvent
 *
 * @property {API.SerializedIntRangeList} event_count
 * @property {string} host
 * @property {API.SerializedRandomHeaders} headers
 * @property {API.SerializedIntRangeList} pkt_count
 * @property {API.SerializedIntRangeList} time
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedRandomHeaders
 *
 * @property {API.SerializedMACRangeList} ethernet_dst
 * @property {API.SerializedMACRangeList} ethernet_src
 * @property {number} ipv4_flags
 * @property {API.SerializedIntRangeList} ipv4_ttl
 * @property {string} ipv4_src - Network ('192.168.178.0/24')
 * @property {string} ipv4_dst - Network ('192.168.178.0/24')
 * @property {API.SerializedIntRangeList} udp_src_port
 * @property {API.SerializedIntRangeList} udp_dst_port
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedEvent
 *
 * @property {string} host
 * @property {number} pkt_count
 * @property {API.SerializedHeaders} headers
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedHeaders
 *
 * @property {string} ethernet_dst
 * @property {string} ethernet_src
 * @property {number} ipv4_flags
 * @property {number} ipv4_ttl
 * @property {string} ipv4_src
 * @property {string} ipv4_dst
 * @property {number} udp_src_port
 * @property {number} udp_dst_port
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedObservationPoint
 *
 * @property {string[]} link - Always has two elements
 * @property {string} node
 * @property {string} direction
 * @property {API.SerializedIntRangeList} count
 * @property {API.SerializedHeaderRule[]} header_rules
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedHeaderRule
 *
 * @property {string} proto
 * @property {string} field
 * @property {API.SerializedRangeList} range
 *
 * @memberof API
 * */

/**
 * @typedef {(
*      SerializedIntRangeList|
*      SerializedMACRangeList|
*      SerializedIPRangeList
* )} SerializedRangeList
*
* @memberof API
* */

/**
 * @typedef {(
*      IntRange|
*      MACRange|
*      IPRange
* )} SerializedRange
*
* @memberof API
* */

/**
 * @typedef {Object} SerializedIntRangeList
 *
 * @property {SerializedIntRangeList|IntRange[]} ranges
 *
 * @memberof API
 */

/**
 * @typedef {Object} IntRange
 *
 * @property {number} minimum
 * @property {number} maximum
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedMACRangeList
 *
 * @property {SerializedMACRangeList|MACRange[]} ranges
 *
 * @memberof API
 */

/**
 * @typedef {Object} MACRange
 *
 * @property {string} minimum
 * @property {string} maximum
 *
 * @memberof API
 * */

/**
 * @typedef {Object} SerializedIPRangeList
 *
 * @property {SerializedIPRangeList|IPRange[]} ranges
 *
 * @memberof API
 */

/**
 * @typedef {Object} IPRange
 *
 * @property {string} net
 *
 * @memberof API
 * */

/**
 * @typedef {Object} PingResult
 *
 * @property {boolean} status
 * @property {string} source
 * @property {string} destination
 */

/**
 * @typedef {Object} DirectoryEntry
 *
 * @property {string} path
 * @property {string} type - One of 'file', 'dir'
 *
 * @memberof API
 * */

/**
 * @typedef {Object.<string, string>} FlowTables
 *
 * @memberof API
 */

const SERVER_ERROR_DEFAULT_MESSAGE =
'Something appears to have unexpectedly gone wrong on the server. ' +
'We apologize for the inconvenience. Consider restarting the server.';
/**
 * Thrown to indicate that something went wrong on the server.
 *
 * @memberof API
 * */
class ServerError extends Error {
    /**
     * Builds a new ServerError.
     *
     * @param {string} message - A message to send along with the error.
     */
    constructor(message=SERVER_ERROR_DEFAULT_MESSAGE) {
        super(message);
        this.name = 'ServerError';
    }
}
export {ServerError};

const CONNECTION_ERROR_DEFAULT_MESSAGE =
'Could not connect to the server. Please make sure the server is ' +
'still running.';
/**
 * Thrown to indicate a connection issue.
 *
 * @memberof API
 * */
class ConnectionError extends Error {
    /**
     * Builds a new ConnectionError.
     *
     * @param {string} message - A message to send along with the error.
     */
    constructor(message=CONNECTION_ERROR_DEFAULT_MESSAGE) {
        super(message);
        this.name = 'ConnectionError';
    }
}
export {ConnectionError};


/**
 * All methods of this class encapsulate calls to one or more API
 * endpoints on the server.
 *
 * On success, all methods return the data received from the endpoint.
 * On error, a {@link API.ServerError} or {@link API.ConnectionError}
 * is thrown or the ApiErrorConverter is
 * used to throw an exception that is appropriate to the received error
 * code.
 *
 * @memberof API
 * */
class API {
    #proxyPort;
    #corePort;

    /**
     * Builds a new API object.
     *
     * @param {APIErrorConverter} ApiErrorConverter - Will be used to
     * translate error codes to exceptions.
     * */
    constructor(ApiErrorConverter, proxy_port, core_port) {
        this.apiErrorConverter = ApiErrorConverter;
        this.#proxyPort = proxy_port;
        this.#corePort = core_port;
    }

    /**
     * Behaves like the built-in fetch(), except that a response with a
     * status code other than 200 results in an exception being thrown.
     * If the status code is 400, the application leven error code is
     * used.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @param {String} url - a relative url
     * @param {...*} args - forwarded to the built-in fetch().
     * */
    async _fetch(url, ...args) {
        let _url = new URL(url, window.location.href);
        _url.port = this.#corePort;
        url = _url.href;

        let response = null;
        try {
            response = await fetch(url, ...args);
        } catch (error) {
            throw new ConnectionError();
        }

        if (response.status == 200) {
            return response.json();
        } else if (response.status == 400) {
            const error = await response.json();

            const code = error.error;
            const message = error.message ?? '';
            const data = error.data ?? {};

            throw this.apiErrorConverter.convert(code, message, data);
        } else {
            throw new ServerError();
        }
    }

    /**
     * Performs a GET request with the given args.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @param {string} url - The requested URL.
     * @param {Object} args - Contains the request's parameters.
     * */
    async _fetch_get(url, args = {}) {
        if (Object.keys(args).length > 0) {
            url += '?';
            url += new URLSearchParams(args).toString();
        }

        const request = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
            },
        };

        return this._fetch(url, request);
    }

    /**
     * Performs a POST request with the given args.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @param {string} url - The requested URL.
     * @param {Object} args - Contains the request's parameters.
     * */
    async _fetch_post(url, args = {}) {
        const request = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(args),
        };

        return this._fetch(url, request);
    }

    /**
     * Returns a websocket for the server's cli.
     *
     * @return {Socket}
     */
    webSocketCli() {
        let url = new URL("./", window.location.href);
        url.port = this.#proxyPort;
        url.protocol = "ws:";
        return io(url.href);
    }

    /**
     * Returns a websocket for the server's network api.
     *
     * @return {Socket}
     */
    webSocketNet() {
        let url = new URL("./api/net/progress", window.location.href);
        url.port = this.#corePort;
        url.protocol = "ws:";
        return io(url.href);
    }

    /**
     * Starts a network on the server.
     *
     * @param {string} scenarioPath - The virtual file system path of
     * the scenario file to be used to create the network.
     * @param {controller} controllerPath - The virtual file system
     * path of the controller's source file to be used to create the
     * network's controller.
     *
     * @throws {NetworkRunningError}
     * @throws {FileNotFoundError}
     * @throws {ScenarioFileSyntaxError}
     * @throws {ScenarioFileSemanticError}
     * @throws {ControllerConnectTimeoutError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async start(scenarioPath, controllerPath) {
        const url = './api/net/start.json';
        const args = {
            scenario: scenarioPath,
            controller: controllerPath,
        };
        await this._fetch_post(url, args);
    }

    /**
     * Stops the network on the server.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async stop() {
        const url = './api/net/stop.json';
        await this._fetch_post(url);
    }

    /**
     * Starts the specified network action in the server's network.
     *
     * @param {string} name - The name of the action.
     *
     * @throws {NetworkStoppedError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async startAction(name) {
        const url = './api/net/start_action.json';
        const args = {
            name: name,
        };
        await this._fetch_post(url, args);
    }

    /**
     * Stops the specified network action running in the server's
     * network.
     *
     * @param {string} name - The name of the action.
     *
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async stopAction(name) {
        const url = './api/net/stop_action.json';
        const args = {
            name: name,
        };
        await this._fetch_post(url, args);
    }

    /**
     * Retrieve the table of flow tables from the server.
     *
     * @throws {NetworkStoppedError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {FlowTables}
     */
    async flowTables() {
        const url = './api/net/flow_tables.json';
        return this._fetch_get(url);
    }

    /**
     * Parse a scenario file and retrieve the parser's output.
     *
     * @param {string} path - The scenario file's path.
     *
     * @throws {FileNotFoundError}
     * @throws {ScenarioFileSyntaxError}
     * @throws {ScenarioFileSemanticError}
     *
     * @return {API.ParserOutput} The output of the parser.
     */
    async parse_scenario(path) {
        const url = './api/parse_scenario.json';
        return this._fetch_get(url, {path: path});
    }

    /**
     * Returns a websocket for the server's file system change api.
     *
     * @return {Socket}
     */
    webSocketFs() {
        let url = new URL("./api/fs/changes", window.location.href);
        url.port = this.#corePort;
        url.protocol = "ws:";
        return io(url.href);
    }

    /**
     * Creates a new directory at the given path.
     *
     * @param {string} path - The new directory's path.
     *
     * @throws {FileExistsError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mkdir(path) {
        const url = './api/fs/mkdir.json';
        await this._fetch_post(url, {path: path});
    }

    /**
     * Creates a new file at the given path.
     *
     * @param {string} path - The new file's path.
     *
     * @throws {FileExistsError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async mkfile(path) {
        const url = './api/fs/mkfile.json';
        await this._fetch_post(url, {path: path});
    }

    /**
     * Reads a the contents of the directory at the given path.
     *
     * @param {string} path - The directory's path.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {Proxy.DirectoryEntry[]} The contents of the directory.
     * */
    async readDir(path) {
        const url = './api/fs/read_dir.json';
        return this._fetch_get(url, {path: path});
    }

    /**
     * Reads the file at the given path.
     *
     * @param {string} path - The file's path.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     *
     * @return {string} The content of the file.
     * */
    async readFile(path) {
        const url = './api/fs/read_file.json';
        return this._fetch_get(url, {path: path});
    }

    /**
     * Writes the given content to the file at the given path.
     *
     * @param {string} path - The file's path.
     * @param {string} content - The file's content.
     *
     * @throws {NotADirectoryError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async writeFile(path, content) {
        const url = './api/fs/write_file.json';
        return this._fetch_post(url, {path: path, content: content});
    }

    /**
     * Deletes the directory at the given path.
     *
     * @param {string} path - The directory's path.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async rmdir(path) {
        const url = './api/fs/rmdir.json';
        await this._fetch_post(url, {path: path});
    }

    /**
     * Deletes the file at the given path.
     *
     * @param {string} path - The file's path.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async rmfile(path) {
        const url = './api/fs/rmfile.json';
        await this._fetch_post(url, {path: path});
    }

    /**
     * Recursively copies a directory to another location.
     *
     * @param {string} src - The path of the directory which will be
     * copied.
     * @param {string} dst - The path the directory at src will be
     * copied to.
     *
     * @throws {FileNotFoundError}
     * @throws {NotADirectoryError}
     * @throws {FileExistsError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async cpdir(src, dst) {
        const url = './api/fs/cpdir.json';
        await this._fetch_post(url, {src: src, dst: dst});
    }

    /**
     * Copies a file to another location.
     *
     * @param {string} src - The path of the file which will be copied.
     * @param {string} dst - The path the file at src will be copied to.
     *
     * @throws {FileNotFoundError}
     * @throws {IsADirectoryError}
     * @throws {FileExistsError}
     * @throws {SameFileError}
     * @throws {FileSystemError}
     * @throws {ConnectionError}
     * @throws {ServerError}
     * */
    async cpfile(src, dst) {
        const url = './api/fs/cpfile.json';
        await this._fetch_post(url, {src: src, dst: dst});
    }
}
export default API;
