import ObservableMixin from '../util/observable.js';

/**
 * A function that accepts a message from a websocket.
 *
 * @callback messageCallback
 * @param {string | ArrayBuffer} message
 */

/**
 * A connection wraps an API such that it can keep track of the
 * connectivity to the server, and makes this knowledge publicly
 * available.
 *
 * @memberof API
 * */
class Connection extends ObservableMixin(Object) {
    #api;
    #cli_ws;
    #cli_handlers;
    #progress_ws;
    #progress_report_handlers;
    #fs_ws;
    #fs_handlers;

    /**
     * Builds a new Connection object.
     *
     * @param {API.API} api - The api to connect through.
     * */
    constructor(api) {
        const configureReconnect = (ws) => {
            ws.on("connect", () => {
                if (this.connected) {
                    this._notifyAll();
                }
            });
            ws.io.on("reconnect_attempt", (attempt) => {
                if (attempt == 3) {
                    this._notifyAll();
                }
            });
            ws.io.on("reconnect", () => {
                if (this.connected) {
                    this._notifyAll();
                }
            });
        }

        super();
        this.#api = api;

        this.#cli_ws = this.#api.webSocketCli();
        this.#cli_handlers = [];
        configureReconnect(this.#cli_ws);
        this.#cli_ws.on("output", (data) => {
            for (const handler of this.#cli_handlers) {
                handler(data);
            }
        });

        this.#progress_ws = this.#api.webSocketNet();
        this.#progress_report_handlers = [];
        configureReconnect(this.#progress_ws)
        this.#progress_ws.on("report", (report) => {
            for (const handler of this.#progress_report_handlers) {
                handler(report);
            }
        });

        this.#fs_ws = this.#api.webSocketFs();
        this.#fs_handlers = [];
        configureReconnect(this.#fs_ws)
        this.#fs_ws.on("change", (path) => {
            for (const handler of this.#fs_handlers) {
                handler(path);
            }
        });
    }

    /**
     * Whether we are connected to the server.
     *
     * @type {boolean}
     *
     * @readonly
     * */
    get connected() {
        return (
            this.#progress_ws.connected &&
            this.#cli_ws.connected &&
            this.#fs_ws.connected
        );
    }

    /**
     * Register a reader for the command line.
     *
     * @param {messageCallback} reader
     */
    cliReader(reader) {
        this.#cli_handlers.push(reader);
    }

    /**
     * Write to the command line.
     *
     * @param {string | Uint8Array} data
     */
    cliWrite(data) {
        this.#cli_ws.emit('input', data);
    }

    registerProgressReportHandler(handler) {
        this.#progress_report_handlers.push(handler);
    }

    registerFileSystemUpdateHandler(handler) {
        this.#fs_handlers.push(handler);
    }

    /** @see API.API#start */
    async start(scenarioPath, controllerPath) {
        return this.#api.start(scenarioPath, controllerPath);
    }

    /** @see API.API#stop */
    async stop() {
        return this.#api.stop();
    }

    /** @see API.API#startAction */
    async startAction(name) {
        return this.#api.startAction(name);
    }

    /** @see API.API#stopAction */
    async stopAction(name) {
        return this.#api.stopAction(name);
    }

    /** @see API.API#flowTables */
    async flowTables() {
        return this.#api.flowTables();
    }

    /** @see API.API#mkdir */
    async mkdir(path) {
        return this.#api.mkdir(path);
    }

    /** @see API.API#mkfile */
    async mkfile(path) {
        return this.#api.mkfile(path);
    }

    /** @see API.API#readDir */
    async readDir(path) {
        return this.#api.readDir(path);
    }

    /** @see API.API#readFile */
    async readFile(path) {
        return this.#api.readFile(path);
    }

    /** @see API.API#writeFile */
    async writeFile(path, content) {
        return this.#api.writeFile(path, content);
    }

    /** @see API.API#rmdir */
    async rmdir(path) {
        return this.#api.rmdir(path);
    }

    /** @see API.API#rmfile */
    async rmfile(path) {
        return this.#api.rmfile(path);
    }

    /** @see API.API#cpdir */
    async cpdir(src, dst) {
        return this.#api.cpdir(src, dst);
    }

    /** @see API.API#cpfile */
    async cpfile(src, dst) {
        return this.#api.cpfile(src, dst);
    }
}
export default Connection;
