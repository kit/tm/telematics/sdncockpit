/**
 * @namespace Main
 */


import makeSplits from './layout.js';

import {APIErrorConverter} from './api/generated/errorcodes.js';
import API from './api/api.js';
import Connection from './api/connection.js';
import ConnectionMonitor from './connection_monitor.js';

import FileSystem from './proxy/file_system.js';
import IONet from './proxy/ionet.js';

import EmulationSelection from './emulation_selection.js';
import FilePicker from './panels/file_picker/file_picker.js';
import EditorPanel from './panels/editor_panel/editor_panel.js';
import ControllerPanel from './panels/controller_panel/controller_panel.js';
import ScenarioPanel from './panels/scenario_panel/scenario_panel.js';
import EvaluationPanel from './panels/evaluation_panel/evaluation_panel.js';
import TerminalPanel from './panels/terminal_panel/terminal_panel.js';


const DEFAULT_CONTROLLER = '/apps/demo.py';
const DEFAULT_SCENARIO = '/scenarios/demo.yaml';
const WELCOME_PATH = '/welcome.txt';


/**
 * Main class. Builds and contains basically the entire page.
 *
 * @memberof Main
 * */
class Main {
    /**
     * Constructor.
     * */
    constructor() {
        makeSplits();

        const configure_proxy = () => {
            fetch('./proxy_port.json').then(response => {
                response.json().then(proxy_port => {
                    this.proxy_port = proxy_port.proxy_port;
                    configure_core();
                });
            });
        }
        const configure_core = () => {
            const url = new URL('./core_port.json', window.location.href);
            url.port = this.proxy_port;
            fetch(url.href).then(response => {
                response.json().then(core_port => {
                    this.core_port = core_port.core_port;
                    this.build();
                });
            })
        }
        configure_proxy();
    }

    build() {
        this.apiErrorConverter = new APIErrorConverter();
        this.api = new API(
            this.apiErrorConverter, this.proxy_port, this.core_port
        );
        this.connection = new Connection(this.api);
        this.connectionMonitor = new ConnectionMonitor(this.connection);

        this.fs = new FileSystem(this.connection);
        this.ionet = new IONet(this.connection);

        this.ems = new EmulationSelection(DEFAULT_CONTROLLER, DEFAULT_SCENARIO);
        this.ems.register(() => {
            // When the selected controller or scenario is switched,
            // emulation stops.
            this.ionet.stop();
        });

        const fpContainer = document.getElementById('file-picker-panel');
        const fpVisibility = document.getElementById('file-picker-box');
        this.filePicker = new FilePicker(
            fpContainer, fpVisibility, this.fs,
        );

        const epContainer = document.getElementById('editor-panel');
        this.editorPanel = new EditorPanel(
            epContainer, this.fs, this.filePicker, WELCOME_PATH,
        );

        const cpContainer = document.getElementById('controller-panel');
        this.controllerPanel = new ControllerPanel(
            cpContainer, this.ems, this.ionet, this.filePicker,
        );

        const spContainer = document.getElementById('scenario-panel');
        this.scenarioPanel = new ScenarioPanel(
            spContainer, this.ems, this.api, this.ionet, this.filePicker,
        );

        const tpContainer = document.getElementById('terminal-panel');
        this.terminalPanel = new TerminalPanel(
            tpContainer, this.connection,
        );

        const evContainer = document.getElementById('evaluation-panel');
        this.evaluationPanel = new EvaluationPanel(
            evContainer, this.ems, this.ionet,
        );
    }
}
document.addEventListener('DOMContentLoaded', () => window.sdnc = new Main());
