const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'development',
    target: 'web',
    resolve: {
        fallback: {
            'path': require.resolve('path-browserify'),
        },
    },
    entry: {
        main: './src/js/index.js',
        flow_tables: './src/js/flow_tables.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.ttf$/,
                type: 'asset/resource',
            },
        ],
    },
    plugins: [new MonacoWebpackPlugin({
        languages: ['python', 'yaml'],
    })],
};
