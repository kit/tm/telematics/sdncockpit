#!/usr/bin/env bash

# This script contains helpers for building, running and testing SDNcockpit.
# Make sure to run it from its own directory.

dependencies() {
    # core
    pushd ../core
    sudo pip3 install -e .[dev]
    popd

    # proxy
    pushd ../proxy
    npm install
    popd

    # frontend
    pushd ../frontend
    npm install
    popd
}

build() {
    # core
    pushd ../core
    # Proxy currently always runs against the installed version,
    # which is probably better for testing anyways.
    sudo pip3 install -e .[dev]
    popd

    # generate frontend files from core
    pushd ../core/src
    python3 -m sdnc.api.errorcodes
    python3 -m sdnc.api.actioncodes
    popd

    # frontend
    pushd ../frontend
    npm run-script build
    popd
}

lint() {
    # core
    pushd ../core
    tox -e type,lint
    popd

    pushd ../proxy
    npm run-script lint
    popd

    pushd ../frontend
    npm run-script lint
    popd
}

run() {
    # run proxy and frontend at the same time
    pushd ..
    sudo parallel --lb ::: "node frontend/server.mjs" "node proxy/src/main.mjs --core-directory=./data"
    popd
}

test() {
    # core
    pushd ../core
    sudo tox -e py310
    popd
}

all () {
    build
    run
}

usage () {
    printf 'Usage: %s [-abdhlrt]\n' $(basename $0) >&2
    printf '\n' >&2
    printf 'This script installs the dependencies of SDN-Cockpit.\n' >&2
    printf '\n' >&2
    printf 'options:\n' >&2
    printf -- ' -a: (A)ll - Build & Run (default).\n' >&2
    printf -- ' -b: (B)uild.\n' >&2
    printf -- ' -d: Install (d)ependencies.' >&2
    printf -- ' -h: Print this (h)elp message.\n' >&2
    printf -- ' -l: (L)int\n' >&2
    printf -- ' -r: (R)un.\n' >&2
    printf -- ' -t: (T)est\n' >&2
    exit 2
}

if [ $# -eq 0 ]
then
    all
else
    while getopts 'abdhlrt' OPTION
    do
        case $OPTION in
            a)          all;;
            b)        build;;
            d) dependencies;;
            h)        usage;;
            l)         lint;;
            r)          run;;
            t)         test;;
            ?)        usage;;
        esac
    done
    shift $(($OPTIND - 1))
fi
