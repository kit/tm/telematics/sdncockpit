#!/usr/bin/env bash

# This script installs SDN-Cockpit.
# Make sure to run it from its own directory.

export DEBIAN_FRONTEND=noninteractive

apt_update_once=false
apt_update () {
    # RUN ONCE
    if [ "$apt_update_once" = false ] ; then
        echo "##### Updating index"
        sudo apt-get update -y
        sudo apt-get upgrade -y
    fi
    apt_update_once=true
}

pip_once=false
pip() {
    # dependencies
    apt_update

    # RUN ONCE
    if [ "$pip_once" = false ] ; then
        echo "##### Installing python"
        sudo apt-get install -y python3-pip
        sudo pip3 install pip==23.1
    fi
    pip_once=true
}

ryu_once=false
ryu() {
    # dependencies
    apt_update
    pip

    # RUN ONCE
    if [ "$ryu_once" = false ] ; then
        echo "##### Installing ryu"
        # ryu is unmaintained at this point.
        # Since we only need it to start user's controllers, we can
        # install it on an appropriate version of python separate from
        # the rest of the project.
        sudo add-apt-repository ppa:deadsnakes/ppa
        sudo apt-get update
        sudo apt-get install -y \
            python3.9 \
            python3.9-dev \
            python3.9-distutils

        sudo python3.9 -m pip install eventlet==0.30.2 setuptools==58.3.0
        sudo python3.9 -m pip install ryu==4.34
    fi
    ryu_once=true
}

node_once=false
node() {
    # dependencies
    apt_update

    # RUN ONCE
    if [ "$node_once" = false ] ; then
        echo "##### Installing node"

        sudo apt-get install -y ca-certificates curl gnupg
        sudo mkdir -p /etc/apt/keyrings
        curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

        NODE_MAJOR=20
        echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list

        sudo apt-get update
        sudo apt-get install -y nodejs
    fi
    node_once=true
}

common_once=false
common() {
    # dependencies
    apt_update
    pip
    ryu
    node

    # RUN ONCE
    if [ "$common_once" = false ] ; then
        echo "##### Installing common"

        sudo apt-get remove -y python3-blinker # to fix a core install issue
        sudo apt-get install -y mininet netsniff-ng
    fi
    common_once=true
}

production_once=false
production () {
    # dependencies
    apt_update
    pip
    ryu
    node
    common

    # RUN ONCE
    if [ "$production_once" = false ] ; then
        echo "##### Installing production"

        # core
        sudo pip3 install -r requirements.txt
        pushd ../core
        sudo pip3 install --no-deps .
        popd

        # generate frontend files from core
        pushd ../core/src
        python3 -m sdnc.api.errorcodes
        python3 -m sdnc.api.actioncodes
        popd

        # proxy
        pushd ../proxy
        npm install --omit=dev
        sudo npm install -g
        popd

        # frontend
        # usually, we would bundle our files with with webpack and then publish
        # everything to npm, so when a user wants to install the production
        # version, they don't actually need webpack.
        # In this project, we don't want to publish any packages, but we also
        # don't want to keep the bundled files in version management AND we
        # want production to use the same repository as development.
        # All of these requirements force us to install with dev dependencies,
        # then bundle, then install globally.
        # The disadvantage: bundling takes time, slowing down production
        # installs.
        pushd ../frontend
        npm install
        npm run-script build
        sudo npm install -g
        popd

        # systemctl
        sudo cp sdnc-frontend.service /etc/systemd/system/sdnc-frontend.service
        sudo cp sdnc-proxy.service /etc/systemd/system/sdnc-proxy.service
        sudo systemctl daemon-reload
        sudo systemctl enable sdnc-proxy.service
        sudo systemctl start sdnc-proxy.service
        sudo systemctl enable sdnc-frontend.service
        sudo systemctl start sdnc-frontend.service
    fi
    production_once=true
}

development_once=false
development () {
    # dependencies
    apt_update
    pip
    ryu
    node
    common

    # RUN ONCE
    if [ "$development_once" = false ] ; then
        echo "##### Installing develoment"

        sudo apt-get install -y parallel

        # ovs-testcontroller
        sudo apt-get install -y openvswitch-testcontroller
        sudo systemctl disable openvswitch-testcontroller
        sudo systemctl stop openvswitch-testcontroller
        sudo killall ovs-testcontroller

        # pip-tools
        sudo pip3 install pip-tools==7.3.0
    fi
    development_once=true
}

all () {
    development
}

usage () {
    printf 'Usage: %s [-adhp]\n' $(basename $0) >&2
    printf '\n' >&2
    printf 'This script installs SDN-Cockpit.\n' >&2
    printf '\n' >&2
    printf 'options:\n' >&2
    printf -- ' -a: (A)ll - install everything.\n' >&2
    printf -- ' -d: Install dependencies for (d)evelopment.\n' >&2
    printf -- ' -h: Print this (h)elp message.\n' >&2
    printf -- ' -p: install for (p)roduction.\n' >&2
    exit 2
}

if [ $# -eq 0 ]
then
    all
else
    while getopts 'adhp' OPTION
    do
        case $OPTION in
            a)    all;;
            d)    development;;
            h)    usage;;
            p)    production;;
            ?)    usage;;
        esac
    done
    shift $(($OPTIND - 1))
fi
