\documentclass{scrarticle}

\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}

\input{listings}

\usepackage[hyphens]{url}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=black,
	filecolor=black,
	urlcolor=blue,
}
\usepackage{cleveref}

\newcommand{\hl}[1]{\texttt{#1}}
\lstset{basicstyle=\ttfamily}

\newcommand{\vboxdl}{\texttt{\href{https://www.virtualbox.org/wiki/Download_Old_Builds_6_1}{virtualbox.org\slash{}wiki\slash{}Download\textunderscore{}Old\textunderscore{}Builds\textunderscore{}6\textunderscore{}1}}}
\newcommand{\vagrantdl}{\texttt{\href{https://developer.hashicorp.com/vagrant/downloads}{developer.hashicorp.com\slash{}vagrant\slash{}downloads}}}

\graphicspath{{./graphics}}

\title{SDN-Cockpit}
\date{\today}
\author{Jakob Gretenkort\\ Institute of Telematics, Karlsruhe Insitute of Technology}

\begin{document}
\maketitle

\tableofcontents
\pagebreak

\section{Introduction}\label{sec:introduction}

This document contains the manual for SDN-Cockpit, an open-source SDN teaching framework developed by the Institute of Telematics (Karlsruhe Insitute of Technology). The main goal of the tool is to provide an easy-to-use environment for fast pace SDN application development with special support for executing automated evaluation scenarios. It builds on the \href{https://github.com/mininet/mininet}{mininet} network emulator and the \href{https://ryu.readthedocs.io/en/latest/}{Ryu} SDN controller software and uses Vagrant to allow for easy setup and reproducibility.

\cref{fig:high-level} highlights the general architecture and workflow. The tool has a backend inside a virtual machine which contains the network emulator and the automated evaluation engine. Via the web-based frontend, the user can write SDN applications and send instructions to the backend to test them. Network activity and test results are then relayed back to the user visually for review.


\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{interaction.pdf}
\caption{SDN-Cockpit Tool: High Level View}
\label{fig:high-level}
\end{figure}

\section{Setup}\label{sec:setup}

The following sections show the installation process for Linux (\cref{sec:setup:linux}), macOS (\cref{sec:setup:macos}) and Windows (\cref{sec:setup:windows}). SDN-Cockpit depends on git, Vagrant (2.3.6) and VirtualBox (6.1.4), which will be installed first.

\subsection{Linux}\label{sec:setup:linux}

VirtualBox can be obtained either from your distribution's packet repository or from the VirtualBox download page (\vboxdl{}). We recommend the latter as the packet repository may contain an outdated version of VirtualBox. Select the entry corresponding to your distribution and install the package with your packet manager.

Vagrant can also be obtained with either method, but even Vagrant themselves recommend downloading directly from their download page (\vagrantdl{}), as packet managers often carry severely outdated versions of vagrant or miss dependencies for it.

Open a new terminal session and continue with \cref{sec:setup:common}.

\subsection{macOS}\label{sec:setup:macos}

The SDN-Cockpit tool is currently not supported on devices with ARM / Apple silicon chips. This is due to the lack of support for VirtualBox on those machines. For other devices running macOS, the following should work:

First, install the Command Line Tools provided by Apple. For this, open a new terminal session by executing the \hl{Terminal}-Application. It can be found under \hl{Applications\slash{}Utilities\slash{}Terminal}. Enter \hl{xcode-select --install} into the session. Confirm the installation by pressing \hl{Install}. Then wait for the installation to complete.

Next, install VirtualBox. Visit (\vboxdl{}) and download the latest version for OS X hosts. Open the downloaded image and follow the installation instructions.

Lastly, install Vagrant. Visit (\vagrantdl{}) and select macOS. Open the downloaded image and follow the installation instructions.

Open a new terminal session and continue with \cref{sec:setup:common}.

\subsection{Windows}\label{sec:setup:windows}

First, download git for Windows from \texttt{\href{https://git-scm.com/download/win}{git-scm.com/download/win}}. Execute the installer and follow the installation instructions. Note that all options can be kept in their default configuration.

Then, install VirtualBox. Visit \vboxdl{} and download the latest version for Windows hosts. Open the downloaded executable and follow the installation instructions.

Lastly, install Vagrant. Visit \vagrantdl{} and select Windows 64-bit. Open the downloaded executable and follow the installation instructions.

Start VirtualBox \textbf{as administrator}. Open a new git bash session \textbf{as administrator} and continue with \cref{sec:setup:common}.

\subsection{Common}\label{sec:setup:common}

Change into the folder where you whish for SDN-Cockpit to reside by typing \hl{cd <path>} where \hl{<path>} is the path to this folder. Now clone the SDN-Cockpit repository by typing \hl{git clone https://git.scc.kit.edu/TM/SDNcockpit}.

Enter the repository by typing \hl{cd SDNcockpit} and create the virtual machine by typing \hl{vagrant up}. Wait for the command to complete (It might take a few minutes). The virtual machine is now running and has automatically started the SDN-Cockpit backend. Open a browser of your choice and go to \href{http://localhost:8080}{localhost:8080}.

If you ever want to stop the VM - and by extension SDN-Cockpit - run \hl{vagrant halt} in a terminal in the repository's directory.

\section{Getting Started}\label{sec:quickstart}

The main user interface of the framework, shown in \cref{fig:ui_full}, consists of four different panes:

\begin{figure}
\centering
\includegraphics[width=\textwidth]{ui_full.png}
\caption{Web-based user interface of the framework}
\label{fig:ui_full}
\end{figure}

\begin{itemize}
	\item[] \textbf{Top left: Editor.} This pane contains a code editor to write SDN applications and edit scenarios. The buttons at the top are for opening files to edit, and for saving the file in the editor's currently focused tab.

	\item[] \textbf{Bottom left: Application.} This pane displays the output of your SDN application. Since SDN-Cockpit allows you to edit many applications in parallel, you need to tell the tool which one to run via the folder button at the top. The selected application will be started and stopped automatically in tandem with the network.

	\item[] \textbf{Top right: Scenario.} This pane allows you to select a scenario via the folder button at the top right. Once selected, the scenario's description will be shown.

	\item[] \textbf{Bottom right: Evaluation.} This pane shows network output. The four buttons in this window, from left to right, have the following functions:
	\begin{itemize}
		\item[] \textbf{Start Network} This button starts/stops the network emulator. This includes creation of a network with the topology that was defined as part of the scenario that was selected in the scenario pane. It also includes starting your SDN application and having it connect to all the network's switches.
		\item[] \textbf{Run Tests} This button starts/stops the automated evaluation defined as part of the scenario that was selected in the scenario pane. The tests, the traffic they inject into the network, and the test's results will be printed to this pane.
		\item[] \textbf{Pingall} Pressing this button will trigger the network emulator to send an ICMP ping from each of the network's hosts to every other host. The results of these pings will be printed to this pane.
		\item[] \textbf{Flow Tables} This button opens a popup window which shows the current state of the flow tables of all switches in the network.
	\end{itemize}
\end{itemize}

When selecting files to edit or to use as the active SDN app or the active scenario, the file explorer will open as shown in \cref{fig:ui_filesys}.

\begin{figure}[h]
\centering
\includegraphics[width=0.25\textwidth]{ui_filesys.png}
\caption{The file explorer in the user interface}
\label{fig:ui_filesys}
\end{figure}

The file explorer allows all common file system interactions (create, delete, cut, copy, paste...) via a right-click context menu.

\subsection{Running and Testing an App}\label{sec:development:workflow}

To run an app and have it control a network, click on the folder button in the application pane (bottom left) and select the app's source file. This lets SDN-Cockpit know which app you want to test. Now, click on the folder button in the scenario pane (top right) and select a scenario to run. Scenarios are yaml files that define the topology of a network, and some tests. Tests consist of descriptions of traffic to send through the network and expectations of how the network should handle that traffic. To learn more about scenarios, check out \cref{sec:scenarios}.

Having selected an app and a scenario, you may run the scenario's tests by clicking the \textit{Run Tests} button in the evaluation pane. This will prompt the SDN-Cockpit backend to build and start a virtual network with the topology defined in the scenario. It will also start the SDN application and begin displaying its output in the applicatin pane. In the evaluation pane, SDN-Cockpit will show you the tests being executed and their results as they come in.

You can stop the network and evaluation at any time by clicking the \textit{Start Network} button, then edit the app and re-run everything.

\subsection{Using Other Editors}\label{sec:quickstart:editors}

The root directory in the file explorer is synchronized to the \hl{data} directory in the local \hl{SDNcockpit} repository directory. This synchronization goes both ways, so you may edit files in an editor of your choice on your host operating system instead of using the built-in editor. However, if you do this, make sure to configure your editor to save files with '\texttt{\textbackslash{}n}' line endings.

\section{Application Development}\label{sec:development}

SDN applications developed with SDN-Cockpit use the SDN controller implementation \hl{Ryu}. A comprehensive documentation of the \hl{Ryu} API is available at \href{http://ryu.readthedocs.io/en/latest/api_ref.html}{ryu.readthedocs.io\slash{}en\slash{}latest\slash{}api\textunderscore{}ref.html}.
Applications use this API to interact with the network in various ways. Since \hl{Ryu} uses a Python-based programming interface some familiarity with this programming language is naturally required.
As a straightforward approach to familiarize yourself with the Python programming language we advise you to take a look at the comprehensive tutorial offered by the Python community: \href{https://docs.python.org/3/tutorial/}{docs.python.org\slash{}3\slash{}tutorial/}.
You may want to pay special attention to classes, inheritance and instance variables, if you are unfamiliar with object oriented programming in Python.
For a quick reference you may find the more aspect-oriented approach to Python at \href{https://www.tutorialspoint.com/python/index.htm}{tutorialspoint.com/python/index.htm} more convenient.

\subsection{The CockpitApp Class}\label{sec:development:cockpitapp}

To simplify interaction with the \hl{Ryu} controller, we provide you with an abstraction class called \hl{CockpitApp}, which can be found in \hl{apps\slash{}controller.py}. This class offers convenience functions to simplify interactions with an SDN switch, that allow you to program flows onto switches (see: \cref{sec:development:flows}) and to send packets into the network (see: \cref{sec:development:packets}). \hl{CockpitApp} will also automatically install a default flow on all switches, which will forward all otherwise unmatched packets to the controller - and by extension your app. To get started on a new SDN application, create a python source file, open it in the editor and create a class that extends \hl{CockpitApp} through inheritance.

\subsection{Installing a flow}\label{sec:development:flows}

To install a flow - a rule that determines how packets should be processed - on a switch, you can use the \hl{program\_flow} function provided by \hl{CockpitApp}.

This function has the following signature:
\begin{python}
def program_flow(self, datapath, match, actions,
	priority = 0, hard_timeout = 0, idle_timeout = 0)
\end{python}

Here, you can think of the \hl{datapath} parameter as simply identifying the switch onto which the new flow will be installed. Datapaths are described in detail in \cref{sec:development:datapath}.

The \hl{match} parameter determines which packets should be handled by a flow. It expects an \hl{OFPMatch} object as its argument, which can be constructed in the following way:
\begin{python}
match = parser.OFPMatch(
    eth_type = ether_types.ETH_TYPE_IP,
    ipv4_src = ('10.0.0.1', '255.255.255.0')
)
\end{python}

The constructor of a match object accepts numerous parameters, which represent the diffent matches that are supported by an OpenFlow switch. The following table lists some of the more commonly used matches:

\begin{center}
\begin{tabular}{l l l}
Argument & Type & Description \\
\hline
\texttt{in\_port}  & 32bit integer & Switch input port            \\
\texttt{eth\_src}  & MAC address   & Ethernet source address      \\
\texttt{eth\_dst}  & MAC address   & Ethernet destination address \\
\texttt{eth\_type} & 16bit integer & Ethernet source address      \\
\texttt{ip\_src}   & IP address    & IP source address            \\
\texttt{ip\_dst}   & IP address    & IP destination address       \\
\end{tabular}
\end{center}

To allow a flow rule to match all packets that have a header field with a value within some range, select parameters accept a combination of a value and a bitmask. In this case, only those parts of the value that align with a \hl{1} in the bitmask are considered when determining if a packet matches the rule. All parts of the value that align with a \hl{0} in the bitmask are instead considered `don't care`s. Parameters like these are usually provided as tuples, as seen in the previous matching example for the IPv4 source address \hl{10.0.0.1} and the subnet mask \hl{255.255.255.0}. In this example, any packet with an IP that equals 10.0.0 for the first three octets will be matched by the rule, while the last octet is considered \textit{don't care}.

You may find it reasonable at this point to expect that if a packet arriving at a switch matches two different flow rules, the switch would automatically choose the more specific match. This is however not guaranteed by either OpenFlow or Open vSwitch (the virtual switch implementation used by SDN-Cockpits network emulator). You should therefore make sure that two different flows that can match the same packet always have diffrent priorities.

For a full list of available parameters please consult the \hl{Ryu} documentation under \href{https://ryu.readthedocs.io/en/latest/ofproto_v1_3_ref.html#flow-match-structure}{ryu.readthedocs.io\slash{}en\slash{}latest\slash{}ofproto\_v1\_3\_ref.html\#flow-match-structure}. There is one caveat with \hl{Ryu} that you should keep in mind: \hl{Ryu} will silently discard any new flow rule if you fail to include a match on underlying protocol \emph{types}. This is why in the previous example an explicit match on the Ethernet type is necessary to ensure that the match on the IPv4 source address will actually be performed.

The \hl{actions} parameter of the \hl{program\_flow} function expects a list of OpenFlow actions. Normally you will just want to drop packets or output them on a specific port. This can easily be achieved by passing a list of actions like this, with the variable \hl{out\_port} set to the number of the output port:
\begin{python}
[parser.OFPActionOutput(out_port)] # Output packet
[] # Drop packet
\end{python}

The parameter \hl{priority} will determine the relative precedence of installed flow rule. By convention, priority 0 is typically used for a default flow rule, which matches all packets not matched by other flows in the flow table. If you are inheriting from CockpitApp, this default rule is already installed with an action to forward all otherwise unmatched packets to the controller.

Finally, the \hl{hard\_timeout} and \hl{idle\_timeout} parameters determine the maximum lifetime of the flow rule, and the lifetime after a flow rule was last invoked in seconds. A value of 0 (default for both) indicates that the flow rule should never time out.

\subsection{Sending a packet}\label{sec:development:packets}

The second function provided by \hl{CockpitApp} is \hl{send\_pkt}, which lets you send packets from the controller via a specific port of an SDN switch. You can use this function to retransmit packets, which were delivered to the controller.
\begin{python}
def send_pkt(self, datapath, data,
    port = ofproto.OFPP_FLOOD)
\end{python}

Like with the \hl{program\_flow} function, the \hl{datapath} parameter determines the switch the controller wishes to interact with.

The parameter \hl{data} contains the actual packet data that is to be transmitted. This is usually the entire packet as it was received by a controller (see \cref{sec:development:events} on how to obtain that data).

Finally, the \hl{port} parameter determines the output port over which that packet will be sent. As indicated in the example, a special \hl{OFPP\_FLOOD} port can be specified to send a packet over all ports of an SDN switch. Remember that you can get the ofproto object from the datapath.

\subsection{Datapaths}\label{sec:development:datapath}

One important concept to understand when interacting with Ryu is that of a datapath. A \hl{datapath} is an object that \hl{Ryu} uses to refer to a connection between a controller and a switch. Apps can use this connection to send messages to the connected switch, which is also what \hl{program\_flow} requires it for.

Usually, you will get a \hl{datapath} object from an OpenFlow event. These events are triggered by \hl{Ryu} any time a switch sends a message to the controller, for example when the switch initially connects, or when it sends a packet to the controller. Events contain the datapath to the switch that sent the message. For details on events, see \cref{sec:development:events}.

Every \hl{datapath} object comes with an \hl{ofproto} and an \hl{ofproto\_parser} property. These contain objects representing the OpenFlow protocol version used on the given connection, and a parser for messages under that version. These properties can be accessed as:

\begin{python}
ofproto = datapath.ofproto
parser = datapath.ofproto_parser
\end{python}

They can make writing apps with multiple supported versions easier. Since all switches started by SDN-Cockpit use OpenFlow 1.3 however, you can also just use a hard coded 1.3 parser and ofproto by inclusion:

\begin{python}
import ryu.ofproto.ofproto_v1_3 as ofproto
import ryu.ofproto.ofproto_v1_3_parser as parser
\end{python}

\subsection{Events}\label{sec:development:events}

Some events in the network, specifically those pretaining to the controller, are reflected by ryu as events that your application can react to. In this document we will only cover the two most common types of events and how to use them, but many other types of events can be found at \href{https://ryu.readthedocs.io/en/latest/ryu_app_api.html#openflow-event-classes}{ryu.readthedocs.io\slash{}en\slash{}latest\slash{}ryu\_app\_api.html\#openflow-event-classes}.

\subsubsection{Reactive Flow Programming - Receiving Packets}\label{sec:reactive}

To allow reactive flow programming, the \hl{Ryu} controller generates an event whenever a new packet arrives at the controller. Furthermore, it offers a decorator (\hl{@set\_ev\_cls}) to designate a function to handle these events. You can implement the following function to add any specific packet handling logic you wish to perform:
\begin{python}
@set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
def packet_in_handler(self, ev):
	# your implementation here ...
\end{python}

The event is passed to the handler as the \hl{ev} parameter. It contains some useful information, for example the datapath through which the packet was received. Keep in mind again, that this datapath will also contain the connection's protocol:

\begin{python}
datapath = ev.msg.datapath
ofproto = datapath.ofproto
\end{python}

The event also contains the captured packet:

\begin{python}
data = ev.msg.data
\end{python}

\subsubsection{Proactive Flow Programming}\label{sec:proactive}

If you wish to perform any actions ahead of time and before individual packets are handled by the SDN controller, you can implement a handler for this specific purpose:
\begin{python}
@set_ev_cls(ofp_event.EventOFPSwitchFeatures,
    CONFIG_DISPATCHER)
def switch_features_handler(self, ev):
	# your implementation here ...
\end{python}

This will register the function \hl{switch\_features\_handler}
to \hl{Ryu} in such a way, that it will be invoked once a new switch has been detected and fully configured. You can use this function to install proactive flow rules. The implementation corresponds to the \hl{packet\_in\_handler} function, except that no packet data will be available.

For more details on SDN application programming see the \hl{demo.py} application in the \hl{apps} folder of the SDN-Cockpit directory tree and the online \hl{Ryu} documentation.

\iffalse % OFPFlowStatsRequest does not seem to work under OF 1.5, so we'll leave this section out for now.
\subsection{Concurrency}

If you should find a need to execute concurrent tasks within your controller application (e.g., polling flow statistics), you can use the \hl{Ryu} switching hub module. A switching hub allows you to register a handler routine that is executed in a concurrent fashion\footnote{Using the standard Python concurrency features instead of the \hl{Ryu} switching hub may lead to problems.}. This handler routine can then perform periodic tasks. The following gives an example of a simple monitoring solution, which polls switch statistics in an interval of 10 seconds:

\begin{python}[tabsize=4]
from ryu.lib import hub

class Monitoring(CockpitApp):
	def __init__(self, *args, **kwargs):
		# Spawn a new hub instance
		self.monitor_thread = hub.spawn(self.monitor)

	def monitor(self):
		datapath = # obtain a datapath reference...
		while True:
			# Poll all switches for statistics ...
			datapath.send_msg(
				parser.OFPFlowStatsRequest(datapath)
			)
			hub.sleep(10)

	@set_ev_cls(ofp_event.EventOFPFlowStatsReply,
		MAIN_DISPATCHER)
	def handle_flow_stats(self, ev):
		# Stat processing logic
\end{python}

First of all the new handler routine \hl{monitor} is registered via the \hl{hub.spawn} function during the execution of the constructor \hl{\_\_init\_\_}. This handler will be executed immediately.

The \hl{monitor} function then proceeds to send \hl{OFPFlowStatsRequest} messages to a switch, thereby instructing a switch to send its collected statistics to the controller.
The \hl{hub.sleep} function limits the execution of this step to every 10 seconds.
When the switch has sent the collected statistics, the \hl{Ryu} controller generates an \hl{EventOFPFlowStatsReply} event. We can designate a function to handle exactly this type of event
.
You can see how to register such a function in the example above: take a look at the \hl{handle\_flow\_stats} function.
This function finally serves to process the collected statistics.
You can find a more elaborate example at  \href{https://osrg.github.io/ryu-book/en/html/traffic_monitor.html}{osrg.github.io\slash{}ryu-book\slash{}en\slash{}html\slash{}traffic\_monitor.html}.
\fi

\section{Scenarios}\label{sec:scenarios}

Scenarios in SDN-Cockpit are controlled network situations that an SDN controller can be confronted with to test its behavior. %If you simply whish to get an understanding of what SDN-Cockpit does when testing your app, reading \cref{sec:scenario_overview} should be sufficient. If you whish to write your own scenario files, you should read \cref{sec:scenario_files} too.

% \subsection{Overview}\label{sec:scenario_overview}
Pressing the start button in the user interface for SDN-Cockpit after selecting a scenario and app to run will instruct SDN-Cockpit's backend to begin emulation, at which point a virtual network with the exact topology described in the scenario file's topology section will be set up. The user's SDN application and the Ryu controller will be started as well, and each switch of the topology will be instructed to connect to the controller. If the user ever stops emulation, the topology will be torn down and the SDN application and controller terminated.

When the user starts the automated evaluation, a series of tests, defined in the scenario file, are run. Every test has two parts: traffic events and observation points. A traffic event is a series of UDP packets with pre-defined ethernet, ip and udp headers sent from a specific host into the network. An observation point is a link in the network that is being monitored, meaning all traffic passing through this link is being captured (logged into a file without interfering with its transport). Later, the captured traffic is compared to a set of expectations, for example 'Exactly 100 packets, all with destination ip 10.0.0.1, should pass through this link'. If all expectations are met for all observation points, the test is marked a success.

To bring traffic events and observation points together, the SDN-Cockpit backend goes through the following steps for every test:
\begin{enumerate}
\item If the test requires a restart, restart the network and the controller, then wait for all switches to connect to the controller.
\item For every observation point: start monitoring the observation point's link.
\item For each traffic event in the test, send traffic from a host according to the event's definition.
\item Monitor the network and wait until no more test traffic is flowing.
\item Stop monitoring the observation points and read what traffic they captured.
\item Compare each observation point's expected traffic with the captured traffic and generate a report.
\end{enumerate}

\iffalse % we don't want to advertise the ability to write your own scenario files just yet, since that part of the app is still too buggy. We have started writing the user manual anyways, to gain an understanding of which parts of the scenario files are unintuitive or unnecessarily verbose
\subsection{Scenario Files}\label{sec:scenario_files}

Scenarios are defined in yaml files we call scenario files. If you would like to familiarize yourself with YAML syntax, \href{https://yaml.org/}{yaml.org} is the right place to start.

A scenario file's root contains up to four entries:
\begin{lstlisting}[language=yaml]
name: "Example"
description: "An example scenario"
topology:
	# ...
tests:
	# ...
\end{lstlisting}
The \hl{name} and \hl{description} entries do exactly what their names suggest and exist purely for the benefit of the user. The \hl{description} and \hl{tests} entries are optional and can be omitted.

\subsubsection{Topology}\label{sec:topo}

The topology section of a scenario file looks like this:

\begin{lstlisting}[language=yaml]
# ...
topology:
  hosts:
    - name: H1
      ipv4: "17.0.0.1"
    - name: H2
      ipv4: "21.0.0.1"

  switches:
    - name: s1
      dpid: 1

  links:
    - [H1, s1]
    - [H2, s1]
# ...
\end{lstlisting}

The \hl{hosts}, \hl{switches}, and \hl{links} entries are all required and should all have at least one entry.

Hosts are defined with a name and (optionally) an IPv4 address. When emulation starts, each host will have its default interface configured to respond to the given IP. This of course is only useful, if traffic that expects a response (e.g. a ping) arrives at the host. Since our test traffic uses UDP and does not expect a response, giving an IP to a host is not required.

Switches are also given a name and additionally a dpid. The SDN application can see the dpid in the \hl{datapath} object as \hl{datapath.id} and can therefore use it to reliably identify the different switches of the topology.

Links are simply lists with two entries, each being the name of a host or switch.

% \subsubsection{Tests}\label{sec:tests}
% \subsubsection{Traffic Generation}\label{sec:trafgen}
% \subsubsection{Observation Points}\label{sec:obsp}
\fi

\end{document}
