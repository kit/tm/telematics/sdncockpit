\documentclass{scrarticle}

\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\usepackage[utf8]{inputenc}

\usepackage{cleveref}

\graphicspath{{./graphics}}

\title{Use Case Analysis for SDN-Cockpit}
\date{\today}
\author{Jakob Gretenkort\\ Institut für Telematik, Karlsruher Institut für Technologie}

\begin{document}
\maketitle

\tableofcontents

\pagebreak

\section{Introduction}
SDN-Cockpit is an open-source teaching framework for Software-defined Networking. One of its main functions is the automatic evaluation of SDN-Controllers against network scenarios, which define a network topology, traffic events and expectations of how the controller will influence the network's response to those events. 

This document describes several broadly defined types of scenarios for which expectations on controller/network behavior would have to be defined in SDN-Cockpit. For each type of scenario, two different methods of defining expectation will be compared with regards to their capabilities and ease of use. This document is intended to help decide which of the two methods will be implemented and used in SDN-Cockpit.

\section{Constraints}
The tasks that will be worked on using SDN-Cockpit will most likely require the implementation of an SDN-Controller that causes some expected behavior of the network under load. These are also the expectations which should be checked by SDN-Cockpit to evaluate an implemented SDN controller for its correct solving of the task. While it would theoretically be feasible to check the rules installed by the controller on the network's switches and calculate from those whether or not network traffic would adhere to the expected behavior, we have decided that in order to make SDN-Cockpit more approachable and closer to real life, we will instead emulate networks and send traffic through them while comparing the observed behavior of that traffic against the expectations defined for the task.

This means that the only data available for an analysis of the network's behavior are traffic captures, either from the internals of the network's emulated nodes or from their interfaces.

We've all but settled on capturing traffic with tcpdump sessions attached to the emulated network's interfaces. In order to identify packets belonging to our test-traffic among the background noise and in order to track packets individually, each packet carries a unique ID in its payload. Because we do not want to confuse users by making unannounced changes to IDs while the packet is travelling through the network, these IDs stay the same throughout the packet's journey, even when packets get duplicated.

\section{Methods for defining Expectations}
There are two different ways to define expectations for how a Network should forward traffic. One is based on paths, while the other is based on obervation points.

\subsection{Observation Point Matching}
In observation point matching, each network traffic event of a scenario comes with a list of network links on which the packets produced by the traffic event are expected to be observed. SDN-Cockpit creates tcpdump instances on those links and checks, if the packets from the event actually show up in the expected quantity.

By default, packets observed on links that are not observation points are ignored.

\subsection{Path Matching} 
In path matching, each network traffic event of a scenario comes with a path, a tree structure who's vertecies correspond to nodes of the network and who's edges correspond to links of the network. 

Packets produced by a traffic event are expected to be observed on the links corresponding to the tree's edges in the exact order in which they appear in the tree from root to leaf. Only observed packet traces which match the tree from root to every leaf are accepted.

Besides network nodes, paths also allow for the matching of wildcards. For example, the path
\begin{lstlisting}
---
path:
  - node: 's1'
  - node: '*'
  - node: 's3'
---
\end{lstlisting}
would match any packet trace where the packet travelled across any node in between s1 and s3.
Similarly, replacing '*' with '**' matches any trace where the packet travelled across any number of nodes (including 0) between s1 and s3, as long as those nodes form a strict line without branching off to any other nodes.
Finally, replacing '*' with '***' matches any trace where the packet travelled across any number of nodes (including 0) between s1 and s3, even if the nodes in that part of the path include branches to other nodes.

To enable packets to disappear from one part of the network and reappear in another, for example because they got routed through the controller, the 'ctrl' node was introduced. So for example
\begin{lstlisting}
---
path:
  - node: 's1'
  - node: 'ctrl'
  - node: 's3'
  - node: 's4'
---
\end{lstlisting}
would match a path that shows the packet entering s1, then never exiting it, but instead reappearing at s3. It would however not match an immediate jump to s4 without passing through s3.

By default, packets observed on links that are not observation points lead to a failed match.

All of this already highlights one of the drawbacks of path matching: it is fairly complex and hard to understand, discouraging people from using it. However, it also shows its advantage: path matching automatically matches not only on whether or not a packet was observed on some links, but also about the order of those observations. 

\subsection{Shared Capabilities}
Both strategies have the following capabilities:
\begin{enumerate}
\item Checking a packet's direction of travel
\item Probabilistic expectations (for packets showing up only some times or more than once on a link)
\item Header checking (for when a switch modifies a packet's header in some expected way)
\end{enumerate}

\section{Use Cases and Comparison}
For ease of creating these graphics, all use cases will be examined for the following network:
\begin{figure}[!h]
	\centering
	\includegraphics{network.pdf}
	\caption{The basic network used for all following examples.}
	\label{fig:overview}
\end{figure}
This network is taken from what is currently task 2 of the PPSDN course tasks.

\subsection{Basic Connectivity}
Basic connectivity between two hosts can be achieved in a variety of ways, for example through broadcasts, point-to-point routes, or via the controller. A goot check for basic connectivity should be able to validate all of these.

\subsubsection{Observation Point Matching}
For the example network, an observation point definition for validating basic connectivity from h1 to h3 could look like this:
\begin{lstlisting}
---
origin: h1
expected:
  - link: ['s8', 'h3']
    node: ['h3']
    direction: incoming
---
\end{lstlisting}
If h3 were connected to multiple switches, we'd have to create new syntax and evaluation logic to express, that packets showing up on any of the connected links would be fine. For example, validating basic connectivity from h1 to s8 could look like this:
\begin{lstlisting}
---
origin: h1
expected:
  - link: [['s4', 's8']['s6', 's8']['s7', 's8']]
    node: ['s8']
    direction: incoming
---
\end{lstlisting}

\subsubsection{Path Matching}
For the example network, a path definition for validating basic connectivity between h1 and h3 could look like this:
\begin{lstlisting}
---
path:
  - node: 'h1'
  - node: '***'
  - node: 'ctrl'
  - node: '***'
  - node: 'h3'
---
\end{lstlisting}
Matching only packets coming in to h3 is implicit here.
This would only support one trip to the controller to skip network nodes, not multiple ones. For that, no syntax currently exists. Although it could be created as something like '****'.
If h3 were connected to multiple switches, this same path match would cover all of those connections implicitly.

\subsection{Specific Paths}
If a specific path through the network is expected to be taken by each packet, for example the shortest path, path matching obviously has quite an advantage.

\subsubsection{Observation Point Matching}
Validating the correct specific path is quite easy with observation point matching. For example, the shortest path from h1 to h3 in the example network would be matched by the following:
\begin{lstlisting}
---
origin: h1
expected:
  - link: ['h1', 's1']
  - link: ['s1', 's4']
  - link: ['s4', 's8']
  - link: ['s8', 'h3']
---
\end{lstlisting}
However, if one wants to not only validate that the correct path has been taken, but also that no other paths have been taken (for example to exclude broadcasting), then the matching must include at least all adjacent links too, with weight = 0:
\begin{lstlisting}
---
origin: h1
expected:
  - link: ['h1', 's1']
  - link: ['s1', 's4']
  - link: ['s1', 's2']
	weight = 0
  - link: ['s1', 's3']
	weight = 0
  - link: ['s4', 's8']
  - link: ['s4', 's3']
	weight = 0
  - link: ['s4', 's6']
	weight = 0
  - link: ['s4', 'h2']
	weight = 0
  - link: ['s8', 'h3']
  - link: ['s8', 's6']
	weight = 0
  - link: ['s8', 's7']
	weight = 0
---
\end{lstlisting}
To make this easier, we could introduce a new configuration option like 'unexpected-default-weight' for the expected weight of all links not in the 'expected' list. This would of course mean monitoring all of those links.

\subsubsection{Path Matching}
Validating a specific path is quite easy with path matching. One can simply write it down, and any deviation from the path, even just a stay packet leaving the path because a node has accidentally been configured to broadcast packets, will lead to a missmatch.
For example, the shortest path from h1 to h3 in the example network would be matched by the following:
\begin{lstlisting}
---
path:
  - node: 'h1'
  - node: 's1'
  - node: 's4'
  - node: 's8'
  - node: 'h3'
---
\end{lstlisting}

\subsection{Unspecific Linear Connectivity}
Unspecific linear connectivity is just like basic connectivity, except that the route which packets take through the network must be linear path (no branches, no skips via the controller). Verifying this is useful for checking the efficiency of the forwarding (no broadcasts, no trips to the controller), without restricting users to implementing a particular route.

A check for unspecific linear connectivity using path matching may look like this:
\begin{lstlisting}
---
path:
  - node: 'h1'
  - node: '**'
  - node: 'h3'
---
\end{lstlisting}
Observation point matching cannot be used to check for this.

\subsection{Branches}
Of course specific paths may not only include linear connections but also branches, sometimes with different weights on each branch. Consider for example a scenario where load-balancing is required. 50 percent of all packets from h1 shall go to h2, and another 50 percent to h4.

\subsubsection{Observation Point Matching}
\begin{lstlisting}
---
origin: h1
expected:
  - link: ['s7', 'h4']
	weight = 0.5
  - link: ['s4', 'h2']
	weight = 0.5
---
\end{lstlisting}

\subsubsection{Path Matching}
\begin{lstlisting}
---
path:
  - node: 'h1'
  - node: '**'
  - branch:
    - path:
      - '**'
	  - node: 'h2'
    - path:
      - '**'
      - node: 'h4'
---
\end{lstlisting}
Once more, this showcases unspecific linear paths. With this match, there may only be one point at which the path branches. If multiple branches should be allowed, use '***' as the last node before the branch instead.

\subsection{Merging and Crossing Paths}\label{MaC}
Remind yourself of the fact that to identify packets on the network, each packet belonging to our evaluation system carries a unique ID. This ID stays the same throughout a packet's journey through the network. When using path matching, this has a unique drawback. While observation point matching does not and can not care about the order in which a packet appears on different links, path matching does. This gives Path matching some of the abilities discussed so far, like the '**' wildcard, but it can also be a hinderance. When packets with the same ID enter the same node twice, and then at least one packet with the ID leaves afterwards, we cannot know whether the leaving packet was triggered by the first one that arrived or the second one. This means, that the path matching algorithm cannot construct a definitive graph of how the packet travelled through the network and that it has to guess one instead. Of course we can simply construct graphs for all possible options and then give the user the benefit of the doubt and just try to find one that fits, but at that point we are back to the behavior of observation point matching.

\subsection{Diagnostics}
Obviously, because paths come with an expected logical order in which things should occur, we can give much more detailed diagnostic information in case those expectations are not met, such as the first link a packet did not arrive on as expected. Observation point matching meanwhile would simply have to report the status of every link that did not observe the packet as expected, leaving it up to the user to find out where in the network the problem was caused.

\subsection{Implementation}
Observation point matching can be implemented very quickly and easily. One essentially only has to start a tcpdump on all links for which expectations were defined, go through each link's pcap file, parse out the packets (at the moment, this is already implemented) and compare the observed packets to the expectations.

Path matching on the other hand is quite difficult to get right. First, all links have to be monitored with tcpdump and those dumps have to be parsed. Then, the parsed packet traces from each link have to be combined, and all observations of a packet throughout a network have to be converted into that packet's path. For this, we have to first reconstruct the direction in which a packet passed over a link, which can be done by comparing the time at which the packet was observed on that link to the time at which the packet was observed on neighbouring links. By constructing a graph of the links where observations occurred, and subsequently using the aforementioned timestamp comparison to make the graph directed, we not only get the directions of travel but the entire path. This is also the part that goes wrong in \ref{MaC}.

This path of course may be a tree if a packet were duplicated and sent down two different routes along the way. Since our expectations are also defined as a tree which potentially comes with some repeatable wildcards and additional branches to the ones in the path, matching the two trees is not a simple equals check. Instead, a nondeterministic finite state automaton (NFSA) can be constructed with states corresponding to expected nodes and transitions corresponding to expected observations, which accepts the input of all expected linear subgraphs from the path tree's root to each of its leaves. The construction of this NFSA is not expanded upon further here. If we then make sure to count which states of the match NFSA were visited while matching each such linear path, we can actually ensure that all expectations are met.

\section{Conclusion}
The only real advantage that path matching possesses over observation point matching is the '**' mechanic. So long as we do not plan on constructing tasks which specifically require sending a packet along an unspecified but strictly linear path, I do not see any reason to implement this overly complicated system. The current tasks for PPSDN do not need this mechanic, neither do the tasks for the Telematics lecture or the SDN Lab.

\end{document}