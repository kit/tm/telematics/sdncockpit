# Generate the example graphs we need for this document
import copy

import matplotlib.pyplot as plt
import networkx as nx

EDGES = {
    ("h1", "s1"): {'arrows': False},
    ("h2", "s4"): {'arrows': False},
    ("h3", "s8"): {'arrows': False},
    ("h4", "s7"): {'arrows': False},
    ("s1", "s2"): {'arrows': False},
    ("s1", "s3"): {'arrows': False},
    ("s1", "s4"): {'arrows': False},
    ("s2", "s3"): {'arrows': False},
    ("s2", "s5"): {'arrows': False},
    ("s3", "s4"): {'arrows': False},
    ("s3", "s5"): {'arrows': False},
    ("s3", "s6"): {'arrows': False},
    ("s4", "s6"): {'arrows': False},
    ("s4", "s8"): {'arrows': False},
    ("s5", "s6"): {'arrows': False},
    ("s5", "s7"): {'arrows': False},
    ("s6", "s7"): {'arrows': False},
    ("s6", "s8"): {'arrows': False},
    ("s7", "s8"): {'arrows': False},
    ("ctrl", "s2"): {'style': 'dashed', 'arrows': False},
    ("ctrl", "s5"): {'style': 'dashed', 'arrows': False},
    ("ctrl", "s7"): {'style': 'dashed', 'arrows': False}
}

POS = {
    "h1": [-1,2],
    "h2": [2,2],
    "h3": [2,-2],
    "h4": [-2,-2],
    "s1": [0,2],
    "s2": [-1,1],
    "s3": [0,1],
    "s4": [1,1],
    "s5": [-1,0],
    "s6": [0,0],
    "s7": [-1,-1],
    "s8": [1,-1],
    "ctrl": [-2,0]
}

def draw_net(senders, path, file="test.pdf"):
    edges = copy.deepcopy(EDGES)

    # Delete overridden edges from edges
    for link in path:
        rev_link = tuple(reversed(link))
        if rev_link in edges:
            # normalize
            edges[link] = edges[rev_link]
            del edges[rev_link]

        if link in edges:
            edges[link].update(path[link])


    net = nx.DiGraph()
    net.add_edges_from(edges)

    passives = [node for node in net.nodes() if node not in senders]

    fixed = list(POS.keys())

    f = plt.figure()

    pos = nx.spring_layout(net, pos = POS, fixed = fixed, iterations = 1)
    nx.draw_networkx_nodes(net, pos, nodelist = senders, node_color = 'y', node_size = 450)
    nx.draw_networkx_nodes(net, pos, nodelist = passives, node_size = 450)
    nx.draw_networkx_labels(net, pos)
    for edge, properties in edges.items():
        nx.draw_networkx_edges(net, pos, edgelist = [edge], **properties)

    f.savefig(file, bbox_inches='tight')

def base_network():
    senders = []
    path = dict()

    draw_net(senders, path, file = "network.pdf")

def load_balance():
    senders = ['h1']
    properties = {'edge_color': 'y', 'arrows': True}
    properties_balanced = properties.copy()
    properties_balanced['style'] = 'dotted'
    path = {
        ('h1', 's1'): properties,
        ('s1', 's3'): properties,
        ('s3', 's6'): properties,
        ('s6', 's7'): properties_balanced,
        ('s7', 'h4'): properties_balanced,
        ('s6', 's4'): properties_balanced,
        ('s4', 'h2'): properties_balanced
    }

    draw_net(senders, path, file = "load_balancer.pdf")

def main():
    base_network()
    load_balance()

if __name__ == "__main__":
    main()