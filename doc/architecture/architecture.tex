\documentclass{scrarticle}

\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage[utf8]{inputenc}

\usepackage{cleveref}

\graphicspath{{./graphics}}

\title{Architecture of SDN-Cockpit}
\date{\today}
\author{Jakob Gretenkort\\ Institut für Telematik, Karlsruher Institut für Technologie}

\begin{document}
\maketitle

\tableofcontents

\pagebreak

\section{Introduction} \label{sec:introduction}
This document describes the software architecture of SDN-Cockpit, an open-source teaching framework for Software-defined Networking.

\subsection{Audience} \label{sec:audience}
This document is aimed at the developers of SDN-Cockpit and at users trying to understand the software to build their own applications upon it.

\section{Guidance} \label{sec:guidance}
The main functional requirements are as follows:

\begin{enumerate}
	\item Users shall be able to write controllers for software defined networking using the ryu controller (\url{https://ryu-sdn.org/}).
    \item Users shall be able to create easily exchangeable test scenarios consisting of a network topology, test traffic and expectations for the routing and transformation of that traffic as it moves through the network.
    \item Checking a controller against a scenario shall be as simple as klicking a button.
	\item Results from a test of a controller against a scenario shall be shown live to the user as much as possible. Also, they shall point out discrepancies between expected and observed behavior wherever possible.
    \item Users shall be able to interact with the scenario network directly during and after testing.
\end{enumerate}

\subsection{Goals} \label{sec:goals}
The architecture chosen aims to fullfill the following goals:
\begin{enumerate}
	\item \textbf{Ease of understanding:} The software's internals should provide a guide to those who seek to build their own evaluation for an sdn controller in other contexts. They should therefore be especially easily understood but also remain somewhat adaptable to other adjacent purposes.
    \item \textbf{Ease of installation:} The software should be easy to install and get started with to encourage use by students.
\end{enumerate}

\section{Overview} \label{sec:overview}
\begin{figure}
	\centering
	\includegraphics{sdnc-overview.drawio.pdf}
	\caption{Interactions between SDN-Cockpit components.}
	\label{fig:overview}
\end{figure}

As can be seen in \cref{fig:overview}, the actual evaluation runs inside of a virtual environment which exposes two user interfaces, the more important of which is the web-based interface. 

Here, users can code sdn-controllers and choose scenarios to test them against. When such a test is requested by the user, their sdn-controller's code is submitted to the webserver inside the virtual environment, which then triggers the frontend controller to request the test to be performed by the evaluation module.

The evaluation module constantly sends updates to the frontend controller throughout the test, which are in turn retrieved by the web interface from the webserver and displayed to the user.

Should a user want to use their own editor to program a controller, they may simply save their files to a special folder which is synchronized between their host system and the virtual environment. Any files in this folder also become available in the web-api for testing. Synchronizing between the two file editors (web-interface and synchronized folder) is one of the frontend controller's main tasks.

\section{Evaluation Module} \label{sec:evaluation}
\begin{figure}
	\centering
	\includegraphics{core-overview.drawio.pdf}
	\caption{The evaluation module's subdivisions.}
	\label{fig:core-overview}
\end{figure}
The most complex component of the overview seen in \cref{fig:overview} is the evaluation module. It has to manage a large number of interacting subprocesses and process some of their output in order to run a successful evaluation. To manage this complexity, it is split into a number of smaller modules as seen in \cref{fig:core-overview}.

To understand the evaluation module, it is useful to first give a high-level explanation of how evaluation works. The idea is to present an sdn-controller written by the software's user with a virtual network, provided by mininet. The controller will then program some flow rules onto the network's switches, altering their behavior both with regards to how they route traffic through the network and with regards to how they manipulate network packet's headers. We can therefore simply send some predefined traffic through the network, see how the network (and by extention the controller) handles it, and compare that to some predefined set of expectations to determine whether the controller is working correctly.

\subsection{Network} \label{sec:network}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{network.drawio.pdf}
	\caption{The extensions to mininet classes introduced by SDN-Cockpit.}
	\label{fig:network}
\end{figure}
Mininet is used to emulate a network for the user's sdn-controller to manage during evaluation. In order for us to be able to send predefined traffic into the network and in order for us to be able to monitor that traffic as it traverses the network, mininet's python API is extended as shown in \cref{fig:mininet}. 

Links are given functions that allow SDN-Cockpit to monitor them through an abstract interface which hides the tcpdump-based implementation. Users can start and stop monitoring a link by only dealing with monitor descriptors (simple ids referencing the internally kept list of tcpdump processes). Stopping a monitor yields the path of the captured pcap file, usually residing in /tmp.

Mininet's host class is also extended by adding a function to generate test-traffic for the evaluation. This does not completely hide the implementation with trafgen however, since users of the module need to pass the string encoded contents of a trafgen config file into the host's send() function.

\subsection{Trafgen} \label{sec:trafgen}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{trafgen.drawio.pdf}
	\caption{The trafgen module's classes.}
	\label{fig:trafgen}
\end{figure}
As mentioned in \cref{sec:network}, SDN-Cockpit's TrafgenHosts can send traffic defined by trafgen configuration files. The trafgen module provides classes to help create these files. Noteworthy in this module is IDUDPTrafgenCfg, which can be used to create config files that cause trafgen to generate streams of udp packets that each carry a unique ID as their payload, allowing SDN-Cockpit to track them individually as they move through a network. RandomUDPTrafgenCfg allows random generation of UDPTrafgenCfgs to allow for randomized testing of controllers.

\subsection{Trace} \label{sec:trace}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{trace.drawio.pdf}
	\caption{The trace classes.}
	\label{fig:trace}
\end{figure}
SDN-Cockpit only really cares about the behavior of the network with regards to its predefined test traffic. To help filter this test traffic from the mass of all traffic captured in the network during evaluation, the trace module has classes to extract only those packets from pcap files that were generated from the trafgen module's IDUDPTrafgenCfg. The IDs in those packets can easily be decoded and accessed by using the IdentifiedPacket class.

\subsection{Scenario} \label{sec:scenario}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{scenario.drawio.pdf} 
	\caption{The classes representing an SDN-Cockpit scenario.}
	\label{fig:scenario}
\end{figure}
The Scenario class and its components seen in \cref{fig:scenario} represent a scenario against which a controller can be tested. It consists of a short description, a network topology and a series of Tests. Each Test is comprised of a set of RandomTrafficEvents, each of which is basically a description of a bit of randomized, predefined traffic to be sent out into the network as part of the evaluation. Tests also carry lists of ObservationPoints, each of which defines a link in the network on which part of or all of the test traffic generated by the RandomizedTrafficEvents is supposed to show up. These observation points also carry rules for what they want packets in the test traffic to look like as they pass the observed network link.

Mostly, classes in this module are dataclasses without much functionality. Notable exceptions concern the generation of actual traffic events from random ones and the check() function of ObservationPoints that can determine if a list of IdentifiedPackets, typically captured on the observed link, meets the ObservationPoint's expectations.

\subsection{Scenario-Parser} \label{sec:scenarip-parser}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{scenario-parser.drawio.pdf} 
	\caption{The scenario-parser module.}
	\label{fig:scenario-parser}
\end{figure}
The Parser class takes scenario-files (yaml files with a specific structure, which is documented elsewhere), and parses them into Scenario objects. It provides tremendous amounts of feedback to the user while doing so, thus enabling users to write their own scenario files.

\subsection{Webserver}
TODO
\subsection{Web-GUI}
TODO

\end{document}