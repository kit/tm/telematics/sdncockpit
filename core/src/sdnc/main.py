import argparse
import logging
import threading
from pathlib import Path

from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO

from sdnc.api.api import build_bp
from sdnc.api.json_provider import SDNCJSONProvider
from sdnc.api.virtfs import VirtFS
from sdnc.cli.api_cli import build_bp_cli
from sdnc.cli.cli import CLI
from sdnc.emulator.emulator import Emulator

logging.getLogger("werkzeug").disabled = True

HOST = "0.0.0.0"
PORT = 10080
FS_ROOT = Path("/tmp/sdnc_data")


def flask_thread(host: str, port: int, emulator: Emulator, fs: VirtFS, cli: CLI):
    app = Flask(__name__)
    app.json_provider_class = SDNCJSONProvider
    app.json = SDNCJSONProvider(app)
    socketio = SocketIO()
    CORS(app, resources={r"/api/*": {"origins": "*"}})

    app.register_blueprint(build_bp(emulator, fs, socketio))
    app.register_blueprint(build_bp_cli(cli))

    socketio.init_app(app, json=app.json, cors_allowed_origins="*")
    socketio.run(app, host=host, port=port, debug=True, use_reloader=False)


def main():
    options = parse_options()
    emulator = Emulator()
    fs = VirtFS(Path(options.fs_root))
    cli = CLI(emulator)
    threading.Thread(
        target=flask_thread, args=[options.host, options.port, emulator, fs, cli]
    ).start()

    cli.run()


def parse_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-d",
        "--directory",
        help="The directory to which the virtual file system API will be mapped.",
        dest="fs_root",
        type=str,
        default=FS_ROOT,
    )

    parser.add_argument(
        "-l",
        "--host",
        help="The address on which to start the API webserver.",
        dest="host",
        type=str,
        default=HOST,
    )

    parser.add_argument(
        "-p",
        "--port",
        help="The port on which the API webserver will listen.",
        dest="port",
        type=int,
        default=PORT,
    )

    options = parser.parse_args()

    return options


if __name__ == "__main__":
    main()
