"""
This file contains type definitions consistent across the module.
"""
from __future__ import annotations

from typing import Union

Numeric = Union[int, float]
