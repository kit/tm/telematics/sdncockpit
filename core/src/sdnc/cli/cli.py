"""
This module contains the CLI for SDNC.
It makes heavy use of code from the mininet CLI, but omits much of what
makes the mininet CLI work in different environments, since SDNC comes
with its own standardized environment.
"""

import _thread
import atexit
import errno
import os
import readline
import select
import sys
import time
from cmd import Cmd
from pathlib import Path
from subprocess import call
from typing import IO, Any

from mininet.log import error, info, output
from mininet.util import dumpNodeConnections, dumpPorts, quietRun

from sdnc.emulator.emulator import Emulator, NetBusyError, NetStoppedError
from sdnc.emulator.scenario_net import ScenarioNet

_SPHINX_BUILD = bool(os.environ.get("SPHINX_BUILD", ""))


def _netlock(f):
    if _SPHINX_BUILD:
        return f  # to generate docs correctly

    def wrapper(*args, **kwargs):
        cli_instance = args[0]
        # pylint: disable-next=protected-access
        try:
            with cli_instance.emulator.lock():
                return f(*args, **kwargs)
        except NetBusyError:
            error(
                "Failed to execute command. The network is busy.\n"
                "Please stop all currently running network actions and try again.\n\n"
            )

    return wrapper


class CLI(Cmd):
    """
    The SDN Cockpit command line.
    """

    readline_inited = False
    prompt = "cockpit> "

    def __init__(
        self, emulator: Emulator, stdin: IO[str] = sys.stdin, **kwargs: Any
    ) -> None:
        self.emulator = emulator

        self.in_poller = select.poll()
        self.in_poller.register(stdin)

        super().__init__(stdin=stdin, **kwargs)

    @classmethod
    def init_readline(cls) -> None:
        """
        Set up history.
        """
        if cls.readline_inited:
            return
        cls.readline_inited = True

        history_path = Path.home() / ".sdnc_history"
        if history_path.is_file():
            readline.read_history_file(str(history_path))  # type: ignore
            readline.set_history_length(1000)  # type: ignore

        def write_history() -> None:
            try:
                readline.write_history_file(history_path)  # type: ignore
            except IOError:
                # Ignore probably spurious IOError
                pass

        atexit.register(write_history)

    def run(self) -> None:
        "Run our cmdloop()"
        self.init_readline()
        while True:
            try:
                # Make sure no nodes are still waiting
                try:
                    for node in self._sn().values():
                        while node.waiting:
                            info("stopping", node, "\n")
                            node.sendInt()
                            node.waitOutput()
                except NetStoppedError:
                    pass
                # if self._isatty():
                #    quietRun('stty echo sane intr ^C')
                self.cmdloop()
            except KeyboardInterrupt:
                try:
                    output("Interrupt\n")
                # pylint: disable-next=broad-exception-caught
                except Exception:
                    pass
            except NetStoppedError:
                error("Error: The network is stopped.\n")

    def sigint(self) -> None:
        _thread.interrupt_main()

    def emptyline(self) -> bool:
        return True

    def precmd(self, line):
        "allow for comments in the cli"
        if "#" in line:
            line = line.split("#")[0]
        return line

    HELPSTR = (
        "You may also send a command to a node using:\n"
        "  <node> command {args}\n"
        "For example:\n"
        "  cockpit> h1 ifconfig\n"
        "\n"
        "The interpreter automatically substitutes IP addresses\n"
        "for node names when a node is the first arg, so commands\n"
        "like\n"
        "  cockpit> h2 ping h3\n"
        "should work.\n"
        "\n"
        "Some character-oriented interactive commands require\n"
        "noecho:\n"
        "  cockpit> noecho h2 vi foo.py\n\n"
    )

    def do_help(self, line):  # pylint: disable=arguments-renamed
        "Describe available CLI commands."
        Cmd.do_help(self, line)
        if line == "":
            output(self.HELPSTR)

    @_netlock
    def do_nodes(self, _):
        "List all nodes."
        nodes = " ".join(sorted(self._sn()))
        output(f"available nodes are: \n{nodes}\n")

    @_netlock
    def do_ports(self, _):
        "display ports and interfaces for each switch"
        dumpPorts(self._sn().switches)

    @_netlock
    def do_net(self, _):
        "List network connections."
        dumpNodeConnections(self._sn().values())

    @_netlock
    def do_sh(self, line):
        """Run an external shell command
        Usage: sh [cmd args]"""
        assert self  # satisfy pylint and allow override
        call(line, shell=True)

    @_netlock
    def do_pingall(self, line):
        "Ping between all hosts."
        self._sn().pingAll(line)

    @_netlock
    def do_pingpair(self, _):
        "Ping between first two hosts, useful for testing."
        self._sn().pingPair()

    @_netlock
    def do_pingallfull(self, _):
        "Ping between all hosts, returns all ping results."
        self._sn().pingAllFull()

    @_netlock
    def do_pingpairfull(self, _):
        "Ping between first two hosts, returns all ping results."
        self._sn().pingPairFull()

    @_netlock
    def do_iperf(self, line):
        """Simple iperf TCP test between two (optionally specified) hosts.
        Usage: iperf node1 node2"""
        args = line.split()
        if not args:
            self._sn().iperf()
        elif len(args) == 2:
            hosts = []
            err = False
            for arg in args:
                if arg not in self._sn():
                    err = True
                    error(f"node '{arg}' not in network\n")
                else:
                    hosts.append(self._sn()[arg])
            if not err:
                self._sn().iperf(hosts)
        else:
            error("invalid number of args: iperf src dst\n")

    @_netlock
    def do_iperfudp(self, line):
        """Simple iperf UDP test between two (optionally specified) hosts.
        Usage: iperfudp bw node1 node2"""
        args = line.split()
        if not args:
            self._sn().iperf(l4Type="UDP")
        elif len(args) == 3:
            udp_bw = args[0]
            hosts = []
            err = False
            for arg in args[1:3]:
                if arg not in self._sn():
                    err = True
                    error(f"node '{arg}' not in network\n")
                else:
                    hosts.append(self._sn()[arg])
            if not err:
                self._sn().iperf(hosts, l4Type="UDP", udpBw=udp_bw)
        else:
            error(
                "invalid number of args: iperfudp bw src dst\n" + "bw examples: 10M\n"
            )

    @_netlock
    def do_intfs(self, _):
        "List interfaces."
        for node in self._sn().values():
            intfs = ",".join(node.intfNames())
            output(f"{node.name}: {intfs}\n")

    @_netlock
    def do_dump(self, _):
        "Dump node info."
        for node in self._sn().values():
            output(f"{repr(node)}\n")

    @_netlock
    def do_link(self, line):
        """Bring link(s) between two nodes up or down.
        Usage: link node1 node2 [up/down]"""
        args = line.split()
        if len(args) != 3:
            error("invalid number of args: link end1 end2 [up down]\n")
        elif args[2] not in ["up", "down"]:
            error("invalid type: link end1 end2 [up down]\n")
        else:
            self._sn().configLinkStatus(*args)

    def do_noecho(self, line):
        """Run an interactive command with echoing turned off.
        Usage: noecho [cmd args]"""
        if self._isatty():
            quietRun("stty -echo")
        self.default(line)
        if self._isatty():
            quietRun("stty echo")

    def do_dpctl(self, line):
        """Run dpctl (or ovs-ofctl) command on all switches.
        Usage: dpctl command [arg1] [arg2] ..."""
        args = line.split()
        if len(args) < 1:
            error("usage: dpctl command [arg1] [arg2] ...\n")
            return
        for sw in self._sn().switches:
            dashes = "-" * (79 - len(sw.name) - 5)
            output(f"*** {sw.name} {dashes} \n")
            output(sw.dpctl(*args))

    def do_time(self, line):
        "Measure time taken for any command in Mininet."
        start = time.time()
        self.onecmd(line)
        elapsed = time.time() - start
        self.stdout.write(f"*** Elapsed time: {elapsed:0.6f} secs\n")

    def do_links(self, _):
        "Report on links"
        for link in self._sn().links:
            output(link, link.status(), "\n")

    def do_switch(self, line):
        "Starts or stops a switch"
        args = line.split()
        if len(args) != 2:
            error("invalid number of args: switch <switch name>" "{start, stop}\n")
            return
        sw = args[0]
        command = args[1]
        if sw not in self._sn() or self._sn().get(sw) not in self._sn().switches:
            error(f"invalid switch: {args[ 1 ]}\n")
        else:
            sw = args[0]
            command = args[1]
            if command == "start":
                self._sn().get(sw).start(self._sn().controllers)
            elif command == "stop":
                self._sn().get(sw).stop(deleteIntfs=False)
            else:
                error("invalid command: " "switch <switch name> {start, stop}\n")

    def do_wait(self, _):
        "Wait until all switches have connected to a controller"
        self._sn().waitConnected()

    def default(self, line):
        """Called on an input line when the command prefix is not recognized.
        Overridden to run shell commands when a node is the first
        CLI argument.  Past the first CLI argument, node names are
        automatically replaced with corresponding IP addrs."""

        first, args, line = self.parseline(line)

        if first in self._sn():
            if not args:
                error(f"*** Please enter a command for node: {first} <cmd>\n")
                return
            node = self._sn()[first]
            rest = args.split(" ")
            # Substitute IP addresses for node names in command
            # If updateIP() returns None, then use node name
            rest = [
                self._sn()[arg].defaultIntf().updateIP() or arg
                if arg in self._sn()
                else arg
                for arg in rest
            ]
            rest = " ".join(rest)
            # Run cmd on node:
            node.sendCmd(rest)
            self.wait_for_node(node)
        else:
            error(f"*** Unknown command: {line}\n")

    def wait_for_node(self, node):
        "Wait for a node to finish, and print its output."
        # Pollers
        node_poller = select.poll()
        node_poller.register(node.stdout)
        both_poller = select.poll()
        both_poller.register(self.stdin, select.POLLIN)
        both_poller.register(node.stdout, select.POLLIN)
        if self._isatty():
            # Buffer by character, so that interactive
            # commands sort of work
            quietRun("stty -icanon min 1")
        while True:
            try:
                both_poller.poll()
                if is_readable(self.in_poller):
                    key = self.stdin.read(1)
                    node.write(key)
                if is_readable(node_poller):
                    data = node.monitor()
                    output(data)
                if not node.waiting:
                    break
            except KeyboardInterrupt:
                # There is an at least one race condition here, since
                # it's possible to interrupt ourselves after we've
                # read data but before it has been printed.
                node.sendInt()
            except select.error as e:
                # pylint: disable=unpacking-non-sequence
                # pylint: disable=unbalanced-tuple-unpacking
                errno_, errmsg = e.args
                if errno_ != errno.EINTR:
                    error(f"select.error: {errno_}, {errmsg}")
                    node.sendInt()

    def _sn(self) -> ScenarioNet:
        if self.emulator.is_running():
            sn = self.emulator.net()
            if sn is not None:
                return sn
        raise NetStoppedError()

    def _isatty(self):
        "Is our standard input a tty?"
        return os.isatty(self.stdin.fileno())


# Helper functions


def is_readable(poller):
    "Check whether a Poll object has a readable fd."
    for fdmask in poller.poll(0):
        mask = fdmask[1]
        if mask & select.POLLIN:
            return True
        return False
