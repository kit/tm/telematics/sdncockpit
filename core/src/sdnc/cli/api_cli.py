from flask import Blueprint, jsonify

from sdnc.cli.cli import CLI


def build_bp_cli(cli: CLI) -> Blueprint:
    api = Blueprint("api_cli", __name__)

    @api.route("/api/cli/sigint", methods=["POST"])
    def sigint():
        cli.sigint()

        return jsonify({}), 200

    return api
