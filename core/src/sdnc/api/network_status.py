from dataclasses import dataclass
from typing import Dict, Type

from sdnc.emulator.network_actions.network_action import NetworkAction
from sdnc.emulator.progress_report import ProgressReport


@dataclass
class NetworkStatusReport(ProgressReport):
    """
    A report on the current status of the network.
    """

    network_running: bool
    actions_running: Dict[Type[NetworkAction], bool]
