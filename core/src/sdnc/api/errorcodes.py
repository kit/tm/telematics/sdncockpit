"""
The errorcodes module contains an enum filled with codes for all errors
the server may want to communicate to the client.

However, beyond this, the module can also be executed as its own script,
in which case it will generate the static/js/api/generated/errorcodes.js
file.
That file will contain definitions for exceptions corresponding to all
errorcodes listed in the :class:`Errorcodes` enum.
It will also contain a function which can be used to translate a code
(presumably received in communication with the server) into an instance
of the corresponding exception.
"""

from enum import IntEnum
from pathlib import Path

from sdnc.api.js_gen import JSGen


class Errorcodes(IntEnum):
    """
    Each value in this enum is an integer code which signifies some type
    of error. The codes can be used by the webserver to communicate
    those errors to the client.
    """

    # file system: 11xx
    FILE_SYSTEM = 1000  # catch all
    FILE_NOT_FOUND = 1001
    FILE_EXISTS = 1002
    NOT_A_DIRECTORY = 1003
    IS_A_DIRECTORY = 1004
    SAME_FILE = 1005
    # network: 12xx
    NETWORK_RUNNING = 1201
    NETWORK_STOPPED = 1202
    NETWORK_BUSY = 1203
    UNKNOWN_ACTION = 1250
    # scenario: 15xx
    SCENARIO_FILE_SYNTAX = 1501
    SCENARIO_FILE_SEMANTIC = 1502
    # controller: 16xx
    CONTROLLER_CONNECT_TIMEOUT = 1601
    # logs: 17xx
    READ_PAST_NEXT = 1701


class _ErrorcodeJSGen(JSGen):
    """
    This class just holds the procedural code for generating errorcodes.js.
    """

    def codename_to_exception(self, codename):
        except_name = "".join(x.capitalize() for x in codename.split("_"))
        except_name += "Error"
        return except_name

    def write_exceptions(self):
        self.write_centered("EXCEPTIONS")
        self.write()
        self.write("export class InvalidCodeError extends Error {}")
        self.write()
        for code in Errorcodes:
            except_name = self.codename_to_exception(code.name)
            self.write(f"export class {except_name} extends Error {{")
            self.write('constructor(message="", data={}, ...args) {', 1)
            self.write("super(message, args);", 2)
            self.write(f'this.name = "{except_name}";', 2)
            self.write("this.data = data;", 2)
            self.write("}", 1)
            self.write("}")
            self.write("")
        self.write()

    def write_conversion(self):
        self.write_centered("CONVERSION")
        self.write()
        self.write("export class APIErrorConverter {")
        self.write("convert(code, message, data) {", 1)
        self.write("switch(code) {", 2)
        for code in Errorcodes:
            except_name = self.codename_to_exception(code.name)
            self.write(f"case {code.value}:", 3)
            self.write(f"return new {except_name}(message, data);", 4)
        self.write("default:", 3)
        self.write("return new InvalidCodeError();", 4)
        self.write("}", 2)
        self.write("}", 1)
        self.write("}")
        self.write()

    def generate(self):
        self.write_header("errorcodes.py")
        self.write_exceptions()
        self.write_conversion()


def _main():
    directory = Path("../../frontend/src/js/api/generated")
    directory.mkdir(parents=False, exist_ok=True)
    path = directory / "errorcodes.js"
    with path.open("w+", encoding="utf-8") as file:
        _ErrorcodeJSGen(file).generate()


if __name__ == "__main__":
    _main()
