"""
The actioncodes module contains an enum filled with codes for all
network actions the client and server may want to communicate.

However, beyond this, the module can also be executed as its own script,
in which case it will generate the file
static/js/api/generated/actioncodes.js, which will contain an enum with
the same information.
"""

from enum import Enum
from pathlib import Path

from sdnc.api.js_gen import JSGen


class Actioncodes(str, Enum):
    """
    Each value in this enum is a string code which signifies some
    network action. The codes can be used in communication between
    server and client.
    """

    EVALUATION = "evaluation"
    PINGALL = "pingall"


class _ActioncodeJSGen(JSGen):
    """
    This class just holds the procedural code for generating
    actioncodes.js.
    """

    def write_enum(self):
        self.write("export const Actioncodes = {")
        for code in Actioncodes:
            self.write(f"{code.name}: '{code.value}',", 1)
        self.write("};")
        self.write()

    def generate(self):
        self.write_header("actioncodes.py")
        self.write_enum()


def _main():
    directory = Path("../../frontend/src/js/api/generated")
    directory.mkdir(parents=False, exist_ok=True)
    path = directory / "actioncodes.js"
    with path.open("w+", encoding="utf-8") as file:
        _ActioncodeJSGen(file).generate()


if __name__ == "__main__":
    _main()
