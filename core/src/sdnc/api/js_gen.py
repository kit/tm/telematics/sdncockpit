from typing import TextIO


class JSGen:
    """
    A helper class for generating javascript from python.
    """

    def __init__(self, file: TextIO, tab_size: int = 4, line_width: int = 80) -> None:
        self.file = file
        self.tab_size = tab_size
        self.line_width = line_width

    def write(self, line: str = "", indent: int = 0):
        """
        Writes the given line with the given indentation.
        """
        self.file.write((" " * indent * self.tab_size) + line + "\n")

    def write_centered(self, text: str):
        """
        Writes a centered comment.
        """
        stars = self.line_width - len(text) - 4
        line = "/"
        line += "*" * (stars // 2)
        line += " " + text + " "
        line += "*" * (stars - stars // 2)
        line += "/"
        self.write(line)

    def write_header(self, generator_name: str):
        self.write_centered("!DO NOT EDIT!")
        self.write_centered("!THIS IS A GENERATED FILE!")
        self.write_centered(f"see: {generator_name}")
        self.write()
