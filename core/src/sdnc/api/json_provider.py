import dataclasses
import inspect
from ipaddress import IPv4Address, IPv4Network
from pathlib import Path
from typing import Any, Dict, Type

from flask.json.provider import DefaultJSONProvider
from mininet import topo
from scapy.packet import Packet

from sdnc.api.actioncodes import Actioncodes
from sdnc.api.network_status import NetworkStatusReport
from sdnc.emulator.controller import (
    OutputReport,
    ProcessReadEndReport,
    ProcessReadStartReport,
)
from sdnc.emulator.emulator import StartEmulatorReport, StopEmulatorReport
from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.netprovider import (
    RestartNetworkReport,
    StartNetworkReport,
    StopNetworkReport,
)
from sdnc.emulator.network_actions.evaluation import (
    AutoEval,
    StartEventReport,
    StartTestReport,
    TestFeedbackReport,
)
from sdnc.emulator.network_actions.network_action import (
    AbortNetworkActionReport,
    CompleteNetworkActionReport,
    NetworkAction,
    StartNetworkActionReport,
)
from sdnc.emulator.network_actions.pingall import Pingall, PingReport
from sdnc.emulator.progress_report import ProgressReport
from sdnc.emulator.range_list import IntRange, IPRange, MACRange, RangeList
from sdnc.emulator.scenario import (
    HeaderRule,
    ObservationPoint,
    RandomTrafficEvent,
    Scenario,
    Test,
    TrafficEvent,
)
from sdnc.emulator.scenario_parser import ScenarioSemanticError, ParseStack
from sdnc.emulator.statistics import FlowTable
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg, UDPTrafgenCfg

REPORT_TYPE: Dict[Type[ProgressReport], str] = {
    NetworkStatusReport: "network_status",
    StartEmulatorReport: "start_emulator",
    StopEmulatorReport: "stop_emulator",
    StartNetworkReport: "start_network",
    StopNetworkReport: "stop_network",
    RestartNetworkReport: "restart_network",
    StartNetworkActionReport: "start_network_action",
    AbortNetworkActionReport: "abort_network_action",
    CompleteNetworkActionReport: "complete_network_action",
    PingReport: "ping",
    StartTestReport: "start_test",
    TestFeedbackReport: "test_feedback",
    StartEventReport: "start_event",
    ProcessReadStartReport: "process_read_start",
    ProcessReadEndReport: "process_read_end",
    OutputReport: "output",
}

NET_ACTION_TYPE: Dict[Type[NetworkAction], str] = {
    AutoEval: Actioncodes.EVALUATION,
    Pingall: Actioncodes.PINGALL,
}

STRING_TYPES = (IPv4Address, IPv4Network, MACaddr, Path, ParseStack)


class SDNCJSONProvider(DefaultJSONProvider):
    """
    A json encoder to serialize some classes which we cannot jsonify
    otherwise.
    """

    def default(self, o: Any):
        if inspect.isclass(o):
            if o in REPORT_TYPE:
                return REPORT_TYPE[o]
            if o in NET_ACTION_TYPE:
                return NET_ACTION_TYPE[o]
            return o.__name__

        if isinstance(o, STRING_TYPES):
            return str(o)

        if isinstance(o, Packet):
            return o.summary()

        if isinstance(o, ScenarioSemanticError):
            return o.alert

        if isinstance(o, IntRange):
            return {
                "minimum": o.minimum,
                "maximum": o.maximum,
            }

        if isinstance(o, MACRange):
            return {
                "minimum": o.minimum,
                "maximum": o.maximum,
            }

        if isinstance(o, IPRange):
            return {
                "net": o.net,
            }

        if isinstance(o, RangeList):
            return list(o.ranges)

        if isinstance(o, RandomUDPTrafgenCfg):
            return {
                "ethernet_dst": o.ethernet_dst,
                "ethernet_src": o.ethernet_src,
                "ipv4_flags": o.ipv4_flags,
                "ipv4_ttl": o.ipv4_ttl,
                "ipv4_src": o.ipv4_src,
                "ipv4_dst": o.ipv4_dst,
                "udp_src_port": o.udp_src_port,
                "udp_dst_port": o.udp_dst_port,
            }

        if isinstance(o, UDPTrafgenCfg):
            return {
                "ethernet_dst": o.ethernet_dst,
                "ethernet_src": o.ethernet_src,
                "ipv4_flags": o.ipv4_flags,
                "ipv4_ttl": o.ipv4_ttl,
                "ipv4_src": o.ipv4_src,
                "ipv4_dst": o.ipv4_dst,
                "udp_src_port": o.udp_src_port,
                "udp_dst_port": o.udp_dst_port,
            }

        if isinstance(o, HeaderRule):
            return {
                "proto": str(o.proto_cls.__name__),
                "field": o.field,
                "range": o.val_range,
            }

        if isinstance(o, ObservationPoint):
            return {
                "link": list(o.link),
                "node": o.node,
                "direction": o.direction,
                "count": o.count,
                "header_rules": o.header_rules,
            }

        if isinstance(o, RandomTrafficEvent):
            return {
                "event_count": o.event_count,
                "host": o.host,
                "headers": o.headers,
                "pkt_count": o.pkt_count,
                "time": o.time,
            }

        if isinstance(o, TrafficEvent):
            return {
                "host": o.host,
                "headers": o.headers,
                "pkt_count": o.pkt_count,
                "time": o.time,
                "event_id": o.event_id,
                "test_id": o.test_id,
            }

        if isinstance(o, Test):
            return {
                "name": o.name,
                "description": o.description,
                "restart": o.restart,
                "rnd_events": o.rnd_events,
                "events": o.events,
                "obs_points": o.obs_points,
                "test_id": o.test_id,
            }

        if isinstance(o, Scenario):
            return {
                "name": o.name,
                "description": o.description,
                "topo": o.topo,
                "tests": o.tests,
            }

        if isinstance(o, topo.Topo):
            return {
                "nodes": [(n, o.nodeInfo(n)) for n in o.nodes()],
                "links": o.links(withInfo=True),
            }

        if isinstance(o, FlowTable):
            return o.table

        if isinstance(o, ProgressReport):
            serialized = {
                field.name: getattr(o, field.name) for field in dataclasses.fields(o)
            }
            serialized["type"] = type(o)
            return serialized

        if dataclasses.is_dataclass(o):
            # Why do this instead of dataclass.asdict?
            # Dataclass.asdict is recursive, so nested dataclasses like
            # ProgressReport will not be serialized correctly.
            return {
                field.name: getattr(o, field.name) for field in dataclasses.fields(o)
            }

        return super().default(o)
