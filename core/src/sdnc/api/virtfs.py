from __future__ import annotations

import errno
import os
import shutil
import time
from pathlib import Path
from typing import Callable, Dict, List
from zipfile import ZipFile

from watchdog.events import FileSystemEventHandler
from watchdog.observers.polling import PollingObserver
from werkzeug import security

_SPHINX_BUILD = bool(os.environ.get("SPHINX_BUILD", ""))


def _convert_exceptions(f):
    if _SPHINX_BUILD:
        return f  # to generate docs correctly

    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except OSError as e:
            vfs_instance = args[0]
            if e.filename:
                e.filename = str(vfs_instance.r_to_v(Path(e.filename)))
            if e.filename2:
                e.filename2 = str(vfs_instance.r_to_v(Path(e.filename2)))
            raise

    return wrapper


class FSAnyEventHandler(FileSystemEventHandler):
    """
    A simple file system event handler which forwards the path of any
    modified file / directory to a callback.
    """

    def __init__(self, callback: Callable[[str], None]) -> None:
        super().__init__()
        self._callback = callback

    def on_closed(self, event):
        self._callback(event.src_path)
        return super().on_closed(event)

    def on_created(self, event):
        self._callback(event.src_path)
        return super().on_created(event)

    def on_deleted(self, event):
        self._callback(event.src_path)
        return super().on_deleted(event)

    def on_modified(self, event):
        self._callback(event.src_path)
        return super().on_modified(event)

    def on_moved(self, event):
        self._callback(event.src_path)
        self._callback(event.dest_path)
        return super().on_moved(event)


class VirtFS:
    """
    Some utilities for safely interacting with a virtual file system
    hosted in some folder of the real file system.

    All paths in exceptions raised by methods of this class are rpaths.
    """

    def __init__(self, base_dir: Path):
        if not base_dir.is_dir():
            base_dir.mkdir(parents=True, exist_ok=True)
        try:
            base_dir.relative_to("/tmp")  # throws if base not in tmp
            # base_dir is in /tmp, we should warn the user about that
            (
                base_dir / Path("WARNING: RUNNING IN TMP DIR, PROGRESS WILL BE LOST")
            ).touch()
        except ValueError:
            pass
        self.base_dir = base_dir.resolve()

        self._handlers: List[Callable[[Path], None]] = []

        self._handler = FSAnyEventHandler(lambda path: self._handle_change(Path(path)))
        self._observer = PollingObserver()
        self._observer.schedule(self._handler, str(self.base_dir), recursive=True)
        self._observer.start()

    def r_to_v(self, rpath: Path) -> Path:
        """
        Converts between a real and a virtual path.

        Args:
            rpath: An absolute path in the real file system.

        Returns:
            An absolute path in the virtual file system pointing to the
            same resource as the given rpath.

        Raises:
            ValueError: If the given rpath does not point
                to a location in the virtual file system.
        """
        return Path("/") / rpath.resolve().relative_to(self.base_dir)

    def v_to_r(self, vpath: Path) -> Path:
        """
        Converts between a virtual and a real path.

        Args:
            vpath: An absolute path in the virtual file system.

        Returns:
            An absolute path in the real file system pointing to the
            same resource as the given ``vpath``.

        Raises:
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        rel_vpath = vpath.resolve().relative_to("/")
        rpath = security.safe_join(str(self.base_dir), str(rel_vpath))
        if rpath is None:  # outside base_dir
            raise ValueError

        return Path(rpath)

    def register(self, handler: Callable[[Path], None]) -> None:
        """
        Register a handler which will be called every time a file or
        directory in this virtfs changes.

        Args:
            handler: The handler to be called on changes.
        """
        self._handlers.append(handler)

    def _handle_change(self, path: Path) -> None:
        for handler in self._handlers:
            handler(self.r_to_v(path))

    @_convert_exceptions
    def mkdir(self, vpath: Path) -> None:
        """
        Creates a directory at the specified vpath. Also creates all
        parent directories if necessary.

        Args:
            vpath: An absolute path in the virtual file system.

        Raises:
            FileExistsError:
                If a file or directory exists at the specified location.
            NotADirectoryError:
                If any ancestor of the directory to be created is not in
                fact a directory.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        self.v_to_r(vpath).mkdir(parents=True, exist_ok=False)

    def _mkfile_impl(self, vpath: Path) -> None:
        """
        Implementation of :meth:`mkfile`.
        """
        rpath = self.v_to_r(vpath)

        if not rpath.parent.exists():
            try:
                rpath.parent.mkdir(parents=True, exist_ok=True)
            except (NotADirectoryError, FileExistsError) as e:
                # Parent path exists and is a file
                raise NotADirectoryError(
                    errno.ENOTDIR, os.strerror(errno.ENOTDIR), rpath
                ) from e

        rpath.touch(exist_ok=False)

    @_convert_exceptions
    def mkfile(self, vpath: Path) -> None:
        """
        Creates a file at the specified vpath. Also creates all parent
        directories if necessary.

        Args:
            vpath: An absolute path in the virtual file system.

        Raises:
            FileExistsError:
                If a file or directory exists at the specified location.
            NotADirectoryError:
                If any ancestor of the file to be created is not in
                fact a directory.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        self._mkfile_impl(vpath)

    @_convert_exceptions
    def read_dir(self, vpath: Path) -> List[Dict[str, str]]:
        """
        Reads a directory's content.

        Args:
            vpath: An absolute path to a directory in the virtual file
                system.

        Returns:
            The content of the directory as a list of dictionaries.
            Each dictionary has two entries 'path' and 'type', where
            'path' is the absoulte path of an entry in the directory at
            'vpath' and 'type' signals whether that entry is a 'file' or
            'dir'.

        Raises:
            FileNotFoundError:
                If the file to be read could not be found in the file
                system.
            NotADirectoryError:
                If the path to be read from points to a file.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        rpath = self.v_to_r(vpath)
        result = []

        for entry in rpath.iterdir():
            result_entry = {"path": str(self.r_to_v(entry))}

            if entry.is_dir():
                result_entry["type"] = "dir"
            elif entry.is_file():
                result_entry["type"] = "file"
            else:
                continue

            result.append(result_entry)

        return result

    @_convert_exceptions
    def read_file(self, vpath: Path) -> str:
        """
        Reads a file's content.

        Args:
            vpath: An absolute path to a file in the virtual file
                system.

        Returns:
            The content of the file.

        Raises:
            FileNotFoundError:
                If the file to be read could not be found in the file
                system.
            IsADirectoryError:
                If the path to be read from points to a directory.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        return self.v_to_r(vpath).read_text(encoding="utf-8")

    @_convert_exceptions
    def write_file(self, vpath: Path, text: str) -> None:
        """
        Overwrites a file.
        Creates the file if necessary.
        Also creates all parent directories if necessary.

        Args:
            vpath: An absolute path to a file in the virtual file
                system.
            text: The content to be written into the file.

        Raises:
            NotADirectoryError:
                If any ancestor of the file to be created is not in
                fact a directory.
            IsADirectoryError:
                If the path to be written to points to a directory.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        rpath = self.v_to_r(vpath)

        try:
            self._mkfile_impl(vpath)
        except FileExistsError as e:
            if rpath.is_dir():
                raise IsADirectoryError(
                    errno.EISDIR, os.strerror(errno.EISDIR), rpath
                ) from e

        rpath.write_text(text, encoding="utf-8")

    @_convert_exceptions
    def rmdir(self, vpath: Path) -> None:
        """
        Deletes a directory.

        Args:
            vpath: An absolute path to a directory in the virtual file
                system.

        Raises:
            FileNotFoundError:
                If the file to be read could not be found in the file
                system.
            NotADirectoryError:
                If the path to be deleted points to a file.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """

        # The obvious shutil solution seen below unfortunately runs into
        # 'text file busy' errors (errno 26) quite a lot.
        # shutil.rmtree(str(self.v_to_r(vpath)))
        # This happens because our virtual file system is located inside
        # a virtualbox shared folder, which is constantly re-syncinc
        # everything, which keeps the directories busy. See also:
        # https://www.virtualbox.org/ticket/19004?cversion=0&cnum_hist=2
        #
        # To deal with this problem, we delete everything manually,
        # retrying when os.rmdir fails with errno 26.
        def _rmdir(path):
            retries = 10
            while retries > 0:
                try:
                    os.rmdir(path)
                    retries = 0
                except OSError as e:
                    if e.errno == 26:
                        retries -= 10
                        if retries == 0:
                            raise
                        time.sleep(0.05)
                    else:
                        raise

        top = str(self.v_to_r(vpath))
        for root, dirs, files in os.walk(top, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                _rmdir(os.path.join(root, name))
        _rmdir(top)

    @_convert_exceptions
    def rmfile(self, vpath: Path) -> None:
        """
        Deletes a file.

        Args:
            vpath: An absolute path to a file in the virtual file
                system.

        Raises:
            FileNotFoundError:
                If the file to be read could not be found in the file
                system.
            IsADirectoryError:
                If the path to be deleted points to a directory.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        self.v_to_r(vpath).unlink()

    @_convert_exceptions
    def cpdir(self, vsrc: Path, vdst: Path) -> None:
        """
        Copies a directory from vsrc to vdst. Note, that vdst is the
        full path of the new resource, not just the target directory.
        Also creates all parent directories at the new location if
        necessary.

        Args:
            vsrc: An absolute path to a directory in the virtual file
                system that is to be copied to a new location.
            vdst: An absolute path in the virtual file system that the
                directory at vsrc will be moved to.

        Raises:
            FileNotFoundError:
                If the directory to be copied could not be found in the
                file system.
            NotADirectoryError:
                If the path to be moved points to a file.
            FileExistsError:
                If vdst already exists.
            OSError:
                For any other errors that may occur.
            ValueError: If either of the given vpaths point to a
                location outside the virtual file system.
        """
        rsrc = self.v_to_r(vsrc)
        rdst = self.v_to_r(vdst)

        try:
            if rsrc.samefile(rdst):
                raise shutil.SameFileError()
        except FileNotFoundError:
            pass

        shutil.copytree(str(rsrc), str(rdst))

    @_convert_exceptions
    def cpfile(self, vsrc: Path, vdst: Path) -> None:
        """
        Copies a file from vsrc to vdst. Note, that vdst is the
        full path of the new resource, not just the target directory.
        Also creates all parent directories at the new location if
        necessary.

        Args:
            vsrc: An absolute path to a file in the virtual file
                system that is to be copied to a new location.
            vdst: An absolute path in the virtual file system that the
                file at vsrc will be moved to.

        Raises:
            FileNotFoundError:
                If the file to be copied could not be found in the file
                system.
            IsADirectoryError:
                If the path to be moved points to a directory.
            FileExistsError:
                If vdst already exists.
            SameFileError:
                If vsrc and vdst point to the same file.
            OSError:
                For any other errors that may occur.
            ValueError: If either of the given vpaths point to a
                location outside the virtual file system.
        """
        rsrc = self.v_to_r(vsrc)
        rdst = self.v_to_r(vdst)

        try:
            if rsrc.samefile(rdst):
                raise shutil.SameFileError()
        except FileNotFoundError:
            pass

        try:
            self._mkfile_impl(vdst)
        except FileExistsError as e:
            if rdst.is_dir():
                raise IsADirectoryError(
                    errno.EISDIR, os.strerror(errno.EISDIR), rdst
                ) from e
            else:
                raise

        shutil.copyfile(str(rsrc), str(rdst))

    @_convert_exceptions
    def zipdir(self, vpath) -> Path:
        """
        Zips a directory into a temporary location and returns the path
        to the zipped file.

        Args:
            vpath: The vpath to the directory to be zipped.

        Returns:
            The vpath to the zipped file.

        Raises:
            FileNotFoundError:
                If the given path does not point to an existing
                resource.
            NotADirectoryError:
                If the given path points to a file.
            OSError:
                For any other errors that may occur.
            ValueError: If the given vpath points to a location
                outside the virtual file system.
        """
        rpath = self.v_to_r(vpath)

        if not rpath.exists():
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), rpath)
        if not rpath.is_dir():
            raise NotADirectoryError(errno.ENOTDIR, os.strerror(errno.ENOTDIR), rpath)

        tmppath = Path("/tmp") / (str(round(time.time() * 1000)) + ".zip")

        with ZipFile(tmppath, "w") as z:
            for root, dirs, files in os.walk(rpath):
                for file in files:
                    filename = Path(root) / Path(file)
                    arcname = filename.relative_to(rpath)
                    z.write(filename, arcname=arcname)
                for directory in dirs:
                    filename = Path(root) / Path(directory)
                    arcname = filename.relative_to(rpath)
                    z.write(filename, arcname=arcname)
        return tmppath
