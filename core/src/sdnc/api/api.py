from pathlib import Path
from shutil import SameFileError
from typing import Dict, List, Type

from flask import Blueprint, jsonify, request, send_file
from flask_socketio import SocketIO
from werkzeug.exceptions import BadRequest
from yaml import MarkedYAMLError, YAMLError

from sdnc.api.actioncodes import Actioncodes
from sdnc.api.errorcodes import Errorcodes
from sdnc.api.network_status import NetworkStatusReport
from sdnc.api.virtfs import VirtFS
from sdnc.emulator.emulator import (
    Emulator,
    NetBusyError,
    NetRunningError,
    NetStoppedError,
)
from sdnc.emulator.netprovider import ControllerConnectTimeoutError
from sdnc.emulator.network_actions.evaluation import AutoEval
from sdnc.emulator.network_actions.network_action import NetworkAction
from sdnc.emulator.network_actions.pingall import Pingall
from sdnc.emulator.progress_report import ProgressObserver, ProgressReport
from sdnc.emulator.scenario_parser import ScenarioSemanticError, Parser


class UnknownActionError(Exception):
    """
    An exception to signal that a parameter meant to contain an action's
    name was not recognized as the name of any known action.
    """

    pass


NAME_TO_ACTION: Dict[str, Type[NetworkAction]] = {
    Actioncodes.EVALUATION: AutoEval,
    Actioncodes.PINGALL: Pingall,
}


class ProgressSender(ProgressObserver):
    """
    Sends all observed progress reports into the given socket as
    "report" events.
    """

    def __init__(self, socketio: SocketIO, namespace: str) -> None:
        self._socketio = socketio
        self._namespace = namespace

    def notify(self, report: ProgressReport) -> None:
        self._socketio.emit("report", report, namespace="/api/net/progress")


def build_bp(emulator: Emulator, fs: VirtFS, socketio: SocketIO) -> Blueprint:
    api = Blueprint("api", __name__)
    progress_namespace = "/api/net/progress"

    actions: Dict[str, NetworkAction] = {}
    emulator.register(ProgressSender(socketio, progress_namespace))

    # ===== NETWORK ========================================================== #
    @socketio.on("connect", namespace=progress_namespace)
    def on_net_connect():
        actions_running = {
            name: action.is_running() for name, action in actions.items()
        }
        report = NetworkStatusReport(
            emulator.is_running(),
            actions_running,
        )
        socketio.emit("report", report, namespace=progress_namespace)

    @api.route("/api/net/start.json", methods=["POST"])
    def start():
        if "scenario" not in request.json or "controller" not in request.json:
            raise BadRequest
        v_scenario_file = Path(request.json.get("scenario"))
        v_controller_file = Path(request.json.get("controller"))

        r_scenario_file = fs.v_to_r(v_scenario_file)
        r_controller_file = fs.v_to_r(v_controller_file)

        with open(r_scenario_file.resolve(), encoding="utf-8") as scenario_handle:
            scenario = Parser().parse(scenario_handle)[0]
        emulator.start(scenario, r_controller_file)

        return jsonify({}), 200

    @api.route("/api/net/stop.json", methods=["POST"])
    def stop():
        emulator.stop()

        return jsonify({}), 200

    @api.route("/api/net/start_action.json", methods=["POST"])
    def start_action():
        name = request.json.get("name")
        if name not in NAME_TO_ACTION:
            raise UnknownActionError
        if name not in actions or not actions[name].is_running():
            action = NAME_TO_ACTION[name]()
            actions[name] = action
            emulator.start_action(action)

        return jsonify({}), 200

    @api.route("/api/net/stop_action.json", methods=["POST"])
    def stop_action():
        name = request.json.get("name")
        if name not in NAME_TO_ACTION:
            raise UnknownActionError
        if name in actions:
            actions[name].stop()

        return jsonify({}), 200

    @api.route("/api/net/flow_tables.json", methods=["GET"])
    def flow_tables():
        return jsonify(emulator.flow_tables()), 200

    # TODO: Add post option for direct upload of yaml file content
    # this would be used for live feedback during editing
    @api.route("/api/parse_scenario.json", methods=["GET"])
    def parse_scenario():
        vpath = Path(request.args.get("path", type=str))
        rpath = fs.v_to_r(vpath)

        with open(rpath.resolve(), encoding="utf-8") as scenario_handle:
            scenario, info, warn = Parser().parse(scenario_handle)
            return (
                jsonify(
                    {
                        "scenario": scenario,
                        "info": info,
                        "warn": warn,
                    }
                ),
                200,
            )

    # ===== FILE SYSTEM ====================================================== #

    fs_namespace = "/api/fs/changes"

    # For some reason, there has to be at least one connect handler for
    # any namespace. Otherwise we get protocol errors on the client.
    @socketio.on("connect", namespace=fs_namespace)
    def on_fs_connect():
        pass

    fs.register(lambda path: socketio.emit("change", path, namespace=fs_namespace))

    @api.route("/api/fs/mkdir.json", methods=["POST"])
    def fs_mkdir():
        vpath = Path(request.json.get("path", ""))

        fs.mkdir(vpath)

        return jsonify({}), 200

    @api.route("/api/fs/mkfile.json", methods=["POST"])
    def fs_mkfile():
        vpath = Path(request.json.get("path", ""))

        fs.mkfile(vpath)

        return jsonify({}), 200

    @api.route("/api/fs/read_dir.json", methods=["GET"])
    def fs_read_dir():
        vpath = Path(request.args.get("path", default="/", type=str))

        content = fs.read_dir(vpath)

        return jsonify(content), 200

    @api.route("/api/fs/read_file.json", methods=["GET"])
    def fs_read_file():
        vpath = Path(request.args.get("path", default="/", type=str))

        content = fs.read_file(vpath)

        return jsonify(content), 200

    @api.route("/api/fs/write_file.json", methods=["POST"])
    def fs_write_file():
        vpath = Path(request.json.get("path", ""))
        content = request.json.get("content", "")

        fs.write_file(vpath, content)

        return jsonify({}), 200

    @api.route("/api/fs/rmdir.json", methods=["POST"])
    def fs_rmdir():
        vpath = Path(request.json.get("path", ""))

        fs.rmdir(vpath)

        return jsonify({}), 200

    @api.route("/api/fs/rmfile.json", methods=["POST"])
    def fs_rmfile():
        vpath = Path(request.json.get("path", ""))

        fs.rmfile(vpath)

        return jsonify({}), 200

    @api.route("/api/fs/cpdir.json", methods=["POST"])
    def fs_cpdir():
        vsrc = Path(request.json.get("src", ""))
        vdst = Path(request.json.get("dst", ""))

        fs.cpdir(vsrc, vdst)

        return jsonify({}), 200

    @api.route("/api/fs/cpfile.json", methods=["POST"])
    def fs_cpfile():
        vsrc = Path(request.json.get("src", ""))
        vdst = Path(request.json.get("dst", ""))

        fs.cpfile(vsrc, vdst)

        return jsonify({}), 200

    @api.route("/fs/", defaults={"vpath": ""}, methods=["GET"])
    @api.route("/fs/<path:vpath>", methods=["GET"])
    def fs_mirror(vpath: str = ""):
        vpath_ = Path("/") / vpath
        rpath = fs.v_to_r(vpath_)
        if rpath.is_file():
            return send_file(rpath)
        elif rpath.is_dir():
            zippath = fs.zipdir(vpath_)
            download_name = vpath_.name + ".zip"
            if vpath_.name in ("", "/"):
                download_name = "root.zip"
            return send_file(
                str(zippath), download_name=download_name, mimetype="application/zip"
            )

    # ===== ERRORS =========================================================== #
    # pylint: disable=unused-argument
    @api.errorhandler(YAMLError)
    def handle_scenario_syntax_error(error):
        if isinstance(error, MarkedYAMLError):
            if error.context_mark is not None:
                error.context_mark.name = str(fs.r_to_v(Path(error.context_mark.name)))
            if error.problem_mark is not None:
                error.problem_mark.name = str(fs.r_to_v(Path(error.problem_mark.name)))
            data = str(error)
        else:
            data = str(error)

        return (
            jsonify(
                {"error": Errorcodes.SCENARIO_FILE_SYNTAX, "data": data}
            ),
            400,
        )

    @api.errorhandler(ScenarioSemanticError)
    def handle_scenario_semantic_error(error):
        return (
            jsonify(
                {"error": Errorcodes.SCENARIO_FILE_SEMANTIC, "data": error}
            ),
            400,
        )

    @api.errorhandler(NetRunningError)
    def handle_net_running_error(error):
        return jsonify({"error": Errorcodes.NETWORK_RUNNING}), 400

    @api.errorhandler(NetStoppedError)
    def handle_net_stopped_error(error):
        return jsonify({"error": Errorcodes.NETWORK_STOPPED}), 400

    @api.errorhandler(NetBusyError)
    def handle_concurrent_action_error(error):
        return jsonify({"error": Errorcodes.NETWORK_BUSY}), 400

    @api.errorhandler(UnknownActionError)
    def handle_unknown_action_error(error):
        return jsonify({"error": Errorcodes.UNKNOWN_ACTION}), 400

    @api.errorhandler(ControllerConnectTimeoutError)
    def handle_controller_connect_timeout_error(error):
        return jsonify({"error": Errorcodes.CONTROLLER_CONNECT_TIMEOUT}), 400

    @api.errorhandler(FileNotFoundError)
    def handle_file_not_found(error):
        return (
            jsonify(
                {
                    "error": Errorcodes.FILE_NOT_FOUND,
                    "message": str(error),
                    "data": error.filename,
                }
            ),
            400,
        )

    @api.errorhandler(FileExistsError)
    def handle_file_exists_error(error):
        return (
            jsonify(
                {
                    "error": Errorcodes.FILE_EXISTS,
                    "message": str(error),
                    "data": error.filename,
                }
            ),
            400,
        )

    @api.errorhandler(NotADirectoryError)
    def handle_not_a_directory_error(error):
        return (
            jsonify(
                {
                    "error": Errorcodes.NOT_A_DIRECTORY,
                    "message": str(error),
                    "data": error.filename,
                }
            ),
            400,
        )

    @api.errorhandler(IsADirectoryError)
    def handle_is_a_directory_error(error):
        return (
            jsonify(
                {
                    "error": Errorcodes.IS_A_DIRECTORY,
                    "message": str(error),
                    "data": error.filename,
                }
            ),
            400,
        )

    @api.errorhandler(SameFileError)
    def handle_same_file_error(error):
        return (
            jsonify(
                {
                    "error": Errorcodes.SAME_FILE,
                    "message": str(error),
                    "data": error.filename,
                }
            ),
            400,
        )

    return api


# pylint: enable=unused-argument
