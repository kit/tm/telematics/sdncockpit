from __future__ import annotations

import os
import subprocess as subp
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from time import time_ns
import time
from typing import Optional, Tuple

from mininet.link import Intf
from mininet.net import Mininet
from mininet.node import DefaultController, Node, OVSKernelSwitch, Switch

from sdnc.emulator.network import MonitoredLink, TrafgenHost
from sdnc.emulator.scenario import ObservationPoint, TrafficEvent
from sdnc.emulator.statistics import FlowTable
from sdnc.emulator.trace import FilteredPcap
from sdnc.emulator.trafgen import IDUDPTrafgenCfg
from sdnc.types import Numeric


class ScenarioNet(Mininet):
    """
    A Mininet net with trafgen hosts, monitored links and the capability
    to manage observation points and run traffic events directly.

    The parameters and attributes for this class are the same as those
    for it's parent class, except that their default values have been
    altered slightly.
    """

    def __init__(
        self,
        topo=None,
        switch=OVSKernelSwitch,
        host=TrafgenHost,
        controller=DefaultController,
        link=MonitoredLink,
        intf=Intf,
        build=True,
        xterms=False,
        cleanup=False,
        ipBase="10.0.0.0/8",
        inNamespace=False,
        autoSetMacs=False,
        autoStaticArp=False,
        autoPinCpus=False,
        listenPort=None,
        waitConnected=False,
    ):
        # We forward args individually purely so we can change the
        # default host and link safely.
        Mininet.__init__(
            self,
            topo=topo,
            switch=switch,
            host=host,
            controller=controller,
            link=link,
            intf=intf,
            build=build,
            xterms=xterms,
            cleanup=cleanup,
            ipBase=ipBase,
            inNamespace=inNamespace,
            autoSetMacs=autoSetMacs,
            autoStaticArp=autoStaticArp,
            autoPinCpus=autoPinCpus,
            listenPort=listenPort,
            waitConnected=waitConnected,
        )
        self._observation_points = {}

    def start_monitor(self, observation_point: ObservationPoint) -> None:
        """
        Starts monitoring a link for the given observation point.

        Args:
            observation_point: The observation point containing the
                parameters for the monitor.
        """
        # TODO: Optimize this so we don't spawn so many duplicate
        # tcpdumps on the same link?

        if observation_point in self._observation_points:
            raise ValueError("Monitor for this observation_point already exists!")

        obs_link = observation_point.link

        found_link = None
        for link in self.links:
            intf1 = link.intf1.node.name
            intf2 = link.intf2.node.name
            if obs_link in ((intf1, intf2), (intf2, intf1)):
                found_link = link
                break

        if found_link is None:
            raise ValueError("Link specified by observation_point does not exist!")

        descriptor = found_link.start_monitor(
            observation_point.node, observation_point.direction
        )

        self._observation_points[observation_point] = (found_link, descriptor)

    def stop_monitor(self, observation_point: ObservationPoint) -> Path:
        """
        Stops monitoring the link that was being monitored for the given
        observation point.

        Args:
            observation_point: The observation point for which the
                about-to-be-stopped monitor was started.

        Returns:
            The captured pcap file's path.
        """
        if observation_point not in self._observation_points:
            raise ValueError("No active monitors for observation_point!")

        link, descriptor = self._observation_points[observation_point]
        self._observation_points.pop(observation_point)

        return link.stop_monitor(descriptor)

    def kill_monitors(self) -> None:
        """
        Stops all active monitors and discards their pcaps.
        """
        # copy keys into list to avoid modify during iteration errors
        for observation_point in list(self._observation_points):
            self.stop_monitor(observation_point)

    def get_activity(self, timeframe: Numeric) -> int:
        """
        Sums up the number of identified packets spotted on all links
        in the network in the specified timeframe.

        Args:
            timeframe: Length of timeframe for which the network should
            be monitored to determine activity (in s). The timeframe
            measures only how long the network's links are monitored for
            and does not take into accout setup and teardown of
            monitoring processes and threads.

        Returns:
            The number of identified packets spotted in the timeframe.
        """
        executor = ThreadPoolExecutor()
        pcaps = [link.timed_monitor(timeframe, executor) for link in self.links]

        packet_lists = [FilteredPcap(pcap.result()).get() for pcap in pcaps]
        return sum(sum(1 for _ in packet_list) for packet_list in packet_lists)

    def wait_inactive(self, timeout: Optional[int] = None, timeframe: int = 1) -> bool:
        """
        Wait until get_activity(timeframe) returns 0 at least once.
        Checks only once per timeframe.

        Args:
            timeout: Time to wait (in s), or None to wait indefinetely.
            timeframe: Length of timeframe (in s) for which the network
                should be monitored to determine inactivity.

        Returns: True if the network activity was 0 on the last check,
            False if the function timed out before then.
        """
        start = time_ns() * 10 ** (-9)

        activity = self.get_activity(timeframe)
        while activity > 0:
            time = time_ns() * 10 ** (-9) - start
            if timeout is not None and time >= timeout:
                return False

            activity = self.get_activity(timeframe)

        return True

    def run_event(self, event: TrafficEvent) -> None:
        """
        Runs the specified traffic event on the appropriate host.
        Ignores event.time.

        Args:
            event: The event to be run.
        """
        if event.test_id is None:
            raise ValueError("Event is missing a test_id!")

        if event.host not in self.nameToNode:
            raise ValueError("Unknown host")
        host = self.getNodeByName(event.host)

        cfg = IDUDPTrafgenCfg(event.headers, event.test_id, event.event_id, offset=0)

        remaining = event.pkt_count
        while remaining > 0:
            batch = 256 if remaining >= 256 else remaining
            host.send(cfg.generate(), batch)
            cfg.offset += 1
            remaining -= batch

    def ping_route(self, source: Node, destination: Node) -> Tuple[int, int]:
        """
        Send a ping from source_host to the ip of destination_host and
        check the response.

        Args:
            source: The host from which to send the ping.
            destination: The host to whom's ip the ping will be
                sent.

        Returns:
            A tuple of counts (sent, received).
        """
        # Mininet has a ping() method, so why make our own here?
        # 1) The interface for ping allows only for back-and-forth,
        #    meaning we can only ping h1->h2 AND h2->h1
        # 2) Because of 1), ping() is unsuitable for generating the
        #    reports for the user I would like it to generate
        # 3) ping() uses node.cmd, making it unsuitable for concurrency
        #    alongside a running test
        # it may be a good idea to switch to fping at some point, as its
        # output is easier to parse and it allows for shorter deadlines

        if source not in self.hosts or destination not in self.hosts:
            raise ValueError("Specified host not in network")

        env = os.environ.copy()
        env["LANG"] = "C"  # disable localization (necessary for parsing)
        cmd = ["ping", "-c", "1", "-W", "1", destination.IP()]
        with source.popen(cmd, env=env, stdout=subp.PIPE, stderr=subp.STDOUT) as p:
            output, _ = p.communicate()
            return self._parsePing(output.decode("ascii"))

    def flow_table(self, switch: Switch) -> FlowTable:
        """
        Retrieve the current flow table of the specified switch.

        Args:
            switch: The switch for which to get the flow table.

        Returns:
            The flow table.
        """
        if switch not in self.switches:
            raise ValueError("Specified switch not in network")

        env = os.environ.copy()
        env["LANG"] = "C"  # disable localization (necessary for parsing)
        cmd = ["ovs-ofctl", "dump-flows", str(switch)]
        with subp.Popen(cmd, env=env, stdout=subp.PIPE, stderr=subp.PIPE) as p:
            output, _ = p.communicate()
            return FlowTable(output.decode("ascii"))


    def stop(self) -> None:
        """
        Stop the controller(s), switches, hosts and monitoring processes.
        """
        self.kill_monitors()

        for node in self:
            if self[node].waiting:
                self[node].sendInt()
        for node in self:
            while self[node].waiting:
                time.sleep(0.01)

        super().stop()

