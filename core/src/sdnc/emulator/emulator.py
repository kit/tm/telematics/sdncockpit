from __future__ import annotations

from contextlib import contextmanager
from pathlib import Path
from threading import RLock
from typing import Dict, Iterator, Optional, Set

from sdnc.emulator.controller import Controller
from sdnc.emulator.netprovider import ControllerConnectTimeoutError, NetProvider
from sdnc.emulator.network_actions.network_action import NetworkAction
from sdnc.emulator.progress_report import (
    ProgressObservable,
    ProgressObserver,
    ProgressReport,
)
from sdnc.emulator.scenario import Scenario
from sdnc.emulator.scenario_net import ScenarioNet
from sdnc.emulator.statistics import FlowTable


class StartEmulatorReport(ProgressReport):
    """
    Reports the start of the emulator.
    """

    pass


class StopEmulatorReport(ProgressReport):
    """
    Reports the stop of the emulator.
    """

    pass


class NetRunningError(Exception):
    """
    An exception to signal that a network is running inside an
    emulator, preventing a requested action from being performed.
    """

    pass


class NetStoppedError(Exception):
    """
    An exception to signal that no network is running inside an
    emulator, preventing a requested action from being performed.
    """

    pass


class NetBusyError(Exception):
    """
    An exception to signal that a requested network action or command
    cannot be executed by the emulator due to another action already
    running.
    """

    pass


class Emulator(ProgressObservable):
    """
    A facade for the sdnc emulation system, most notably the
    :class:`~sdnc.core.scenario_net.ScenarioNet` and
    :class:`~sdnc.core.evaluation.AutoEval` classes.

    This class is threadsafe.
    """

    def __init__(self):
        super().__init__()
        self._net_lock = RLock()
        self._net_provider = None
        self._observer = self._ProgessObserver(self)
        self._actions: Set[NetworkAction] = set()

    @contextmanager
    def lock(self) -> Iterator[None]:
        """
        While holding the returned context manager, no other thread can
        access the emulator.

        Raises:
            NetBusyError if
        """
        with self._net_lock:
            if self._is_busy():
                raise NetBusyError()
            else:
                yield

    def net(self) -> Optional[ScenarioNet]:
        """
        Get the emulator's current ScenarioNet if available.
        """
        return self._net_provider.get()

    def start(self, scenario: Scenario, app: Path) -> None:
        """
        Starts emulation with the given scenario and controller.

        Args:
            scenario: The scenario to be used by the emulator.
            app: The ryu app to be used by the emulator.

        Raises:
            NetRunningError: If the emulation is running already.
        """
        controller = Controller(app)
        controller.register(self._observer)

        with self._net_lock:
            if self.is_running():
                raise NetRunningError()
            self._notify_all(StartEmulatorReport())

            topo = scenario.topo
            self._net_provider = NetProvider(topo, controller)
            self._net_provider.register(self._observer)
            try:
                self._net_provider.start()
            except ControllerConnectTimeoutError:
                self._net_provider = None
                self._notify_all(StopEmulatorReport())
                raise

            self._scenario = scenario

    def stop(self) -> None:
        """
        Stops the emulator. If none is running, no action is performed.
        """
        with self._net_lock:
            if self.is_running():
                for action in self._actions:
                    action.stop()
                self._net_provider.stop()
                self._net_provider = None
                self._notify_all(StopEmulatorReport())

    def is_running(self) -> bool:
        """
        Checks if emulation is running.
        """
        with self._net_lock:
            return self._net_provider is not None

    def _is_busy(self) -> bool:
        return any(a.is_running() for a in self._actions)

    def start_action(self, action: NetworkAction) -> None:
        """
        Starts a network action in the context of this emulator.
        The emulator will report the action's progress as its own.

        Raises:
            NetStoppedError: If emulation is not running.
            NetBusyError: If another action which cannot be
                executed concurrently with the requested one is already
                running.
        """
        self._actions = set(a for a in self._actions if a.is_running())
        with self._net_lock:
            if not self.is_running():
                raise NetStoppedError()
            if self._is_busy():
                raise NetBusyError()
            self._actions.add(action)
            action.register(self._observer)
            action.start(self._scenario, self._net_provider)

    def flow_tables(self) -> Dict[str, FlowTable]:
        """
        Request a table of all flow tables.

        Returns:
            A dict containing all switches as keys and their flow tables
            as values.
        """
        with self._net_lock:
            if not self.is_running():
                raise NetStoppedError()
            net = self._net_provider.get()
            return {str(s): net.flow_table(s) for s in net.switches}

    class _ProgessObserver(ProgressObserver):
        def __init__(self, emulator: Emulator):
            super().__init__()
            self.emulator = emulator

        def notify(self, report: ProgressReport) -> None:
            # pylint: disable=protected-access
            self.emulator._notify_all(report)
            # pylint: enable=protected-access
