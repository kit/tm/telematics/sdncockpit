from __future__ import annotations

from copy import deepcopy
from dataclasses import dataclass
from typing import Any, Iterator, List, Optional, Tuple, Type

from mininet.topo import Topo
from scapy.packet import Packet

from sdnc.emulator.range_list import BasicRange
from sdnc.emulator.trace import IdentifiedPacket
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg, UDPTrafgenCfg


class HeaderRule:
    """
    This class allows the user to define a rule for the contents of one
    of a packet's protocol headers.

    The rule checks if the value of a field of one of the packet's
    protocol headers is in some range.

    Args:
        proto_cls: A ryu protocol header class. See:
            https://ryu.readthedocs.io/en/latest/library_packet_ref.html#protocol-header-classes
        field: Name of an attribute of the given :attr:`proto_cls`.
        val_range: The range of values this rule allows packets to
            contain in the given proto_cls and field.

    Attributes:
        proto_cls: A ryu protocol header class. See:
            https://ryu.readthedocs.io/en/latest/library_packet_ref.html#protocol-header-classes
        field: Name of an attribute of the given :attr:`proto_cls`.
        val_range: The range of values this rule allows packets to
            contain in the given proto_cls and field.
    """

    def __init__(
        self,
        proto_cls: Type[Packet],
        field: str,
        val_range: BasicRange[Any],
    ):
        self.proto_cls = proto_cls
        self.field = field
        self.val_range = val_range

        # check if field is a valid attribute
        getattr(self.proto_cls(), self.field)

    def check(self, pkt: Packet) -> HeaderFeedback:
        """
        Check if the given packet adheres to this rule.

        Args:
            pkt: A packet.
        Returns:
            Feedback on if and how the packet (miss-)matched this rule.
        """
        proto = pkt[self.proto_cls] if self.proto_cls in pkt else None

        if proto is not None and hasattr(proto, self.field):
            value = getattr(proto, self.field)
            return HeaderFeedback(value in self.val_range, self)
        else:
            return HeaderFeedback(False, self)


class ObservationPoint:
    """
    This class defines an observation point for a
    :class:`~sdnc.core.scenario.Test`. Observation points are links in
    the network at which traffic is recorded during a test. This traffic
    must have certain quantitive and qualitative characteristics defined
    as part of the observation point.

    Args:
        link: The link in the network where this point is located.
            Given as a tuple of nodes' names.
        node: The name of the a attached to :attr:`link` on which the
            observation point should run its tcpdump or None to indicate
            that either node is fine.
        direction: The direction of travel for which this observation
            point is defined. Can be either of the two nodes attached to
            :attr:`link` or None for bidirectional observation.
        count: Defines the expected number of packets expected to show
            up at this observation point during the
            :class:`~sdnc.core.scenario.Test` for which it is defined.
        header_rules: The
            :class:`HeaderRules <sdnc.core.scenario.HeaderRule>` each
            observed packet's headers must adhere to.

    Attributes:
        link: The link in the network where this point is located.
            Given as a tuple of nodes' names.
        node: The name of the a attached to :attr:`link` on which the
            observation point should run its tcpdump or None to indicate
            that either node is fine.
        direction: The direction of travel for which this observation
            point is defined. Can be either of the two nodes attached to
            :attr:`link` or None for bidirectional observation.
        count: Defines the expected number of packets expected to show
            up at this observation point during the
            :class:`~sdnc.core.scenario.Test` for which it is defined.
        header_rules: The
            :class:`HeaderRules <sdnc.core.scenario.HeaderRule>` each
            observed packet's headers must adhere to.
    """

    def __init__(
        self,
        link: Tuple[str, str],
        node: Optional[str] = None,
        direction: Optional[str] = None,
        count: Optional[BasicRange[int]] = None,
        header_rules: Optional[List[HeaderRule]] = None,
    ):
        self._node = None  # Link checks node when set
        self._direction = None  # Link checks direction when set
        self.link = link
        self.node = node
        self.direction = direction
        self.count = count
        self.header_rules = header_rules if header_rules else []

    def check(self, pkt_iter: Iterator[IdentifiedPacket]) -> ObservationPointFeedback:
        """
        Tests if a set of packets has the quantitive and qualitative
        characteristics defined by this observation point.

        Args:
            pkt_iter: An iterator yielding packets relevant to this
                observation point.
        Returns:
            Feedback.
        """

        pkt_fbs = []
        for pkt in pkt_iter:
            hdr_fbs = []
            for header_rule in self.header_rules:
                hdr_fbs.append(header_rule.check(pkt.pkt))

            pkt_status = all(hdr_fb.status for hdr_fb in hdr_fbs)

            pkt_fb = PacketFeedback(pkt_status, pkt.pkt, hdr_fbs)

            pkt_fbs.append(pkt_fb)

        status = all(pkt_fb.status for pkt_fb in pkt_fbs) and (
            self.count is None or len(pkt_fbs) in self.count
        )

        return ObservationPointFeedback(status, self, pkt_fbs)

    @property
    def link(self) -> Tuple[str, str]:
        return self._link

    @link.setter
    def link(self, value: Tuple[str, str]) -> None:
        # Unfortunately, we can't check if link is actually in the
        # topology. We could do that in the scenario class perhaps?
        if self.node is not None and self.node not in value:
            raise ValueError("Point's node is not in link!")
        if self.direction is not None and self.node not in value:
            raise ValueError("Point's direction is not in link")
        self._link: Tuple[str, str] = value

    @property
    def node(self) -> Optional[str]:
        return self._node

    @node.setter
    def node(self, value: Optional[str]) -> None:
        if value is not None and value not in self.link:
            raise ValueError(
                "Node must be either of the two nodes in the point's link!"
            )
        self._node = value

    @property
    def direction(self) -> Optional[str]:
        return self._direction

    @direction.setter
    def direction(self, value: Optional[str]) -> None:
        if value is not None and value not in self.link:
            raise ValueError(
                ("Direction must be either of the two nodes in the point's " "link!")
            )
        self._direction = value

    @property
    def count(self) -> Optional[BasicRange[int]]:
        return self._count

    @count.setter
    def count(self, value: Optional[BasicRange[int]]) -> None:
        # TODO: Ensure no negative values in the range
        self._count = value


class RandomTrafficEvent:
    """
    This class can be used to generate
    :class:`~sdnc.core.scenario.TrafficEvent` objects with randomly
    chosen parameters.

    Most attributes are tuples (start, end) defining ranges from which
    the parameters of generated
    :class:`TrafficEvents <sdnc.core.scenario.TrafficEvent>` are chosen.
    All such ranges are (inclusive, exclusive)!

    Attributes:
        event_count: How many events should be generated from this
            template.
        host: The name of the host from which packets are to be sent in
            this event. Is not randomized.
        headers: A randomized trafgen configuration from which the
            generated event's headers are generated.
        pkt_count: How many packets are to be sent as part of
            generated events.
        time: Time (in ms) at which the generated events take place,
            relative to the beginning of the test they are part of.
    """

    def __init__(
        self,
        event_count: BasicRange[int],
        host: str,
        headers: RandomUDPTrafgenCfg,
        pkt_count: BasicRange[int],
        time: BasicRange[int],
    ):
        self.event_count = event_count
        self.host = host
        self.headers = headers
        self.pkt_count = pkt_count
        self.time = time

    def generate(self) -> List[TrafficEvent]:
        """
        Randomly generates a list of
        :class:`TrafficEvents <sdnc.core.scenario.TrafficEvent>`. The
        number of events generated is a random variable drawn from the
        range set in :attr:`event_count`.

        Can be made deterministic via random.seed() or
        random.setstate().

        :returns: The list of generated events.
        """
        events = []
        for _ in range(self.event_count.random()):
            ev = TrafficEvent(
                host=self.host,
                headers=self.headers.generate(),
                pkt_count=self.pkt_count.random(),
                time=self.time.random(),
            )
            events.append(ev)

        return events

    @property
    def event_count(self) -> BasicRange[int]:
        return self._event_count

    @event_count.setter
    def event_count(self, value: BasicRange[int]) -> None:
        if -1 in value:
            raise ValueError("Event count must be positive integer!")
        self._event_count = value

    @property
    def pkt_count(self) -> BasicRange[int]:
        return self._pkt_count

    @pkt_count.setter
    def pkt_count(self, value: BasicRange[int]) -> None:
        if -1 in value:
            raise ValueError("Packet count must be positive integer!")
        self._pkt_count = value

    @property
    def time(self) -> BasicRange[int]:
        return self._time

    @time.setter
    def time(self, value: BasicRange[int]) -> None:
        if -1 in value:
            raise ValueError("Time must be positive!")
        self._time = value


class TrafficEvent:
    """
    A traffic event is a number of packets logically belonging together
    sent from the same host. This class is meant to carry all
    information needed to create such an event.

    Args:
        host: The name of the host from which packets are to be sent in
            this event.
        headers: A randomized trafgen configuration carrying information
            on how the headers of packets should be generated for this
            event.
        pkt_count: How many packets are to be sent as part of this
            event.
        time: Time (in ms) at which this event takes place, relative to
            the beginning of the test it is part of.
        event_id: An id for this event. Must be unique within a test.
            Must be between 0 and 2^16-1.
        test_id: The id of the test (if any) this event belongs to.
            Must be between 0 and 2^16-1 or None.

    Attributes:
        host: The name of the host from which packets are to be sent in
            this event.
        headers: A randomized trafgen configuration carrying information
            on how the headers of packets should be generated for this
            event.
        pkt_count: How many packets are to be sent as part of this
            event.
        time: Time (in ms) at which this event takes place, relative to
            the beginning of the test it is part of.
        event_id: An id for this event. Must be unique within a test.
            Must be between 0 and 2^16-1.
        test_id: The id of the test (if any) this event belongs to.
            Must be between 0 and 2^16-1 or None.
    """

    def __init__(
        self,
        host: str,
        headers: UDPTrafgenCfg,
        pkt_count: int,
        time: int,
        event_id: int = 0,
        test_id: Optional[int] = None,
    ):
        self.host = host
        self.headers = headers
        self.pkt_count = pkt_count
        self.time = time
        self.event_id = event_id
        self.test_id = test_id

    @property
    def pkt_count(self) -> int:
        return self._pkt_count

    @pkt_count.setter
    def pkt_count(self, value: int) -> None:
        if not 0 <= value:
            raise ValueError("Packet count must be positive integer!")
        self._pkt_count = value

    @property
    def time(self) -> int:
        return self._time

    @time.setter
    def time(self, value: int) -> None:
        if not 0 <= value:
            raise ValueError("Time must be positive integer!")
        self._time = value

    @property
    def event_id(self) -> int:
        return self._event_id

    @event_id.setter
    def event_id(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1:
            raise ValueError("Event ID must be int between 0 and 65535!")
        self._event_id = value

    @property
    def test_id(self) -> Optional[int]:
        return self._test_id

    @test_id.setter
    def test_id(self, value: int) -> None:
        if value is not None and (not 0 <= value <= 2**16 - 1):
            raise ValueError("Test ID must be int between 0 and 65535!")
        self._test_id = value


class Test:
    """
    This class defines a single test. A test is a series of traffic
    events started in a network along with a set of expectations for
    the network's reaction to that traffic.

    Because tests usually need to be randomized, only
    :class:`RandomTrafficEvents <RandomTrafficEvent>` objecs can be
    added to a test directly.

    This class then uses these randomized generators to create the
    events that will actually be used for testing.

    Args:
        name: A user-given name for the test.
        description: A user-given description for the test.
        restart: A boolean that defines whether the network and its
            controllers need to be restarted before this test is run.
        parallel: A boolean indicating whether or not this test should
            be executed in parallel with adjacent tests.
        rnd_events: The list of randomized event generators used to
            generate :attr:`events`.
        obs_points: A set of observations points which define the
            expectations this test places on the network's reaction to
            the traffic events.
        test_id: An id for this test. Must be unique within a scenario.
            Must be between 0 and 2^16-1.

    Attributes:
        name: A user-given name for the test.
        description: A user-given description for the test.
        restart: A boolean that defines whether the network and its
            controllers need to be restarted before this test is run.
        parallel: A boolean indicating whether or not this test should
            be executed in parallel with adjacent tests.
        rnd_events: (Read-Only) The list of randomized event generators
            used to generate :attr:`events`.
        events: (Read-Only) The list of events contained in this test.
        obs_points: A set of observations points which define the
            expectations this test places on the network's reaction to
            the traffic events.
        test_id: An id for this test. Must be unique within a scenario.
            Must be between 0 and 2^16-1.
    """

    __test__ = False

    def __init__(
        self,
        name: str = "",
        description: str = "",
        restart: bool = False,
        parallel: bool = False,
        rnd_events: Optional[List[RandomTrafficEvent]] = None,
        obs_points: Optional[List[ObservationPoint]] = None,
        test_id: int = 0,
    ):
        self.name = name
        self.description = description
        self.restart = restart
        self.parallel = parallel
        self._rnd_events: List[RandomTrafficEvent] = []
        self._events: List[TrafficEvent] = []
        self.obs_points = obs_points if obs_points else []
        self.test_id = test_id
        self._event_id = 0

        if rnd_events:
            for rnd_event in rnd_events:
                self.add_rnd_event(rnd_event)

    def add_rnd_event(self, rnd_event: RandomTrafficEvent) -> None:
        """
        Adds the given randomized event generator to the list of events
        that will be run as part of this test. Immediately generates
        events from the given generator and appends them to
        :attr:`events`.

        Can be made deterministic via random.seed() or
        random.setstate().

        Args:
            event: The random traffic event to be added to the test.
        """
        self._rnd_events.append(rnd_event)
        self._generate_events(rnd_event)

    def reroll(self) -> None:
        """
        Regenerates the events list from the list of rnd_events.

        Can be made deterministic via random.seed() or
        random.setstate().
        """
        self._events = []
        self._event_id = 0

        for rnd_event in self._rnd_events:
            self._generate_events(rnd_event)

    def _generate_events(self, rnd_event: RandomTrafficEvent) -> None:
        for event in rnd_event.generate():
            event.event_id = self._event_id
            event.test_id = self.test_id
            self._event_id += 1

            self._events.append(event)

    @property
    def rnd_events(self) -> List[RandomTrafficEvent]:
        return deepcopy(self._rnd_events)

    @property
    def events(self) -> List[TrafficEvent]:
        return deepcopy(self._events)

    @property
    def test_id(self) -> int:
        return self._test_id

    @test_id.setter
    def test_id(self, value: int) -> None:
        if value is not None and (not 0 <= value <= 2**16 - 1 or not value % 1 == 0):
            raise ValueError("Test ID must be int between 0 and 65535!")
        self._test_id = value

        for event in self._events:
            event.test_id = self._test_id


class Scenario:
    """
    A scenario is a network topology on which a network controller will
    be evaluated against a series of tests.

    Args:
        name: This scenario's name. Can be None.
        description: A string describing the scenario. Can be None.
        topo: The scenario's mininet network topology.
        tests: (Read-only) A list of tests.

    Attributes:
        name: This scenario's name. Can be None.
        description: A string describing the scenario. Can be None.
        topo: The scenario's mininet network topology.
        tests: (Read-only) A list of tests.

    """

    def __init__(
        self,
        name: str,
        description: str,
        topo: Topo,
        tests: Optional[List[Test]] = None,
    ):
        self.name = name
        self.description = description
        self.topo = topo
        self._tests: List[Test] = []
        self._test_id = 0

        if tests:
            for test in tests:
                self.add_test(test)

    def add_test(self, test: Test) -> None:
        """
        Adds the given test to the list of tests that will be performed
        as part of this scenario. Ensures that the test_id of the given
        test is unique within this scenario.

        Args:
            test: The test to be added to the scenario.
        """
        test.test_id = self._test_id
        self._test_id += 1

        self._tests.append(test)

    def reroll_tests(self) -> None:
        """
        Convenience method to reroll all tests.

        Can be made deterministic via random.seed() or
        random.setstate().
        """
        for test in self._tests:
            test.reroll()

    @property
    def tests(self) -> List[Test]:
        return deepcopy(self._tests)


@dataclass
class HeaderFeedback:
    """
    This class contains the results of evaluating a :class:`HeaderRule`
    for a packet.

    Attributes:
        status: True to indicate success, False to indicate Failure.
        header_rule: The rule that was evaluated.
    """

    status: bool
    header_rule: HeaderRule


@dataclass
class PacketFeedback:
    """
    This class contains feedback on a single packet observed at a single
    observation point during an automated evaluation.

    Attributes:
        status: True to indicate success, False to indicate Failure.
        packet: The packet that had its header's evaluated.
        hdr_feedbacks: The feedback objects for the packet's header.
    """

    status: bool
    packet: Packet
    header_feedbacks: List[HeaderFeedback]


@dataclass
class ObservationPointFeedback:
    """
    This class contains feedback on the packets observed at an
    observation point during an automated evaluation test both in terms
    of packet count and in terms of the expecations placed on each
    individual observed packet.

    Attributes:
        status: True to indicate success, False to indicate Failure.
        obs: The observation point this feedback relates to.
        pkt_feedbacks: The feedback objects for the packets. One for
            every observed packet.
    """

    status: bool
    observation_point: ObservationPoint
    packet_feedbacks: List[PacketFeedback]
