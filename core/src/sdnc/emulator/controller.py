from __future__ import annotations

import os
import subprocess
import traceback
from dataclasses import dataclass
from pathlib import Path
from threading import Event, Thread
from types import TracebackType
from typing import Optional, Type

from sdnc.emulator.progress_report import ProgressObservable, ProgressReport


@dataclass
class OutputReport(ProgressReport):
    """
    A progress report which reports the printing of a string, for
    example by a process or the mininet command line.
    """

    line: str


@dataclass
class ProcessReadStartReport(ProgressReport):
    """
    A progress report indicating the start of a process.
    """

    pass


@dataclass
class ProcessReadEndReport(ProgressReport):
    """
    A progress report indicating the termination of a process.
    """

    pass


class Controller(ProgressObservable):
    """
    This is a class to run a RYU SDN controller with an app.

    Args:
        app: The path to the app's python source file.

    Attributes:
        app: The path to the app's python source file.
    """

    def __init__(self, app: Path):
        super().__init__()
        self.app = app
        self._read_thread: Optional[Thread] = None
        self._stop_flag: Event = Event()
        self._process: Optional[subprocess.Popen] = None

    def start(self) -> None:
        """
        (Re-) start the controller.

        If the controller is currently running, this function will
        terminate the old instance.
        """
        # If monitor or process are still running, shut them down first.
        self.stop()
        self.wait()

        env = os.environ.copy()
        env["PYTHONUNBUFFERED"] = "TRUE"
        env["PYTHONDONTWRITEBYTECODE"] = "1"
        cmd = ["ryu-manager", str(self.app)]
        # pylint: disable=consider-using-with
        self._process = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=env
        )
        # pylint: enable=consider-using-with

        self._stop_flag.clear()
        self._read_thread = Thread(target=self._read)
        self._read_thread.start()

    def stop(self) -> None:
        """
        Stop the controller.
        """
        if self._process is not None and self._process.poll() is None:
            self._process.terminate()
        self._stop_flag.set()

    def wait(self) -> None:
        """
        Wait for the controller to terminate and discard its process.
        """
        if self._process is not None:
            self._process.wait()
            self._process = None
        if self._read_thread is not None:
            self._read_thread.join()
            self._read_thread = None

    def _read(self) -> None:
        assert self._process is not None
        assert self._process.stdout is not None
        self._notify_all(ProcessReadStartReport())
        line = self._process.stdout.readline()
        while line and not self._stop_flag.is_set():
            self._notify_all(OutputReport(line))
            line = self._process.stdout.readline()
        self._notify_all(ProcessReadEndReport())

    def __enter__(self) -> Controller:
        self.start()
        return self

    def __exit__(
        self,
        ex_t: Optional[Type[BaseException]],
        ex_val: Optional[BaseException],
        trace: Optional[TracebackType],
    ) -> None:
        if ex_t is not None:
            traceback.print_exception(ex_t, ex_val, trace)
        self.stop()
        self.wait()
