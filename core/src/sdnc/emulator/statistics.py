class FlowTable:
    """
    A FlowTable retrieved from a switch.

    Args:
        table: The string output of dpctl dump-flows.

    TODO: Parse the dpctl dump-flows output into a more structured
    representation.
    """

    def __init__(self, table: str) -> None:
        self.table = table
