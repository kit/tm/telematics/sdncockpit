from __future__ import annotations

import traceback
from functools import partial
from types import TracebackType
from typing import Optional

from mininet.clean import Cleanup
from mininet.node import OVSSwitch, RemoteController
from mininet.topo import Topo

from sdnc.emulator.controller import Controller
from sdnc.emulator.progress_report import ProgressObservable, ProgressReport
from sdnc.emulator.scenario_net import ScenarioNet


class ControllerConnectTimeoutError(Exception):
    """
    Signifies that sdnc tried to wait for the controller to connect to
    all switches but timed out before that could happen.
    """

    pass


class StartNetworkReport(ProgressReport):
    """
    Reports the network start.
    """

    pass


class StopNetworkReport(ProgressReport):
    """
    Reports the network stoppage.
    """

    pass


class RestartNetworkReport(ProgressReport):
    """
    Reports the network restart.
    """

    pass


class NetProvider(ProgressObservable):
    """
    This class can be used to start
    :class:`ScenarioNets <sdnc.core.scenario_net.ScenarioNet>` in a safe
    and controlled manner.
    """

    def __init__(self, topo: Topo, controller: Controller):
        super().__init__()
        self.topo = topo
        self.controller = controller
        self._net: Optional[ScenarioNet] = None

    def _do_start(self) -> None:
        if self._net is not None:
            return

        Cleanup().cleanup()  # for safety
        self._net = ScenarioNet(
            controller=RemoteController,
            switch=partial(OVSSwitch, protocols="OpenFlow10,OpenFlow13"),
        )
        ctrl = RemoteController("c0", ip="127.0.0.1", port=6633)
        self._net.addController(ctrl)
        self._net.buildFromTopo(self.topo)
        self._net.start()
        self.controller.start()
        if not self._net.waitConnected(timeout=10):
            self._do_stop()
            self._notify_all(StopNetworkReport())
            raise ControllerConnectTimeoutError()

    def _do_stop(self) -> None:
        if self._net is not None:
            self.controller.stop()
            self.controller.wait()
            try:
                self._net.stop()
            # These exceptions are known to occur on partial teardowns
            # they may of course also occur for other reasons, which
            # might become a debugging nightmare at some point
            # TODO: Perhaps there is some way of capturing only more
            # specific exceptions?
            except (AttributeError, AssertionError):
                Cleanup.cleanup()
            self._net = None

    def restart(self) -> None:
        """
        :meth:`stop` followed by :meth:`start`, but produces the correct
        report for observers.
        """
        if self.has_net():
            self._notify_all(RestartNetworkReport())
            self._do_stop()
            self._do_start()
        else:
            self.start()

    def start(self) -> None:
        """
        Starts a new network, if none is currently running. Otherwise,
        does nothing.
        """
        if not self.has_net():
            self._notify_all(StartNetworkReport())
            self._do_start()

    def stop(self) -> None:
        """
        Stops the most recent network previously started by this
        provider. If no network is currently running, this method does
        nothing.
        """
        if self.has_net():
            self._do_stop()
            self._notify_all(StopNetworkReport())

    def get(self) -> Optional[ScenarioNet]:
        """
        Returns the last network started by this provider. The returned
        net may not be running. May return null, if :meth:`has_net` is
        false.
        """
        return self._net

    def has_net(self) -> bool:
        """
        Returns true if and only if :meth:`start` was called at least
        once and no call to :meth:`stop` has been made since.
        """
        return self._net is not None

    def __enter__(self) -> NetProvider:
        return self

    def __exit__(
        self, ex_t: type[BaseException], ex_val: BaseException, trace: TracebackType
    ) -> None:
        if ex_t is not None:
            traceback.print_exception(ex_t, ex_val, trace)
        self.stop()
