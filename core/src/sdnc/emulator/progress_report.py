from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass
class ProgressReport:
    """
    A base class for dataclasses containing information regarding the
    progress of an automated evaluation, as well as general network
    status updates.
    """

    pass


class ProgressObserver(ABC):
    """
    Abstract base class for observers to be called when an automated
    evaluation reports progress.
    """

    @abstractmethod
    def notify(self, report: ProgressReport) -> None:
        """
        Notify this observer that an event has occured.

        Args:
            report: A report describing the event.
        """
        pass


class ProgressObservable:
    """
    Interface for observables.
    """

    def __init__(self):
        self._observers = []

    def register(self, observer: ProgressObserver) -> None:
        """
        Registers a progress observer.

        Args:
            observer: The observer to be registered.
        """
        self._observers.append(observer)

    def remove(self, observer: ProgressObserver) -> None:
        """
        Removes a previously registered progress observer.
        If the same observer was registered multiple times, all
        registrations will be removed.

        Args:
            observer: The observer to be removed.
        """
        self._observers = [o for o in self._observers if o != observer]

    def _notify_all(self, report: ProgressReport) -> None:
        """
        Notifies all registered observers of the given report.

        Args:
            report: The report.
        """
        for observer in self._observers:
            observer.notify(report)
