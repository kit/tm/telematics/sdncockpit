import copy
import random
from abc import abstractmethod
from ipaddress import IPv4Address, IPv4Network
from typing import AbstractSet, Any, Iterable, Iterator, List, Optional, TypeVar

from sdnc.emulator.macaddr import MACaddr

T = TypeVar("T")


class BasicRange(AbstractSet[T]):
    """
    Abstract class.
    A set that can be sampled from randomly.

    Inheriting classes must at least provide __contains__, __iter__ and
    __len__.
    """

    @abstractmethod
    def random(self) -> T:
        """
        Randomly draw a single element from the range.
        """
        element = next(iter(self))
        index = random.randrange(0, len(self))
        for i, e in enumerate(self, 0):
            if i == index:
                element = e
        return element


class IntRange(BasicRange[int]):
    """
    A range of integers between a minimum and maximum (both inclusive).

    Args:
        minimum: The beginning of the range.
        maximum: The end of the range.

    Attributes:
        minimum: The beginning of the range.
        maximum: The end of the range. Defaults to minimum.
    """

    def __init__(self, minimum: int, maximum: Optional[int] = None) -> None:
        super().__init__()
        self._minimum = minimum
        self._maximum = maximum if maximum is not None else minimum

        if self._minimum > self._maximum:
            raise ValueError("Range bonudaries are reversed!")

    @property
    def minimum(self) -> int:
        return self._minimum

    @property
    def maximum(self) -> int:
        return self._maximum

    def random(self) -> int:
        return random.randint(self._minimum, self._maximum)

    def __contains__(self, value: Any) -> bool:
        if isinstance(value, (int, float)):
            return self._minimum <= value <= self._maximum
        if isinstance(value, IntRange):
            return self._minimum <= value._minimum and value._maximum <= self._maximum
        if isinstance(value, BasicRange):
            return all(e in self for e in value)
        return False

    def __iter__(self) -> Iterator[int]:
        return iter(range(self._minimum, self._maximum + 1))

    def __len__(self) -> int:
        return self._maximum - self._minimum + 1

    def __eq__(self, other: object) -> bool:
        if isinstance(other, IntRange):
            return self._minimum == other._minimum and self._maximum == other._maximum
        return super().__eq__(other)

    def __hash__(self) -> int:
        return hash((self._minimum, self._maximum))


class MACRange(BasicRange[MACaddr]):
    """
    A range of mac addresses between a minimum and maximum (both
    inclusive).

    Args:
        minimum: The beginning of the range.
        maximum: The end of the range.

    Attributes:
        minimum: The beginning of the range. (Read-Only)
        maximum: The end of the range. Defaults to minimum. (Read-Only)
    """

    def __init__(self, minimum: MACaddr, maximum: Optional[MACaddr] = None) -> None:
        super().__init__()
        self._minimum = minimum
        self._maximum = maximum if maximum is not None else minimum

        if int(self._minimum) > int(self._maximum):
            raise ValueError("Range bonudaries are reversed!")

    @property
    def minimum(self) -> MACaddr:
        return self._minimum

    @property
    def maximum(self) -> MACaddr:
        return self._maximum

    def random(self) -> MACaddr:
        return MACaddr(random.randint(int(self._minimum), int(self._maximum)))

    def __contains__(self, value: Any) -> bool:
        if isinstance(value, str):
            return MACaddr(value) in self
        if isinstance(value, MACaddr):
            return self._minimum <= value <= self._maximum
        if isinstance(value, MACRange):
            return self._minimum <= value._minimum and value._maximum <= self._maximum
        return False

    def __iter__(self) -> Iterator[MACaddr]:
        for i in range(int(self._minimum), int(self._maximum) + 1):
            yield MACaddr(i)

    def __len__(self) -> int:
        return int(self._maximum) - int(self._minimum) + 1

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MACRange):
            return self._minimum == other._minimum and self._maximum == other._maximum
        return super().__eq__(other)

    def __hash__(self) -> int:
        return hash((self._minimum, self._maximum))


class IPRange(BasicRange[IPv4Address]):
    """
    A range of IP addresses as an IPv4 CIDR subnet.

    Args:
        net: The subnet.

    Attributes:
        net: The subnet. (Read-Only)
    """

    def __init__(self, net: IPv4Network) -> None:
        super().__init__()
        self._net = net

    @property
    def net(self) -> IPv4Network:
        return self._net

    def random(self) -> IPv4Address:
        if self._net.num_addresses > 1:
            cidr_len = self._net.max_prefixlen - self._net.prefixlen
            bits = random.getrandbits(cidr_len)
        else:
            bits = 0
        return IPv4Address(self._net.network_address + bits)

    def __contains__(self, value: Any) -> bool:
        if isinstance(value, str):
            return IPv4Network(value) in self
        if isinstance(value, IPv4Address):
            return str(value) in self
        if isinstance(value, IPv4Network):
            return value.subnet_of(self._net)
        return False

    def __iter__(self) -> Iterator[IPv4Address]:
        return iter(self._net)

    def __len__(self) -> int:
        return self._net.num_addresses

    def __eq__(self, other: object) -> bool:
        if isinstance(other, IPRange):
            return self._net == other._net
        return super().__eq__(other)

    def __hash__(self) -> int:
        return hash(self._net)


U = TypeVar("U")


class RangeList(BasicRange[U]):
    """
    A list of ranges.

    Args:
        ranges: An iterable of ranges.

    Attributes:
        ranges: A list of ranges. (Read-Only)
    """

    def __init__(self, ranges: Iterable[BasicRange[U]]) -> None:
        super().__init__()
        # we need to store the ranges as a set, so that __eq__ and
        # __hash__ behave sensibly.
        # we also need to store the ranges as a list, so that
        # random.choice works in O(1) instead of O(n)

        self._ranges = frozenset(ranges)
        self._ranges_list = list(ranges)

    @property
    def ranges(self) -> List[BasicRange[U]]:
        return copy.deepcopy(self._ranges_list)

    def random(self) -> U:
        """
        Randomly draw a single element from the range.
        """
        return random.choice(self._ranges_list).random()

    def __contains__(self, value: Any) -> bool:
        return any((value in r) for r in self._ranges)

    def __iter__(self) -> Iterator[U]:
        for range_ in self._ranges:
            for element in range_:
                yield element

    def __len__(self) -> int:
        return sum(len(r) for r in self._ranges)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, RangeList):
            return self._ranges == other._ranges
        return NotImplemented

    def __hash__(self) -> int:
        return hash(self._ranges)
