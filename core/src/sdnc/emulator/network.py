from __future__ import annotations

import signal
import subprocess
from concurrent.futures import Executor, Future
from pathlib import Path
from tempfile import NamedTemporaryFile
from time import sleep
from typing import Any, Dict, Tuple

from mininet.link import Intf, TCLink
from mininet.node import Host, Node, Switch

from sdnc.types import Numeric


class TrafgenHost(Host):
    """
    Mininet host that can send traffic using trafgen.
    """

    def __init__(self, *args: Any, **kwargs: Any):
        name = kwargs.get("name", args[0])
        if "defaultRoute" not in kwargs:
            kwargs["defaultRoute"] = f"dev {name}-eth0"
        super().__init__(*args, **kwargs)

    def send(self, cfg: str, count: int) -> None:
        """
        Send traffic using the given trafgen config.

        Args:
            cfg: The trafgen config file's content.
            count: The number of packets to send with the given cfg.
        """
        if count == 0:
            return
        if count < 0:
            raise ValueError("count must be positive integer!")

        intf = self.intfList()[0]

        with NamedTemporaryFile(mode="w", suffix=".cfg") as conf_file:
            conf_file.write(cfg)
            conf_file.flush()

            cmd = ["trafgen", "-d", str(intf), "-c", conf_file.name, "-n", str(count)]
            # write trafgen's mmap files to /tmp/ so they don't
            # clutter our working directory
            with self.popen(
                cmd, cwd="/tmp/", stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
            ) as trafgenp:
                trafgenp.wait()


# type alias
MonitorDescriptor = int


class MonitoredLink(TCLink):
    """
    Mininet link that can be monitored for evaluation packets.
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self._monitors: Dict[MonitorDescriptor, Tuple[subprocess.Popen, Path]] = {}
        self._descriptor = 0

    def start_monitor(
        self, intf: Intf = None, direction: Node = None
    ) -> MonitorDescriptor:
        """
        Start monitoring the link. Multiple monitors can be started on
        the same link.

        Args:
            intf: The interface on which packets are captured. Can be
                this link's intf1 or intf2.
            direction: The direction of travel for which packets are
                captured. Can be either of the two nodes this link is
                connected to. Captured packets move 'towards' the
                specified node.
        Returns: A descriptor used to refer to the started monitor.
        """
        if intf is not None:
            if intf not in [self.intf1, self.intf2]:
                raise ValueError("intf must be one of intf1 or intf2!")
            if not isinstance(intf.node, Switch):
                raise ValueError("only interfaces on switches can be captured!")
        else:
            if isinstance(self.intf1.node, Switch):
                intf = self.intf1
            elif isinstance(self.intf2.node, Switch):
                intf = self.intf2
            else:
                raise ValueError("found no interface to capture on!")
                # this should probably raise a state error or something
                # similar, but we intend to implement this at some point
                # anyways, so it does not matter.
                # TODO: implement capture on host node interfaces

        parsed_direction = "inout"
        if direction is not None:
            if direction not in [self.intf1.node, self.intf2.node]:
                raise ValueError("direction mus be one of the connected nodes!")
            if intf.node == direction:
                parsed_direction = "in"
            else:
                parsed_direction = "out"

        with NamedTemporaryFile("wb", suffix=".pcap", delete=False) as file:
            path = Path(file.name)

        cmd = ["tcpdump", "-Q", parsed_direction, "-i", str(intf), "-w", str(path)]

        # pylint: disable=consider-using-with
        process = subprocess.Popen(cmd, stdout=None, stderr=subprocess.PIPE)
        # pylint: enable=consider-using-with

        assert process.stderr is not None
        for _ in process.stderr:
            # Tcpdump always prints one line to stderr on startup.
            # We'll use that to wait for the process to start running.
            break

        descriptor = self._descriptor
        self._descriptor += 1

        self._monitors[descriptor] = (process, path)

        return descriptor

    def timed_monitor(self, timeframe: Numeric, executor: Executor) -> Future[Path]:
        """
        Asynchronously monitor this link over some timeframe.

        Args:
            timeframe: The length of time in seconds for which the
                link will be monitored. The timeframe measures only how
                long the network's links are monitored for and does not
                take into accout setup and teardown of monitoring
                processes.
            executor: The executor used to make this asynchronous.
        Returns: A future containing the captured pcap file's path.
        """

        def _timed_monitor(self, timeframe: float) -> Path:
            descriptor = self.start_monitor()
            sleep(timeframe)
            return self.stop_monitor(descriptor)

        return executor.submit(_timed_monitor, self, timeframe)

    def stop_monitor(self, descriptor: MonitorDescriptor) -> Path:
        """
        Stop a monitoring process.

        Args:
            descriptor: A monitoring descriptor.
        Returns: The captured pcap file's path.
        """

        if descriptor not in self._monitors:
            raise ValueError("Unknown or stopped monitoring descriptor!")

        process, path = self._monitors.pop(descriptor)

        process.send_signal(signal.SIGINT)
        process.communicate()
        process.terminate()

        return path
