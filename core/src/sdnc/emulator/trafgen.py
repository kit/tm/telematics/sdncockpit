from __future__ import annotations

from ipaddress import IPv4Address, IPv4Network
from typing import Any, Dict

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.range_list import BasicRange, IntRange, IPRange, MACRange


class RandomUDPTrafgenCfg:
    """
    This class can be used to generate
    :class:`UDPTrafgenCfgs <UDPTrafgenCfg>` with randomly chosen
    parameters.

    On ethernet_src and ethernet_dst, the U/L and I/G bits do not
    receive special consideration!

    Args:
        ethernet_dst: BasicRange of ethernet destination addresses.
        ethernet_src: BasicRange of ethernet source addresses.
        ipv4_flags: IPv4 'flags' header field. Must be between 0 and 3.
            Is not randomized.
        ipv4_ttl: BasicRange of IPv4 'TTL' header field entries.
            Must be between 0 and 2^8-1.
        ipv4_src: BasicRange of IPv4 source addresses.
        ipv4_dst: BasicRange of IPv4 destination addresses.
        udp_src_port: BasicRange of UDP source ports.
            Must be between 0 and 2^16-1.
        udp_dst_port: BasicRange of UDP destination ports.
            Must be between 0 and 2^16-1.

    Raises:
        ValueError: If any parameters / attributes are outside their
            specified ranges.

    Attributes:
        ethernet_dst: BasicRange of ethernet destination addresses.
        ethernet_src: BasicRange of ethernet source addresses.
        ipv4_flags: IPv4 'flags' header field. IS between 0 and 3.
            Is not randomized.
        ipv4_ttl: BasicRange of IPv4 'TTL' header field entries.
            Is between 0 and 2^8-1.
        ipv4_src: BasicRange of IPv4 source addresses.
        ipv4_dst: BasicRange of IPv4 destination addresses.
        udp_src_port: BasicRange of UDP source ports.
            Is between 0 and 2^16-1.
        udp_dst_port: BasicRange of UDP destination ports.
            Is between 0 and 2^16-1.
    """

    def __init__(
        self,
        ethernet_dst: BasicRange[MACaddr] = MACRange(MACaddr("cc:cc:cc:cc:cc:11")),
        ethernet_src: BasicRange[MACaddr] = MACRange(MACaddr("cc:cc:cc:cc:cc:22")),
        ipv4_flags: int = 2,
        ipv4_ttl: BasicRange[int] = IntRange(255),
        ipv4_src: BasicRange[IPv4Address] = IPRange(IPv4Network("11.11.11.11")),
        ipv4_dst: BasicRange[IPv4Address] = IPRange(IPv4Network("22.22.22.22")),
        udp_src_port: BasicRange[int] = IntRange(11111),
        udp_dst_port: BasicRange[int] = IntRange(22222),
    ):
        self.ethernet_dst = ethernet_dst
        self.ethernet_src = ethernet_src
        self.ipv4_flags = ipv4_flags
        self.ipv4_ttl = ipv4_ttl
        self.ipv4_src = ipv4_src
        self.ipv4_dst = ipv4_dst
        self.udp_src_port = udp_src_port
        self.udp_dst_port = udp_dst_port

    def generate(self) -> UDPTrafgenCfg:
        """
        Randomly generates a trafgen configuration.
        Can be made deterministic via random.seed() or
        random.setstate().

        Returns:
            The generated configuration.
        """
        return UDPTrafgenCfg(
            ethernet_dst=self.ethernet_dst.random(),
            ethernet_src=self.ethernet_src.random(),
            ipv4_flags=self.ipv4_flags,
            ipv4_ttl=self.ipv4_ttl.random(),
            ipv4_src=self.ipv4_src.random(),
            ipv4_dst=self.ipv4_dst.random(),
            udp_src_port=self.udp_src_port.random(),
            udp_dst_port=self.udp_dst_port.random(),
        )

    @property
    def ipv4_flags(self) -> int:
        return self._ipv4_flags

    @ipv4_flags.setter
    def ipv4_flags(self, value: int) -> None:
        if not 0 <= value <= 3:
            raise ValueError("IPv4 Flags must be int between 0 and 3!")
        self._ipv4_flags = value

    @property
    def ipv4_ttl(self) -> BasicRange[int]:
        return self._ipv4_ttl

    @ipv4_ttl.setter
    def ipv4_ttl(self, value: BasicRange[int]) -> None:
        bounds = IntRange(0, 2**8 - 1)
        if value not in bounds:
            raise ValueError("IPv4 TTL must be int between 0 and 255!")
        self._ipv4_ttl = value

    @property
    def udp_src_port(self) -> BasicRange[int]:
        return self._udp_src_port

    @udp_src_port.setter
    def udp_src_port(self, value: BasicRange[int]) -> None:
        bounds = IntRange(0, 2**16 - 1)
        if value not in bounds:
            raise ValueError("Port number must be int between 0 and 65535!")
        self._udp_src_port = value

    @property
    def udp_dst_port(self) -> BasicRange[int]:
        return self._udp_dst_port

    @udp_dst_port.setter
    def udp_dst_port(self, value: BasicRange[int]) -> None:
        bounds = IntRange(0, 2**16 - 1)
        if value not in bounds:
            raise ValueError("Port number must be int between 0 and 65535!")
        self._udp_dst_port = value


class UDPTrafgenCfg:
    """
    This class is used to compile trafgen configuration files from a
    set of ethernet/ip/udp header fields.

    The trafgen configuration files generated in this way can be passed
    to the trafgen command line utility to generate network traffic
    consisting of UDP packets.

    Args:
        ethernet_dst: Ethernet destination address.
        ethernet_src: Ethernet source address.
        ipv4_flags: IPv4 'flags' header field. Must be between 0 and 3.
        ipv4_ttl: IPv4 'TTL' header field. Must be between 0 and 2^8-1.
        ipv4_src: IPv4 source address.
        ipv4_dst: IPv4 destination address.
        udp_src_port: UDP source port. Must be between 0 and 2^16-1.
        udp_dst_port: UDP destination port. Must be between 0 and
            2^16-1.

    Raises:
        ValueError: If any parameters / attributes are outside their
            specified ranges.

    Attributes:
        ethernet_dst: Ethernet destination address.
        ethernet_src: Ethernet source address.
        ipv4_flags: IPv4 'flags' header field. Is between 0 and 3.
        ipv4_ttl: IPv4 'TTL' header field. Is between 0 and 2^8-1.
        ipv4_src: IPv4 source address.
        ipv4_dst: IPv4 destination address.
        udp_src_port: UDP source port. Is between 0 and 2^16-1.
        udp_dst_port: UDP destination port. Is between 0 and 2^16-1.
    """

    def __init__(
        self,
        ethernet_dst: MACaddr = MACaddr("cc:cc:cc:cc:cc:11"),
        ethernet_src: MACaddr = MACaddr("cc:cc:cc:cc:cc:22"),
        ipv4_flags: int = 2,
        ipv4_ttl: int = 255,
        ipv4_src: IPv4Address = IPv4Address("11.11.11.11"),
        ipv4_dst: IPv4Address = IPv4Address("22.22.22.22"),
        udp_src_port: int = 11111,
        udp_dst_port: int = 22222,
    ):
        self.ethernet_dst = ethernet_dst
        self.ethernet_src = ethernet_src
        self.ipv4_flags = ipv4_flags
        self.ipv4_ttl = ipv4_ttl
        self.ipv4_src = ipv4_src
        self.ipv4_dst = ipv4_dst
        self.udp_src_port = udp_src_port
        self.udp_dst_port = udp_dst_port

    def generate(self, data_cfg: str = "") -> str:
        """
        Compiles the content of a trafgen configuration file as per this
        class's description and returns it as a string.


        Args:
            data_cfg: A string which can be inserted into the trafgen
                configuration file after the section which builds up
                this class's headers. Will be contained 'as is' in the
                compiled configuration.

        Returns:
            The compiled configuration.
        """
        s = "cpu(0): {\n"
        s += f"eth(daddr={str(self.ethernet_dst)}, "
        s += f"saddr={str(self.ethernet_src)}, "
        s += "proto=2048),\n"  # proto=IP

        s += f"ipv4(flags={self.ipv4_flags}, ttl={self.ipv4_ttl}, "
        s += "proto=17, "  # proto=UDP
        s += f"saddr={str(self.ipv4_src)}, daddr={str(self.ipv4_dst)}),\n"

        s += f"udp(sport={self.udp_src_port}, dport={self.udp_dst_port}),\n"

        if len(data_cfg) > 0:
            data_cfg = data_cfg.strip(",\n")
            s += f"{data_cfg},\n"

        s += "}\n"

        return s

    @property
    def ipv4_flags(self) -> int:
        return self._ipv4_flags

    @ipv4_flags.setter
    def ipv4_flags(self, value: int) -> None:
        if not 0 <= value <= 3 or not value % 1 == 0:
            raise ValueError("IPv4 Flags must be int between 0 and 3!")
        self._ipv4_flags = value

    @property
    def ipv4_ttl(self) -> int:
        return self._ipv4_ttl

    @ipv4_ttl.setter
    def ipv4_ttl(self, value: int) -> None:
        if not 0 <= value <= 2**8 - 1 or not value % 1 == 0:
            raise ValueError("IPv4 TTL must be int between 0 and 255!")
        self._ipv4_ttl = value

    @property
    def udp_src_port(self) -> int:
        return self._udp_src_port

    @udp_src_port.setter
    def udp_src_port(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1 or not value % 1 == 0:
            raise ValueError("Port number must be int between 0 and 65535!")
        self._udp_src_port = value

    @property
    def udp_dst_port(self) -> int:
        return self._udp_dst_port

    @udp_dst_port.setter
    def udp_dst_port(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1 or not value % 1 == 0:
            raise ValueError("Port number must be int between 0 and 65535!")
        self._udp_dst_port = value

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, UDPTrafgenCfg):
            return (
                self.ethernet_dst == other.ethernet_dst
                and self.ethernet_src == other.ethernet_src
                and self.ipv4_flags == other.ipv4_flags
                and self.ipv4_ttl == other.ipv4_ttl
                and self.ipv4_src == other.ipv4_src
                and self.ipv4_dst == other.ipv4_dst
                and self.udp_src_port == other.udp_src_port
                and self.udp_dst_port == other.udp_dst_port
            )
        return NotImplemented

    def serialize(self) -> Dict[str, Any]:
        return {
            "ethernet_dst": str(self.ethernet_dst),
            "ethernet_src": str(self.ethernet_src),
            "ipv4_flags": self.ipv4_flags,
            "ipv4_ttl": self.ipv4_ttl,
            "ipv4_src": str(self.ipv4_src),
            "ipv4_dst": str(self.ipv4_dst),
            "udp_src_port": self.udp_src_port,
            "udp_dst_port": self.udp_dst_port,
        }


class IDUDPTrafgenCfg:
    """
    This class is used to compile trafgen configuration files from a
    set of ethernet/ip/udp header fields and an ID generator scheme.

    The trafgen configuration files generated in this way can be passed
    to the trafgen command line utility to generate network traffic
    consisting of UDP packets with IDs as their payloads.

    Those IDs have the following structure:

    +---------+----------+--------------+---------+------------------+
    | test_id | event_id | padding (0s) | offset  | numbers 0 to 255 |
    +---------+----------+--------------+---------+------------------+
    | 16 bits | 16 bits  | 8 bits       | 16 bits | 8 bits           |
    +---------+----------+--------------+---------+------------------+

    The last field implies, that if a configuration file generated by
    this class is used to generate no more than 256 packets at a time,
    all of those packets will have IDs that are guaranteed to be unique
    within the generator run. If the calling code guarantees to use a
    unique combination of test_id, event_id and offset for each run and
    not to generate more than 250 packets per run, it is guaranteed that
    all combined IDs are unique across all generator runs.

    Args:
        udp_cfg: A trafgen configuration to supply the udp/ip stack cfg.
        test_id: Must be integer between 0 and 2^16-1.
        event_id: Must be integer between 0 and 2^16-1.
        offset: Must be integer between 0 and 2^16-1.

    Raises:
        ValueError: If any parameters / attributes are outside their
            specified ranges.

    Attributes:
        udp_cfg: A trafgen configuration to supply the udp/ip stack cfg.
        test_id: Is integer between 0 and 2^16-1.
        event_id: Is integer between 0 and 2^16-1.
        offset: Is integer between 0 and 2^16-1.
    """

    def __init__(
        self, udp_cfg: UDPTrafgenCfg, test_id: int, event_id: int, offset: int = 0
    ):
        self.udp_cfg = udp_cfg
        self.test_id = test_id
        self.event_id = event_id
        self.offset = offset

    def generate(self) -> str:
        """
        Compiles the content of a trafgen configuration file as per this
        class's description and returns it as a string.

        Returns:
            The compiled configuration.
        """
        s = f"c16({self.test_id}),\n"
        s += f"c16({self.event_id}),\n"
        s += "c8(0),\n"
        s += f"c16({self.offset}),\n"
        s += "dinc(0, 255)\n"

        return self.udp_cfg.generate(s)

    @property
    def test_id(self) -> int:
        return self._test_id

    @test_id.setter
    def test_id(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1 or not value % 1 == 0:
            raise ValueError("Test ID must be int between 0 and 65535!")
        self._test_id = value

    @property
    def event_id(self) -> int:
        return self._event_id

    @event_id.setter
    def event_id(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1 or not value % 1 == 0:
            raise ValueError("Event ID must be int between 0 and 65535!")
        self._event_id = value

    @property
    def offset(self) -> int:
        return self._offset

    @offset.setter
    def offset(self, value: int) -> None:
        if not 0 <= value <= 2**16 - 1 or not value % 1 == 0:
            raise ValueError("Offset must be int between 0 and 65535!")
        self._offset = value

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, IDUDPTrafgenCfg):
            return (
                self.udp_cfg == other.udp_cfg
                and self.test_id == other.test_id
                and self.event_id == other.event_id
                and self.offset == other.offset
            )
        return NotImplemented
