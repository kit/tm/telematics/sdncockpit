from __future__ import annotations

from abc import abstractmethod
from dataclasses import dataclass
from threading import Event, Thread
from typing import Optional, Type

from sdnc.emulator.netprovider import NetProvider
from sdnc.emulator.progress_report import ProgressObservable, ProgressReport
from sdnc.emulator.scenario import Scenario


class NetworkAction(ProgressObservable):
    """
    A network action is a program which can be executed on a given
    network. It is executed in its own thread and makes its progress
    during execution known via the ProgessObservable interface.
    This often includes intermediate and complete results.

    A network action may want to restart the network it is executed on,
    hence all network actions are given access to a NetProvider, not
    just a ScenarioNet.

    By default, only one NetworkAction should run on any given network
    at the same time.
    """

    def __init__(self) -> None:
        super().__init__()
        self._stop_flag = Event()
        self._thread: Optional[Thread] = None

    def start(self, scenario: Scenario, net_provider: NetProvider) -> None:
        """
        Starts the network action.

        The action may customize its behavior based on the given
        scenario.

        Args:
            scenario: The scenario for which to run this action.
            net_provider: The network(s) in which to run this action.
                May be used to start additional networks during
                execution.
        """
        self._thread = Thread(target=self._run, args=(scenario, net_provider))
        self._thread.start()

    @abstractmethod
    def _run(self, scenario: Scenario, net_provider: NetProvider) -> None:
        """
        The function that is executed in the thread started as part of
        the start() method.
        """
        pass

    def stop(self) -> None:
        """
        Signals the network action to stop early. If the action is not
        currently running, this does nothing.
        """
        self._stop_flag.set()
        self.wait()

    def is_running(self) -> bool:
        """
        Checks if the action is currently running.
        """
        return self._thread is not None and self._thread.is_alive()

    def wait(self) -> None:
        """
        Wait for the action to complete.
        """
        if self._thread is not None:
            self._thread.join()
            self._thread = None


@dataclass
class NetworkActionReport(ProgressReport):
    """
    Abstract base class for all generic progress reports about network
    actions.
    """

    action_type: Type[NetworkAction]


@dataclass
class StartNetworkActionReport(NetworkActionReport):
    """
    Reports the start of a network action.
    """

    pass


@dataclass
class AbortNetworkActionReport(NetworkActionReport):
    """
    Reports the abortion of a network action.
    """

    pass


@dataclass
class CompleteNetworkActionReport(NetworkActionReport):
    """
    Reports the completion of a network action.
    """

    pass
