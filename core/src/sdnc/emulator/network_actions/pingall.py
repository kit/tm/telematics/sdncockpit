from dataclasses import dataclass

from sdnc.emulator.netprovider import NetProvider
from sdnc.emulator.network_actions.network_action import (
    AbortNetworkActionReport,
    CompleteNetworkActionReport,
    NetworkAction,
    StartNetworkActionReport,
)
from sdnc.emulator.progress_report import ProgressReport
from sdnc.emulator.scenario import Scenario


@dataclass
class PingResult:
    """
    This class represents the result of a ping.

    Attributes:
        status: True to indicate success, False to indicate Failure.
        source: The name of the host from which the ping originated.
        destination: The name of the host to which the ping was sent.
    """

    status: bool
    source: str
    destination: str


@dataclass
class PingReport(ProgressReport):
    """
    Reports the completion and results of a ping.
    """

    result: PingResult


class Pingall(NetworkAction):
    """
    Runs pings between all pairs of hosts.
    """

    def _run(self, scenario: Scenario, net_provider: NetProvider) -> None:
        net = net_provider.get()

        self._notify_all(StartNetworkActionReport(Pingall))
        if net is None:
            self._notify_all(AbortNetworkActionReport(Pingall))
            return

        for source in net.hosts:
            for destination in net.hosts:
                if self._stop_flag.is_set():
                    self._notify_all(AbortNetworkActionReport(Pingall))
                    return
                sent, received = net.ping_route(source, destination)
                result = PingResult(
                    status=sent == received,
                    source=str(source),
                    destination=str(destination),
                )
                self._notify_all(PingReport(result))
        self._notify_all(CompleteNetworkActionReport(Pingall))
