from __future__ import annotations

from dataclasses import dataclass
from threading import Event
from time import sleep, time_ns
from typing import List

from sdnc.emulator.netprovider import NetProvider
from sdnc.emulator.network_actions.network_action import (
    AbortNetworkActionReport,
    CompleteNetworkActionReport,
    NetworkAction,
    StartNetworkActionReport,
)
from sdnc.emulator.progress_report import (
    ProgressObservable,
    ProgressObserver,
    ProgressReport,
)
from sdnc.emulator.scenario import (
    ObservationPointFeedback,
    Scenario,
    Test,
    TrafficEvent,
)
from sdnc.emulator.scenario_net import ScenarioNet
from sdnc.emulator.trace import FilteredPcap


@dataclass
class StartTestReport(ProgressReport):
    """
    Reports the start of a test.
    """

    test: Test


@dataclass
class TestFeedbackReport(ProgressReport):
    """
    Reports the completion of a test.
    """

    __test__ = False  # Avoid pytest trying to run this class as a test

    feedback: TestFeedback


@dataclass
class StartEventReport(ProgressReport):
    """
    Reports the start of a traffic event.
    """

    event: TrafficEvent


class _EventSchedule(ProgressObservable):
    """
    This class represents a schedule of
    :class:`TrafficEvents <sdnc.core.scenario.TrafficEvent>`.

    Args:
        events: A list of events to be scheduled.
    """

    def __init__(self, events: list[TrafficEvent]) -> None:
        super().__init__()
        self._events = sorted(events, key=lambda e: e.time)

    def run(self, net: ScenarioNet, stop_flag: Event = Event()) -> bool:
        """
        Runs the events.

        The order and (approximate) timing in which the events are run
        is determined by the events' time property.

        This function will `sleep()` as needed to achieve the
        appropriate timing.

        Args:
            net: The network to run on.
            stop_flag: A flag that can be set to abort the run.

        Returns:
            True if the run completed, False if it was aborted.
        """
        start = time_ns() * 10 ** (-6)  # we want milliseconds
        for event in self._events:
            if stop_flag.is_set():
                return False
            current = time_ns() * 10 ** (-6)  # we want milliseconds
            delta = start + event.time - current  # in ms
            if delta > 0:
                sleep(delta * 10 ** (-3))
            if stop_flag.is_set():
                return False
            self._notify_all(StartEventReport(event))
            net.run_event(event)
        return True


class _TestGroup(ProgressObservable, ProgressObserver):
    """
    Runs multiple tests in parallel.

    Args:
        tests: A list of tests to run in parallel.
    """

    def __init__(self, tests: list[Test]) -> None:
        super().__init__()
        self.tests = tests
        if any(t.restart for t in tests[1:]):
            raise ValueError("Only the first test in a group may restart!")

    def notify(self, report: ProgressReport) -> None:
        return self._notify_all(report)

    def run(self, net_provider: NetProvider, stop_flag: Event) -> bool:
        """
        Runs the test group.

        Args:
            net_provider: The network to run on.
            stop_flag: A flag that can be set to abort the run.

        Returns:
            True if the run completed, False if it was aborted.
        """
        if len(self.tests) == 0:
            return False
        for test in self.tests:
            self._notify_all(StartTestReport(test))

        # restart the network if necessary
        if self.tests[0].restart:
            net_provider.restart()
        if not net_provider.has_net():
            net_provider.start()
        net = net_provider.get()
        assert net is not None

        # setup observation points
        for test in self.tests:
            for obs_point in test.obs_points:
                if stop_flag.is_set():
                    return False
                net.start_monitor(obs_point)

        # run events
        events = [ev for test in self.tests for ev in test.events]
        schedule = _EventSchedule(events)
        schedule.register(self)
        if not schedule.run(net, stop_flag):
            return False

        # wait for network to process sent packets
        if stop_flag.is_set():
            return False
        net.wait_inactive(timeout=None)

        # evaluate observation points
        for test in self.tests:
            obs_feedbacks = []
            for obs_point in test.obs_points:
                if stop_flag.is_set():
                    return False
                pcap = net.stop_monitor(obs_point)

                if stop_flag.is_set():
                    return False
                pkt_gen = FilteredPcap(pcap).get(test_id=test.test_id)
                obs_feedbacks.append(obs_point.check(pkt_gen))

            # gather and emit feedback
            status = all(obs_fb.status for obs_fb in obs_feedbacks)
            test_feedback = TestFeedback(status, test, obs_feedbacks)
            self._notify_all(TestFeedbackReport(test_feedback))

        return True


class AutoEval(NetworkAction, ProgressObserver):
    """
    Evaluates controllers for given scenarios.
    """

    def notify(self, report: ProgressReport) -> None:
        self._notify_all(report)

    def _run(self, scenario: Scenario, net_provider: NetProvider) -> None:
        """
        Run the automated evaluation, notifying registered handlers
        about progress and results in the process. After the last test
        has been performed, the last network provided by
        this object's net_provider is kept running.
        """
        self._notify_all(StartNetworkActionReport(AutoEval))

        groups = self._group_tests(scenario.tests)
        for group in groups:
            group.register(self)
            aborted = not group.run(net_provider, self._stop_flag)
            if aborted or self._stop_flag.is_set():
                self._notify_all(AbortNetworkActionReport(AutoEval))
                return
        self._notify_all(CompleteNetworkActionReport(AutoEval))

    def _group_tests(self, tests: List[Test]) -> List[_TestGroup]:
        """
        Takes a list of tests and groups all that should be run in
        parallel.
        """
        groups: List[_TestGroup] = []
        group: List[Test] = []
        for test in tests:
            if test.restart or not test.parallel:
                if len(group) > 0:
                    groups.append(_TestGroup(group))
                group = []

            group.append(test)

            if not test.parallel:
                if len(group) > 0:
                    groups.append(_TestGroup(group))
                group = []
        if len(group) > 0:
            groups.append(_TestGroup(group))
        group = []
        return groups


@dataclass
class TestFeedback:
    """
    This class represents the result of an automated evaluation
    of a single, network-wide test.

    Attributes:
        status: True to indicate success, False to indicate failure.
        test: The test this object contains feedback for.
        obs_feedbacks: Objects representing feedback from the network's
            observation points.
    """

    status: bool
    test: Test
    observation_point_feedbacks: List[ObservationPointFeedback]
