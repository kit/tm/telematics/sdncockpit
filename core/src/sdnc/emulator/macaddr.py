from __future__ import annotations

import macaddress  # type: ignore


class MACaddr(macaddress.MAC):
    """
    This class only exists to set this application's canonical format
    for mac addresses.
    """

    formats = ("xx:xx:xx:xx:xx:xx",) + macaddress.MAC.formats

    def __str__(self) -> str:
        return super().__str__().lower()
