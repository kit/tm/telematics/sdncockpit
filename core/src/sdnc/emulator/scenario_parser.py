from __future__ import annotations

import os
from contextlib import contextmanager
from dataclasses import dataclass
from ipaddress import IPv4Address, IPv4Network
from typing import (
    Any,
    Callable,
    Dict,
    Iterator,
    List,
    Optional,
    TextIO,
    Tuple,
    Type,
    TypeVar,
)

import yaml
from mininet.topo import Topo
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.packet import Packet

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.range_list import BasicRange, IntRange, IPRange, MACRange, RangeList
from sdnc.emulator.scenario import (
    HeaderRule,
    ObservationPoint,
    RandomTrafficEvent,
    Scenario,
    Test,
)
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg
from sdnc.types import Numeric

_SPHINX_BUILD = bool(os.environ.get("SPHINX_BUILD", ""))


class ParseStack:
    """
    Data structure to keep track of where the parser is inside the file.
    """

    def __init__(self) -> None:
        super().__init__()
        self.prefixes: List[str] = []
        self.suffixes: List[str] = []

    def push(self, prefix: str = "", suffix: str = "") -> None:
        self.prefixes.append(prefix.strip())
        self.suffixes.append(suffix.strip())

    def pop(self) -> None:
        self.prefixes.pop()
        self.suffixes.pop()

    @contextmanager
    def frame(self, prefix: str = "", suffix: str = "") -> Iterator[None]:
        self.push(prefix, suffix)
        yield
        self.pop()

    def copy(self) -> ParseStack:
        copy = ParseStack()
        copy.prefixes = self.prefixes.copy()
        copy.suffixes = self.suffixes.copy()
        return copy

    def __str__(self) -> str:
        def pad(pref):
            if len(pref) > 0 and pref[-1] in [":", ","]:
                pref += " "
            return pref

        prefix = "".join(pad(pref) for pref in self.prefixes)
        suffix = "".join(reversed(self.suffixes))
        return f"{prefix} ~~~~~ {suffix}"


class ScenarioSemanticError(Exception):
    """
    Parsing was aborted due to a critical error in the scenario file's
    semantics.

    Attributes:
        alert: An alert describing the error.
    """

    def __init__(self, alert: ParseAlert):
        super().__init__(str(alert))
        self.alert = alert


@dataclass
class ParseAlert:
    """
    An alert generated during parsing.

    Attributes:
        stack: Describes what part of the scenario file the alert is
            related to.
        message: A message describing the alert.
    """

    stack: ParseStack
    message: str

    def __str__(self) -> str:
        return f"{self.message}\n here: {str(self.stack)}"


def _objectparser(f):
    if _SPHINX_BUILD:
        return f  # to generate docs correctly

    def wrapper(*args, **kwargs):
        parser_instance = args[0]
        # pylint: disable-next=protected-access
        with parser_instance._stack.frame("{", "}"):
            return f(*args, **kwargs)

    return wrapper


class Parser:
    """
    Parses scenario files.
    """

    _MISSING_ATTR_INFO = "Consider adding an attribute '{attr}'."
    _MISSING_ATTR_WARN = "No '{attr}' attribute found."
    _MISSING_ATTR_ERROR = "No '{attr}' attribute found."
    _UNEXPECTED_ATTR_WARN = "Unexpected attribute '{attr}'."

    _EMPTY_LIST_ERROR = "List must not be empty."

    _EXPECTED_TYPE_ERROR = "Expected {expected}, found {found}."

    _BOUNDED_VALUE_ERROR = "Value must be between {min} and {max}, but is {val}."

    _NON_NEGATIVE = "Value must be non-negative, but is {value}!"

    _RANGE_FORM_ERROR = (
        "A range mus be a single value, or an object containing two "
        "entries: min, and max. Alternatively, you may also provide a "
        "list of such ranges."
    )
    _RANGE_ORDER_ERROR = (
        "Minimum of range (here: {min}) must be no greater than maximum "
        "(here: {max})."
    )
    _IP_RANGE_FORM_ERROR = (
        "A range of IPs must be written as a single CIDR. "
        "Alternatively, you may also provide a list of such ranges."
    )

    _UNKNOWN_NODE = "Unknown node '{node}'"
    _UNKNOWN_HOST = "Unknown host '{host}'"
    _UNKNOWN_LINK = "Unknown link '{link}'"
    _DUPLICATE_NODE = "Duplicate network nodt: '{node}'"
    _DUPLICATE_DPID = "Duplicate dpid: '{dpid}'"
    _DUPLICATE_LINK = "Duplicate link: '{link}'"
    _LINK_FORM = (
        "Links are specified as lists [node1, node2] with exactly two " "entries!"
    )

    _OBS_NODE_HOST_ERR = "An observation point node can only be a switch, not a host!"
    _OBS_NODE_ERR = (
        "An observation point node can be any switch connected to the "
        "observed link. Node '{node}' is not connected to the link '{link}'."
    )
    _OBS_DIRECTION_ERR = (
        "The observation point direction can be either node conencted to the "
        "observed link. Node '{node}' is not connected to the link '{link}'."
    )

    # the following arrays and dicts specify how different header fields should
    # be parsed
    _RYU_NAME_TO_FIELD: Dict[str, Tuple[Packet, str]] = {
        "ethernet.dst": (Ether, "dst"),
        "ethernet.src": (Ether, "src"),
        "ethernet.ethertype": (Ether, "type"),
        "ipv4.version": (IP, "version"),
        "ipv4.tos": (IP, "tos"),
        "ipv4.ttl": (IP, "ttl"),
        "ipv4.src": (IP, "src"),
        "ipv4.dst": (IP, "dst"),
        "udp.src_port": (UDP, "sport"),
        "udp.dst_port": (UDP, "dport"),
    }
    _HEADER_ENTRIES_INT = {  # values are boundaries for the field's values
        "ethernet.ethertype": (0, 0xFFFF),
        "ipv4.version": (0, 0xF),
        "ipv4.tos": (0, 0xFF),
        "ipv4.ttl": (0, 0xFF),
        "udp.src_port": (0, 0xFFFF),
        "udp.dst_port": (0, 0xFFFF),
    }
    _HEADER_ENTRIES_ETH = ["ethernet.src", "ethernet.dst"]
    _HEADER_ENTRIES_IP = ["ipv4.src", "ipv4.dst"]

    _UNKNOWN_FIELD = (
        "The field '{field}' is not supported. Supported fields are: "
        + str(list(_RYU_NAME_TO_FIELD.keys()))
    )

    _LEVEL_IGNORE = 0
    _LEVEL_INFO = 1
    _LEVEL_WARN = 2
    _LEVEL_ERR = 3

    def __init__(self):
        self._clean_state()

    def _clean_state(self) -> None:
        self._stack = ParseStack()
        self._info: List[ParseAlert] = []
        self._warn: List[ParseAlert] = []
        self._topo = Topo()

    def parse(
        self, file: TextIO
    ) -> Tuple[Scenario, List[ParseAlert], List[ParseAlert]]:
        """
        Parses the given scenario file.

        Args:
            file: The opened scenario file.

        Returns:
            A tuple consisting of a fully parsed, validated and
            canonized :class:`~sdnc.core.scenario.Scenario`, a list of
            :class:`ParseAlerts <ParseAlert>` as infos, and a list of
            :class:`ParseAlerts <ParseAlert>` as warnings.
        """
        self._clean_state()

        base = yaml.load(file, Loader=yaml.Loader)

        scenario = self._parse_base(base)

        info = self._info
        warn = self._warn

        self._clean_state()
        return scenario, info, warn

    # ...
    @_objectparser
    def _parse_base(self, base: Any) -> Scenario:
        """
        Parses the base element of the scenario file and returns the
        parsed scenario.

        Args:
            base: The base element of the scenario file containing
                the scenario's name, description, topology and
                evaluations.
        Returns:
            The parsed scenario.
        """
        attributes = {
            "name": Parser._LEVEL_INFO,
            "description": Parser._LEVEL_WARN,
            "topology": Parser._LEVEL_ERR,
            "tests": Parser._LEVEL_INFO,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("name:"):
            name = self._parse_string(base.get("name", ""))

        with self._stack.frame("description:"):
            desc = self._parse_string(base.get("description", ""))

        with self._stack.frame("topology:"):
            topo = self._parse_topology(base["topology"])

        tests = []
        if base.get("tests") is not None:
            with self._stack.frame("tests:"):
                tests = self._parse_list(base.get("tests"), self._parse_test)

        return Scenario(name, desc, topo, tests)

    # {topology: ...}
    @_objectparser
    def _parse_topology(self, base: Any) -> Topo:
        """
        Parses the scenario file's topology section.

        Args:
            base: The topology section containing hosts, switches
                and links.

        Returns: A topology object containing the parsed topology info.
            This object is also placed in self._topo.
        """
        attributes = {
            "hosts": Parser._LEVEL_ERR,
            "switches": Parser._LEVEL_ERR,
            "links": Parser._LEVEL_ERR,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("hosts:"):
            self._parse_list(base["hosts"], self._parse_host, allow_empty=False)

        with self._stack.frame("switches:"):
            self._parse_list(base["switches"], self._parse_switch, allow_empty=False)

        with self._stack.frame("links:"):
            self._parse_list(base["links"], self._parse_link, allow_empty=False)

        return self._topo

    # {topology: {hosts: [...]}}
    @_objectparser
    def _parse_host(self, base: Any) -> None:
        """
        Parses a host entry and adds the host to self._topo.

        Args:
            base: The host entry containing the host's name and ip.
        """
        attributes = {
            "name": Parser._LEVEL_ERR,
            "ipv4": Parser._LEVEL_INFO,
            "mac": Parser._LEVEL_INFO,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        param = {}

        with self._stack.frame("name:"):
            param["name"] = self._parse_string(base["name"])
            self._check_duplicate_node(param["name"])

        with self._stack.frame("ipv4:"):
            if "ipv4" in base:
                # TODO: can/should we accept a subnet here?
                param["ip"] = str(self._parse_ipv4addr(base["ipv4"]))

        with self._stack.frame("mac:"):
            if "mac" in base:
                param["mac"] = str(self._parse_mac(base["mac"]))

        self._topo.addHost(**param)

    # {topology: {switches: [...]}}
    @_objectparser
    def _parse_switch(self, base: Any) -> None:
        """
        Parses a switch entry and adds the switch to self._topo.

        Args:
            base: The switch entry containing the switch's name and
                dpid.
        """
        attributes = {"name": Parser._LEVEL_ERR, "dpid": Parser._LEVEL_ERR}
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("name:"):
            name = self._parse_string(base["name"])
            self._check_duplicate_node(name)

        with self._stack.frame("dpid:"):
            dpid = self._parse_int(base["dpid"])
            # we use decimal dpids, but for some reason mininet switches
            # expect hex strings as dpid arguments
            hex_dpid = format(dpid, "x")
            self._check_bounds(dpid, 0, 2**64 - 1)
            for switch in self._topo.switches():
                if self._topo.nodeInfo(switch)["dpid"] == hex_dpid:
                    msg = Parser._DUPLICATE_DPID.format(dpid=dpid)
                    raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        self._topo.addSwitch(name, dpid=hex_dpid)

    # {topology: {links: [...]}}
    def _parse_link(self, base: Any) -> None:
        """
        Parses a link entry and adds the link to self._topo.

        Args:
            base: The link entry containing the list containing the
                two hosts connected with this link.
        """
        # TODO: deal with traffic control parameters (proper ones)
        # new format for links:
        # links:
        #   - nodes: [h1, s1]
        #     options: {}
        link = self._parse_linkname(base)

        if self._link_exists(link):
            msg = Parser._DUPLICATE_LINK.format(link=str(link))
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        try:
            self._topo.addLink(*link)
        except KeyError as err:
            msg = Parser._UNKNOWN_NODE.format(node=str(err))
            # pylint: disable-next=raise-missing-from
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # {tests: [...]}
    @_objectparser
    def _parse_test(self, base: Any) -> Test:
        """
        Parses a test.

        Args:
            base: The test entry containing the test's name,
                description, restart switch, traffic events and
                observation points.

        Returns:
            The parsed test object.
        """
        attributes = {
            "name": Parser._LEVEL_INFO,
            "description": Parser._LEVEL_INFO,
            "restart": Parser._LEVEL_IGNORE,
            "traffic_events": Parser._LEVEL_ERR,
            "observation_points": Parser._LEVEL_INFO,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("name:"):
            name = self._parse_string(base.get("name", ""))

        with self._stack.frame("description:"):
            desc = self._parse_string(base.get("description", ""))

        with self._stack.frame("restart:"):
            restart = self._parse_bool(base.get("restart", False))

        with self._stack.frame("parallel:"):
            parallel = self._parse_bool(base.get("parallel", False))

        with self._stack.frame("traffic_events:"):
            rnd_events = self._parse_list(
                base["traffic_events"], self._parse_traffic_event, allow_empty=False
            )

        with self._stack.frame("observation_points:"):
            obs_points = self._parse_list(
                base.get("observation_points", []), self._parse_observation_point
            )

        return Test(name, desc, restart, parallel, rnd_events, obs_points)

    # {tests: [{traffic_events: [...]}]}
    @_objectparser
    def _parse_traffic_event(self, base: Any) -> RandomTrafficEvent:
        """
        Parses a traffic event.

        Args:
            base: The traffic event entry containing the traffic
                event's event count, node, packet_count, time and
                headers.
        Returns:
            The parsed random traffic event object.
        """
        attributes = {
            "event_count": Parser._LEVEL_INFO,
            "host": Parser._LEVEL_ERR,
            "header": Parser._LEVEL_WARN,
            "packet_count": Parser._LEVEL_INFO,
            "time": Parser._LEVEL_IGNORE,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("event_count:"):
            event_count = self._parse_int_range(base.get("event_count", 1))

        with self._stack.frame("host:"):
            host = self._parse_string(base.get("host", ""))
            if host not in self._topo.hosts():
                msg = Parser._UNKNOWN_HOST.format(host=host)
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        with self._stack.frame("header:"):
            host_info = self._topo.nodeInfo(host)
            src_mac = host_info["mac"] if "mac" in host_info else None
            src_ip = host_info["ip"] if "ip" in host_info else None

            rnd_header = self._parse_event_header(
                base.get("header", {}), src_mac, src_ip
            )

        with self._stack.frame("packet_count:"):
            packet_count = self._parse_int_range(base.get("packet_count", 1))

        with self._stack.frame("time:"):
            time = self._parse_int_range(base.get("time", 0))

        return RandomTrafficEvent(event_count, host, rnd_header, packet_count, time)

    # {tests: [{traffic_events: [{header: ...}]}]}
    @_objectparser
    def _parse_event_header(
        self, base: Dict, src_mac: Optional[str] = None, src_ip: Optional[str] = None
    ) -> RandomUDPTrafgenCfg:
        """
        Parses a header section of a traffic event.

        Args:
            base: The header section containing header field entries.
            src_mac: The default source mac to use.
            src_ip: The default source ip to use.
        Returns:
            The parsed randomized trafgen configuration.
        """
        attributes = {
            "ethernet.dst": Parser._LEVEL_IGNORE,
            "ethernet.src": Parser._LEVEL_IGNORE,
            "ipv4.flags": Parser._LEVEL_IGNORE,
            "ipv4.ttl": Parser._LEVEL_IGNORE,
            "ipv4.src": Parser._LEVEL_IGNORE,
            "ipv4.dst": Parser._LEVEL_IGNORE,
            "udp.src_port": Parser._LEVEL_IGNORE,
            "udp.dst_port": Parser._LEVEL_IGNORE,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        params: Dict[str, Any] = {}
        for key, value in base.items():
            self._stack.push(f"{key}:")
            param_key = key.replace(".", "_")

            if key in ("ethernet.dst", "ethernet.src"):
                params[param_key] = self._parse_mac_range(value)

            elif key in ("ipv4.flags"):
                flags = self._parse_int(value)
                self._check_bounds(flags, 0, 3)
                params[param_key] = flags

            elif key in ("ipv4.ttl"):
                params[param_key] = self._parse_int_range(
                    value, lower_bound=0, upper_bound=2**8 - 1
                )

            elif key in ("udp.src_port", "udp.dst_port"):
                params[param_key] = self._parse_int_range(
                    value, lower_bound=0, upper_bound=2**16 - 1
                )

            elif key in ("ipv4.src", "ipv4.dst"):
                params[param_key] = self._parse_ip_range(value)

            self._stack.pop()

        if "ethernet_src" not in params and src_mac is not None:
            macrange = MACRange(MACaddr(src_mac), MACaddr(src_mac))
            params["ethernet_src"] = RangeList([macrange])
        if "ipv4_src" not in params and src_ip is not None:
            iprange = IPRange(IPv4Network(src_ip))
            params["ipv4_src"] = RangeList([iprange])

        return RandomUDPTrafgenCfg(**params)

    @_objectparser
    def _parse_observation_point(self, base: Any) -> ObservationPoint:
        """
        Parses an observation point.

        Args:
            base: The observation point entry.
        Returns:
            The parsed observation point.
        """
        attributes = {
            "link": Parser._LEVEL_ERR,
            "node": Parser._LEVEL_IGNORE,
            "direction": Parser._LEVEL_INFO,
            "packet_count": Parser._LEVEL_INFO,
            "header_rules": Parser._LEVEL_INFO,
        }
        self._check_is_dict(base)
        self._check_attributes(base, attributes)

        with self._stack.frame("link:"):
            link = self._parse_linkname(base["link"])
            if not self._link_exists(link):
                msg = Parser._UNKNOWN_LINK.format(link=link)
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        with self._stack.frame("node:"):
            if "node" in base:
                node = self._parse_string(base["node"])
                if node not in link:
                    msg = Parser._OBS_NODE_ERR.format(node=node, link=link)
                    raise ScenarioSemanticError(ParseAlert(self._stack, msg))
                if node in self._topo.hosts():
                    msg = Parser._OBS_NODE_HOST_ERR
                    raise ScenarioSemanticError(ParseAlert(self._stack, msg))
            else:
                node = None

        with self._stack.frame("direction:"):
            if "direction" in base:
                direction = self._parse_string(base["direction"])
                if direction not in link:
                    msg = Parser._OBS_DIRECTION_ERR.format(node=direction, link=link)
                    raise ScenarioSemanticError(ParseAlert(self._stack, msg))
            else:
                direction = None

        with self._stack.frame("packet_count:"):
            if "packet_count" in base:
                count = self._parse_int_range(base["packet_count"], 0, None)
            else:
                count = None

        with self._stack.frame("header_rules:"):
            header_rules = self._parse_header_rules(base.get("header_rules", {}))

        return ObservationPoint(link, node, direction, count, header_rules)

    @_objectparser
    def _parse_header_rules(self, base: Any) -> List[HeaderRule]:
        """
        Parses a dictionary of header rules.

        Args:
            base: The header rules section.

        Returns:
            The parsed list of header rules.
        """
        self._check_is_dict(base)
        header_rules = []
        for field, value in base.items():
            header_rules.append(self._parse_header_rule(field, value))
        return header_rules

    def _parse_header_rule(self, proto_field: Any, value: Any) -> HeaderRule:
        """
        Parses a single header rule.

        Args:
            proto_field: The protocol field to which the rule applies.
            value: The expected value of the field.

        Returns:
            The parsed header rule.
        """
        with self._stack.frame("<protocol.field>"):
            protocol, field = self._parse_proto_field(proto_field)

        with self._stack.frame(f"{proto_field}:"):
            val_range: Optional[BasicRange[Any]] = None

            if proto_field in Parser._HEADER_ENTRIES_INT:
                lower_bound, upper_bound = Parser._HEADER_ENTRIES_INT[proto_field]
                val_range = self._parse_int_range(value, lower_bound, upper_bound)

            if proto_field in Parser._HEADER_ENTRIES_ETH:
                val_range = self._parse_mac_range(value)

            if proto_field in Parser._HEADER_ENTRIES_IP:
                val_range = self._parse_ip_range(
                    value,
                )

            if val_range:
                return HeaderRule(protocol, field, val_range)
            else:
                msg = Parser._UNKNOWN_FIELD.format(field=proto_field)
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # #### Helpers for Types and Values ##################################### #
    # ---- Generic Types ---------------------------------------------------- #
    def _check_type(
        self,
        obj: Any,
        exp_type: Type[Any],
        expected: Optional[str] = None,
        found: Optional[str] = None,
    ):
        if expected is None:
            expected = exp_type.__name__
        if found is None:
            found = type(obj).__name__

        if not isinstance(obj, exp_type):
            msg = Parser._EXPECTED_TYPE_ERROR.format(expected=expected, found=found)
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # ---- Strings ---------------------------------------------------------- #
    def _parse_string(self, obj: Any) -> str:
        self._check_type(obj, str, "string")
        return obj

    # ---- Booleans --------------------------------------------------------- #
    def _parse_bool(self, obj: Any) -> bool:
        self._check_type(obj, bool, "boolean")
        return obj

    # ---- Numbers ---------------------------------------------------------- #
    def _parse_int(self, obj: Any) -> int:
        self._check_type(obj, int, "integer")
        return obj

    def _check_bounds(
        self,
        val: Numeric,
        min_val: Optional[Numeric] = None,
        max_val: Optional[Numeric] = None,
    ) -> None:
        if (
            min_val is not None
            and val < min_val
            or max_val is not None
            and max_val < val
        ):
            msg = Parser._BOUNDED_VALUE_ERROR.format(min=min_val, max=max_val, val=val)
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # ---- Addresses -------------------------------------------------------- #
    def _parse_ipv4addr(self, obj: Any) -> IPv4Address:
        try:
            obj = IPv4Address(obj)
        except ValueError:
            pass
        self._check_type(
            obj,
            IPv4Address,
            expected="valid IPv4 address string with format 255.255.255.255",
            found=str(obj),
        )
        return obj

    def _parse_ipv4net(self, obj: Any) -> IPv4Network:
        try:
            obj = IPv4Network(obj)
        except (ValueError, TypeError):
            pass
        self._check_type(
            obj,
            IPv4Network,
            expected=(
                "valid IPv4 network string with format "
                "255.255.255.255/32 or valid IPv4 address with format "
                "255.255.255.255. Make sure no host bits are set when "
                "using CIDR."
            ),
            found=str(obj),
        )
        return obj

    def _parse_mac(self, obj: Any) -> MACaddr:
        """
        Args:
            obj: The MAC address as a string.

        Returns:
            The parsed MAC address.

        Raises:
            ParseError: If the given object is not a string or the given
                object is a string but not a valid MAC address.
        """
        expected = "valid mac address string with format ff:ff:ff:ff:ff:ff"
        msg = Parser._EXPECTED_TYPE_ERROR.format(expected=expected, found=str(obj))
        if not isinstance(obj, str):
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        try:
            return MACaddr(obj)
        except ValueError:
            # pylint: disable-next=raise-missing-from
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # ---- Ranges ----------------------------------------------------------- #
    def _parse_int_range(
        self,
        base: Any,
        lower_bound: Optional[int] = None,
        upper_bound: Optional[int] = None,
    ) -> BasicRange[int]:
        if isinstance(base, list):

            def parser(base_):
                return self._parse_int_range(base_, lower_bound, upper_bound)

            ranges = self._parse_list(base, parser, False)
            return RangeList(ranges)
        elif isinstance(base, int):
            value = self._parse_int(base)
            self._check_bounds(value, lower_bound, upper_bound)
            return IntRange(value)
        elif isinstance(base, dict):
            attributes = {
                "min": Parser._LEVEL_ERR,
                "max": Parser._LEVEL_ERR,
            }
            self._check_attributes(base, attributes)

            with self._stack.frame("min:"):
                minimum = self._parse_int(base.get("min", ""))
                self._check_bounds(minimum, lower_bound, upper_bound)
            with self._stack.frame("max:"):
                maximum = self._parse_int(base.get("max", ""))
                self._check_bounds(maximum, lower_bound, upper_bound)
            try:
                return IntRange(minimum, maximum)
            except ValueError:
                msg = Parser._RANGE_ORDER_ERROR.format(min=minimum, max=maximum)
                # pylint: disable-next=raise-missing-from
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))
        else:
            msg = Parser._RANGE_FORM_ERROR.format()
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    def _parse_mac_range(self, base: Any) -> BasicRange[MACaddr]:
        if isinstance(base, list):

            def parser(base_):
                return self._parse_mac_range(base_)

            ranges = self._parse_list(base, parser, False)
            return RangeList(ranges)
        elif isinstance(base, str):
            return MACRange(self._parse_mac(base))
        elif isinstance(base, dict):
            attributes = {
                "min": Parser._LEVEL_ERR,
                "max": Parser._LEVEL_ERR,
            }
            self._check_attributes(base, attributes)

            with self._stack.frame("min:"):
                minimum = self._parse_mac(base.get("min", ""))
            with self._stack.frame("max:"):
                maximum = self._parse_mac(base.get("max", ""))
            try:
                return MACRange(minimum, maximum)
            except ValueError:
                msg = Parser._RANGE_ORDER_ERROR.format(min=minimum, max=maximum)
                # pylint: disable-next=raise-missing-from
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))
        else:
            msg = Parser._RANGE_FORM_ERROR.format()
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    def _parse_ip_range(self, base: Any) -> BasicRange[IPv4Address]:
        if isinstance(base, list):

            def parser(base_):
                return self._parse_ip_range(base_)

            ranges = self._parse_list(base, parser, False)
            return RangeList(ranges)
        elif isinstance(base, str):
            net = self._parse_ipv4net(base)
            return IPRange(net)
        else:
            msg = Parser._IP_RANGE_FORM_ERROR.format(base)
            # pylint: disable-next=raise-missing-from
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    # ---- Network Objects -------------------------------------------------- #
    def _check_duplicate_node(self, node: str) -> None:
        if node in self._topo.nodes():
            msg = Parser._DUPLICATE_NODE.format(node=node)
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

    def _parse_linkname(self, obj: None) -> Tuple[str, str]:
        """
        :param: The link name entry.
        :returns: The link as a tuple.
        """
        nodelist = self._check_is_list(obj)

        if len(nodelist) != 2:
            msg = Parser._LINK_FORM
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        with self._stack.frame("[", ", ...]"):
            node1 = self._parse_string(nodelist[0])

        with self._stack.frame("[...,", "]"):
            node2 = self._parse_string(nodelist[1])

        return node1, node2

    def _link_exists(self, link: Tuple[str, str]) -> bool:
        """
        :returns: True if and only if the given link exists in self._topo.
        """
        node1, node2 = link
        return link in self._topo.links() or (node2, node1) in self._topo.links()

    # ---- Lists ------------------------------------------------------------ #
    def _check_is_list(self, obj: Any) -> list:
        self._check_type(obj, list, "list")
        return obj

    U = TypeVar("U")

    def _parse_list(
        self, base: Any, parse_entry: Callable[[Any], U], allow_empty: bool = True
    ) -> List[U]:
        """
        Calls the given parse function applies it to each entry of base,
        if base is a list. Returns a list containing the returned values
        of all those function calls.
        """
        with self._stack.frame("[", "]"):
            self._check_is_list(base)
            if not allow_empty and not base:
                msg = Parser._EMPTY_LIST_ERROR
                raise ScenarioSemanticError(ParseAlert(self._stack, msg))

            parsed = []
            for i, entry in enumerate(base):
                if len(base) == 1:
                    parsed.append(parse_entry(entry))
                else:
                    if i == 0:
                        with self._stack.frame("", ", ..."):
                            parsed.append(parse_entry(entry))
                    elif i == len(base) - 1:
                        with self._stack.frame("...,", ""):
                            parsed.append(parse_entry(entry))
                    else:
                        with self._stack.frame("...,", ", ..."):
                            parsed.append(parse_entry(entry))

        return parsed

    # ---- Dicts ------------------------------------------------------------ #
    def _check_is_dict(self, obj: Any) -> Dict:
        self._check_type(obj, dict, "non-empty dictionary")
        return obj

    def _check_attributes(self, base: Dict, attributes: Dict[str, int]) -> None:
        for attr, level in attributes.items():
            if attr not in base:
                if level == Parser._LEVEL_INFO:
                    msg = Parser._MISSING_ATTR_INFO.format(attr=attr)
                    self._info.append(ParseAlert(self._stack, msg))
                if level == Parser._LEVEL_WARN:
                    msg = Parser._MISSING_ATTR_WARN.format(attr=attr)
                    self._warn.append(ParseAlert(self._stack, msg))
                if level == Parser._LEVEL_ERR:
                    msg = Parser._MISSING_ATTR_ERROR.format(attr=attr)
                    raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        for key in base.keys():
            if key not in attributes.keys():
                msg = Parser._UNEXPECTED_ATTR_WARN.format(attr=key)
                self._warn.append(ParseAlert(self._stack, msg))

    # ---- Header Field Entry Keys ------------------------------------------ #
    def _parse_proto_field(self, base: Any) -> Tuple[Type[Packet], str]:
        """
        Parses a header field.

        Args:
            base: The encoded field.
        Returns:
            The parsed (protocol, field) tuple.
        """
        encoded = self._parse_string(base)
        if encoded not in Parser._RYU_NAME_TO_FIELD:
            msg = Parser._UNKNOWN_FIELD.format(field=encoded)
            raise ScenarioSemanticError(ParseAlert(self._stack, msg))

        return Parser._RYU_NAME_TO_FIELD[encoded]
