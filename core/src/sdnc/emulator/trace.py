from __future__ import annotations

from pathlib import Path
from typing import Iterator, Optional

from scapy.layers.inet import UDP
from scapy.packet import Packet, Raw
from scapy.utils import PcapReader


class IdentifiedPacket:
    """
    A packet which carries an ID in it's payload. This ID has the
    following structure:

    +---------+----------+-----------+
    | test_id | event_id | packet_id |
    +---------+----------+-----------+
    | 16 bits | 16 bits  | 32 bits   |
    +---------+----------+-----------+

    Functions to extract all parts of this id are provided.

    Args:
        pkt: The packet containing an id.

    Raises:
        ValueError: if the given packet does not contain an ID in its
            payload.

    Attributes:
        pkt: The packet containing an id.
    """

    def __init__(self, pkt: Packet):
        self.pkt = pkt
        if self.get_combined_id() is None:
            raise ValueError(
                "Packet does not contain payload or payload is not a valid ID!"
            )

    def get_combined_id(self) -> Optional[int]:
        """
        Get this packet's identifier from its payload.

        Returns:
            The id.
        """
        pkt_id = None
        data = self.pkt.lastlayer() if UDP in self.pkt else None
        if isinstance(data, Raw) and len(data) == 8:
            try:
                pkt_id = int.from_bytes(data, "big")
            except TypeError:
                pass
        return pkt_id

    def get_test_id(self) -> Optional[int]:
        """
        Get this packet's test identifier from its payload.

        :returns: The test_id.
        """
        combined_id = self.get_combined_id()
        if combined_id is not None:
            return combined_id >> (32 + 16) & 0xFFFF
        else:
            return None

    def get_event_id(self) -> Optional[int]:
        """
        Get this packet's event identifier from its payload.

        Returns:
            The event_id.
        """
        combined_id = self.get_combined_id()
        if combined_id is not None:
            return combined_id >> 32 & 0xFFFF
        else:
            return None

    def get_packet_id(self) -> Optional[int]:
        """
        Get this packet's packet identifier from its payload.

        Returns:
            The packet_id.
        """
        combined_id = self.get_combined_id()
        if combined_id is not None:
            return combined_id & 0xFFFFFFFF
        else:
            return None


class FilteredPcap:
    """
    This class contains a pcap file and can create generators which
    return only the packets contained in the file that are valid
    :class:`IdentifiedPackets <IdentifiedPacket>`.

    Args:
        pcap: Path to a pcap file.
    """

    def __init__(self, pcap: Path):
        self._pcap = pcap

    def get(
        self, test_id: Optional[int] = None, event_id: Optional[int] = None
    ) -> Iterator[IdentifiedPacket]:
        """
        Iterator that steps through all packets in this pcap file and
        filters them by test_id and event_id.

        Args:
            test_id: Only packets that have this test_id will be
                yielded.
            event_id: Only packets that have this event_id will be
                yielded.
        Yields:
            The identified packets from the pcap that match the given
            filtering parameters.
        """
        for buf in PcapReader(str(self._pcap)):
            try:
                pkt = IdentifiedPacket(buf)
            except ValueError:
                # Not a packet with an id
                continue

            if (event_id is None or pkt.get_event_id() == event_id) and (
                test_id is None or pkt.get_test_id() == test_id
            ):
                yield pkt
