import sys

from ryu.base import app_manager
from ryu.ofproto import ofproto_v1_3


class Flood(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        sys.exit(-1)
