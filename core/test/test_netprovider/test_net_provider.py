from pathlib import Path

import pytest
from wrapt_timeout_decorator import timeout

from sdnc.emulator.controller import Controller
from sdnc.emulator.netprovider import ControllerConnectTimeoutError, NetProvider
from sdnc.emulator.scenario_parser import Parser


@timeout(15)
def start_with_timeout(net_provider: NetProvider) -> None:
    net_provider.start()


def test_dead_controller():
    scenario_path = Path("test/files/scenarios/flood.yaml")
    controller_path = Path("test/files/controllers/dead.py")

    with open(scenario_path.resolve(), encoding="utf-8") as scenario_file:
        scenario = Parser().parse(scenario_file)[0]
    controller = Controller(controller_path)

    net_provider = NetProvider(scenario.topo, controller)

    with pytest.raises(ControllerConnectTimeoutError):
        start_with_timeout(net_provider)
