from sdnc.emulator.trafgen import IDUDPTrafgenCfg, UDPTrafgenCfg


def test_id_udp_trafgen_cfg_generate():
    # pylint: disable=line-too-long
    reference = "cpu(0): {\n"
    reference += "eth(daddr=cc:cc:cc:cc:cc:11, saddr=cc:cc:cc:cc:cc:22, proto=2048),\n"
    reference += (
        "ipv4(flags=2, ttl=255, proto=17, saddr=11.11.11.11, daddr=22.22.22.22),\n"
    )
    reference += "udp(sport=11111, dport=22222),\n"
    reference += "c16(10),\n"
    reference += "c16(20),\n"
    reference += "c8(0),\n"
    reference += "c16(30),\n"
    reference += "dinc(0, 255),\n"
    reference += "}\n"
    # pylint: disable=line-too-long

    udp_cfg = UDPTrafgenCfg(
        ethernet_dst="cc:cc:cc:cc:cc:11",
        ethernet_src="cc:cc:cc:cc:cc:22",
        ipv4_flags=2,
        ipv4_ttl=255,
        ipv4_src="11.11.11.11",
        ipv4_dst="22.22.22.22",
        udp_src_port=11111,
        udp_dst_port=22222,
    )

    cfg = IDUDPTrafgenCfg(udp_cfg, test_id=10, event_id=20, offset=30)

    gen_cfg = cfg.generate()

    print(gen_cfg)

    assert gen_cfg == reference
