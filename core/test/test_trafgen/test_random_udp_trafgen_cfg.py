from ipaddress import IPv4Network

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.range_list import IntRange, IPRange, MACRange, RangeList
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg


def test_generation():
    tests = 1000

    ethernet_dst = MACRange(MACaddr("cc:cc:cc:00:00:00"), MACaddr("cc:cc:cc:cc:cc:cc"))
    ethernet_src = MACRange(MACaddr("dd:dd:dd:00:00:00"), MACaddr("dd:dd:dd:dd:dd:dd"))
    ipv4_flags = 2
    ipv4_ttl = IntRange(200, 255)
    ipv4_src = IPRange(IPv4Network("11.11.0.0/16"))
    ipv4_dst = IPRange(IPv4Network("22.22.0.0/16"))
    udp_src_port = IntRange(10000, 19999)
    udp_dst_port = IntRange(20000, 29999)

    rcfg = RandomUDPTrafgenCfg(
        ethernet_dst=RangeList([ethernet_dst]),
        ethernet_src=RangeList([ethernet_src]),
        ipv4_flags=ipv4_flags,
        ipv4_ttl=RangeList([ipv4_ttl]),
        ipv4_src=RangeList([ipv4_src]),
        ipv4_dst=RangeList([ipv4_dst]),
        udp_src_port=RangeList([udp_src_port]),
        udp_dst_port=RangeList([udp_dst_port]),
    )

    for _ in range(tests):
        cfg = rcfg.generate()

        assert cfg.ethernet_dst in ethernet_dst
        assert cfg.ethernet_src in ethernet_src
        assert cfg.ipv4_flags == ipv4_flags
        assert cfg.ipv4_ttl in ipv4_ttl
        assert cfg.ipv4_src in ipv4_src
        assert cfg.ipv4_dst in ipv4_dst
        assert cfg.udp_src_port in udp_src_port
        assert cfg.udp_dst_port in udp_dst_port
