from ipaddress import IPv4Address

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.trafgen import UDPTrafgenCfg


def test_udp_trafgen_cfg_generate():
    # pylint: disable=line-too-long
    reference = "cpu(0): {\n"
    reference += "eth(daddr=cc:cc:cc:cc:cc:11, saddr=cc:cc:cc:cc:cc:22, proto=2048),\n"
    reference += (
        "ipv4(flags=2, ttl=255, proto=17, saddr=11.11.11.11, daddr=22.22.22.22),\n"
    )
    reference += "udp(sport=11111, dport=22222),\n"
    reference += "}\n"
    # pylint: enable=line-too-long

    cfg = UDPTrafgenCfg(
        ethernet_dst=MACaddr("cc:cc:cc:cc:cc:11"),
        ethernet_src=MACaddr("cc:cc:cc:cc:cc:22"),
        ipv4_flags=2,
        ipv4_ttl=255,
        ipv4_src=IPv4Address("11.11.11.11"),
        ipv4_dst=IPv4Address("22.22.22.22"),
        udp_src_port=11111,
        udp_dst_port=22222,
    )

    gen_cfg = cfg.generate()

    assert gen_cfg == reference
