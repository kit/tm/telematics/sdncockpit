import ipaddress as ip
from pathlib import Path

import pytest
from mininet.topo import MinimalTopo, Topo
from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.range_list import IntRange, IPRange, MACRange, RangeList
from sdnc.emulator.scenario_parser import ParseError, Parser

# we"re testing some private methods because we don"t want to pass in a
# new test scenario file for every little parser feature
# pylint: disable=protected-access

# ---- Scenario ------------------------------------------------------------- #
# TODO


# ---- Topology ------------------------------------------------------------- #
def test_parse_topology():
    host1 = "h1"
    host2 = "h2"
    switch = "s1"
    dpid = 1
    links = [["h1", "s1"], ["h2", "s1"]]
    topo_dict = {
        "hosts": [{"name": host1}, {"name": host2}],
        "switches": [{"name": switch, "dpid": dpid}],
        "links": links,
    }

    parser = Parser()
    topo = parser._parse_topology(topo_dict)

    assert host1 in topo.hosts()
    assert host2 in topo.hosts()
    assert switch in topo.switches()
    assert topo.nodeInfo(switch)["dpid"] == str(dpid)
    assert (links[0][0], links[0][1]) in topo.links() or (
        links[0][1],
        links[0][0],
    ) in topo.links()


def test_parse_topology_with_optionals():
    host1 = "h1"
    host1_ip = "192.168.178.1"
    host2 = "h2"
    host2_ip = "192.168.178.2"
    switch = "s1"
    dpid = 1
    links = [[host1, switch], [host2, switch]]
    topo_dict = {
        "hosts": [{"name": host1, "ipv4": host1_ip}, {"name": host2, "ipv4": host2_ip}],
        "switches": [{"name": switch, "dpid": dpid}],
        "links": links,
    }

    parser = Parser()
    topo = parser._parse_topology(topo_dict)

    assert host1 in topo.hosts()
    assert topo.nodeInfo(host1)["ip"] == host1_ip
    assert host2 in topo.hosts()
    assert topo.nodeInfo(host2)["ip"] == host2_ip
    assert switch in topo.switches()
    assert topo.nodeInfo(switch)["dpid"] == str(dpid)
    assert (links[0][0], links[0][1]) in topo.links() or (
        links[0][1],
        links[0][0],
    ) in topo.links()


def test_parse_host_double_host():
    parser = Parser()
    parser._topo = Topo()
    host = {"name": "h1"}

    parser._parse_host(host)
    with pytest.raises(ParseError):
        parser._parse_host(host)


def test_parse_switch_double_dpid():
    parser = Parser()
    parser._topo = Topo()
    switches = [{"name": "s1", "dpid": 1}, {"name": "s2", "dpid": 1}]

    parser._parse_switch(switches[0])
    with pytest.raises(ParseError):
        parser._parse_switch(switches[1])


def test_parse_switch_negative_dpid():
    parser = Parser()
    parser._topo = Topo()
    switch = {"name": "s1", "dpid": -1}
    with pytest.raises(ParseError):
        parser._parse_switch(switch)


def test_parse_switch_double_switch():
    parser = Parser()
    parser._topo = Topo()
    switches = [{"name": "s1", "dpid": 1}, {"name": "s1", "dpid": 2}]

    parser._parse_switch(switches[0])
    with pytest.raises(ParseError):
        parser._parse_switch(switches[1])


def test_parse_switch_double_link():
    parser = Parser()
    parser._topo = Topo()
    parser._topo.addHost("h1")
    parser._topo.addSwitch("s1")
    link = ["h1", "s1"]

    parser._parse_link(link)
    with pytest.raises(ParseError):
        parser._parse_link(link)


# ---- Test ----------------------------------------------------------------- #
# TODO


# ---- Traffic Events ------------------------------------------------------- #
def test_parse_traffic_event_full():
    te_d = {
        "event_count": 5,
        "host": "h1",
        "header": {"ethernet.dst": "aa:bb:cc:dd:ee:ff"},
        "packet_count": 10,
        "time": {"min": 0, "max": 7},
    }

    parser = Parser()
    parser._topo = MinimalTopo()

    rnd_traffic_event = parser._parse_traffic_event(te_d)

    event_count = RangeList([IntRange(te_d["event_count"])])
    assert rnd_traffic_event.event_count == event_count
    assert rnd_traffic_event.host == te_d["host"]
    headers = rnd_traffic_event.headers
    mac_range = RangeList([MACRange(MACaddr(te_d["header"]["ethernet.dst"]))])
    assert headers.ethernet_dst == mac_range
    packet_count = RangeList([IntRange(te_d["packet_count"])])
    assert rnd_traffic_event.pkt_count == packet_count
    time = RangeList([IntRange(te_d["time"]["min"], te_d["time"]["max"])])
    assert rnd_traffic_event.time == time


def test_parse_traffic_event_minimal():
    te_d = {
        "host": "h1",
    }

    parser = Parser()
    parser._topo = MinimalTopo()

    parser._parse_traffic_event(te_d)


def test_parse_traffic_event_unknown_host():
    te_d = {
        "host": "unknown",
    }

    parser = Parser()
    parser._topo = MinimalTopo()

    with pytest.raises(ParseError):
        parser._parse_traffic_event(te_d)


# ---- Event Header --------------------------------------------------------- #
def test_parse_event_header_full():
    attr = {
        "ethernet.dst": "aa:bb:cc:dd:ee:ff",
        "ethernet.src": {"min": "00:00:00:00:00:00", "max": "00:00:00:00:00:ff"},
        "ipv4.flags": 2,
        "ipv4.ttl": {"min": 1, "max": 7},
        "ipv4.src": "192.168.178.0/24",
        "ipv4.dst": "192.168.176.0",
        "udp.src_port": 20,
        "udp.dst_port": {"min": 80, "max": 8080},
    }

    parser = Parser()

    rnd_cfg = parser._parse_event_header(attr)

    eth_dst = MACRange(MACaddr(attr["ethernet.dst"]))
    assert rnd_cfg.ethernet_dst == RangeList([eth_dst])
    eth_src = MACRange(
        MACaddr(attr["ethernet.src"]["min"]),
        MACaddr(attr["ethernet.src"]["max"]),
    )
    assert rnd_cfg.ethernet_src == RangeList([eth_src])
    assert rnd_cfg.ipv4_flags == attr["ipv4.flags"]
    ttl = IntRange(attr["ipv4.ttl"]["min"], attr["ipv4.ttl"]["max"])
    assert rnd_cfg.ipv4_ttl == RangeList([ttl])
    ip_src = IPRange(ip.IPv4Network(attr["ipv4.src"]))
    assert rnd_cfg.ipv4_src == RangeList([ip_src])
    ip_dst = IPRange(ip.IPv4Network(attr["ipv4.dst"] + "/32"))
    assert rnd_cfg.ipv4_dst == RangeList([ip_dst])
    udp_src = IntRange(attr["udp.src_port"])
    assert rnd_cfg.udp_src_port == RangeList([udp_src])
    udp_dst = IntRange(attr["udp.dst_port"]["min"], attr["udp.dst_port"]["max"])
    assert rnd_cfg.udp_dst_port == RangeList([udp_dst])


def test_parse_event_header_invalid_key():
    attributes = {"invalid.dst": "aa:bb:cc:dd:ee:ff"}

    parser = Parser()

    parser._parse_event_header(attributes)

    assert len(parser._warn) == 1


def test_parse_event_header_default_src_mac():
    src_mac = "aa:bb:cc:dd:ee:ff"
    attributes = {}
    reference = MACaddr(src_mac)

    parser = Parser()

    rnd_cfg = parser._parse_event_header(attributes, src_mac)

    assert rnd_cfg.ethernet_src == RangeList([MACRange(reference)])


def test_parse_event_header_unused_default():
    src_mac = "aa:bb:cc:dd:ee:ff"
    attributes = {"ethernet.src": "11:22:33:44:55:66"}
    reference = MACaddr(attributes["ethernet.src"])

    parser = Parser()

    rnd_cfg = parser._parse_event_header(attributes, src_mac)

    assert rnd_cfg.ethernet_src == RangeList([MACRange(reference)])


def test_parse_event_header_default_ip():
    src_ip = "129.168.178.9"
    attributes = {}

    parser = Parser()

    rnd_cfg = parser._parse_event_header(attributes, src_ip=src_ip)

    src_net = ip.IPv4Network(src_ip + "/32")
    assert rnd_cfg.ipv4_src == RangeList([IPRange(src_net)])


# ---- Observation Points --------------------------------------------------- #
def test_parse_obs_point_full():
    obs_pt_d = {
        "link": ["h1", "s1"],
        "node": "s1",
        "direction": "s1",
        "packet_count": {"min": 5, "max": 10},
        "header_rules": {"udp.dst_port": 80},
    }

    parser = Parser()
    parser._topo = MinimalTopo()

    obs_pt = parser._parse_observation_point(obs_pt_d)

    assert obs_pt.link == (obs_pt_d["link"][0], obs_pt_d["link"][1])
    assert obs_pt.node == obs_pt_d["node"]
    assert obs_pt.direction == obs_pt_d["direction"]
    assert obs_pt.count == RangeList([IntRange(5, 10)])
    rules = obs_pt.header_rules
    assert len(rules) == 1  # rule parsing is tested elsewhere


def test_parse_obs_point_minimal():
    obs_pt_d = {
        "link": ["h1", "s1"],
    }

    parser = Parser()
    parser._topo = MinimalTopo()

    obs_pt = parser._parse_observation_point(obs_pt_d)

    assert obs_pt.link == (obs_pt_d["link"][0], obs_pt_d["link"][1])
    assert obs_pt.node is None
    assert obs_pt.direction is None
    assert obs_pt.count is None
    rules = obs_pt.header_rules
    assert len(rules) == 0


def test_parse_obs_point_negative_count():
    obs_pt_d = {"link": ["h1", "s1"], "packet_count": -1}

    parser = Parser()
    parser._topo = MinimalTopo()

    with pytest.raises(ParseError):
        parser._parse_observation_point(obs_pt_d)


def test_parse_obs_point_direction_not_in_link():
    obs_pt_d = {"link": ["h1", "s1"], "direction": "h2"}

    parser = Parser()
    parser._topo = MinimalTopo()

    with pytest.raises(ParseError):
        parser._parse_observation_point(obs_pt_d)


def test_parse_obs_point_node_not_in_link():
    obs_pt_d = {"link": ["h1", "s1"], "node": "h2"}

    parser = Parser()
    parser._topo = MinimalTopo()

    with pytest.raises(ParseError):
        parser._parse_observation_point(obs_pt_d)


# ---- Header Rules --------------------------------------------------------- #
def test_header_rule_int_range():
    parser = Parser()

    proto_field = "ipv4.ttl"
    value = {"min": 200, "max": 250}

    rule = parser._parse_header_rule(proto_field, value)
    assert rule.proto_cls == IP
    assert rule.field == "ttl"
    assert rule.val_range == RangeList([IntRange(value["min"], value["max"])])


def test_header_rule_subnet():
    parser = Parser()

    proto_field = "ipv4.src"
    value = "192.168.178.0/24"

    rule = parser._parse_header_rule(proto_field, value)
    assert rule.proto_cls == IP
    assert rule.field == "src"
    assert rule.val_range == RangeList([IPRange(ip.IPv4Network(value))])


def test_header_rule_invalid_fieldname():
    parser = Parser()

    proto_field = "invalid...src"
    value = "192.168.178.0/24"

    with pytest.raises(ParseError):
        parser._parse_header_rule(proto_field, value)


# ---- Addresses (Ethernet) ------------------------------------------------- #
def test_parse_mac():
    parser = Parser()

    addr = "aa:bb:cc:11:22:33"

    assert MACaddr(addr) == parser._parse_mac(addr)


def test_parse_mac_invalid():
    parser = Parser()

    addr = "aa:bb"

    with pytest.raises(ParseError):
        parser._parse_mac(addr)


# ---- Addresses (IPv4) ----------------------------------------------------- #
def test_parse_ipv4addr():
    parser = Parser()

    addr = "192.168.178.1"

    assert ip.IPv4Address(addr) == parser._parse_ipv4addr(addr)


def test_parse_ipv4addr_network():
    parser = Parser()

    addr = "192.168.178.1/16"

    with pytest.raises(ParseError):
        parser._parse_ipv4addr(addr)


def test_parse_ipv4net():
    parser = Parser()

    addr = "192.168.178.1"

    assert ip.IPv4Network(addr) == parser._parse_ipv4net(addr)


def test_parse_ipv4net_addr():
    parser = Parser()

    addr = "192.168.178.1"
    subnet = "192.168.178.1/32"

    assert ip.IPv4Network(subnet) == parser._parse_ipv4net(addr)


# ---- Ranges --------------------------------------------------------------- #
def test_parse_int_range_value():
    parser = Parser()

    assert IntRange(10) == parser._parse_int_range(10)


def test_parse_int_range_range():
    parser = Parser()

    assert IntRange(10, 20) == parser._parse_int_range({"min": 10, "max": 20})


def test_parse_int_range_reverse():
    parser = Parser()

    with pytest.raises(ParseError):
        parser._parse_int_range({"min": 20, "max": 10})


def test_parse_int_range_incomplete():
    parser = Parser()

    with pytest.raises(ParseError):
        parser._parse_int_range({"max": 10})


def test_parse_int_range_wrong_key():
    parser = Parser()

    with pytest.raises(ParseError):
        parser._parse_int_range({"maxmin": 10})


# ---- Network Objects ------------------------------------------------------ #
def test_parse_linkname():
    parser = Parser()

    assert ("h1", "s1") == parser._parse_linkname(["h1", "s1"])


def test_parse_linkname_short():
    parser = Parser()

    with pytest.raises(ParseError):
        parser._parse_linkname(["h1"])


def _test_link_exists():
    parser = Parser()

    parser._topo = MinimalTopo()

    assert parser._link_exists(("h1", "s1"))


def _test_link_exists_not():
    parser = Parser()

    parser._topo = MinimalTopo()

    assert not parser._link_exists(("h1", "h2"))


# ---- Proto Fields --------------------------------------------------------- #
def _test_parse_field_valid():
    parser = Parser()

    proto, field = parser._parse_proto_field("ethernet.src")

    assert proto == Ether
    assert field == "src"


def _test_parse_field_invalid_field():
    parser = Parser()

    with pytest.raises(ParseError):
        _, _ = parser._parse_proto_field("ethernet.invalid")


def _test_parse_field_invalid_proto():
    parser = Parser()

    with pytest.raises(ParseError):
        _, _ = parser._parse_proto_field("invalid.src")


def _test_parse_field_invalid_type():
    parser = Parser()

    with pytest.raises(ParseError):
        _, _ = parser._parse_proto_field(1)


# pylint: enable=protected-access


# ---- Test Scenarios ------------------------------------------------------- #
def test_scenario_0():
    path = Path("test/test_scenario_parser/scenarios/0.yaml")

    parser = Parser()

    with pytest.raises(ParseError):
        with open(path.resolve(), encoding="utf-8") as scenario_file:
            parser.parse(scenario_file)


def test_scenario_1():
    path = Path("test/test_scenario_parser/scenarios/1.yaml")

    parser = Parser()

    with open(path.resolve(), encoding="utf-8") as scenario_file:
        scenario, _, warnings = parser.parse(scenario_file)

    assert len(warnings) == 0

    # all reference values can be found in 1.yaml
    assert scenario.name == "Example"
    assert scenario.description == "Example description"

    topo = scenario.topo
    assert "h1" in topo.hosts()
    assert topo.nodeInfo("h1")["ip"] == "192.168.178.1"
    assert "h2" in topo.hosts()
    assert topo.nodeInfo("h2")["ip"] == "192.168.178.2"
    assert "s1" in topo.switches()
    assert topo.nodeInfo("s1")["dpid"] == "1"
    assert ("h1", "s1") in topo.links() or ("s1", "h1") in topo.links()
    assert ("s1", "h2") in topo.links() or ("h2", "s1") in topo.links()

    assert len(scenario.tests) == 1
    test = scenario.tests[0]
    assert test.name == "Test Test"
    assert test.description == "Check for basic connectivity"
    assert test.restart

    assert len(test.events) == 1
    event = test.events[0]
    assert event.host == "h1"
    assert event.pkt_count == 5
    assert 0 <= event.time <= 7

    cfg = event.headers
    assert cfg.ethernet_src == MACaddr("aa:bb:cc:dd:ee:ff")
    assert cfg.ipv4_dst == ip.IPv4Address("192.168.178.2")
    assert cfg.udp_dst_port == 8080

    assert len(test.obs_points) == 2
    obs_pt = test.obs_points[0]
    assert obs_pt.link == ("h1", "s1")
    assert obs_pt.node == "s1"
    assert obs_pt.direction == "s1"
    assert obs_pt.count == RangeList([IntRange(5)])

    assert len(obs_pt.header_rules) == 1
    header_rule = obs_pt.header_rules[0]
    assert header_rule.proto_cls == UDP
    assert header_rule.field == "dport"
    assert header_rule.val_range == RangeList([IntRange(8080)])

    obs_pt = test.obs_points[1]
    assert obs_pt.link == ("h2", "s1")
    assert obs_pt.node is None
    assert obs_pt.direction is None
    assert obs_pt.count == RangeList([IntRange(5)])

    assert len(obs_pt.header_rules) == 1
    header_rule = obs_pt.header_rules[0]
    assert header_rule.proto_cls == IP
    assert header_rule.field == "dst"
    ip_range = IPRange(ip.IPv4Network("192.168.178.0/24"))
    assert header_rule.val_range == RangeList([ip_range])
