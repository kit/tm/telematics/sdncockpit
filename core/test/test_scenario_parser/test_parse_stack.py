from sdnc.emulator.scenario_parser import ParseStack


def test_str():
    stack = ParseStack()
    stack.push("{", "}")
    stack.push("test:")
    stack.push("[", "]")
    stack.push("...,", ", ...")
    stack.push("{", "}")

    assert str(stack) == ("{test: [..., { ~~~~~ }, ...]}")
