# Before using the macaddress library, we had our own implementation.
# These tests are mostly left over from that.
import pytest

from sdnc.emulator.macaddr import MACaddr


def test_mac():
    addr = "11:22:33:44:55:66"

    assert addr == str(MACaddr(addr))


def test_mac_invalid():
    with pytest.raises(ValueError):
        MACaddr("invalid ethernet address")


def test_mac_short():
    with pytest.raises(ValueError):
        MACaddr("11:22:33:44:55")


def test_mac_long():
    with pytest.raises(ValueError):
        MACaddr("11:22:33:44:55:66:77")


def test_mac_short_segment():
    with pytest.raises(ValueError):
        MACaddr("11:22:33:44:55:6")


def test_mac_long_segment():
    with pytest.raises(ValueError):
        MACaddr("011:22:33:44:55:66")


def test_mac_invalid_hex():
    with pytest.raises(ValueError):
        MACaddr("11:22:33:44:55:gg")
