from pathlib import Path

from sdnc.emulator.controller import (
    Controller,
    OutputReport,
    ProcessReadEndReport,
    ProcessReadStartReport,
)
from sdnc.emulator.netprovider import (
    NetProvider,
    RestartNetworkReport,
    StartNetworkReport,
    StopNetworkReport,
)
from sdnc.emulator.network_actions.evaluation import (
    AutoEval,
    StartEventReport,
    StartTestReport,
    TestFeedbackReport,
)
from sdnc.emulator.progress_report import ProgressObserver, ProgressReport
from sdnc.emulator.scenario_parser import Parser


class SimpleLoggingProgressObserver(ProgressObserver):
    def __init__(self):
        self.log = []

    def notify(self, report: ProgressReport):
        self.log.append(report)


def flooding_eval():
    """
    Runs an auto eval on the flood scenario and controller. Returns the
    scenario, the observer and the monitor.
    """
    scenario_path = Path("test/files/scenarios/flood.yaml")
    controller_path = Path("test/files/controllers/flood.py")

    observer = SimpleLoggingProgressObserver()

    with open(scenario_path.resolve(), encoding="utf-8") as scenario_file:
        scenario, _, _ = Parser().parse(scenario_file)
    controller = Controller(controller_path.resolve())
    controller.register(observer)
    netprovider = NetProvider(scenario.topo, controller)
    netprovider.register(observer)

    auto_eval = AutoEval()
    auto_eval.register(observer)

    auto_eval.start(scenario, netprovider)
    auto_eval.wait()
    netprovider.stop()

    return (scenario, observer)


def test_flood_tests():
    scenario, observer = flooding_eval()

    reports = [r for r in observer.log if isinstance(r, StartTestReport)]

    assert len(scenario.tests) == len(reports)

    for report, test in zip(reports, scenario.tests):
        assert report.test.test_id == test.test_id


def test_flood_test_feedbacks():
    scenario, observer = flooding_eval()

    reports = [r for r in observer.log if isinstance(r, TestFeedbackReport)]

    assert len(scenario.tests) == len(reports)

    for report, test in zip(reports, scenario.tests):
        assert report.feedback.test.test_id == test.test_id


def test_flood_events():
    scenario, observer = flooding_eval()
    events = [event for test in scenario.tests for event in test.events]

    reports = [r for r in observer.log if isinstance(r, StartEventReport)]

    assert len(events) == len(reports)

    for report, event in zip(reports, events):
        assert report.event.event_id == event.event_id
        assert report.event.test_id == event.test_id


def test_flood_restart():
    scenario, observer = flooding_eval()
    expected_restarts = len([test for test in scenario.tests if test.restart])
    if scenario.tests[0].restart:
        # because the net is not running, the first test starts,
        # not restarts
        expected_restarts -= 1

    reports = [r for r in observer.log if isinstance(r, RestartNetworkReport)]

    assert expected_restarts == len(reports)


def test_flood_start():
    _, observer = flooding_eval()
    expected_starts = 1

    reports = [r for r in observer.log if isinstance(r, StartNetworkReport)]

    assert expected_starts == len(reports)


def test_flood_stop():
    _, observer = flooding_eval()
    expected_stops = 1

    reports = [r for r in observer.log if isinstance(r, StopNetworkReport)]

    assert expected_stops == len(reports)


def test_flood_controller_log():
    scenario, observer = flooding_eval()
    expected_restarts = len([test for test in scenario.tests if test.restart])
    expected_line_count = 4 * expected_restarts

    log = observer.log
    reports = [r for r in log if isinstance(r, OutputReport)]

    assert expected_line_count == len(reports)


def test_flood_start_controller_read():
    scenario, observer = flooding_eval()
    expected_starts = len([test for test in scenario.tests if test.restart])

    log = observer.log
    reports = [r for r in log if isinstance(r, ProcessReadStartReport)]

    assert expected_starts == len(reports)


def test_flood_end_controller_read():
    scenario, observer = flooding_eval()
    expected_stops = len([test for test in scenario.tests if test.restart])

    log = observer.log
    reports = [r for r in log if isinstance(r, ProcessReadEndReport)]

    assert expected_stops == len(reports)
