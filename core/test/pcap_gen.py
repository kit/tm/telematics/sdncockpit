import subprocess
from tempfile import NamedTemporaryFile
from time import sleep

from mininet.clean import Cleanup
from mininet.net import Mininet
from mininet.node import Switch
from mininet.topo import MinimalTopo

from sdnc.emulator.network import TrafgenHost


def generate_pcap(cfg, count):
    """
    Run a minimal network topology, send count packets with the given
    ``IDUDPTrafgenConfig`` from a ``TrafgenHost``, capture those packets
    at the nearest switch and return the path to the pcap containing
    the captured data.

    When using this function in tests, keep in mind that the results are
    only as accurate as the implementations of ``IDUDPTrafgenConfig``
    and ``TrafgenHost``. This function is in fact used as part of the
    testing of ``TrafgenHost``.
    :param cfg: The ``IDUDPTrafgenConfig`` to generate packets with.
    :param count: How many packets to generate.
    :returns: Path to the pcap file. Please delete it at some point.
    """
    Cleanup.cleanup()

    net = Mininet(topo=MinimalTopo(), host=TrafgenHost, waitConnected=True)
    net.start()

    with NamedTemporaryFile("wb", suffix=".pcap", delete=False) as file:
        path = file.name

    host = net.hosts[0]
    switch = net.switches[0]
    link = net.linksBetween(host, switch)[0]

    intf = link.intf1 if isinstance(link.intf1.node, Switch) else link.intf2

    cmd = ["tcpdump", "-i", str(intf), "-w", path]

    with subprocess.Popen(cmd, stdout=None, stderr=subprocess.PIPE) as process:
        for _ in process.stderr:
            break

        offset = 0
        remaining = count
        while remaining > 0:
            batch = 256 if remaining >= 256 else remaining

            cfg.offset = offset
            host.send(cfg.generate(), batch)

            remaining -= batch
            offset += 1

        sleep(10)  # We don't detect when the network is done processing

        process.send_signal(subprocess.signal.SIGINT)
        process.communicate()  # wait for termination
        process.terminate()  # make sure it's dead

    net.stop()

    return path
