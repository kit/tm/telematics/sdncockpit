from concurrent.futures import Future
from pathlib import Path

from mininet.link import TCLink
from mininet.node import Host


class TrafgenHostMock(Host):
    """
    Moc of sdnc.network.TrafgenHost that logs calls to its methods.
    """

    def __init__(self, logger, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.logger = logger

    def send(self, cfg, count):
        kwargs = {"cfg": cfg, "count": count}
        self.logger(self, self.send, kwargs)


class MonitoredLinkMock(TCLink):
    """
    Moc of sdnc.network.MonitoredLink that logs calls to its methods.
    """

    empty_pcap = Path(__file__).resolve()
    empty_pcap = empty_pcap.parent.parent  # tests
    empty_pcap = empty_pcap / "files" / "empty.pcap"

    def __init__(self, logger, pcap_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pcap_path = pcap_path
        self.logger = logger

    def start_monitor(self, intf, direction):
        kwargs = {"intf": intf, "direction": direction}
        self.logger(self, self.start_monitor, kwargs)
        return 0

    def timed_monitor(self, timeframe, executor):
        kwargs = {"timeframe": timeframe, "executor": executor}
        self.logger(self, self.timed_monitor, kwargs)
        future = Future()
        future.set_result(self.pcap_path)
        return future

    def stop_monitor(self, descriptor):
        kwargs = {
            "descriptor": descriptor,
        }
        self.logger(self, self.stop_monitor, kwargs)
        future = Future()
        future.set_result(self.pcap_path)
        return future
