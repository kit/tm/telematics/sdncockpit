from functools import partial
from pathlib import Path
from test.mock_network import MonitoredLinkMock, TrafgenHostMock
from test.pcap_gen import generate_pcap
from time import time_ns

import pytest
from mininet.clean import Cleanup
from mininet.node import OVSSwitch, RemoteController
from mininet.topo import MinimalTopo

from sdnc.emulator.controller import Controller
from sdnc.emulator.scenario import ObservationPoint, TrafficEvent
from sdnc.emulator.scenario_net import ScenarioNet
from sdnc.emulator.trafgen import IDUDPTrafgenCfg, UDPTrafgenCfg


def test_event_short():
    Cleanup.cleanup()
    calls = []

    def logger(host, fkt, kwargs):
        calls.append((host, fkt, kwargs))

    net = ScenarioNet(host=partial(TrafgenHostMock, logger))
    net.buildFromTopo(MinimalTopo())
    net.start()

    host = str(net.hosts[0])
    headers = UDPTrafgenCfg()
    pkt_count = 10
    time = 0
    event_id = 10
    test_id = 10

    cfg = IDUDPTrafgenCfg(headers, test_id, event_id)

    event = TrafficEvent(host, headers, pkt_count, time, event_id, test_id)

    net.run_event(event)

    net.stop()

    assert len(calls) == 1

    for c_host, c_fkt, c_kwargs in calls:
        assert str(c_host) == host
        assert c_fkt == c_host.send
        assert c_kwargs["cfg"] == cfg.generate()
        assert c_kwargs["count"] == pkt_count


def test_event_long():
    Cleanup.cleanup()
    calls = []

    def logger(host, fkt, kwargs):
        calls.append((host, fkt, kwargs))

    net = ScenarioNet(host=partial(TrafgenHostMock, logger))
    net.buildFromTopo(MinimalTopo())
    net.start()

    host = str(net.hosts[0])
    headers = UDPTrafgenCfg()
    pkt_count = 1000
    time = 0
    event_id = 10
    test_id = 10

    event = TrafficEvent(host, headers, pkt_count, time, event_id, test_id)

    net.run_event(event)

    net.stop()

    assert len(calls) == pkt_count // 256 + 1

    for _, _, c_kwargs in calls[:-1]:
        assert c_kwargs["count"] == 256

    _, _, c_kwargs = calls[-1]
    assert c_kwargs["count"] == pkt_count % 256

    cfg = IDUDPTrafgenCfg(headers, event_id, test_id, offset=0)
    for i, (_, _, c_kwargs) in zip(range(len(calls)), calls):
        print(i)
        cfg.offset = i
        print(cfg.generate())
        assert c_kwargs["cfg"] == cfg.generate()


def test_event_missing_id():
    Cleanup.cleanup()

    net = ScenarioNet()
    net.buildFromTopo(MinimalTopo())
    net.start()

    host = str(net.hosts[0])
    headers = UDPTrafgenCfg()
    pkt_count = 10
    time = 0

    event = TrafficEvent(host, headers, pkt_count, time)

    with pytest.raises(ValueError):
        net.run_event(event)

    net.stop()


def test_event_dead_net():
    Cleanup.cleanup()

    net = ScenarioNet()
    net.buildFromTopo(MinimalTopo())

    host = str(net.hosts[0])
    headers = UDPTrafgenCfg()
    pkt_count = 1000
    time = 0
    event_id = 10
    test_id = 10

    event = TrafficEvent(host, headers, pkt_count, time, event_id, test_id)

    net.run_event(event)

    net.stop()


def test_monitor():
    Cleanup.cleanup()

    calls = []

    def logger(host, fkt, kwargs):
        calls.append((host, fkt, kwargs))

    net = ScenarioNet(
        link=partial(MonitoredLinkMock, logger, MonitoredLinkMock.empty_pcap)
    )
    net.buildFromTopo(MinimalTopo())
    net.start()

    mn_link = net.links[0]
    link = (str(mn_link.intf1.node), str(mn_link.intf2.node))
    node = str(mn_link.intf1.node)
    direction = str(mn_link.intf2.node)

    obs_point = ObservationPoint(link, node, direction)

    net.start_monitor(obs_point)
    net.stop_monitor(obs_point)  # returns empty pcap in this case

    net.stop()

    assert len(calls) == 2  # start and stop
    for c_link, c_fkt, c_kwargs in calls:
        assert c_link == mn_link
        if c_fkt == mn_link.start_monitor:
            assert c_kwargs["intf"] == node
            assert c_kwargs["direction"] == direction
        elif c_fkt == mn_link.stop_monitor:
            pass
        else:
            assert False


def test_wait_inactive_timeout():
    Cleanup.cleanup()

    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), 0, 0)
    pcap = generate_pcap(cfg, 100)

    timeout = 3
    timeframe = 0.5
    margin = 0.75

    # pylint: disable-next=unused-argument
    def logger(host, fkt, kwargs):
        pass

    net = ScenarioNet(link=partial(MonitoredLinkMock, logger, pcap))
    net.buildFromTopo(MinimalTopo())
    net.start()

    start = time_ns() * 10 ** (-9)
    assert not net.wait_inactive(timeout, timeframe)
    end = time_ns() * 10 ** (-9)

    net.stop()

    duration = end - start

    assert duration >= timeout
    assert duration <= timeout + timeframe + margin


def test_ping():
    controller_path = Path("test/files/controllers/flood.py")
    controller = Controller(controller_path)
    Cleanup.cleanup()

    net = ScenarioNet(
        controller=RemoteController,
        switch=partial(OVSSwitch, protocols="OpenFlow10,OpenFlow13"),
    )
    ctrl = RemoteController("c0", ip="127.0.0.1", port=6633)
    net.addController(ctrl)
    net.buildFromTopo(MinimalTopo())
    net.start()
    controller.start()
    if not net.waitConnected(timeout=10):
        assert False

    source = net.hosts[0]
    destination = net.hosts[1]
    result = net.ping_route(source, destination)
    assert result == (1, 1)

    controller.stop()
    controller.wait()
    net.stop()


def test_flow_table():
    controller_path = Path("test/files/controllers/flood.py")
    controller = Controller(controller_path)
    Cleanup.cleanup()

    net = ScenarioNet(
        controller=RemoteController,
        switch=partial(OVSSwitch, protocols="OpenFlow10,OpenFlow13"),
    )
    ctrl = RemoteController("c0", ip="127.0.0.1", port=6633)
    net.addController(ctrl)
    net.buildFromTopo(MinimalTopo())
    net.start()
    controller.start()
    if not net.waitConnected(timeout=10):
        assert False

    switch = net.switches[0]
    result = net.flow_table(switch)
    assert result.table.count("\n") == 2

    controller.stop()
    controller.wait()
    net.stop()
