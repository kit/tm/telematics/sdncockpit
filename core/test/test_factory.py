from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.packet import Packet, Raw


def udp_packet(
    eth_dst="cc:cc:cc:cc:cc:02",
    eth_src="cc:cc:cc:cc:cc:01",
    ipv4_flags=0x02,
    ipv4_src="192.168.178.1",
    ipv4_dst="192.168.178.2",
    udp_src_port=62123,
    udp_dst_port=62124,
    data=b"\x00\x00\x00\x00\x00\x00\x00\x01",
) -> Packet:
    """
    This factory creates a packet with properties specified by parameters.

    It can contain data passed into this function as a bytestring param.
    """
    pkt = (
        Ether(dst=eth_dst, src=eth_src)
        / IP(flags=ipv4_flags, src=ipv4_src, dst=ipv4_dst)
        / UDP(sport=udp_src_port, dport=udp_dst_port)
        / Raw(load=data)
    )

    return pkt
