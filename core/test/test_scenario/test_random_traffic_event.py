from sdnc.emulator.range_list import IntRange, RangeList
from sdnc.emulator.scenario import RandomTrafficEvent
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg, UDPTrafgenCfg


def test_generation():
    tests = 100

    event_count = (100, 200)
    host = "h1"
    headers = RandomUDPTrafgenCfg()
    pkt_count = (200, 500)
    time = (5000, 12000)

    revent = RandomTrafficEvent(
        event_count=RangeList([IntRange(*event_count)]),
        host=host,
        headers=headers,
        pkt_count=RangeList([IntRange(*pkt_count)]),
        time=RangeList([IntRange(*time)]),
    )

    for _ in range(tests):
        events = revent.generate()

        begin, end = event_count
        assert begin <= len(events) <= end

        for event in events:
            assert event.host == host

            assert isinstance(event.headers, UDPTrafgenCfg)

            begin, end = pkt_count
            assert begin <= event.pkt_count <= end

            begin, end = time
            assert begin <= event.time <= end
