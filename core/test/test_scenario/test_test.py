from sdnc.emulator.range_list import IntRange, RangeList
from sdnc.emulator.scenario import RandomTrafficEvent, Test
from sdnc.emulator.trafgen import RandomUDPTrafgenCfg


def test_event_ids():
    event_count_1 = 3
    event_count_2 = 5
    event_count = event_count_1 + event_count_2
    test_id = 3

    test = Test(test_id=test_id)

    test.add_rnd_event(
        RandomTrafficEvent(
            event_count=RangeList([IntRange(event_count_1, event_count_1)]),
            host="h1",
            headers=RandomUDPTrafgenCfg(),
            pkt_count=RangeList([IntRange(100, 200)]),
            time=RangeList([IntRange(0, 5000)]),
        )
    )

    test.add_rnd_event(
        RandomTrafficEvent(
            event_count=RangeList([IntRange(event_count_2, event_count_2)]),
            host="h2",
            headers=RandomUDPTrafgenCfg(),
            pkt_count=RangeList([IntRange(10, 20)]),
            time=RangeList([IntRange(0, 2000)]),
        )
    )

    assert len(test.events) == event_count

    ids = []
    for event in test.events:
        assert event.test_id == test_id
        assert event.event_id not in ids
        ids.append(event.event_id)


def test_change_test_id():
    test_id_1 = 3
    test_id_2 = 5

    test = Test(test_id=test_id_1)

    test.add_rnd_event(
        RandomTrafficEvent(
            event_count=RangeList([IntRange(3, 3)]),
            host="h1",
            headers=RandomUDPTrafgenCfg(),
            pkt_count=RangeList([IntRange(100, 200)]),
            time=RangeList([IntRange(0, 5000)]),
        )
    )

    test.test_id = test_id_2

    for event in test.events:
        assert event.test_id == test_id_2
