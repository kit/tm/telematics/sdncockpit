from mininet.topo import MinimalTopo

from sdnc.emulator.scenario import Scenario, Test


def test_test_ids():
    test_count = 50
    scenario = Scenario("", "", MinimalTopo())

    for _ in range(test_count):
        scenario.add_test(Test(test_id=5))

    ids = []
    for test in scenario.tests:
        assert test.test_id not in ids
        ids.append(test.test_id)
