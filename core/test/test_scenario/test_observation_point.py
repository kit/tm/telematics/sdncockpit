from test.test_factory import udp_packet

from scapy.layers.inet import UDP

from sdnc.emulator.range_list import IntRange, RangeList
from sdnc.emulator.scenario import HeaderRule, ObservationPoint
from sdnc.emulator.trace import IdentifiedPacket


def pkt_gen(pkt, count):
    for _ in range(count):
        yield IdentifiedPacket(pkt)


def test_check():
    count = 100
    src_port = 2000

    val_range = RangeList([IntRange(src_port)])
    rules = [HeaderRule(UDP, "sport", val_range)]

    count_range = RangeList([IntRange(count)])
    obs_point = ObservationPoint(("h", "s"), None, None, count_range, rules)

    pkt = udp_packet(udp_src_port=src_port)

    feedback = obs_point.check(pkt_gen(pkt, count))

    assert feedback.status
    assert all(fb.status for fb in feedback.packet_feedbacks)
    assert len(feedback.packet_feedbacks) == count


def test_check_no_rules():
    count = 100
    src_port = 2000

    rules = []

    count_range = RangeList([IntRange(count)])
    obs_point = ObservationPoint(("h", "s"), None, None, count_range, rules)

    pkt = udp_packet(udp_src_port=src_port)

    feedback = obs_point.check(pkt_gen(pkt, count))

    assert feedback.status
    assert all(fb.status for fb in feedback.packet_feedbacks)
    assert len(feedback.packet_feedbacks) == count


def test_check_no_pkts():
    count = 0
    src_port = 2000

    val_range = RangeList([IntRange(src_port)])
    rules = [HeaderRule(UDP, "sport", val_range)]

    count_range = RangeList([IntRange(count)])
    obs_point = ObservationPoint(("h", "s"), None, None, count_range, rules)

    pkt = udp_packet(udp_src_port=src_port)

    feedback = obs_point.check(pkt_gen(pkt, count))

    assert feedback.status
    assert all(fb.status for fb in feedback.packet_feedbacks)
    assert len(feedback.packet_feedbacks) == count


def test_check_missing_pkts():
    count = 100
    src_port = 2000

    val_range = RangeList([IntRange(src_port)])
    rules = [HeaderRule(UDP, "sport", val_range)]

    count_range = RangeList([IntRange(count)])
    obs_point = ObservationPoint(("h", "s"), None, None, count_range, rules)

    pkt = udp_packet(udp_src_port=src_port)

    gen = pkt_gen(pkt, count)
    next(gen)

    feedback = obs_point.check(gen)

    assert not feedback.status
    assert all(fb.status for fb in feedback.packet_feedbacks)
    assert len(feedback.packet_feedbacks) == count - 1
