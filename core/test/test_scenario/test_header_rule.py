from test import test_factory

import pytest
from scapy.layers.inet import TCP, UDP
from scapy.layers.l2 import Ether

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.range_list import IntRange, MACRange, RangeList
from sdnc.emulator.scenario import HeaderRule


def test_success():
    proto_cls = Ether
    eth_src = "cc:cc:cc:cc:cc:11"

    val_range = RangeList([MACRange(MACaddr(eth_src))])
    rule = HeaderRule(proto_cls, "src", val_range)

    pkt = test_factory.udp_packet(eth_src=eth_src)

    feedback = rule.check(pkt)

    assert feedback.status
    assert feedback.header_rule == rule


def test_fail():
    proto_cls = Ether
    expected = "cc:cc:cc:cc:cc:11"
    actual = "cc:cc:cc:cc:cc:22"

    val_range = RangeList([MACRange(MACaddr(expected))])
    rule = HeaderRule(proto_cls, "src", val_range)

    pkt = test_factory.udp_packet(eth_src=actual)

    feedback = rule.check(pkt)

    assert not feedback.status
    assert feedback.header_rule == rule


def test_missing_proto():
    proto_cls = TCP
    src_port = 1000

    val_range = RangeList([IntRange(src_port)])
    rule = HeaderRule(proto_cls, "sport", val_range)

    pkt = test_factory.udp_packet()

    feedback = rule.check(pkt)

    assert not feedback.status
    assert feedback.header_rule == rule


def test_missing_attr():
    proto_cls = UDP
    src_port = 1000

    with pytest.raises(AttributeError):
        val_range = RangeList([IntRange(src_port)])
        HeaderRule(proto_cls, "invalid_attr", val_range)
