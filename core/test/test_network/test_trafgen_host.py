from pathlib import Path
from test.pcap_gen import generate_pcap
from test.test_network.capture_check import check_capture

import pytest
from mininet.clean import Cleanup
from mininet.net import Mininet
from mininet.topo import MinimalTopo

from sdnc.emulator.network import TrafgenHost
from sdnc.emulator.trafgen import IDUDPTrafgenCfg, UDPTrafgenCfg


def test_basic():
    id_test = 11
    id_event = 22
    count = 100
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), id_test, id_event)
    pcap = generate_pcap(cfg, count)
    check_capture(cfg, count, pcap)
    Path(pcap).unlink()


def test_large_count():
    id_test = 11
    id_event = 22
    count = 2000
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), id_test, id_event)
    pcap = generate_pcap(cfg, count)
    check_capture(cfg, count, pcap)
    Path(pcap).unlink()


def test_minimal_count():
    id_test = 11
    id_event = 22
    count = 1
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), id_test, id_event)
    pcap = generate_pcap(cfg, count)
    check_capture(cfg, count, pcap)
    Path(pcap).unlink()


def test_zero_count():
    id_test = 11
    id_event = 22
    count = 0
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), id_test, id_event)
    pcap = generate_pcap(cfg, count)
    check_capture(cfg, count, pcap)
    Path(pcap).unlink()


def test_negative_count():
    Cleanup.cleanup()

    net = Mininet(topo=MinimalTopo(), host=TrafgenHost, waitConnected=True)
    net.start()

    host = net.hosts[0]
    cfg = UDPTrafgenCfg()

    with pytest.raises(ValueError):
        host.send(cfg.generate(), -1)


# TODO: Test things like broadcast ip address, illegal values in trafgen_cfg
