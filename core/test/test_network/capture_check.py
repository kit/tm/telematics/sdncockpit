from ipaddress import IPv4Address

from scapy.layers.inet import IP, UDP
from scapy.layers.l2 import Ether
from scapy.utils import PcapReader

from sdnc.emulator.macaddr import MACaddr
from sdnc.emulator.trace import IdentifiedPacket


def check_capture(cfg, count, path):
    """
    Checks if a packet capture is consistent with the given cfg and
    count.
    Works only for IDUDPTrafgenConfig.
    """

    for pkt in PcapReader(str(path)):
        try:
            id_pkt = IdentifiedPacket(pkt)
            count -= 1
            assert id_pkt.get_test_id() == cfg.test_id
            assert id_pkt.get_event_id() == cfg.event_id

        except ValueError:
            continue  # Not an identified packet

        udp_cfg = cfg.udp_cfg
        assert MACaddr(pkt[Ether].dst) == udp_cfg.ethernet_dst
        assert MACaddr(pkt[Ether].src) == udp_cfg.ethernet_src
        assert pkt[IP].flags == udp_cfg.ipv4_flags
        assert pkt[IP].ttl == udp_cfg.ipv4_ttl
        assert IPv4Address(pkt[IP].src) == udp_cfg.ipv4_src
        assert IPv4Address(pkt[IP].dst) == udp_cfg.ipv4_dst
        assert pkt[UDP].dport == udp_cfg.udp_dst_port
        assert pkt[UDP].sport == udp_cfg.udp_src_port

    assert count == 0
