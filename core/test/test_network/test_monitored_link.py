from time import sleep

from mininet.clean import Cleanup
from mininet.net import Mininet
from mininet.topo import LinearTopo

from sdnc.emulator.network import MonitoredLink, TrafgenHost
from sdnc.emulator.trace import FilteredPcap
from sdnc.emulator.trafgen import IDUDPTrafgenCfg, UDPTrafgenCfg


def test_either_undirected():
    Cleanup.cleanup()

    net = Mininet(
        topo=LinearTopo(k=2, n=1),
        host=TrafgenHost,
        link=MonitoredLink,
        waitConnected=True,
    )
    net.start()

    dst = net.hosts[-1].intfList()[0]
    udp_cfg = UDPTrafgenCfg(ethernet_dst=dst.MAC(), ipv4_dst=dst.IP())
    cfg = IDUDPTrafgenCfg(udp_cfg, 11, 22)

    host = net.hosts[0]
    link = net.linksBetween(net.switches[0], net.switches[1])[0]

    descriptor = link.start_monitor()

    host.send(cfg.generate(), 100)

    sleep(3)

    pcap = link.stop_monitor(descriptor)

    packets = FilteredPcap(pcap).get()

    assert sum(1 for _ in packets) == 100


def test_directional():
    Cleanup.cleanup()

    net = Mininet(
        topo=LinearTopo(k=2, n=1),
        host=TrafgenHost,
        link=MonitoredLink,
        waitConnected=True,
    )
    net.start()

    dst = net.hosts[-1].intfList()[0]
    udp_cfg = UDPTrafgenCfg(ethernet_dst=dst.MAC(), ipv4_dst=dst.IP())
    cfg = IDUDPTrafgenCfg(udp_cfg, 11, 22)

    host = net.hosts[0]
    link = net.linksBetween(net.switches[0], net.switches[1])[0]

    descriptors = [
        link.start_monitor(intf=link.intf1, direction=net.switches[0]),
        link.start_monitor(intf=link.intf1, direction=net.switches[1]),
    ]

    host.send(cfg.generate(), 100)

    sleep(3)

    pcaps = [link.stop_monitor(descriptor) for descriptor in descriptors]

    packets_list = [FilteredPcap(pcap).get() for pcap in pcaps]

    sizes = [sum(1 for _ in packets) for packets in packets_list]

    assert sizes[0] == 0
    assert sizes[1] == 100
