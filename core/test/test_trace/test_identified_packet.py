from test import test_factory

import pytest

from sdnc.emulator.trace import IdentifiedPacket


def test_id_extraction():
    test_id = 0x1111
    event_id = 0x2222
    packet_id = 0x33333333
    combined_id = 0x1111222233333333
    payload = b"\x11\x11\x22\x22\x33\x33\x33\x33"

    pkt = test_factory.udp_packet(data=payload)

    id_pkt = IdentifiedPacket(pkt)

    assert id_pkt.get_combined_id() == combined_id
    assert id_pkt.get_test_id() == test_id
    assert id_pkt.get_event_id() == event_id
    assert id_pkt.get_packet_id() == packet_id


def test_missing_id_field():
    pkt = test_factory.udp_packet(data=b"")
    with pytest.raises(ValueError):
        IdentifiedPacket(pkt)


def test_malformed_id_field():
    pkt = test_factory.udp_packet(data=b"\x00\x00")
    with pytest.raises(ValueError):
        IdentifiedPacket(pkt)
