from test.pcap_gen import generate_pcap

from sdnc.emulator.trace import FilteredPcap
from sdnc.emulator.trafgen import IDUDPTrafgenCfg, UDPTrafgenCfg


def test_good():
    count = 100
    test_id = 10
    event_id = 100
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), test_id=test_id, event_id=event_id)

    pcap = generate_pcap(cfg, count)

    assert sum(1 for _ in FilteredPcap(pcap).get(test_id, event_id)) == count


def test_filter_tid():
    count = 100
    test_id = 10
    event_id = 100
    cfg = IDUDPTrafgenCfg(UDPTrafgenCfg(), test_id=test_id, event_id=event_id)

    pcap = generate_pcap(cfg, count)

    assert sum(1 for _ in FilteredPcap(pcap).get(0, event_id)) == 0


def test_filter_missing_id():
    count = 100

    cfg = UDPTrafgenCfg()

    pcap = generate_pcap(cfg, count)

    assert sum(1 for _ in FilteredPcap(pcap).get()) == 0
