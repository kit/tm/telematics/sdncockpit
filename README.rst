SDN-Cockpit 2
=============
SDN-Cockpit is an open-source teaching framework for Software-defined Networking
(SDN) developed by the Institute of Telematics, Karlsruhe Insitute of
Technology.

The tool provides an easy-to-use environment for fast pace SDN application
development with special support for executing automated evaluation scenarios.
It builds on the mininet_ emulator and the Ryu_ controller software.

Installation
============
SDN-Cockpit supports Linux, macOS (notes below) and Windows. It depdends on
Vagrant_ and VirtualBox_ , which need to be installed beforehand. The software
is tested with Vagrant 2.3.7 and VirtualBox 7.0.12.
Install SDN-Cockpit with::

    git clone https://gitlab.kit.edu/kit/tm/telematics/sdncockpit
    cd SDNcockpit
    vagrant up

macOS & ARM
-----------
SDN-Cockpit depdends on VirtualBox, which currently does not support machines
with ARM chips, e.g. those with Apple silicon (M1 & M2). Therefore, SDN-Cockpit
is also not supported on such devices.

Quickstart
==========
Once your are done with the installation, open your browser and connect to
localhost:8080, where you will be greeted with the SDN-Cockpit user interface.

The user interface will, by default, open a text file explaining the basics of
how to use the tool. Additional information on SDN-Cockpit and on how to write
applications with it can be found in this repository under doc/user_manual/.

If you ever need to restart SDN-Cockpit, you can just restart the VM by opening
a shell in SDN-Cockpit's directory and running ``vagrant reload``.

Common Issues
=============
One source of problems are Ad-Blockers and similar browser extensions.
Please disable them for localhost while using the tool.

Development
===========
SDN-Cockpit 2 is still in active development and new features are being added
over time.

To develop SDN-Cockpit, it is advised to spin up a vm using::

    VAGRANT_VAGRANTFILE=Vagrantfile.dev vagrant up

If you find any bugs, or would like to request a feature, please open an issue
or contact `Jakob Gretenkort`_.

.. _mininet: https://github.com/mininet/mininet
.. _Ryu: https://github.com/faucetsdn/ryu
.. _Vagrant: https://www.vagrantup.com/
.. _VirtualBox: https://www.virtualbox.org/
.. _Jakob Gretenkort: jakob.gretenkort@student.kit.edu
